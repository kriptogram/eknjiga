var boxId = 3;
var alt = 0;
var roundCounter = 0;
var posTracker = 0;
var selectedCipher;
var stop = false;
var encrypting = true;
var blockIndex = 0;

function xorChars(c1, c2) {
    bin1 = parseInt(c1, 16);
    bin2 = parseInt(c2, 16);
    xored = (bin1 ^ bin2).toString(16);
    return xored;
}

String.prototype.xor = function(val) {
    // Convert the hex to binary
    arr = val.split("");
    return this.split("").map((el, idx) => {
        return xorChars(el, arr[idx]);
    }).join("");
}

function setEncryption() {
    if(roundCounter != 0 || alt != 0) {
        if(!window.confirm("Ali ste prepričani?")) {
            return;
        }
    }
    encrypting = true;
    document.getElementById("en-diagram").style.display = "block";
    document.getElementById("de-diagram").style.display = "none";
    resetSlides();
}
function setDecryption() {
    if(roundCounter != 0 || alt != 0) {
        if(!window.confirm("Ali ste prepričani?")) {
            return;
        }
    }
    encrypting = false;
    document.getElementById("en-diagram").style.display = "none";
    document.getElementById("de-diagram").style.display = "block";
    resetSlides();
}

window.onload = function() {
    document.getElementById("output-data").value = "";

    //document.getElementById("slide1").style.left = "0%";
    document.getElementById("slide2").style.left = "33%";
    document.getElementById("slide3").style.left = "66%";

    if(document.getElementById("cipher-radio").checked) {
        setEncryption();
    } else {
        setDecryption();
    }

    document.getElementById("next-btn").onclick = function() {
        if(encrypting) {
            if(checkInputs() && !stop) {
                if(alt == 0 && posTracker == 0) {
                    topBlock = document.getElementById("top-en" + (roundCounter + 1)).value;
                    bottomBlock = document.getElementById("bottom-en" + (roundCounter + 1));
                    prev = "";
                    if(roundCounter == 0) {
                        // Use initialization vector
                        prev = document.getElementById("init-vect-en").value;
                        // Get the first block from the input field
                        topBlock = getBlock();
                        document.getElementById("top-en" + (roundCounter + 1)).value = topBlock;
                    } else {
                        // Use the previous result
                        prev = document.getElementById("bottom-en" + (roundCounter)).value;
                    }

                    // Xor the values
                    xored = topBlock.xor(prev);
    
                    // Perform the calculation and write to the output
                    key = document.getElementById("input-key").value;
                    res = functions[selectedCipher](key, xored, true);
    
                    bottomBlock.value = res;
                    alt = 1;
    
                    outputElement = document.getElementById("output-data");
                    outputElement.value = outputElement.value + res;
                } else {
                    // Slide right
                    container = document.getElementById("slide-container-en");
    
                    slides = document.getElementsByClassName("slide-en");
    
                    // Get the position of the last slide
                    var lastPos = Number(slides[slides.length-1].style.left.slice(0, -1));
    
                    // Add a new slide
                    var newSlide = document.createElement("DIV");
                    newSlide.classList.add("slide-en");
                    newSlide.style.left = (lastPos+33) + "%";
                    newSlide.innerHTML =
                            '<div class="slide-row">\
                                <div style="width: 90%; display: flex; flex-direction: column;">\
                                    <textarea style="margin-left: 10%;" class="hex-box" id="top-en' + boxId + '" readonly></textarea>\
                                    <svg style="width: 90%; height: 2em; margin-left: 10%;">\
                                        <line x1="50%" y1="0%" x2="50%" y2="100%" stroke="#000" stroke-width="3" />\
                                    </svg>\
                                    <div style="display: flex; flex-direction: row; padding-left: 0px;">\
                                        <svg style="width: 100%; height: 2em;">\
                                            <line x1="0%" y1="50%" x2="90%" y2="50%" stroke="#000" stroke-width="3" marker-end="url(#arrow)" />\
                                        </svg>\
                                        <svg style="height: 2em; width: 4.2em;">\
                                            <circle cx="50%" cy="50%" r="0.8em" stroke="black" fill="white" stroke-width="3" />\
                                            <line x1="6%" y1="50%" x2="94%" y2="50%" stroke="#000" stroke-width="3" />\
                                            <line x1="50%" y1="6%" x2="50%" y2="94%" stroke="#000" stroke-width="3" />\
                                        </svg>\
                                        <svg style="width: 80%; height: 2em;"></svg>\
                                    </div>\
                                    <svg style="width: 90%; height: 2em; margin-left: 10%;">\
                                        <line x1="50%" y1="0%" x2="50%" y2="70%" stroke="#000" stroke-width="3" marker-end="url(#arrow)" />\
                                    </svg>\
                                    <div style="margin-left: 10%; height: 41.5px; overflow-x: auto;" class="card card-body p-2 text-center fun-name">' + funNames[selectedCipher] + '</div>\
                                    <svg style="width: 90%; height: 2em; margin-left: 10%;">\
                                        <line x1="50%" y1="0%" x2="50%" y2="70%" stroke="#000" stroke-width="3" marker-end="url(#arrow)" />\
                                    </svg>\
                                    <textarea style="margin-left: 10%;" class="hex-box" id="bottom-en' + boxId + '" readonly></textarea>\
                                </div>\
                                <div style="width: 10%">\
                                    <svg style="width: 100%; height: 100%;">\
                                        <line x1="0%" y1="87%" x2="90%" y2="87%" stroke="#000" stroke-width="3" />\
                                        <line x1="93%" y1="87%" x2="93%" y2="143px" stroke="#000" stroke-width="3" />\
                                    </svg>\
                                </div>\
                            </div>';

                    next = getBlock();

                    if(posTracker > 0) {
                        posTracker--;
                        if(posTracker == 0 && next == "") {
                            stop = true;
                        }
                    } else {
                        if(next != "") {
                            container.appendChild(newSlide);
                            topBlock = document.getElementById("top-en" + (roundCounter + 2));
                            topBlock.value = next;
                            boxId++;
                            roundCounter++;
                        } else {
                            stop = true;
                        }
                        
                        alt = 0;
                        
                    }

                    if(!stop) {
                        slides = document.getElementsByClassName("slide-en");
                        slideLeft(slides);
                    }
                }
            }
        } else {
            if(checkInputs() && !stop) {
                if(alt == 0 && posTracker == 0) {
                    topBlock = document.getElementById("top-de" + (roundCounter + 1)).value;
                    bottomBlock = document.getElementById("bottom-de" + (roundCounter + 1));
                    prev = "";
                    if(roundCounter == 0) {
                        // Use initialization vector
                        prev = document.getElementById("init-vect-de").value;
                        // Get the first block from the input field
                        topBlock = getBlock();
                        document.getElementById("top-de" + (roundCounter + 1)).value = topBlock;
                    } else {
                        // Use the previous result
                        prev = document.getElementById("top-de" + (roundCounter)).value;
                    }
    
                    // Perform the calculation
                    key = document.getElementById("input-key").value;
                    res = functions[selectedCipher](key, topBlock, false);

                    // Xor the values
                    xored = res.xor(prev);
    
                    bottomBlock.value = xored;
                    alt = 1;
    
                    outputElement = document.getElementById("output-data");
                    outputElement.value = outputElement.value + xored;
                } else {
                    // Slide left
                    container = document.getElementById("slide-container-de");
    
                    slides = document.getElementsByClassName("slide-de");
    
                    // Get the position of the last slide
                    var lastPos = Number(slides[slides.length-1].style.left.slice(0, -1));

                    // Add a new slide
                    var newSlide = document.createElement("DIV");
                    newSlide.classList.add("slide-de");
                    newSlide.style.left = (lastPos+33) + "%";
                    newSlide.innerHTML =
                            '<div class="slide-row">\
                                <div style="width: 90%; display: flex; flex-direction: column;">\
                                    <textarea style="margin-left: 10%;" class="hex-box" id="top-de' + boxId + '" readonly></textarea>\
                                    <svg style="width: 90%; height: 3em; margin-left: 10%;">\
                                        <line x1="50%" y1="0%" x2="50%" y2="100%" stroke="#000" stroke-width="3" />\
                                        <line x1="50%" y1="50%" x2="100%" y2="50%" stroke="#000" stroke-width="3" />\
                                    </svg>\
                                    <div style="margin-left: 10%; height: 41.5px; overflow-x: auto;" class="card card-body p-2 text-center fun-name">' + funNames[selectedCipher] + '</div>\
                                    <svg style="width: 90%; height: 1.5em; margin-left: 10%;">\
                                        <line x1="50%" y1="0%" x2="50%" y2="100%" stroke="#000" stroke-width="3" />\
                                    </svg>\
                                    <div style="display: flex; flex-direction: row; padding-left: 0px;">\
                                        <svg style="width: 100%; height: 2em;">\
                                            <line x1="0%" y1="50%" x2="90%" y2="50%" stroke="#000" stroke-width="3" marker-end="url(#arrow)" />\
                                        </svg>\
                                        <svg style="height: 2em; width: 4.2em;">\
                                            <circle cx="50%" cy="50%" r="0.8em" stroke="black" fill="white" stroke-width="3" />\
                                            <line x1="6%" y1="50%" x2="94%" y2="50%" stroke="#000" stroke-width="3" />\
                                            <line x1="50%" y1="6%" x2="50%" y2="94%" stroke="#000" stroke-width="3" />\
                                        </svg>\
                                        <svg style="width: 80%; height: 2em;"></svg>\
                                    </div>\
                                    <svg style="width: 90%; height: 1.5em; margin-left: 10%;">\
                                        <line x1="50%" y1="0%" x2="50%" y2="70%" stroke="#000" stroke-width="3" marker-end="url(#arrow)" />\
                                    </svg>\
                                    <textarea style="margin-left: 10%;" class="hex-box" id="bottom-de' + boxId + '" readonly></textarea>\
                                </div>\
                                <div style="width: 10%">\
                                    <svg style="width: 100%; height: 100%;">\
                                        <line x1="0%" y1="120px" x2="90%" y2="120px" stroke="#000" stroke-width="3" />\
                                        <line x1="93%" y1="120px" x2="93%" y2="227px" stroke="#000" stroke-width="3" />\
                                    </svg>\
                                </div>\
                            </div>';
    
                    next = getBlock();

                    if(posTracker > 0) {
                        posTracker--;
                        if(posTracker == 0 && next == "") {
                            slides = document.getElementsByClassName("slide-de");
                            slideLeft(slides);
                            stop = true;
                        }
                    } else {
                        if(next != "") {
                            container.appendChild(newSlide);
                            topBlock = document.getElementById("top-de" + (roundCounter + 2));
                            topBlock.value = next;
                            boxId++;
                            roundCounter++;
                        } else {
                            stop = true;
                        }
                        
                        alt = 0;
                        
                    }

                    if(!stop) {
                        slides = document.getElementsByClassName("slide-de");
                        slideLeft(slides);
                    }
                }
            }
        }
        highlightInput(blockIndex);
        highlightOutput(blockIndex);
    }

    document.getElementById("prev-btn").onclick = function() {
        if(posTracker < roundCounter) {
            if(encrypting) {
                slides = document.getElementsByClassName("slide-en");
                slideRight(slides);
            } else {
                slides = document.getElementsByClassName("slide-de");
                slideRight(slides);
            }
            posTracker++;
            stop = false;
        }
        highlightInput(blockIndex);
        highlightOutput(blockIndex);
    }
    cipherUpdate();
    input_area = document.getElementById("input-data");
    input_area.onscroll = handleScroll;
    output_area = document.getElementById("output-data");
    output_area.onscroll = handleScrollRes;
}

function slideLeft(slides) {
    blockIndex++;
    var pos = slides[0].style.left;
    var num = Number(pos.slice(0, -1));
    var target = num - 33;
    var id = setInterval(frame, 10);
    function frame() {
        if (num === target) {
            clearInterval(id);
            document.getElementById("next-btn").disabled = false;
        } else {
            document.getElementById("next-btn").disabled = true;
            num--;
            var temp = num;
            for(var i = 0; i < slides.length; i++) {
                slides[i].style.left = temp + "%";
                temp += 33;
            }
        }
    }
}

function slideRight(slides) {
    blockIndex--;
    var pos = slides[0].style.left;
    var num = Number(pos.slice(0, -1));
    var target = num + 33;
    var id = setInterval(frame, 10);
    function frame() {
        if (num === target) {
            clearInterval(id);
            document.getElementById("prev-btn").disabled = false;
        } else {
            document.getElementById("prev-btn").disabled = true;
            num++;
            var temp = num;
            for(var i = 0; i < slides.length; i++) {
                slides[i].style.left = temp + "%";
                temp += 33;
            }
        }
    }
}

function checkInputs() {
    // Check if it is the first round
    if(roundCounter == 0) {
        // Check the validity of inputs
        one = checkKey();
        two = checkInput();
        three = true;
        if(encrypting) {
            three = checkInitVectorEn();
        } else {
            three = checkInitVectorDe();
        }
        if(one && two && three) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

function padString(input) {
    remainder = input.length % selectedLength;
    needed = 0;
    if(remainder > 0) {
        needed = selectedLength - remainder;
    }
    // Pad the data if necessary
    pad = "";
    for(var i = 0; i < needed; i++) {
        pad += "0";
    }
    return input + pad;
}

function checkKey() {
    if(selectedCipher == "ID") {
        return true;
    }
    // Check that the key is the right length for the chosen cipher
    select = document.getElementById("key-len");
    selectedLength = select.options[select.selectedIndex].value;
    // Get the input key
    key = document.getElementById("input-key").value;
    re = new RegExp("^[0-9a-fA-F]{" + selectedLength + "}$","g");
    label = document.getElementById("input-key-warn");
    if(key.match(re)) {
        // The key is valid
        label.innerHTML = "";
        return true;
    } else {
        // The key is invalid
        if(Number(selectedLength) != key.length) {
            // Display invalid length warning
            label.innerHTML = "Vnesli ste ključ dolžine " + key.length + ", izbrana dolžina je " + selectedLength;
        } else {
            // Display invalid character warning
            label.innerHTML = "V ključu je neveljaven znak, podprti so samo 0 do 9, a do f ali A do F";
        }
        return false;
    }
}

function checkInput() {
    // Check that the data is valid
    select = document.getElementById("blk-len");
    selectedLength = select.options[select.selectedIndex].value;
    // Get the input data
    data = document.getElementById("input-data").value;
    re = new RegExp("^[0-9a-fA-F]+$","g");
    label = document.getElementById("input-data-warn");
    if(data.match(re)) {
        // The block is valid
        label.innerHTML = "";
        return true;
    } else {
        // The block is invalid
        if(data.length == 0) {
            // Display invalid length warning
            label.innerHTML = "Niste vnesli nobenih podatkov. Prosim vnesite vsaj en znak.";
        } else {
            // Display invalid character warning
            label.innerHTML = "V podatkih je neveljaven znak, podprti so samo 0 do 9, a do f ali A do F";
        }
        return false;
    }
}

function checkInitVectorEn() {
    // Check that the init vector is the right length for the chosen cipher
    select = document.getElementById("blk-len");
    selectedLength = select.options[select.selectedIndex].value;
    // Get the initialization vector
    vect = document.getElementById("init-vect-en").value;
    re = new RegExp("^[0-9a-fA-F]{" + selectedLength + "}$","g");
    label = document.getElementById("input-vect-warn-en");
    if(vect.match(re)) {
        // The vector is valid
        label.innerHTML = "";
        return true;
    } else {
        // The vector is invalid
        if(Number(selectedLength) != vect.length) {
            // Display invalid length warning
            label.innerHTML = "Vnesli ste vektor dolžine " + vect.length + ", izbrana dolžina je " + selectedLength;
        } else {
            // Display invalid character warning
            label.innerHTML = "V vektorju je neveljaven znak, podprti so samo 0 do 9, a do f ali A do F";
        }
        return false;
    }
}

function checkInitVectorDe() {
    // Check that the init vector is the right length for the chosen cipher
    select = document.getElementById("blk-len");
    selectedLength = select.options[select.selectedIndex].value;
    // Get the initialization vector
    vect = document.getElementById("init-vect-de").value;
    re = new RegExp("^[0-9a-fA-F]{" + selectedLength + "}$","g");
    label = document.getElementById("input-vect-warn-de");
    if(vect.match(re)) {
        // The vector is valid
        label.innerHTML = "";
        return true;
    } else {
        // The vector is invalid
        if(Number(selectedLength) != vect.length) {
            // Display invalid length warning
            label.innerHTML = "Vnesli ste vektor dolžine " + vect.length + ", izbrana dolžina je " + selectedLength;
        } else {
            // Display invalid character warning
            label.innerHTML = "V vektorju je neveljaven znak, podprti so samo 0 do 9, a do f ali A do F";
        }
        return false;
    }
}

// Key lengths of different ciphers
var keyLengths = {
    "ID": [],
    "DES": [16],
    "AES": [32, 48, 64],
    "RC5": [32, 48, 64]
};

// Block lengths of different ciphers
var blockLengths = {
    "ID": [8, 16, 32, 64, 128, 256],
    "DES": [16],
    "AES": [32],
    "RC5": [8, 16]
};

// Function names
var funNames = {
    "ID": "Brez operacije",
    "DES": "DES",
    "AES": "AES",
    "RC5": "RC5"
};

function cipherUpdate() {
    if(roundCounter != 0 || alt != 0) {
        if(!window.confirm("Ali ste prepričani?")) {
            return;
        }
    }
    resetSlides();

    select = document.getElementById("select-cipher");
    val = select.options[select.selectedIndex].value;
    selectedCipher = val;
    names = document.getElementsByClassName("fun-name");
    for(var i = 0; i < names.length; i++) {
        names[i].innerHTML = funNames[selectedCipher];
    }
    // BLOCK LENGTHS /////////////////////////////////////
    if(blockLengths[val].length > 0) {
        lengthSelect = document.getElementById("blk-len");
        // Remove old options
        len = lengthSelect.options.length;
        for(var i = len-1; i >= 0; i--) {
            lengthSelect.remove(i);
        }
        // Add new options
        for(var i = 0; i < blockLengths[val].length; i++) {
            var option = document.createElement("option");
            option.text = blockLengths[val][i];
            option.value = blockLengths[val][i];
            lengthSelect.add(option);
        }
        // Enable selection
        lengthSelect.disabled = false;
    } else {
        lengthSelect = document.getElementById("blk-len");
        // Remove previous options
        len = lengthSelect.options.length;
        for(var i = len-1; i >= 0; i--) {
            lengthSelect.remove(i);
        }
        // Disable selection
        lengthSelect.disabled = true;
    }
    // KEY LENGTHS ///////////////////////////////////////
    if(keyLengths[val].length > 0) {
        lengthSelect = document.getElementById("key-len");
        keyInput = document.getElementById("input-key");
        // Remove old options
        len = lengthSelect.options.length;
        for(var i = len-1; i >= 0; i--) {
            lengthSelect.remove(i);
        }
        // Add new options
        for(var i = 0; i < keyLengths[val].length; i++) {
            var option = document.createElement("option");
            option.text = keyLengths[val][i];
            option.value = keyLengths[val][i];
            lengthSelect.add(option);
        }
        // Enable selection
        lengthSelect.disabled = false;
        keyInput.disabled = false;
    } else {
        lengthSelect = document.getElementById("key-len");
        keyInput = document.getElementById("input-key");
        // Remove previous options
        len = lengthSelect.options.length;
        for(var i = len-1; i >= 0; i--) {
            lengthSelect.remove(i);
        }
        // Disable selection
        lengthSelect.disabled = true;
        keyInput.disabled = true;
    }
    blkLenChange();
    keyLenChange();
}

// mode: true  -> encryption
//       false -> decryption
var functions = {
    "ID": function(key, block, mode) {return block;},
    "DES": function(key, block, mode) {
        block = hexToBin(block).split("").map(x => Number(x));
        key = hexToBin(key).split("").map(x => Number(x));
        return binToHex(des(key, block, mode).join(""));
    },
    "AES": function(key, block, mode) {
        if(mode) {
            return sifriraj(key, block);
        } else {
            return odsifriraj(key, block);
        }
    },
    "RC5": function(key, block, mode) {
        if(mode) {
            return rc5Encipher(block, key);
        } else {
            return rc5Decipher(block, key);
        }
    }
};

input = "";
blockLength = null;
inputCounter = 0;
function getBlock() {
    if(input == "") {
        input = document.getElementById("input-data").value;
    }
    if(blockLength == null) {
        select = document.getElementById("blk-len");
        blockLength = Number(select.options[select.selectedIndex].value);
    }
    
    val = input.substring(inputCounter * blockLength, (inputCounter + 1) * blockLength);
    inputCounter++;

    if(val == "") {return "";}

    needed = selectedLength - val.length;
    // Pad the data if necessary
    pad = "";
    for(var i = 0; i < needed; i++) {
        pad += "0";
    }
    val += pad;

    return val;
}

function resetSlides() {
    slidesEncrypt = document.getElementsByClassName("slide-en");
    num = slidesEncrypt.length;
    for(var i = num-1; i >= 3; i--) {
        slidesEncrypt[i].remove();
    }
    for(var i = 0; i < 3; i++) {
        slidesEncrypt[i].style.left = (i * 33) + "%";
    }
    for(var i = 1; i < 3; i++) {
        document.getElementById("top-en"+i).value = "";
        document.getElementById("bottom-en"+i).value = "";
    }

    slidesDecrypt = document.getElementsByClassName("slide-de");
    num = slidesDecrypt.length;
    for(var i = num-1; i >= 3; i--) {
        slidesDecrypt[i].remove();
    }
    for(var i = 0; i < 3; i++) {
        slidesDecrypt[i].style.left = (i * 33) + "%";
    }
    for(var i = 1; i < 3; i++) {
        document.getElementById("top-de"+i).value = "";
        document.getElementById("bottom-de"+i).value = "";
    }

    document.getElementById("output-data").value = "";

    boxId = 3;
    alt = 0;
    roundCounter = 0;
    outputPos = 0;
    input = "";
    blockLength = null;
    inputCounter = 0;
    stop = false;
    blockIndex = 0;

    highlights = document.getElementById("input-highlights");
    highlights.innerHTML = "";
    highlights = document.getElementById("output-highlights");
    highlights.innerHTML = "";
}

// Handle highlighting of the blocks in the input text area
function highlightInput(blockIndex) {
    textarea = document.getElementById("input-data");
    highlights = document.getElementById("input-highlights");
    var text = textarea.value;
    var highlightedText = applyHighlights(text, blockIndex);
    highlights.innerHTML = highlightedText;
}
function highlightOutput(blockIndex) {
    textarea = document.getElementById("output-data");
    highlights = document.getElementById("output-highlights");
    var text = textarea.value;
    var highlightedText = applyHighlights(text, blockIndex);
    highlights.innerHTML = highlightedText;
}
function applyHighlights(text, index) {
    // Split text into eight character chunks
    var regex = new RegExp(".{" + blockLength + "}","g");
    padded = padString(text);
    parts = padded.match(regex);
    if(parts[index]) {
        parts[index] = "<mark>" + parts[index] + "</mark>";
    } else {
        str = "";
        for(var i = 0; i < blockLength; i++) {
            str += " ";
        }
        parts[index] = "<mark>" + str + "</mark>";
    }
    return parts.join("");
}
function handleScroll() {
    textarea = document.getElementById("input-data");
    backdrop = document.getElementById("input-backdrop");
    var scrollTop = textarea.scrollTop;
    backdrop.scrollTop = scrollTop;
}
function handleScrollRes() {
    textarea = document.getElementById("output-data");
    backdrop = document.getElementById("output-backdrop");
    var scrollTop = textarea.scrollTop;
    backdrop.scrollTop = scrollTop;
}

// ======== ======== ======== ======== ======== ======== ======== ======== //
// ======== ======== ======== ======== ======== ======== ======== ======== //
// DES
var IP_perm = [
    58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4, 62, 54, 46, 38, 30, 22, 14, 6,
    64, 56, 48, 40, 32, 24, 16, 8, 57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3,
    61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7
];

var FP_perm = [
    40,8,48,16,56,24,64,32,
    39,7,47,15,55,23,63,31,
    38,6,46,14,54,22,62,30,
    37,5,45,13,53,21,61,29,
    36,4,44,12,52,20,60,28,
    35,3,43,11,51,19,59,27,
    34,2,42,10,50,18,58,26,
    33,1,41,9,49,17,57,25
];

var E_perm = [
    32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12, 13, 14, 15, 16, 17,
    16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1
];

var P_perm = [
    16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5, 18, 31, 10, 2, 8, 24, 14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25
];

var sBox1 = [
    14,0,4,15,13,7,1,4, 2,14,15,2,11,13,8,1, 3,10,10,6,6,12,12,11, 5,9,9,5,0,3,7,8,
    4,15,1,12,14,8,8,2, 13,4,6,9,2,1,11,7, 15,5,12,11,9,3,7,14, 3,10,10,0,5,6,0,13
];

var sBox2 = [
    15,3,1,13,8,4,14,7, 6,15,11,2,3,8,4,14, 9,12,7,0,2,1,13,10, 12,6,0,9,5,11,10,5,
    0,13,14,8,7,10,11,1, 10,3,4,15,13,4,1,2, 5,11,8,6,12,7,6,12, 9,0,3,5,2,14,15,9,
];

var sBox3 = [
    10,13,0,7,9,0,14,9, 6,3,3,4,15,6,5,10, 1,2,13,8,12,5,7,14, 11,12,4,11,2,15,8,1,
    13,1,6,10,4,13,9,0, 8,6,15,9,3,8,0,7, 11,4,1,15,2,14,12,3, 5,11,10,5,14,2,7,12
];

var sBox4 = [
    7,13,13,8,14,11,3,5, 0,6,6,15,9,0,10,3, 1,4,2,7,8,2,5,12, 11,1,12,10,4,14,15,9,
    10,3,6,15,9,0,0,6, 12,10,11,1,7,13,13,8, 15,9,1,14,3,5,14,11, 5,12,2,7,8,2,4,14
];

var sBox5 = [
    2,14,12,11,4,2,1,12, 7,4,10,7,11,13,6,1, 8,5,5,0,3,15,15,10, 13,3,0,9,14,8,9,6,
    4,11,2,8,1,12,11,7, 10,1,13,4,7,2,8,13, 15,6,9,15,12,0,5,9, 6,10,3,4,0,5,14,3
];

var sBox6 = [
    12,10,1,15,10,4,15,2, 9,7,2,12,6,9,8,5, 0,6,13,1,3,13,4,14, 14,0,7,11,5,3,11,8,
    9,4,14,3,15,2,5,12, 2,9,8,5,12,15,3,10, 7,11,0,14,4,1,10,7, 1,6,13,0,11,8,6,13
];

var sBox7 = [
    4,13,11,0,2,11,14,7, 15,4,0,9,8,1,13,10, 3,14,12,3,9,5,7,12, 5,2,10,15,6,8,1,6,
    1,6,4,11,11,13,13,8, 12,1,3,4,7,10,14,7, 10,9,5,5,6,0,8,15, 0,14,5,2,9,3,2,12
];

var sBox8 = [
    13,1,2,15,8,13,4,8, 6,10,15,3,11,7,1,4, 10,12,9,5,3,6,14,11, 5,0,0,14,12,9,7,2,
    7,2,11,1,4,14,1,7, 9,4,12,10,14,8,2,13, 0,15,6,12,10,9,13,0, 15,3,3,5,5,6,8,11
];

var pc1 = [
    57,49,41,33,25,17,9, 1,58,50,42,34,26,18, 10,2,59,51,43,35,27, 19,11,3,60,52,44,36,
    63,55,47,39,31,23,15, 7,62,54,46,38,30,22, 14,6,61,53,45,37,29, 21,13,5,28,20,12,4 
];

var pc2 = [
    14,17,11,24,1,5, 3,28,15,6,21,10, 23,19,12,4,26,8, 16,7,27,20,13,2, 41,52,31,37,47,55,
    30,40,51,45,33,48, 44,49,39,56,34,53, 46,42,50,36,29,32
];

var shifts = [
    1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1
];

function getSubkeys(key) {
    perm = pc1.map(x => key[x-1]);
    left = perm.slice(0,28);
    right = perm.slice(28,56);
    keys = [];
    shifts.forEach(function(s, i) {
        // Left shift
        left[left.length-1] = left.shift();
        right[right.length-1] = right.shift();
        if(s == 2) {
            left[left.length-1] = left.shift();
            right[right.length-1] = right.shift();
        }
        // join and PC2
        joined = left.concat(right);
        keys[i] = pc2.map(y => joined[y-1]);
    });
    return keys;
}

function feistel(subkey, right) {
    // Perform the feistel function on the input block and subkey
    // Expand the input block
    expanded = E_perm.map(x => right[x-1]);
    // XOR
    xored = expanded.xor(subkey);
    // Pass the xored bits to the sboxes
    // Create an array to hold the result
    arr = new Array();
    // Get the first 6 bits and convert them to an integer
    number = arrayToInt(xored.slice(0, 6));
    // Get the result from the first sBox and save it
    arr = arr.concat(intToArr(sBox1[number]));

    number = arrayToInt(xored.slice(6, 12));
    arr = arr.concat(intToArr(sBox2[number]));

    number = arrayToInt(xored.slice(12, 18));
    arr = arr.concat(intToArr(sBox3[number]));

    number = arrayToInt(xored.slice(18, 24));
    arr = arr.concat(intToArr(sBox4[number]));

    number = arrayToInt(xored.slice(24, 30));
    arr = arr.concat(intToArr(sBox5[number]));

    number = arrayToInt(xored.slice(30, 36));
    arr = arr.concat(intToArr(sBox6[number]));

    number = arrayToInt(xored.slice(36, 42));
    arr = arr.concat(intToArr(sBox7[number]));

    number = arrayToInt(xored.slice(42, 48));
    arr = arr.concat(intToArr(sBox8[number]));

    // Perform the final permutation and return
    perm = P_perm.map(x => arr[x-1]);

    return perm;
}

function des(key, block, encrypt) {
    subkeys = getSubkeys(key);
    perm = IP_perm.map(x => block[x-1]);
    left = perm.slice(0,32);
    right = perm.slice(32,64);

    if(encrypt) {
        for(var i = 0; i < 16; i++) {
            f = feistel(subkeys[i], right);
            xored = f.xor(left);
            left = right;
            right = xored;
        }
    } else {
        for(var i = 15; i >= 0; i--) {
            f = feistel(subkeys[i], right);
            xored = f.xor(left);
            left = right;
            right = xored;
        }
    }
    

    temp = right;
    right = left;
    left = temp;

    joined = left.concat(right);
    final = FP_perm.map(x => joined[x-1]);
    return final;
}

function arrayToInt(arr) {
    // arr is an array of 6 bits
    return arr[0]*32 + arr[1]*16 + arr[2]*8 + arr[3]*4 + arr[4]*2 + arr[5]*1;
}
function intToArr(num) {
    // Convert the integer to 4 bits
    switch(num) {
        case(0): return [0,0,0,0];
        case(1): return [0,0,0,1];
        case(2): return [0,0,1,0];
        case(3): return [0,0,1,1];
        case(4): return [0,1,0,0];
        case(5): return [0,1,0,1];
        case(6): return [0,1,1,0];
        case(7): return [0,1,1,1];
        case(8): return [1,0,0,0];
        case(9): return [1,0,0,1];
        case(10): return [1,0,1,0];
        case(11): return [1,0,1,1];
        case(12): return [1,1,0,0];
        case(13): return [1,1,0,1];
        case(14): return [1,1,1,0];
        case(15): return [1,1,1,1];
    }
    return [0,0,0,0];
}

function hexToBin(hex) {
    chars = hex.split("");
    bins = chars.map(el => {
        switch(el) {
            case "0": return "0000";
            case "1": return "0001";
            case "2": return "0010";
            case "3": return "0011";
            case "4": return "0100";
            case "5": return "0101";
            case "6": return "0110";
            case "7": return "0111";
            case "8": return "1000";
            case "9": return "1001";
            case "a": return "1010";
            case "A": return "1010";
            case "b": return "1011";
            case "B": return "1011";
            case "c": return "1100";
            case "C": return "1100";
            case "d": return "1101";
            case "D": return "1101";
            case "e": return "1110";
            case "E": return "1110";
            case "f": return "1111";
            case "F": return "1111";
        }
    });
    return bins.join("");
}

function binToHex(bin) {
    quartets = bin.match(/[\s\S]{1,4}/g)
    hex = quartets.map(el => {
        switch(el) {
            case "0000": return "0";
            case "0001": return "1";
            case "0010": return "2";
            case "0011": return "3";
            case "0100": return "4";
            case "0101": return "5";
            case "0110": return "6";
            case "0111": return "7";
            case "1000": return "8";
            case "1001": return "9";
            case "1010": return "a";
            case "1011": return "b";
            case "1100": return "c";
            case "1101": return "d";
            case "1110": return "e";
            case "1111": return "f";
        }
    });
    return hex.join("");
}

Array.prototype.xor = function(arr) {
    ret = new Array();
    for(var i = 0; i < this.length; i++) {
        ret = ret.concat((Number(this[i]) + Number(arr[i])) % 2);
    }
    return ret;
}

Array.prototype.lShift = function() {
    first = this[0];
    for(var i = 0; i < this.length-1; i++) {
        this[i] = this[i+1];
    }
    this[this.length-1] = first;
}

Array.prototype.llShift = function() {
    first = this[0];
    second = this[1];
    for(var i = 0; i < this.length-2; i++) {
        this[i] = this[i+2];
    }
    this[this.length-1] = second;
    this[this.length-2] = first;
}

// ======== ======== ======== ======== ======== ======== ======== ======== //
// ======== ======== ======== ======== ======== ======== ======== ======== //
// AES by @maticsuc
function is_hexadecimal(str){
    var regexp = /^[0-9a-fA-F]+$/;
    if (regexp.test(str))
        return true;
    else
        return false;
}

function preveriHex(id){
    if(!is_hexadecimal(document.getElementById(id).value[document.getElementById(id).value.length - 1])){
        document.getElementById(id).value = document.getElementById(id).value.substring(0, document.getElementById(id).value.length - 1)
    }
}

function preveriDolzino(id){
    var key = document.getElementById(id).value;
    if(id == 'kljuc' && key.length < document.getElementById('kljuc').maxLength){
        var dodaneNicle = "";
        for(i=key.length; i<document.getElementById('kljuc').maxLength; i++){
            dodaneNicle += "0";
        }
        document.getElementById(id).value = key + dodaneNicle;
    }
    else if(key.length < 32){
        var dodaneNicle = "";
        for(i=key.length; i<32; i++){
            dodaneNicle += "0";
        }
        document.getElementById(id).value = key + dodaneNicle;
    }
}

function spremeniVelikostKljuca(){
    if(vrstaAES.value == 'AES128'){
        document.getElementById('kljuc').maxLength = "32";
        document.getElementById('kljuc').style.width = "320px";
    }
    else if(vrstaAES.value == 'AES192'){
        document.getElementById('kljuc').maxLength = "48";
        document.getElementById('kljuc').style.width = "460px";
    }
    else if(vrstaAES.value == 'AES256'){
        document.getElementById('kljuc').maxLength = "64";
        document.getElementById('kljuc').style.width = "600px";
    }
}

function sifriraj(key, block){
    var keyBytes = aesjs.utils.hex.toBytes(key);
    var textBytes = aesjs.utils.hex.toBytes(block);

    var aesEcb = new aesjs.ModeOfOperation.ecb(keyBytes);
    var encryptedBytes = aesEcb.encrypt(textBytes);

    var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
    return encryptedHex;
}

function odsifriraj(key, block){
    var keyBytes = aesjs.utils.hex.toBytes(key);
    var encryptedBytes = aesjs.utils.hex.toBytes(block);

    var aesEcb = new aesjs.ModeOfOperation.ecb(keyBytes);
    var decryptedBytes = aesEcb.decrypt(encryptedBytes);

    var decryptedText = aesjs.utils.hex.fromBytes(decryptedBytes);
    return decryptedText;
}

// ======== ======== ======== ======== ======== ======== ======== ======== //
// ======== ======== ======== ======== ======== ======== ======== ======== //
// RC5
// http://tutorialspots.com/rc5-block-cipher-implement-in-php-and-javascript-3635.html

var RC5={
    S : [],
    r : 12,
    w : 32,
    cyclicShiftLeft: function (array, positions)
    { 
        var temp = array.slice(0, positions);
        temp = array.slice(positions).concat(temp);
        return temp;
    },     
    ROTL: function (v,n,m) { 
        y = (v>=0?v:-1-v).toString(2); 
        z = y.length;
        if(z < m) y = ("0".repeat(m-z))+y;
        r = y.split("");
        if(r.length>m) r = r.slice(r.length-m);
        if(v<0) r = r.map(function(h){return h=="1"?"0":"1"})
        r = this.cyclicShiftLeft(r,n);  
        return parseInt(r.join(""),2) ;         
    },     
    ROTR: function (v,n,m){
        return this.ROTL(v,m-n,m);
    } ,     

    hexStringToIntArray : function(hex) {
        for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
        return bytes;
    },

    /* Key scheduling */
    rc5_init: function (key, width)
    {
       K = this.hexStringToIntArray(key, width);
       this.w = width

       var b = K.length,
       u = this.w/8,
       c = Math.max(1, Math.ceil(b/u)),
       t = 2 * (this.r+1),
       L = [],

        P,Q,i,j,k;
        if(this.w == 16) {
            P = 0xb7e1;
            Q = 0x9e37;
        } else {
            P = 0xb7e15163;
            Q = 0x9e3779b9;
        }
         
       for(i = 0; i < b; i++)
          L[i] = 0;
         
       for(i = b-1, L[c-1] = 0; i != -1; i--)
          L[Math.floor(i/u)] = (L[Math.floor(i/u)] << 8) + K[i];
           
       for(this.S[0] = P, i = 1; i < t; i++)
          this.S[i] = this.S[i-1] + Q;
           
       var n = Math.max(t,c);
        
       for(A = B = i = j = k = 0; k < 3 * n; k++, i = (i+1) % t, j = (j+1) % c)
       {    
          A = this.S[i] = this.ROTL(this.S[i] + (A + B), 3, this.w);  
          B = L[j] = this.ROTL(L[j] + (A + B), (A + B)&(this.w-1), this.w); 
       }
    },
    rc5_encrypt: function (pt)
    {
         var A,B,i;
       A = parseInt(pt[0]) + this.S[0];
       B = parseInt(pt[1]) + this.S[1];
       for(i = 1; i <= this.r; i++)
       {
          A = this.ROTL(A ^ B, B&(this.w-1), this.w) + this.S[2*i];
          B = this.ROTL(B ^ A, A&(this.w-1), this.w) + this.S[2*i + 1];
       }
       return [A & (Math.pow(2,this.w)-1), B & (Math.pow(2,this.w)-1)];
    },
      
    rc5_decrypt: function (ct)
    {
       var A,B,i;
       B=parseInt(ct[1]); A=parseInt(ct[0]);
       for(i = this.r; i > 0; i--)
       {
          B = this.ROTR(B - this.S[2*i + 1], A&(this.w-1), this.w) ^ A;          
          A = this.ROTR(A - this.S[2*i], B&(this.w-1), this.w) ^ B; 
       }        
       return [(A - this.S[0]) & (Math.pow(2,this.w)-1), (B - this.S[1]) & (Math.pow(2,this.w)-1)];
    }
}

var oldKey = "";
var oldW = 0;
function rc5Encipher(block, key) {
    w = block.length * 2;
    if(oldW != w || oldKey != key) {
        RC5.rc5_init(key, w);
        oldKey = key;
        oldW = w;
    }
    one = hexToSignInt(block.substring(0, w/4));
    two = hexToSignInt(block.substring(w/4, w/2));
    res = RC5.rc5_encrypt([one, two]);
    return signIntToHex(res[0], w) + signIntToHex(res[1], w);
}
function rc5Decipher(block, key) {
    w = block.length * 2;
    if(oldW != w || oldKey != key) {
        RC5.rc5_init(key, w);
        oldKey = key;
        oldW = w;
    }
    one = hexToSignInt(block.substring(0, w/4));
    two = hexToSignInt(block.substring(w/4, w/2));
    res = RC5.rc5_decrypt([one, two]);
    return signIntToHex(res[0], w) + signIntToHex(res[1], w);
}

function hexToSignInt(hex) {
    first = parseInt(hex.charAt(0), 16)
    if(first >= 8) {
        len = hex.length * 4;
        val = parseInt(hex, 16);
        return BigInt(val) - (1n << BigInt(len));
    } else {
        return parseInt(hex, 16);
    }
}

function signIntToHex(n, w) {
    if(n >= 0) {
        str = n.toString(16);
        while(str.length < w/4) {
            str = "0" + str;
        }
        return str;
    } else {
        temp = (1n << BigInt(w)) + BigInt(n);
        return temp.toString(16);
    }
}

function blkLenChange() {
    vect = document.getElementById("init-vect-en");
    vect.setAttribute("maxlength", document.getElementById("blk-len").value);
}

function keyLenChange() {
    key = document.getElementById("input-key");
    key.setAttribute("maxlength", document.getElementById("key-len").value);
}