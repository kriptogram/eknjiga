var originalEmso = "";

function check(emso) {
        
    rez.innerHTML = "Postopek izračuna kontrolne števke:<div class='card border-secondary card-body rez-card-col'>Prvo števko se pomnoži s 7: " + emso.charAt(0) + " &times; 7 = " + Number(emso.charAt(0)) * 7 + "<br>"
        +"Drugo števko se pomnoži s 6: " + emso.charAt(1) + " &times; 6 = " + Number(emso.charAt(1)) * 6 + "<br>"
        +"Tretjo števko se pomnoži s 5: " + emso.charAt(2) + " &times; 5 = " + Number(emso.charAt(2)) * 5 + "<br>"
        +"Četrto števko se pomnoži s 4: " + emso.charAt(3) + " &times; 4 = " + Number(emso.charAt(3)) * 4 + "<br>"
        +"Peto števko se pomnoži s 3: " + emso.charAt(4) + " &times; 3 = " + Number(emso.charAt(4)) * 3 + "<br>"
        +"Šesto števko se pomnoži z 2: " + emso.charAt(5) + " &times; 2 = " + Number(emso.charAt(5)) * 2 + "<br>"
        +"Sedmo števko se pomnoži s 7: " + emso.charAt(6) + " &times; 7 = " + Number(emso.charAt(6)) * 7 + "<br>"
        +"Osmo števko se pomnoži s 6: " + emso.charAt(7) + " &times; 6 = " + Number(emso.charAt(7)) * 6 + "<br>"
        +"Deveto števko se pomnoži s 5: " + emso.charAt(8) + " &times; 5 = " + Number(emso.charAt(8)) * 5 + "<br>"
        +"Deseto števko se pomnoži s 4: " + emso.charAt(0) + " &times; 4 = " + Number(emso.charAt(0)) * 4 + "<br>"
        +"Enajsto števko se pomnoži s 3: " + emso.charAt(1) + " &times; 3 = " + Number(emso.charAt(1)) * 3 + "<br>"
        +"Dvanajsto števko se pomnoži z 2: " + emso.charAt(2) + " &times; 2 = " + Number(emso.charAt(2)) * 2 + "</div>";

    // Calculate the controll digit
    sum  = Number(emso[0])  * 7;
    sum += Number(emso[1])  * 6;
    sum += Number(emso[2])  * 5;
    sum += Number(emso[3])  * 4;
    sum += Number(emso[4])  * 3;
    sum += Number(emso[5])  * 2;
    sum += Number(emso[6])  * 7;
    sum += Number(emso[7])  * 6;
    sum += Number(emso[8])  * 5;
    sum += Number(emso[9])  * 4;
    sum += Number(emso[10]) * 3;
    sum += Number(emso[11]) * 2;

    rez.innerHTML += "Zmnožke seštejemo in delimo z 11, da dobimo ostanek:<div class='card border-secondary card-body rez-card-row'>" +
        sum + " / 11 = " + Math.floor(sum / 11) + " ostanek " + sum % 11 + "</div>";

    remainder = sum % 11;

    control = 11 - remainder;

    rez.innerHTML += "Ostanek odštejemo od 11, da dobimo kontrolno števko:<div class='card border-secondary card-body rez-card-row'>" +
        "11 - " + remainder + " = " + control + "</div>";

    rezDiv = document.getElementById("rez");
    if(Number(emso[12]) == control) {
        rezDiv.innerHTML += "EMŠO je veljavna";
    } else {
        rezDiv.innerHTML += "Kontrolna števka v EMŠO je " + emso[12] +
            " izračunana kontrolna števka je " + control +
            "<br> EMŠO ni veljavna";
    }
}

window.addEventListener('load', function() {

    document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("preveriBtn").click();
		  }
	});

    const form = document.querySelector('form');
    form.addEventListener('submit', event => {
        event.preventDefault();
        emso = document.getElementById("emso").value;
        if(originalEmso == "") {
            originalEmso = emso;
        }
        check(emso);
    });
    document.getElementById("err-btn").addEventListener("click", function() {
        emso = document.getElementById("emso").value;
        if(originalEmso == "") {
            originalEmso = emso;
        }
        chars = emso.split('');
        idx = Math.round(12 * Math.random());
        chars[idx] = Math.round(9 * Math.random());
        document.getElementById("emso").value = chars.join('');
        check(chars.join(''));
    });
    document.getElementById("reset-btn").addEventListener("click", function() {
        if(originalEmso != "") {
            document.getElementById("emso").value = originalEmso;
            emso = document.getElementById("emso").value;
            check(emso);
        }
    });
});