window.addEventListener('load', function(){

	document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("izracunajBtn").click();
		  }
	});

	document.getElementById("reset").addEventListener('submit', function(e){
		e.preventDefault();
		var v=document.getElementById("input").value;
		var izhod=document.getElementById("izhod");
		
		var isnum = /^\d+$/.test(v);
		if(v.length > 0 && isnum==false){
			izhod.innerHTML="Vhod vsebuje nedolovljene znake. Številka plačilne kartice</br>lahko vsebuje samo števila med 0 in 9.";
		}else if(v.length!=15){
			izhod.innerHTML="Nepravilna dolžina vhoda.";
		}else{
			var kont;
			var sum=0;
			for(var i=0; i<v.length; i++){
				var cifra=parseInt(v.charAt(i), 10);
				if(i%2==0){
					cifra=cifra*2;
					if(cifra>=10){
						cifra=1+cifra%10;
					}
				}
				sum+=cifra;
			}
			if(sum%10==0){
				kont="0";
			}else{
				kont=(10-(sum%10)).toString(10);
			}
			izhod.innerHTML="Vpisana vrednost je "+v+"</br>Kontrolna vsota je "+kont+"</br>Torej je celotna številka plačilne kartice:</br>"+v+kont;
		}
	});
});