window.addEventListener('load', function(){

	document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("izracunajBtn").click();
		  }
	});
	
	document.getElementById("reset").addEventListener('submit', function(e){
		e.preventDefault();
		var v=document.getElementById("input").value;
		v = v.replace(/-/gi, '');
		var izhod=document.getElementById("izhod");
		var kont;
		var isnum = /^\d+$/.test(v);

		if(v.length > 0 && isnum==false){
			izhod.innerHTML="Vhod vsebuje nedolovljene znake. ISBN koda lahko </br>vsebuje samo števila med 0 in 9.";
			return;
		} else if(v.length!=9 && v.length!=12){
				izhod.innerHTML="Nepravilna dolžina vhoda";
				return;
		}else if(v.length==9){
			var sum=0;
			var ind=0;
			while(ind<=8){
				sum+=(parseInt(v.charAt(ind), 10)*(10-ind));
				ind++;
			}
			sum=sum%11;
			
			if(sum==0)kont='0';
			else if(sum == 1)kont='X';
			else kont=(11-sum).toString(10);
		}else if(v.length==12){
			var sum=0;
			var ind=0;
			var mult=1;
			while(ind<=11){
				sum+=(parseInt(v.charAt(ind), 10)*mult);
				if(mult==1)mult=3;
				else mult=1;
				ind++;
			}
			sum=sum%10;
			if(sum==0)kont="0";
			else kont=(10-sum).toString(10);
		}else{
			izhod.innerHTML="Neznana napaka.";
			return;
		}
		izhod.innerHTML="Vpisana vrednost je "+document.getElementById("input").value+"</br>Kontrolna vsota je "+kont+".</br>Torej je celotna ISBN koda:</br>"+v+kont;
	});
});