let fileupload = null;
 
window.addEventListener("load", function(event) {
  //initialise the view.
  var funkcije = ["SHA-1","SHA-256","SHA-384","SHA-512"];
  var x = document.createElement('div'); // Create New Element div
  x.classList.add('main');
  
  var createform = document.createElement('div'); // Create New Element div
  createform.classList.add('content');

  //naslov
  var heading = document.createElement('h3');
  heading.innerHTML = "Preizkusi se v uporabi zgoščevalnih funkcij";
  createform.appendChild(heading);

  //3x div 
  var div1 = document.createElement('div');
  div1.setAttribute("id", "div1");

  var div1a = document.createElement('div');
  div1a.setAttribute("id", "div1a");
  var div1b = document.createElement('div');
  div1b.setAttribute("id", "div1b");

  var div2 = document.createElement('div');
  div2.setAttribute("id", "div2");

  var div3 = document.createElement('div');
  div3.setAttribute("id", "div3");

  var div3a = document.createElement('div');
  div3a.setAttribute("id", "div3a");
  var div3b = document.createElement('div');
  div3b.setAttribute("id", "div3b");


  createform.appendChild(div1);
  div1.appendChild(div1a);
  div1.appendChild(div1b);
  createform.appendChild(div2);
  createform.appendChild(div3);
  div3.appendChild(div3a);
  div3.appendChild(div3b);
 

  //polje za vpis 
  var texta = document.createElement('textarea');
  texta.setAttribute("rows", "8");
  texta.setAttribute("cols","30");
  texta.setAttribute("name","text");
  texta.setAttribute("id", "text");

  p = document.createElement('p');
  p.setAttribute('id','naslov1');
  p.appendChild(document.createTextNode('Besedilo (UTF-8 kodiranje znakov)'));
  div1a.appendChild(p);

  //polje za nalaganje datoteke
  if (window.File && window.FileReader && window.FileList && window.Blob) {
    fileupload = this.document.createElement('input');
    fileupload.setAttribute("type", "file");
    div1.appendChild(fileupload);
  }

  div1a.appendChild(texta);

  var texta2 = document.createElement('textarea');
  texta2.setAttribute("rows", "8");
  texta2.setAttribute("cols","30");
  texta2.setAttribute("name","textB");
  texta2.setAttribute("id", "textB");
  texta2.setAttribute("readonly","readonly");

  p = document.createElement('p');
  p.setAttribute('id','naslov2');
  p.appendChild(document.createTextNode('Binarno (šestnajstiški zapis)'));
  div1b.appendChild(p);

  div1b.appendChild(texta2);


  //polje za izpis
  var textb = document.createElement('textarea');
  textb.setAttribute("rows", "8");
  textb.setAttribute("cols","30");
  textb.setAttribute("name","textb");
  textb.setAttribute("id", "textb");
  textb.setAttribute("readonly","readonly");

  p = document.createElement('p');
  p.setAttribute('id','naslov3');
  p.appendChild(document.createTextNode('Binarno (šestnajstiški zapis)'));
  div3a.appendChild(p);

  div3a.appendChild(textb);

  var textb2 = document.createElement('textarea');
  textb2.setAttribute("rows", "8");
  textb2.setAttribute("cols","30");
  textb2.setAttribute("name","textbB");
  textb2.setAttribute("id", "textbB");
  textb2.setAttribute("readonly","readonly");

  p = document.createElement('p');
  p.setAttribute('id','naslov4');
  p.appendChild(document.createTextNode('Besedilo (UTF-8 kodiranje znakov)'));
  div3b.appendChild(p);

  div3b.appendChild(textb2);



  //select za izbiro hash funkcije
  var izbira = document.createElement('select');
  izbira.setAttribute("name","hashFunction");
  izbira.setAttribute("id", "hashFunction");
  izbira.setAttribute("multiple","multiple");
  izbira.setAttribute("size","4");

  //vse hash funkcije dam v select
  var i;
  for (i=0; i<funkcije.length;i++){
    var t = document.createElement('option');
    t.setAttribute("value", funkcije[i]);
    var u = document.createTextNode(funkcije[i]);
    t.appendChild(u);
    izbira.appendChild(t)

  }
  div2.appendChild(izbira);

  //button submit
  var submitelement = document.createElement('input');
  submitelement.setAttribute("type", "submit");
  submitelement.setAttribute("value", "Start");
  submitelement.setAttribute("dname", "Start");
  submitelement.id = "do";
  submitelement.addEventListener("click",function izpis(event){   //funkcija za izpis besedila in hash-a
    var m = document.getElementById("hashFunction");
    var besedilo = document.getElementById("text").value;
    var h_funkcija = m.options[m.selectedIndex].value;
  ////////////////////////////////////
    function hexString(buffer) {
      const byteArray = new Uint8Array(buffer);
    
      const hexCodes = [...byteArray].map(value => {
        const hexCode = value.toString(16);
        const paddedHexCode = hexCode.padStart(2, '0');
        return paddedHexCode;
      });
      return hexCodes.join('');
    }
 ////////////////////////////////////
    function ascii_to_hexa(str)
    {
      var arr1 = [];
      for (var n = 0, l = str.length; n < l; n ++) 
        {
        var hex = Number(str.charCodeAt(n)).toString(16);
        arr1.push(hex);
      }
      return arr1.join('');
    }
 ////////////////////////////////////
    function convertToHex(str) {
      var hex = '';
      for(var i=0;i<str.length;i++) {
          hex += ''+str.charCodeAt(i).toString(16);
      }
      return hex;
  }
 ////////////////////////////////////
    function toUTF8Array(str) {
      let utf8 = [];
      for (let i = 0; i < str.length; i++) {
          let charcode = str.charCodeAt(i);
          if (charcode < 0x80) utf8.push(charcode);
          else if (charcode < 0x800) {
              utf8.push(0xc0 | (charcode >> 6),
                        0x80 | (charcode & 0x3f));
          }
          else if (charcode < 0xd800 || charcode >= 0xe000) {
              utf8.push(0xe0 | (charcode >> 12),
                        0x80 | ((charcode>>6) & 0x3f),
                        0x80 | (charcode & 0x3f));
          }
          // surrogate pair
          else {
              i++;
              // UTF-16 encodes 0x10000-0x10FFFF by
              // subtracting 0x10000 and splitting the
              // 20 bits of 0x0-0xFFFFF into two halves
              charcode = 0x10000 + (((charcode & 0x3ff)<<10)
                        | (str.charCodeAt(i) & 0x3ff));
              utf8.push(0xf0 | (charcode >>18),
                        0x80 | ((charcode>>12) & 0x3f),
                        0x80 | ((charcode>>6) & 0x3f),
                        0x80 | (charcode & 0x3f));
          }
      }
      return utf8;
  }
  /////////////////
  function toHexString(byteArray) {
    return Array.from(byteArray, function (byte) {
        return ('0' + (byte & 0xFF).toString(16)).slice(-2);
    }).join('')
}
////////////////
  ////////////////
  function digest(data, h_funkcija) {
    window.crypto.subtle.digest(h_funkcija, data).then(function(digestValue) {
      textb.innerHTML = makeSpace(hexString(digestValue));
      textb2.innerHTML = hex2a(hexString(digestValue));
      texta2.innerHTML = makeSpace(toHexString(new Uint8Array(data)));
    });
  }
  
    if (fileupload != null && fileupload.files.length > 0) {
      var reader = new FileReader();
      reader.readAsArrayBuffer(fileupload.files[0]);

      reader.onload = function (evt) {
        digest(evt.target.result, h_funkcija);
      }
    } else if(besedilo!=""){
      const encoder = new TextEncoder();
      const data = encoder.encode(besedilo);
      digest(data, h_funkcija);
    }
    else{
      textb.innerHTML="";
      textb2.innerHTML="";
      texta2.innerHTML="";
    }
    

  });




  div2.appendChild(submitelement);
 

  
  
  x.appendChild(createform);
  document.getElementById('hash_funkcije').appendChild(x);
  
});

function hex2a(hexx) {
  var hex = hexx.toString();//force conversion
  var str = '';
  for (var i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
      str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
  return str;
}

function makeSpace(niz){
  newNiz = '';
  for(i=1;i<=niz.length;i++){
    newNiz+=niz.charAt(i-1);
    if(i%2==0){
      newNiz+=' ';
    }
  }
  return newNiz;
}
