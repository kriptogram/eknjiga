

var index = -1;
maxIndex = 4;

document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("naslednjiBtn").click();
      }
});


function img_create(src, alt, title) {
    var img = document.createElement('img');
    img.width = window.innerWidth/4;
    img.height = window.innerWidth/4;
    img.src = src;
    img.id = "demo_image"
    if ( alt != null ) img.alt = alt;
    if ( title != null ) img.title = title;
    return img;
}
function nextImage() {
    console.log("next")
    if (index < 3) {
        index++;    
    }
    displayImage(index);

}



function previosImage() {
    console.log("prev")
    if (index > 0) {
        index--;    
    }
    displayImage(index);

}

function displayImage(index) {
    console.log(index)

    let stepText = `Korak ${index+1}: `
    switch(index) {
        case 0:
            stepText += "Anita zaklene sporočilo v škatlo s svojo ključavnico ter vse skupaj pošlje Bojanu."
            
          break;
        case 1:
            stepText += "Bojan prejme pošiljko, prepozna Anitino ključavnico, doda še svojo ključavnico in vrne pošiljko Aniti."

          break;
        case 2:
            stepText += "Anita prejme pošiljko, prepozna svojo in Bojanovo ključavnico ter odklene in odstrani svojo ključavnico. Nato še drugič pošlje pošiljko Bojanu."

            break;
        case 3:
            stepText += "Bojan prejme pošiljko, odklene in odstrani svojo ključavnico ter pride do sporočila"

                break;
        default:
          // code block
      }
      document.getElementById(`step${index}`).innerText = stepText;

    if (document.getElementById("demo_image")) {
        document.getElementById("demo_image").remove()
    }
    
    let oImage = img_create(`images/omura${index}.png`, "Prikaz", "Prikaz")

    document.getElementById('image-space').appendChild(oImage);

}

$( document ).ready(function() {
    
    nextImage()
    console.log( "ready!" );
});