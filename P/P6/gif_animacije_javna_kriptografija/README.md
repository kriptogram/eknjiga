OS: Windows 10
Krita: 4.2.7.1

Renderirani animaciji v obliki .gif se nahajata pod P/static/gif.

Prvi projekt (Sipk Ani 1) je na podlagi scenarija za digitalni podpis:
1. Na pošti imajo steklene nabiralnike. Njihove reže so zaklenjene s ključi lastnikov (zasebni ključ).
2. Bojan odda v svoj poštni predal sporočilo za Anito.
3. Anita gre na pošto in poišče nabiralnik z Bojanovim imenom (javni ključ) in prebere sporočilo.
4. Anita ve, da je to sporočilo napisal Bojan, ker ima le on ključ (pristnost).
5. Prav tako ve, da sporočila ni nihče spreminjal, saj se poštnega predala ne da odpreti (celovitost).
6. Bojan ne more trditi, da ni napisal tega sporočila, saj je le on lahko oddal sporočilo v svoj poštni predal (nezatajljivost).

Drugi projekt (Sipk Ani 2) je na podlagi scenarija za šifriranje:
1. Če želi Anita poslati Bojanu sporočilo, mora poznati njegov naslov. Njegov naslov najde v imeniku. (Bojanov domači naslov predstavlja javni ključ.)
2. Anita želi Bojanu poslati sporočilo. Anita s pomočjo naslova poišče Bojanovo hišo in odda sporočilo v Bojanov poštni nabiralnik. 
3. Bojan s ključem odklene svoj poštni nabiralnik in prebere sporočilo, ki mu ga je poslala Anita. Ker ima le Bojan ključ svojega nabiralnika, lahko le on prebere sporočilo, ki mu ga je poslala Anita. (Ključ poštnega nabiralnika predstavlja zasebni ključ.)
4. Ljubosumna napadalka želi prebrati sporočilo, ki ga je Anita poslala Bojanu. Poišče Bojanov naslov v imenuku in s pomočjo naslova poišče hišo. Ker nima ključa Bojanovega nabiralnika ne more prebrati sporočila, ki ga je Anita poslala Bojanu. (Ker lahko le on bere sporočila, je zagotovljena tajnost njune komunikacije, svoj naslov lahko pove komurkoli in s tem ne bo nikomur omogočil branja svojih sporočil.)

SLOJI:
Spik Ani 1:
-Plast 9: Napisi za ključe.
-Vektorska plast 2: Napis Ana.
-Vektorska plast 1: Napis Bojan.
-Plast 2: ni v uporabi
-Plast 5: Figuri in pismo.
-Plast 8: odsev stekla.
-Plast 1: ni v uporabi
-Plast 3: Ozadje.

Sipk Ani 2:
-besedilo 9: Napis Ana na pismu (zadna scena).
-besedilo 8: Napis Bojan na ključu.
-besedilo 7: Napis Napadalec na ključu.
-besedilo 6: Povezave v sceni 2 - Google.
-besedilo 4: Napis na pismu v sceni 3.
-besedilo 3: Bojanov naslov v sceni 2 - Google.
-besedilo 2: Napis Bojanov naslov v sceni 1.
-rough 2: ni v uporabi
-rough 1: ni v uporabi
-moving background: Večina premikajoče se animacije, kot so osebe in ključi.
-besedilo 1: Barve pri hiši, mreža pri nabiralniku.
-besedilo 5: Napis Bojan na nabiralniku.
-fixed background: Nepremično ozadje.