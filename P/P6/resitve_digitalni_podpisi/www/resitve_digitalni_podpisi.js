// Configure mathjax to recognize $...$ as in-line math delimiter.
window.MathJax = {
  tex: {
    inlineMath: [['$', '$'], ['\\(', '\\)']]
  }
};

// Array of formulas for 5th task
var questions = [
    {
        correct: String.raw`$(\alpha^a)^{\gamma} (\alpha^k)^{\delta}$`,
        incorrect: [String.raw`$(\alpha^k)^{\gamma} (\alpha^a)^{\delta}$`,
                    String.raw`$(\alpha^x)^{\gamma} (\alpha^k)^{\delta}$`],
        hint: String.raw`Vstavimo formuli za $\beta$ in $\gamma$.`
    },
    {
        correct: String.raw`$\alpha^{a \gamma + k \delta}$`,
        incorrect: [String.raw`$\alpha^{a k \gamma \delta}$`,
                    String.raw`$\alpha^{a^{\gamma} + k^{\delta}}$`],
        hint: String.raw`Uporabimo pravila za računanje s potencami.`
    },
    {
        correct: String.raw`$\alpha^{a \gamma + k (x - a \gamma) k^{-1}}$`,
        incorrect: [String.raw`$\alpha^{a \gamma + k \alpha^k}$`,
                    String.raw`$\alpha^{a \gamma + k (x - a \gamma) k}$`],
        hint: String.raw`Vstavimo formulo za $\delta$.`
    },
    {
        correct: String.raw`$\alpha^{a \gamma + x - a \gamma}$`,
        incorrect: [String.raw`$\alpha^{a \gamma - a \gamma}$`,
                    String.raw`$\alpha^{a \gamma + k (x - a \gamma)}$`],
        hint: String.raw`Poenostavimo izraz. V eksponentu računamo po modulu $p-1$, ker za vsak $x \in \mathbb{Z}_p^{*}$ velja $x^{p-1} \equiv 1 \pmod{p}$.`
    },
    {
        correct: String.raw`$\alpha^x$`,
        incorrect: [String.raw`$\alpha^1$`,
                    String.raw`$\alpha^{2a \gamma + x}$`],
        hint: String.raw`Poenostavimo izraz.`
    }
];

// global variables needed for 5th task
var table, current_column = 0, correct_answer = -1;

function create_element(tag, parent, id=null, class_name=null, row_num=null, col_num=null) {
    var div = document.createElement(tag);
    if(id !== null) div.id = id;
    if(class_name !== null) div.className = class_name;
    if(row_num !== null) div.setAttribute("data-row-num", row_num);
    if(col_num !== null) div.setAttribute("data-col-num", col_num);
    parent.appendChild(div);
    return div;
}

function create_table_new(parent) {
    table = [];
    table_element = create_element("table", parent);
    for(var i=0; i<3; i++) {
        row = create_element("tr", table_element);
        table.push([]);
        for(var j=0; j<6; j++) {
            el = create_element("td", row, null, "rounded-lg", i, j);
            el.addEventListener('click', function(){
                table_click(this.getAttribute("data-row-num"), this.getAttribute("data-col-num"));
            });
            table[i].push(el);
        }
    }
    table[1][0].innerHTML = String.raw`$\beta^{\gamma} \gamma^{\delta} \equiv$`;
    MathJax.typeset();
    current_column = 0;
}

function next_question() {
    current_column++;
    if(current_column < 6) {
        var question_number = current_column - 1;
        correct_answer = Math.floor(Math.random() * 3);
        table[correct_answer][current_column].innerHTML = questions[question_number].correct;
        var next = Math.floor(Math.random() * 2);
        for(var i=0; i<3; i++) {
            if(i != correct_answer) {
                table[i][current_column].innerHTML = questions[question_number].incorrect[next];
                next = (next + 1) % 2;
            }
        }
    }
    MathJax.typeset();
}

function hide_wrong_answers() {
    table[0][current_column].innerHTML = "";
    if(current_column == 5) {
        table[1][current_column].innerHTML = questions[current_column-1].correct.slice(0, -1) + "\\pmod{p}$"
    }
    else {
        table[1][current_column].innerHTML = questions[current_column-1].correct.slice(0, -1) + "\\equiv$";
    }
    table[2][current_column].innerHTML = "";
}

function set_hint(hint) {
    document.getElementById("hint-text").innerHTML = hint;
    if(hint !== "") MathJax.typeset();
}

function table_click(row, column) {
    row = parseInt(row);
    column = parseInt(column);
    var clicked = table[row][column];

    if(column === current_column) {
        if(row === correct_answer) {
            clicked.classList.add("bg-light-green");
            setTimeout(function() {
                    set_hint("");
                    clicked.classList.remove("bg-light-green");
                    hide_wrong_answers();
                    next_question();
                }, 200);
        }
        else {
            clicked.classList.add("bg-light-red");
            setTimeout(function() {
                    set_hint(questions[current_column-1].hint);
                    clicked.classList.remove("bg-light-red");
                }, 200);
        }
    }
}

window.onload = function() {
    // generate table
    create_table_new(document.getElementById("table-horizontal-scroll"));

    next_question();

    document.getElementById("hint-button").onclick = function() {
        if(current_column < 6) {
            set_hint(questions[current_column-1].hint);
        }
    };
};

function check_task2(praslike, praslike2, trki) {
    correct = {"odpornost_praslik_div": true, "odpornost_2praslik_div": true, "odpornost_trki_div": false};
    answers = {"odpornost_praslik_div": praslike, "odpornost_2praslik_div": praslike2, "odpornost_trki_div": trki};
    for(var name of ["odpornost_praslik_div", "odpornost_2praslik_div", "odpornost_trki_div"]) {
        var div = document.getElementById(name);
        if(answers[name] === correct[name]) {
            div.classList.remove("bg-light-red");
            div.classList.add("bg-light-green");
        }
        else {
            div.classList.remove("bg-light-green");
            div.classList.add("bg-light-red");
        }
    }
    document.getElementById("odpornost_praslik_razlaga").hidden = false;
    document.getElementById("odpornost_2praslik_razlaga").hidden = false;
    document.getElementById("odpornost_trki_razlaga").hidden = false;
}