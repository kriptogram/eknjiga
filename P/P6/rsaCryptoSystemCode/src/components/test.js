let bigInt = require("big-integer");

/**
 * Kodiranje niza znakov v število
 * @param msg
 * @returns {bigint}
 */
function encodeString(msg) {
    let newString = "";
    for (let i = 0; i < msg.length; i++) {
        newString += msg.codePointAt(i);
    }
    return BigInt(newString);

}

// function decodeString(msg) {
//     let newString = "";
//     for (let i = 0; i < msg.length; i++) {
//         newString += String.fromCodePoint(msg[i]);
//     }
//     return newString;
// }



/**
 * TODO: popravi, random byte
 * @param k - number of decimal places
 * @returns {bigint}
 */
function generateBigPrimeCandidate(k) {

    return BigInt(bigInt.randBetween(2**(k-1)+1, 2**k-1).toString());
}

function getBits(number) {

    let i;
    for(i = 0; 2n**(BigInt(i.toString())) < number;i++) {

    }
    return i;


}


/**
 * Square and multiply algorithm for computing x^y mod p
 * @param x
 * @param y
 * @param p
 * @returns {bigint}
 */
function power(x, y, p) {
    let res = 1n;
    x = x % p;
    while (y > 0n) {

        if (y % 2n == 1n) {
            res = (res * x) % p;
        }

        y = y/2n;
        x = (x * x) % p;
    }
    return res;
}


/**
 * Probalistic test if given number is probable prime
 * @param d
 * @param n
 * @returns {boolean}
 */
function miillerTest(d, n) {

    console.assert(n > 4n);
    let a = 2n + BigInt(parseInt(Math.random()) );

    let x = power(a, d, n);

    if (x == 1n || x == n - 1n)
        return true;


    while (d != n - 1n) {
        x = (x * x) % n;
        d *= 2n;

        if (x == 1n) return false;
        if (x == n - 1n) return true;
    }

    return false;
}


/**
 * Determines if number n is prime by performing k trials
 * @param n
 * @param k
 * @returns {boolean}
 */
function isPrimeMiller(n, k) {

    if (n <= 1n || n == 4n) return false;
    if (n <= 3n) return true;

    let d = n - 1n;
    while (d % 2n == 0n)
        d /= 2n;

    for (let i = 0n; i < k; i++) {
        if (!miillerTest(d, n)) {
            return false;
        }
    }

    return true;
}

/**
 * Finds a large prime number that has n digits
 * @param n
 * @returns {bigint}
 */
function findLargePrime(n) {
    let k = 256;
    let startingPrime = generateBigPrimeCandidate(n);

    while(true) {

        if (isPrimeMiller(startingPrime, k)) break;
        startingPrime--;

    }
    return startingPrime;
}

/**
 * Computes general common denominator of two numbers
 * @param a
 * @param b
 * @returns {*}
 */
function gcd(a, b) {
    if (!b) {
        return a;
    }

    return gcd(b, a % b);
}

/**
 * Chooses encryption exponent
 * @param phiN
 * @param maxPrime
 * @returns {bigint}
 */
function findE(phiN, maxPrime) {

    while(true) {

        // A Method for Obtaining DigitalSignatures and Public-Key Cryptosystems by R.L. Rivest, A. Shamir, and L. Adleman
        // any prime number greater than max(p,q) will do.
        let startingE = generateBigPrimeCandidate(getBits(maxPrime));

        // TODO: maybe eea namesto gcd
        let divisor = gcd(startingE,phiN);


        if (divisor == 1n) {
            return startingE;
        }
        startingE++;

    }

}

/**
 * Computes an inverse of a mod n
 * @param a
 * @param n
 * @returns {bigint}
 */
function inverse(a,n) {
    let b = n;
    let x = 0n;
    let y = 1n;

    while(a != 0n) {
        let k = (b / a);

        let tmp = b;
        b = a;
        a = tmp - k*a;
        let tmpX = x;
        x = y;
        y = tmpX - k*y;

    }

    let number = b*x;
    if (number < 0) {
        number += n;
    }

    return (number) % n
}

/**
 * Prints the messages on the standard ouput or returns them
 * @param message
 * @returns {*[]}
 */
function print(...message) {
    let cons = true;
    if (cons) {
        console.log(message[0],message[1])
    } else {
        return message;
    }

}

/**
 * Performs RSA algoririthm.
 * Note: RSA without padding is not secure
 */
function rsa(characterMessage) {

    let messageInNumeric = encodeString(characterMessage);
    print("origin sporocilo je ",messageInNumeric);

    let p = findLargePrime(getBits(messageInNumeric));

    print("p je ",p);
    let q = findLargePrime(getBits(messageInNumeric));

    print("q je ",q);
    let n = p * q;
    print("n je ",p*q);

    // Message size must be smaller than modulus
    console.assert(messageInNumeric.toString().length < n);


    let phiN = (p-1n)*(q-1n);
    print("phi(n) je ",phiN);

    // Šifriranje
    // E more bit tuj  phiN
    let maxPrime = (p > q) ? p : q;
    let e = findE(phiN, maxPrime);

    print("e je ", e);
    let d = inverse(e,phiN);
    print("d je ", d);
    let c = power(messageInNumeric,e,n);
    print("c je ", c);

    // Decryption
    let extractedMessage = power(c,d,n);
    print("dešifr sporocilo je ",extractedMessage)


}

rsa("bl sdf sdf sd fsdf a dsfsd sd ffsdsfd sdf sdfsfds df");


/**
 * Naive brute-force algorithm for checking if given number is prime
 * @param n
 * @returns {boolean}
 */
function isPrime(n) {
    if (n <= 3) {

        return n > 1;
    } else if (n % 2 == 0 || n % 3 == 0) {
        return false;
    }
    let i = 5;
    while (i * i <= n) {
        if (n % i == 0 || n % (i + 2) == 0) {
            return false;
        }
        i = i + 6;
    }

    return true;
}



//
// // simulate();
//
// // Set the p and q as obtained in previos step
// let p = 5564863237 // TODO: vpiši neko naključno veliko praštevilo iz generatorja
// let q = 33113413 // TODO: vpiši neko naključno veliko praštevilo iz generatorja
//
// function phi(prime) {
//     return (prime - 1);
// }
//
// function simulate() {
//     let n = p * q;
//     let phiN = phi(n);
//     return "n = p*q: " + n + ", phi(n) = " + phiN
// }
//
// // simulate();
//
// // Encryption
// function encrypt(x, e, n) {
//     return Math.pow(x, e) % n;
// }
//
// function simulate() {
//     let x = 42;
//     let n = 184271614655297900 // TODO: from previous step
//     let e = 3;
//     return encrypt(x, e, n)
// }
//
// // simulate();
//
// // Decryption
// function decrypt(x, d, n) {
//     return Math.pow(x, d) % n;
// }
//
// function simulate() {
//     let x = 42;
//     let n = 184271614655297900 // TODO: from previous step
//     let d = 3;
//     return decrypt(x, d, n)
// }

// simulate();

// Extended euclidian algorithm
// Compute the inverse of a modulo n using the Extended Euclidean algorithm.


