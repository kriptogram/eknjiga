import Vue from 'vue'
import App from './App.vue'
import VueCodemirror from 'vue-codemirror'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// import base style
import 'codemirror/lib/codemirror.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)


// you can set default global options and events when Vue.use
Vue.use(VueCodemirror, /* {
  options: { theme: 'base16-dark', ... },
  events: ['scroll', ...]
} */)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
