imena_z = ["Marija ","Ana ","Maja ","Irena ","Mojca ","Mateja ","Nina ","Nataša ","Andreja ","Barbara ","Jožica ","Petra ","Eva ","Anja ","Katja ","Sara ","Sonja ","Tatjana ","Jožefa ","Katarina ","Tanja ","Tina ","Milena ","Alenka ","Vesna ","Nika ","Martina ","Majda ","Urška ","Ivana ","Špela ","Tjaša ","Frančiška ","Anica ","Helena ","Dragica ","Darja ","Nada ","Terezija ","Kristina ","Simona ","Lara ","Danica ","Marjeta ","Olga ","Suzana ","Zdenka ","Neža ","Lidija ","Ema ","Sabina ","Janja ","Marta ","Antonija ","Vida ","Angela ","Ivanka ","Maša ","Silva ","Zala ","Veronika ","Karmen ","Darinka ","Aleksandra ","Lana ","Anita ","Ljudmila ","Klara ","Kaja ","Brigita ","Alojzija ","Jana ","Lucija ","Metka ","Monika ","Lea ","Stanislava ","Natalija ","Cvetka ","Nevenka ","Jasmina ","Štefanija ","Elizabeta ","Renata ","Marjana ","Branka ","Tamara ","Julija ","Slavica ","Saša ","Hana ","Manca ","Klavdija ","Ajda ","Bojana ","Bernarda ","Erika ","Teja ","Vera ","Danijela "]
imena_m = ["Janez ","Marko ","Ivan ","Anton ","Andrej ","Jožef ","Jože ","Luka ","Peter ","Marjan ","Matej ","Tomaž ","Milan ","Aleš ","Branko ","Bojan ","Robert ","Rok ","Boštjan ","Matjaž ","Gregor ","Miha ","Stanislav ","Martin ","David ","Igor ","Jan ","Dejan ","Boris ","Dušan ","Nejc ","Žiga ","Jure ","Uroš ","Alojz ","Blaž ","Žan ","Mitja ","Simon ","Matic ","Klemen ","Darko ","Primož ","Jernej ","Anže ","Gašper ","Drago ","Aleksander ","Jaka ","Jakob ","Aljaž ","Miran ","Tadej ","Denis ","Roman ","Nik ","Štefan ","Vladimir ","Damjan ","Matija ","Borut ","Srečko ","Slavko ","Filip ","Janko ","Tilen ","Zoran ","Mirko ","Alen ","Miroslav ","Domen ","Vid ","Danijel ","Goran ","Mark ","Tim ","Stanko ","Mihael ","Leon ","Matevž ","Urban ","Sašo ","Jurij ","Andraž ","Iztok ","Marijan ","Vinko ","Dragan ","Alojzij ","Maks ","Viktor ","Benjamin ","Erik ","Lovro ","Zvonko","Samo ","Gal ","Zdravko ","Rudolf","Sandi "]
priimki = ["Novak ","Horvat ","Kovačič ","Krajnc ","Zupančič ","Potočnik ","Kovač ","Mlakar ","Vidmar ","Kos ","Golob ","Turk ","Kralj ","Božič ","Korošec ","Bizjak ","Zupan ","Hribar ","Kotnik ","Rozman ","Kavčič ","Kastelic ","Oblak ","Hočevar ","Petek ","Kolar ","Žagar ","Košir ","Koren ","Klemenčič ","Zajc ","Knez ","Medved ","Petrič ","Zupanc ","Pirc ","Hrovat ","Pavlič ","Kuhar ","Lah ","Zorko ","Tomažič ","Uršič ","Erjavec ","Babič ","Sever ","Jerman ","Jereb ","Kovačević ","Kranjc ","Majcen ","Rupnik ","Pušnik ","Breznik ","Lesjak ","Perko ","Dolenc ","Močnik ","Furlan ","Pečnik ","Pavlin ","Vidic ","Logar ","Jenko ","Petrović ","Ribič ","Žnidaršič ","Janežič ","Tomšič ","Marolt ","Jelen ","Pintar ","Blatnik ","Maček ","Dolinar ","Černe ","Gregorič ","Hren ","Mihelič ","Cerar ","Zadravec ","Fras ","Kokalj ","Lešnik ","Bezjak ","Hodžić ","Leban ","Čeh ","Rus ","Jug ","Vidovič ","Kocjančič ","Jovanović ","Kobal ","Bogataj ","Kolenc ","Primožič ","Marković ","Lavrič ","Kolarič "]

var keys_str = [
    "FAFEFB869C6ADAA5",
    "5266556A586E3272",
    "404D635166546A57",
    "4528482B4D625165",
    "2F413F4428472B4B",
    "D1AA5081DD97E502"
];


var pseudoRandom = Math.random;

var visaPrefixList = new Array("4539", "4556", "4916", "4532", "4929", "40240071", "4485", "4716", "4");

window.addEventListener("load", function() {    
    var c = document.getElementById("visa_canvas");
    var ctx = c.getContext("2d");

    var img = new Image();
    img.src = ("./visa_cleared_keys.svg");

    ctx.drawImage(img, 0, 0);
});

/**
 * Revert a String
 * @param  {String} str
 * @return {String}
 */
function strrev(str) {
   if (!str) return '';
   var revstr='';
   for (var i = str.length-1; i>=0; i--)
       revstr+=str.charAt(i)
   return revstr;
}

/**
 * Complete a prefixed number-string
 * @param  {String} prefix  is the start of the CC number as a string, any number of digits
 * @param  {Number} length  is the length of the CC number to generate. Typically 13 or 16
 * @return {String}
 */
function completed_number(prefix, length) {

    var ccnumber = prefix;

    // generate digits

    while ( ccnumber.length < (length - 1) ) {
        ccnumber += Math.floor(pseudoRandom()*10);
    }

    // reverse number and convert to int

    var reversedCCnumberString = strrev( ccnumber );

    var reversedCCnumber = new Array();
    for ( var i=0; i < reversedCCnumberString.length; i++ ) {
        reversedCCnumber[i] = parseInt( reversedCCnumberString.charAt(i) );
    }

    // calculate sum

    var sum = 0;
    var pos = 0;

    while ( pos < length - 1 ) {

        var odd = reversedCCnumber[ pos ] * 2;
        if ( odd > 9 ) {
            odd -= 9;
        }

        sum += odd;

        if ( pos != (length - 2) ) {

            sum += reversedCCnumber[ pos +1 ];
        }
        pos += 2;
    }

    // calculate check digit

    var checkdigit = (( Math.floor(sum/10) + 1) * 10 - sum) % 10;
    ccnumber += checkdigit;

    return ccnumber;

}

function credit_card_number(prefixList, length, howMany) {

    var result = new Array();
    for (var i = 0; i < howMany; i++) {

        var randomArrayIndex = Math.floor(pseudoRandom() * prefixList.length);
        var ccnumber = prefixList[ randomArrayIndex ];
        result.push( completed_number(ccnumber, length) );
    }

    return result;
}

/**
 * The entry-point function
 * @param {String} CardScheme  The Card Scheme
 * @param {Number} [howMany]   Defaults to 1
 * @param {Number} [randomGen] Pseudo Random Generator. Must generate a random number between 0 an 1
 * @return {String}
 */
function genCC (randomGen){
    pseudoRandom = randomGen || pseudoRandom;
    return credit_card_number(visaPrefixList, 16, 1);
}

function genPVV (des_out) {
    var count = 0;
    var pvv = "";
    var letters = ["a", "b", "c", "d", "e", "f"]

    for (var i = 0; i < des_out.length && count < 4; i++) {
        var lttr = des_out.charAt(i);
        if (!letters.includes(lttr)) {
            pvv += lttr;
            count++;
        }
    }
    for (var i = 0; i < des_out.length && count < 4; i++) {
        var lttr = des_out.charAt(i);
        if (letters.includes(lttr)) {
            switch (lttr) {
                case "a":
                    pvv += 0;
                    break;
                case "b":
                    pvv += 1;  
                    break;
                case "c":
                    pvv += 2;
                    break;
                case "d":
                    pvv += 3;
                    break;
                case "e":
                    pvv += 4;  
                    break;
                case "f":
                    pvv += 5; 
                    break;
            }
            count++;
        }
    }
    return pvv;
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function buildTSP(ccNo, pin, index) {
    var tsp = "";
    tsp += ccNo.substring(4, ccNo.length - 1);
    tsp += index;
    tsp += pin;
    return tsp;
}

function buildName() {
    var randomNumIme = Math.floor(Math.random()*100);
    var randomNumPriimek = Math.floor(Math.random()*100);
    var name = "";
    var sex = Math.floor(Math.random()*2);

    if (sex === 0) {
        name += imena_z[randomNumIme];
    }
    else {
        name += imena_m[randomNumIme];
    }
    
    name += priimki[randomNumPriimek];

    return name;
}

function buildImageScg(selec,cardNum,Pin,Tsp,des,Pvv){
    var c = document.getElementById("visa_canvas");
    var ctx = c.getContext("2d");
    ctx.clearRect(0, 0, c.width, c.height);

    var img = new Image();
    img.src = ("./visa_cleared_keys.svg");

    ctx.drawImage(img, 0, 0);

    ctx.font = "22px Arial";
    ctx.fillStyle = "black";

    var ccNo_x = 285;
    var top_y = 56;

    ctx.fillText(cardNum.substring(0,4), ccNo_x, top_y);
    ccNo_x += ctx.measureText(cardNum.substring(0,4)).width;
    ctx.fillStyle = "green";
    ctx.fillText(cardNum.substring(4, cardNum.length - 2), ccNo_x, top_y);
    ccNo_x += ctx.measureText(cardNum.substring(4, cardNum.length - 2)).width;
    ctx.fillStyle = "black";
    ctx.fillText(cardNum.charAt(cardNum.length -1), ccNo_x, top_y);

    ccNo_x = 585;
    ctx.fillStyle = "red";
    ctx.fillText(selec, ccNo_x, top_y);

    ccNo_x = 675;
    ctx.fillStyle = "blue";
    ctx.fillText(Pin, ccNo_x, top_y);

    var tsp_x = 355;
    var tsp_y = 158;

    ctx.fillStyle = "green";
    ctx.fillText(cardNum.substring(4, cardNum.length - 2), tsp_x, tsp_y);
    tsp_x += ctx.measureText(cardNum.substring(4, cardNum.length - 2)).width;

    ctx.fillStyle = "red";
    ctx.fillText(selec, tsp_x,tsp_y);
    tsp_x += ctx.measureText(selec).width;

    ctx.fillStyle = "blue";
    ctx.fillText(Pin, tsp_x,tsp_y);

    var des_x = 497;
    var des_y = 458;

    var des = des.toString();
    var count = 0;
    var letters = ["a", "b", "c", "d", "e", "f"]
    for (var i = 0; i < des.length; i++) {
        var lttr = des.charAt(i);
        if (!letters.includes(lttr) && count < 4) {
            ctx.fillStyle = "green";
            count++
        }
        else{
            ctx.fillStyle = "black";
        }
        ctx.fillText(lttr, des_x, des_y);
        des_x += ctx.measureText(lttr).width;
    }

    var pvv_x = 540;
    var pvv_y = 545;

    ctx.fillStyle = "green";
    ctx.fillText(Pvv, pvv_x, pvv_y);
}


function buildCreditCard(){
    var pin = document.getElementById("pin").value;
    var pin_flag = false;
    var numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    for (var i = 0; i < pin.length; i++) {
        if(!numbers.includes(pin.charAt(i))) {
            pin_flag = true;
            break;
        }
    }
    if (pin.length != 4 || pin_flag) {
        pin = pad(Math.floor(Math.random() * 10000), 4);        
    }
    var selector = parseInt(document.getElementById("selector").value);
    var cardNumber = genCC()[0];
    var tsp = buildTSP(cardNumber.toString(), pin.toString(), selector.toString());
    var des_out = encryptByDES(tsp, keys_str[selector - 1]);
    var pvv = genPVV(des_out);
    var cvv = pad(Math.floor(Math.random() * 1000), 3);
    document.getElementById("imeInPriimek").innerHTML = "<p><b>Ime in priimek</b>: "+ buildName()+"</p>";
    document.getElementById("drzava").innerHTML = "<p><b>Država</b>: Slovenija</p>";
    document.getElementById("Pin").innerHTML = "<p><b>PIN</b>: "+pin+"</p>";
    document.getElementById("Pvv").innerHTML = "<p><b>PVV</b>: "+pvv+"</p>";
    document.getElementById("Cvv").innerHTML = "<p><b>CVV</b>: "+cvv+"</p>";
    document.getElementById("Tsp").innerHTML = "<p><b>TSP</b>: "+tsp+"</p>";
    document.getElementById("Key").innerHTML = "<p><b>Ključ za DES</b>: "+keys_str[selector-1]+"</p>";
    document.getElementById("Des").innerHTML = "<p><b>Izhod DES</b>: "+des_out+"</p>";
    document.getElementById("CardNumber").innerHTML = "<p><b>Številka kartice</b>: "+cardNumber+"</p>";
    document.getElementById("naslov").innerHTML = "<h4><b>NOVO GENERIRANA VISA:</b></h4>";
    buildImageScg(selector,cardNumber,pin,tsp,des_out,pvv);
    buildImageScg(selector,cardNumber,pin,tsp,des_out,pvv);
}
