// Configure mathjax to recognize $...$ as in-line math delimiter.
/*window.MathJax = {
  tex: {
    inlineMath: [['$', '$'], ['\\(', '\\)']]
  }
};*/

function generiraj_besede(st_besed, min_dolzina, max_dolzina) {
    st_besed = parseInt(st_besed);
    min_dolzina = parseInt(min_dolzina);
    max_dolzina = parseInt(max_dolzina);
    document.getElementById("geslo").innerHTML = "Dela " + st_besed + "!";
}