# About us (2.PART - Mia) {#about -}

## Team members

TO- DO

## Introcuction

Motivation and goals - TO-DO

## Our approach

After defining the project (problem) in general, we had started thining 
about how to make it happen. We had defined to create the ebook
that would enable learning Cryptography trough interactions. 
We had started with that on mind. We had decided go with
a web application, either SPA (single page application) or
only frontend based application that can (but is not restricted to) be an SPA
application. As the project lasted only 4 months we had decided to use
an oracle that enables hot start. We had been choosing between: 

* python based (Run Stone Interactive)[http://runestoneinteractive.org/build/html/intro.html], and
* R based Shiny interactions enviroment with Bookdown (Markdown), in the RStudio IDE.

We had choosen the second option which had already realised version > 1. 
We had tought that R and RStudio won't we hard to learn, but on the way we have
managed to deploy an instance of teh Boodown Gitbook template with interactions
in plain HTML and JavaScript, and styled with CSS. We were even more saticfied with
the new option as it enables higher degree of the modularity. The modularity is quite
importan for this project. We had 10 students and 4 metors workking on the
project at the same time. This team was complitelly new, and most of the
participants get to know each other for the first time. 
Not all participants think alike, have the same
working habits and speed, and have the same knowledge level or background. The
team was conducted of two first year students, 3 second year students, 4 third 
year students and one master level student. They came from Multimadia,
Informatics and Computer science (both, University and Upper School),
Mathematics and Economy. To most of them, the working enviroment used was something
complitely new. Moreover, only three of them has passed Web Programming course
and have had some nowledge on how to create a web application, but using
different oracles. They knew HTML, JavaScript and CSS.

Thus, the more moduler the development process is, the better. In meaning, each
participant is working on its part of the ebook, for every textual content or
interaction, fix and overview we had tried to force creatin of an asociated
issue, then branch and merge request. At first this was pretty hart to conduct
as almost all participants had not encountered git and had worked with it.
Moreover, most of the students were unfamiliar with cryptography and some were 
beginners in programming (in every sense). Thus, they were scared with all the
new facts: the ebook oracle (R and Boodown, using Gitboo template and Mardown), the woring
enviroment (RStudio), the interactions development instructions and the language
(HTML + JavaScript + CSS), and the cryptography. That waa too much to process at
first. Thus, we loosen bit our approach to give them all at once, and request
from them to incorporate all that in their work. The participants that are more
keen in Cryptography area were in charge for defining most of the issues and
giving them to 1st, 2nd and 3rd year students. We aimed to slightly mitigate,
firts 3rd, then 2nd, and finally 1st year students to get thir own ideas which
interaction to do next and write its own issues. But that can not be entirely
done in only 4 months, we need at least 6 to 9 months. At the end of the project
we managed to get that some 3rd year students are able to define their own
issues in the (totally new for them) area of Cryptography. Most of the students
were able to overview their own applications in the last month, fix, style or
modify them. To be entirely correct, in the first phase of the project, we
ommited styling the interactions, as we were in need for one student (Nina) to
define the unified style instructions: (1) which color to use in which chapter
and (2) how to use special colors. The others had started with plain programming
interactions and writing textual content in HTML+JavaScript. One student (Jan,
and sometimes Janez) was in charge to create associated branches, test, give
comments (and repeat till we were staisfied with the interaction), merge and embed each
interaction into the ebook. With the time, we have managed that students are
creating associated branches and merge requests. Merge approval pard was left to
be done as at the beginning. After the embeding instructions were online,
students were encouraged, to before the final merge approval, to embed their
interaction into the ebook and  to write the textual content using Mardown at
the intended place of the ebook. 

As said before, we had started with the RStudio IDE, but had mitigated to
VisualStudioCode because of its easier usage in creating interactions with plain
HTML and JavaScript (RStudio is better only if uusing Shiny package). The whole
build were donen mannuually. Eventually, we had managed to create the script
that does the job and stores the output (ebook - html pages and javascrtipt
code) into `_crypto_ebook` folder. 

### Background Structure

To share the code, annd have the current status quo at onne place, we are using
GitLab. The project is public, as most of the issues. The core of the ebook is
writen in P folder. By running `./build.command` in the P folder's terminal,
everyone with the ebook content is able to build the ebook. Builed ebook is then
generated in the `P/_crypto_eknjiga` folder. To open the ebook, just go to the
`P/_crypto_eknjiga` folder and double-click on ove `.html` file. 

The project is conducted of, initially 4, but with the aim to be extended to
9 independant chapters. It has one main page, with 9 rotating cubes, each
corresponding one chapter. Doble-click leads to the selected chapter.Each
chapter is build from one markdown file, in precise R Markdown file, named
`P/P*.RmD`, and obe `P/P*` folder,  where `*` denotes the index of the chapter.
`P/P*.RmD` file contains the textual content and embeds interactions using html
`<iframe>` tag. Using `<iframe>` we had enabled modularity on each interaction
level which was crucial for the project as we were rather diverse group and 
had limited time to make our own product usable. Moreover, the project
continuation will probably lead to (we hope not) complitelly new project group
rising the diversibility in background and interest of the ebook developers.

`P/P*` folder stores all interactions to be embeded into `*` chapter trough
`P/P*.RmD` file. The name of the folder containing one interaction files should
be named by the interaction name. This enables easier differentiation between
iframes of the `P/P*.RmD` file, and their positioning in the text. 

### One chapter - hours spent, when looking back

Let us take one light (in number of interactions) chapter: the first Chapter
PIN. To read, learn and create the textual content, to select associated 
pictures from the internet, to proof
read all the textual content multiple times (3), and to style pictures-text 
correspondance we have spent about 40 hours. Then, we have to count the hours
spent on all the interactions which is between 90 and 100 hours (5 of them).
Furthermore, we had to replace interned-found pictures with our own creations
which took abouut 50hours.
Lastly, we needed to give it to someone independant to proof-read the entire
chapter and give comments (multiple times when needed), thus spending 5-10 more
hours. Thus, the overall cost is 40+100+50+5 = 195 hours. This is more than one
student can spent in one run of the project lating 4 months, which is equal 4*40
= 160.  And that was 'light' chapter. Thus, this had lead to the conclusion that
when having 10 students and 4 month project of this kind, we can not go with
more than 4 chapters + introduction. 

The best is to go with 2 lengthly or
more advanced (in textual content or style)
chapters or 4 light chapters. As having 4+introduction, we needed to made one
chapter extra light to meet the goal. 

