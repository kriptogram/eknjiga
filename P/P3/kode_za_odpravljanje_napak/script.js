String.prototype.replaceAt = function (index, replacement) {
  return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}

function spremeniCelico(celica) {
  // document.getElementById("klik-obvestilo").innerHTML = "";
  if (vhod.charAt(celica) === '0') {
    vhod = vhod.replaceAt(celica, '1');
  } else {
    vhod = vhod.replaceAt(celica, '0');
  }
  posodobi();
}

function spremeniSum(celica) {
  if (sum.charAt(celica) === '0') {
    sum = sum.replaceAt(celica, '1');
  } else {
    sum = sum.replaceAt(celica, '0');
  }
  posodobi();
}

function posodobi() {
  posodobiVhodnoTabelo();
  kodiraniVhod = izracunajKodiraniVhod(vhod);
  while (sum.length < kodiraniVhod.length) {
    sum += '0';
  }
  sum = sum.substring(0, kodiraniVhod.length);
  posodobiKodiranoVhodnoTabelo();
  izracunajRezultat();
  posodobiTabeloSuma();
  dekodirano = izracunajDekodiraniIzhod(rezultat);
  posodobiTabeloPrejetihBitov();
  posodobiDekodiranoTabelo();
  posodobiStatistiko();
}

var verjetnostNapake = 0;

var vhod = "00000000";
var kodiraniVhod = "00000000";
var sum = "00000000";
var rezultat = "00000000";
var dekodirano = "00000000";

function xor(/**/) {
  let args = arguments;
  let rezultat = "0";
  for (let i = 0; i < args.length; i++) {
    if (rezultat == args[i]) {
      rezultat = "0";
    } else {
      rezultat = "1";
    }
  }
  return rezultat;
}

function izracunajKodiraniVhod(vhod) {
  let kodiranje = document.getElementById("kodiranje");
  let izbranoKodiranje = kodiranje.options[kodiranje.selectedIndex].value;
  let izhod = "";
  if (izbranoKodiranje === "trojno") {
    izhod = "";
    for (let i = 0; i < vhod.length; i++) {
      izhod += vhod[i] + vhod[i] + vhod[i];
    }
    return izhod;
  }
  if (izbranoKodiranje === "brez") {
    return vhod;
  }
  if (izbranoKodiranje === "hamming") {
    izhod = "";
    while (vhod.length >= 4) {
      let kosVhoda = vhod.substring(0, 4)
      vhod = vhod.substring(4)
      izhod += (xor(kosVhoda[0], kosVhoda[1], kosVhoda[3]));
      izhod += (xor(kosVhoda[0], kosVhoda[2], kosVhoda[3]));
      izhod += (xor(kosVhoda[0]));
      izhod += (xor(kosVhoda[1], kosVhoda[2], kosVhoda[3]));
      izhod += (xor(kosVhoda[1]));
      izhod += (xor(kosVhoda[2]));
      izhod += (xor(kosVhoda[3]));
    }
    return izhod;
  }
  return izhod;
}

function izracunajDekodiraniIzhod(vhod) {
  let kodiranje = document.getElementById("kodiranje");
  let izbranoKodiranje = kodiranje.options[kodiranje.selectedIndex].value;
  let dekodirano = "";
  if (izbranoKodiranje === "trojno") {
    dekodirano = "";
    for (let i = 0; i < vhod.length / 3; i++) {
      let st0 = 0;
      let st1 = 0;
      for (let j = 0; j < 3; j++) {
        if (vhod[3 * i + j] === '0') {
          st0++;
        } else {
          st1++;
        }
      }
      dekodirano += (st0 > st1 ? '0' : '1');
    }
  }
  if (izbranoKodiranje === "brez") {
    return vhod;
  }
  if (izbranoKodiranje === "hamming") {
    izhod = "";
    while (vhod.length >= 7) {
      let kosVhoda = vhod.substring(0, 7)
      vhod = vhod.substring(7)
      mestoNapake = 0;
      mestoNapake += parseInt(xor(kosVhoda[3], kosVhoda[4], kosVhoda[5], kosVhoda[6]));
      mestoNapake *= 2;
      mestoNapake += parseInt(xor(kosVhoda[1], kosVhoda[2], kosVhoda[5], kosVhoda[6]));
      mestoNapake *= 2;
      mestoNapake += parseInt(xor(kosVhoda[0], kosVhoda[2], kosVhoda[4], kosVhoda[6]));
      if (mestoNapake > 0) {
        // prislo je do napake
        if (kosVhoda[mestoNapake - 1] == "0") {
          kosVhoda = kosVhoda.replaceAt(mestoNapake - 1, "1");
        } else {
          kosVhoda = kosVhoda.replaceAt(mestoNapake - 1, "0");
        }
      }

      izhod += (kosVhoda[2]);
      izhod += (kosVhoda[4]);
      izhod += (kosVhoda[5]);
      izhod += (kosVhoda[6]);

    }
    return izhod;
  }
  return dekodirano;
}

function generirajSum() {
  sum = "";
  for (let i = 0; i < kodiraniVhod.length; i++) {
    sum += (Math.random() < verjetnostNapake) ? '1' : '0';
  }
  izracunajRezultat();
}

function izracunajRezultat() {
  rezultat = "";
  for (let i = 0; i < kodiraniVhod.length; i++) {
    if (kodiraniVhod.charAt(i) === sum.charAt(i)) {
      rezultat += "0";
    } else {
      rezultat += "1";
    }
  }
}

function posodobiVhodnoTabelo() {
  let tabela = `
  <table class="table table-bordered">
    <tbody>
      <tr>
        <th scope="row" style="padding-left: 0px; padding-right: 0px; text-align: center;">Vhod</th>`;
  for (let i = 0; i < vhod.length; i++) {
    tabela += `<td onclick="spremeniCelico(` + i + `)" style="cursor: pointer; padding-left: 0px; padding-right: 0px; text-align: center;">` + vhod[i] + `</td>`;
  }
  tabela += `
      </tr>
    </tbody>
  </table>`;
  if (document.getElementById("vhod")) {
    document.getElementById("vhod").innerHTML = tabela;
  }
}

function posodobiKodiranoVhodnoTabelo() {
  if (document.getElementById("kodiraniVhod")) {
    let tabela = `
  <table class="table table-bordered">
    <tbody>
      <tr>
        <th scope="row" style="padding-left: 0px; padding-right: 0px; text-align: center;">Kodirano</th>`;
    for (let i = 0; i < kodiraniVhod.length; i++) {
      tabela += `<td style="padding-left: 0px; padding-right: 0px; text-align: center;">` + kodiraniVhod[i] + `</td>`;
    }
    tabela += `
      </tr>
    </tbody>
  </table>`;
    document.getElementById("kodiraniVhod").innerHTML = tabela;
  }
}

function posodobiTabeloPrejetihBitov() {
  let prepoznaniVhod = izracunajKodiraniVhod(dekodirano);
  let tabela = `
  <table class="table table-bordered">
    <tbody>
      <tr>
        <th scope="row" style="padding-left: 0px; padding-right: 0px; text-align: center;" title="Biti, ki so bili prepoznani kot napačni, so obarvani rdeče.">Prejeto</th>`;
  for (let i = 0; i < rezultat.length; i++) {
    tabela += `<td style="padding-left: 0px; padding-right: 0px; text-align: center;` + (prepoznaniVhod[i] === rezultat[i] ? '' : 'color: brown;') + `">` + rezultat[i] + `</td>`;
  }
  tabela += `
      </tr>
      <tr>
        <th scope="row" style="padding-left: 0px; padding-right: 0px; text-align: center;">Popravljeno</th>`;
  for (let i = 0; i < rezultat.length; i++) {
    tabela += `<td style="padding-left: 0px; padding-right: 0px; text-align: center;">` + prepoznaniVhod[i] + `</td>`;
  }
  tabela += `
      </tr>
    </tbody>
  </table>`;
  document.getElementById("prejeto").innerHTML = tabela;
}

function posodobiDekodiranoTabelo() {
  let tabela = `
  <table class="table table-bordered">
    <tbody>
      <tr>
        <th scope="row" style="padding-left: 0px; padding-right: 0px; text-align: center;" title="Biti, ki so se dekodirali narobe, so obarvani rdeče.">Odkodirano</th>`;
  for (let i = 0; i < dekodirano.length; i++) {
    tabela += `<td style="padding-left: 0px; padding-right: 0px; text-align: center; ` + (dekodirano[i] === vhod[i] ? '' : 'color: brown;') + `">` + dekodirano[i] + `</td>`;
  }
  tabela += `
      </tr>
    </tbody>
  </table>`;
  document.getElementById("izhod").innerHTML = tabela;
}

function posodobiTabeloSuma() {
  let tabela = `
  <table class="table table-bordered">
  <tbody>
  <tr>
  <th scope="row" style="padding-left: 0px; padding-right: 0px; text-align: center;">Šum</th>`;

  for (let i = 0; i < sum.length; i++) {
    tabela += `<td onclick="spremeniSum(` + i + `)" style="cursor: pointer; padding-left: 0px; padding-right: 0px; text-align: center;">` + sum[i] + `</td>`;
  }

  tabela += `
  </tr>
  <tr>
  <th scope="row" style="padding-left: 0px; padding-right: 0px; text-align: center;">Poslano</th>`;

  for (let i = 0; i < kodiraniVhod.length; i++) {
    tabela += `<td style="padding-left: 0px; padding-right: 0px; text-align: center;">` + kodiraniVhod[i] + `</td>`;
  }

  tabela += `
  </tr>
  <tr>
  <th scope="row" style="padding-left: 0px; padding-right: 0px; text-align: center;">Rezultat</th>`;

  for (let i = 0; i < rezultat.length; i++) {
    tabela += `<td style="padding-left: 0px; padding-right: 0px; text-align: center;">` + rezultat[i] + `</td>`;
  }

  tabela += `    
  </tr>
  </tbody>
  </table>`;
  document.getElementById("sum").innerHTML = tabela;
}

function posodobiStatistiko() {
  let stNapak = 0;
  for (let i = 0; i < dekodirano.length; i++) {
    if (dekodirano[i] !== vhod[i]) {
      stNapak++;
    }
  }

  let prepoznaniVhod = izracunajKodiraniVhod(dekodirano);
  let zaznanihNapak = 0;
  for (let i = 0; i < rezultat.length; i++) {
    if (prepoznaniVhod[i] !== rezultat[i]) {
      zaznanihNapak++;
    }
  }

  let statistika = "Statistika:<ul>";
  statistika += "<li>Zaznane napake: " + zaznanihNapak + "</li>";
  statistika += "<li>Napačno dekodirani biti: " + stNapak + " (" + (100 * stNapak / dekodirano.length) + " %)</li>";
  statistika += "</ul>";
  document.getElementById("statistika").innerHTML = statistika;

}

function nastaviVerjetnost(verjetnost) {
  verjetnostNapake = verjetnost / 200.0;
  document.getElementById("napisVerjetnosti").innerHTML = Math.round(verjetnostNapake * 200) + '% <small>(' + Math.round(verjetnostNapake * 100) + '% bitov bo napačnih)</small>';
  generirajSum();
  posodobi();
}

function izberiKodiranje() {
  posodobi();
}

window.addEventListener('load', function () {
  posodobi();
  let fileUpload = document.getElementById("fileUpload");
  if (fileUpload) {
    fileUpload.onchange = fileSelected;
  }
  let datoteka = document.getElementById("datoteka");
  if (datoteka) {
    datoteka.onchange = enableFileUpload;
  }
});