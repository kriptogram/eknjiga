//https://en.wikipedia.org/wiki/Glagolitic_script
//https://en.wikipedia.org/wiki/Slovene_alphabet
//https://en.wikipedia.org/wiki/Russian_alphabet
//https://en.wikipedia.org/wiki/Serbian_Cyrillic_alphabet
//https://en.wikipedia.org/wiki/Cyrillic_numerals <-- TODO
//srbska glagolica

document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("zacniBtn").click();
      }
});
var slo_zastave = {
    'A': 'slike/zastave/a.png',
    'B': 'slike/zastave/b.png',
    'C': 'slike/zastave/c.png',
    'D': 'slike/zastave/d.png',
    'E': 'slike/zastave/e.png',
    'F': 'slike/zastave/f.png',
    'G': 'slike/zastave/g.png',
    'H': 'slike/zastave/h.png',
    'I': 'slike/zastave/i.png',
    'J': 'slike/zastave/j.png',
    'K': 'slike/zastave/k.png',
    'L': 'slike/zastave/l.png',
    'M': 'slike/zastave/m.png',
    'N': 'slike/zastave/n.png',
    'O': 'slike/zastave/o.png',
    'P': 'slike/zastave/p.png',
    'Q': 'slike/zastave/q.png',
    'R': 'slike/zastave/r.png',
    'S': 'slike/zastave/s.png',
    'T': 'slike/zastave/t.png',
    'U': 'slike/zastave/u.png',
    'V': 'slike/zastave/v.png',
    'W': 'slike/zastave/w.png',
    'X': 'slike/zastave/x.png',
    'Y': 'slike/zastave/y.png',
    'Z': 'slike/zastave/z.png'
}
var zastave_slo = {
    'slike/zastave/a.png': 'A',
    'slike/zastave/b.png': 'B',
    'slike/zastave/c.png': 'C',
    'slike/zastave/d.png': 'D',
    'slike/zastave/e.png': 'E',
    'slike/zastave/f.png': 'F',
    'slike/zastave/g.png': 'G',
    'slike/zastave/h.png': 'H',
    'slike/zastave/i.png': 'I',
    'slike/zastave/j.png': 'J',
    'slike/zastave/k.png': 'K',
    'slike/zastave/l.png': 'L',
    'slike/zastave/m.png': 'M',
    'slike/zastave/n.png': 'N',
    'slike/zastave/o.png': 'O',
    'slike/zastave/p.png': 'P',
    'slike/zastave/q.png': 'Q',
    'slike/zastave/r.png': 'R',
    'slike/zastave/s.png': 'S',
    'slike/zastave/t.png': 'T',
    'slike/zastave/u.png': 'U',
    'slike/zastave/v.png': 'V',
    'slike/zastave/w.png': 'W',
    'slike/zastave/x.png': 'X',
    'slike/zastave/y.png': 'Y',
    'slike/zastave/z.png': 'Z'
}
var slo_znakovna = {
    '1': 'slike/znakovna/1.png',
    '2': 'slike/znakovna/2.png',
    '3': 'slike/znakovna/3.png',
    '4': 'slike/znakovna/4.png',
    '5': 'slike/znakovna/5.png',
    '6': 'slike/znakovna/6.png',
    '7': 'slike/znakovna/7.png',
    '8': 'slike/znakovna/8.png',
    '9': 'slike/znakovna/9.png',
    'B': 'slike/znakovna/b.png',
    'C': 'slike/znakovna/c.png',
    'D': 'slike/znakovna/d.png',
    'E': 'slike/znakovna/e.png',
    'F': 'slike/znakovna/f.png',
    'G': 'slike/znakovna/g.png',
    'H': 'slike/znakovna/h.png',
    'I': 'slike/znakovna/i.png',
    'J': 'slike/znakovna/j.png',
    'K': 'slike/znakovna/k.png',
    'L': 'slike/znakovna/l.png',
    'M': 'slike/znakovna/m.png',
    'N': 'slike/znakovna/n.png',
    'O': 'slike/znakovna/o.png',
    'P': 'slike/znakovna/p.png',
    'Q': 'slike/znakovna/q.png',
    'R': 'slike/znakovna/r.png',
    'S': 'slike/znakovna/s.png',
    'T': 'slike/znakovna/t.png',
    'U': 'slike/znakovna/u.png',
    'V': 'slike/znakovna/v.png',
    'W': 'slike/znakovna/w.png',
    'X': 'slike/znakovna/x.png',
    'Y': 'slike/znakovna/y.png',
    'Z': 'slike/znakovna/z.png'
}
var znakovna_slo = {
    'slike/znakovna/1.png': '1',
    'slike/znakovna/2.png': '2',
    'slike/znakovna/3.png': '3',
    'slike/znakovna/4.png': '4',
    'slike/znakovna/5.png': '5',
    'slike/znakovna/6.png': '6',
    'slike/znakovna/7.png': '7',
    'slike/znakovna/8.png': '8',
    'slike/znakovna/9.png': '9',
    'slike/znakovna/b.png': 'B',
    'slike/znakovna/c.png': 'C',
    'slike/znakovna/d.png': 'D',
    'slike/znakovna/e.png': 'E',
    'slike/znakovna/f.png': 'F',
    'slike/znakovna/g.png': 'G',
    'slike/znakovna/h.png': 'H',
    'slike/znakovna/i.png': 'I',
    'slike/znakovna/j.png': 'J',
    'slike/znakovna/k.png': 'K',
    'slike/znakovna/l.png': 'L',
    'slike/znakovna/m.png': 'M',
    'slike/znakovna/n.png': 'N',
    'slike/znakovna/o.png': 'O',
    'slike/znakovna/p.png': 'P',
    'slike/znakovna/q.png': 'Q',
    'slike/znakovna/r.png': 'R',
    'slike/znakovna/s.png': 'S',
    'slike/znakovna/t.png': 'T',
    'slike/znakovna/u.png': 'U',
    'slike/znakovna/v.png': 'V',
    'slike/znakovna/w.png': 'W',
    'slike/znakovna/x.png': 'X',
    'slike/znakovna/y.png': 'Y',
    'slike/znakovna/z.png': 'Z'
}
var slo_grska = {
    'A': 'slike/grska/a.png',
    'B': 'slike/grska/b.png',
    'D': 'slike/grska/d.png',
    'E': 'slike/grska/e.png',
    'G': 'slike/grska/g.png',
    'I': 'slike/grska/i.png',
    'K': 'slike/grska/k.png',
    'L': 'slike/grska/l.png',
    'M': 'slike/grska/m.png',
    'N': 'slike/grska/n.png',
    'O': 'slike/grska/o.png',
    'P': 'slike/grska/p.png',
    'R': 'slike/grska/r.png',
    'S': 'slike/grska/s.png',
    'T': 'slike/grska/t.png',
    'Z': 'slike/grska/z.png'
}
var grska_slo = {
    'slike/grska/a.png': 'A',
    'slike/grska/b.png': 'B',
    'slike/grska/d.png': 'D',
    'slike/grska/e.png': 'E',
    'slike/grska/g.png': 'G',
    'slike/grska/i.png': 'I',
    'slike/grska/k.png': 'K',
    'slike/grska/l.png': 'L',
    'slike/grska/m.png': 'M',
    'slike/grska/n.png': 'N',
    'slike/grska/o.png': 'O',
    'slike/grska/p.png': 'P',
    'slike/grska/r.png': 'R',
    'slike/grska/s.png': 'S',
    'slike/grska/t.png': 'T',
    'slike/grska/z.png': 'Z'
}
var rusl = {
    'А': 'A',
    'Б': 'B',
    'В': 'V',
    'Г': 'G',
    'Д': 'D',
    'Е': 'JE',
    'Ё': 'JO',
    'Ж': 'Ž',
    'З': 'Z',
    'И': 'I',
    'Й': 'J',
    'К': 'K',
    'Л': 'L',
    'М': 'M',
    'Н': 'N',
    'О': 'O',
    'П': 'P',
    'Р': 'R',
    'С': 'S',
    'Т': 'T',
    'У': 'U',
    'Ф': 'F',
    'Х': 'H',
    'Ц': 'C',
    'Ч': 'Č',
    'Ш': 'Š',
    'Щ': 'Š',
    'Ъ': '',
  //  'Ъ': '\'',
    'Ы': 'I',
     'Ь': '',
   // 'Ь': '\"',
    'Э': 'E',
    'Ю': 'JU',
    'Я': 'JA',
};


var slru = {
   'A' : 'А' ,
   'B' : 'Б' ,
   'V' : 'В' ,
   'G' : 'Г' ,
   'D' : 'Д',
   'JE' : 'Е',
   'JO' : 'Ё',
   'Ž' : 'Ж' ,
   'Z' : 'З' ,
   'I' : 'И' ,
   'J' : 'Й' ,
   'K' : 'К' ,
   'L' : 'Л' ,
   'M' : 'М' ,
   'N' : 'Н' ,
   'O' : 'О' ,
   'P' : 'П' ,
   'R' : 'Р' ,
   'S' : 'С' ,
   'T' : 'Т' ,
   'U' : 'У' ,
   'F' : 'Ф' ,
   'H' : 'Х' ,
   'C' : 'Ц' ,
   'Č' : 'Ч' ,
   'Š' : 'Ш' ,
   'E' : 'Э' ,
   'JU' : 'Ю',
   'JA' : 'Я'
};


var srsl = {
    'А': 'A',
    'Б': 'B',
    'В': 'V',
    'Г': 'G',
    'Д': 'D',
    'Ђ': 'Đ',
    'Е': 'E',
    'Ж': 'Ž',
    'З': 'Z',
    'И': 'I',
    'Ј': 'J',
    'К': 'K',
    'Л': 'L',
    'Љ': 'LJ',
    'М': 'M',
    'Н': 'N',
    'Њ': 'NJ',
    'О': 'O',
    'П': 'P',
    'Р': 'R',
    'С': 'S',
    'Т': 'T',
    'Ћ': 'Ć',
    'У': 'U',
    'Ф': 'F',
    'Х': 'H',
    'Ц': 'C',
    'Ч': 'Č',
    'Џ': 'DŽ',
    'Ш': 'Š'
};

var slsr = {
    'A':'А',
    'B':'Б',
    'V':'В',
    'G':'Г',
    'D':'Д',
    'Đ':'Ђ',
    'E':'Е',
    'Ž':'Ж',
    'Z':'З',
    'I':'И',
    'J':'Ј',
    'K':'К',
    'L':'Л',
    'M':'М',
    'N':'Н',
    'NJ':'Њ',
    'O':'О',
    'P':'П',
    'R':'Р',
    'S':'С',
    'T':'Т',
    'Ć':'Ћ',
    'U':'У',
    'F':'Ф',
    'H':'Х',
    'C':'Ц',
    'Č':'Ч',
    'Š':'Ш',
    'DŽ': 'Џ',
    'LJ':'Љ'
};


var glsl ={
    'Ⰰ': 'A', 
    'Ⰱ': 'B', 
    'Ⰲ': 'V', 	
    'Ⰳ': 'G', 	
    'Ⰴ': 'D', 	
    'Ⰵ': 'E', 
    'Ⰶ': 'Ž', 
    'Ⰷ': 'DŽ', 
    'Ⰸ': 'Z', 
    'Ⰹ': 'I', 
    'Ⰺ': 'J',
    'Ⰻ': 'I', 
    'Ⰼ': 'Đ', 
    'Ⰽ': 'K', 
    'Ⰾ': 'L', 
    'Ⰿ': 'M', 
    'Ⱀ': 'N', 	
    'Ⱁ': 'O', 	
    'Ⱂ': 'P', 	
    'Ⱃ': 'R', 	
    'Ⱄ': 'S', 	
    'Ⱅ': 'T', 	
    'Ⱆ': 'U', 	
    'Ⱇ': 'F', 	
    'Ⱈ': 'H', 	
    'Ⱉ': 'O', 	
    'Ⱋ': 'Š', 	
    'Ⱌ': 'C', 	
    'Ⱍ': 'Č', 	
    'Ⱎ': 'Š', 	
    'Ⱏ': '', 
    'Ⱐ': '',
    'ⰟⰊ': 'I', 
    'Ⱐ': '',
    'Ⱑ': 'JA',
    'Ⱖ': 'JO',
    'Ⱓ':'JU',
    'Ⱔ':'E',
    'Ⱗ':'JE',
    'Ⱘ':'O',
    'Ⱙ':'JO',
    'Ⱚ':'TH',
    'Ⱛ':'I'
}

var slgl ={
    'A': 'Ⰰ', 
    'B': 'Ⰱ', 
    'V': 'Ⰲ', 	
    'G': 'Ⰳ', 	
    'D': 'Ⰴ', 	
    'E': 'Ⰵ', 
    'Ž': 'Ⰶ', 
    'Z': 'Ⰸ', 
    'I': 'Ⰹ',
    'J': 'Ⰺ',
    'K': 'Ⰽ', 
    'L': 'Ⰾ', 
    'M': 'Ⰿ', 
    'N': 'Ⱀ', 	
    'O': 'Ⱁ', 	
    'P': 'Ⱂ', 	
    'R': 'Ⱃ', 	
    'S': 'Ⱄ', 	
    'T': 'Ⱅ', 	
    'U': 'Ⱆ', 	
    'F': 'Ⱇ', 	
    'H': 'Ⱈ', 	
    'C': 'Ⱌ', 	
    'Č': 'Ⱍ', 	
    'Š': 'Ⱎ', 
    'DŽ': 'Ⰷ', 
    'JA': 'Ⱑ',
    'JO': 'Ⱖ',
    'JU': 'Ⱓ',
    'Đ': 'Ⰼ'
}




document.getElementById('card').style.display='none';
document.getElementById('c1').style.display='none';
document.getElementById('c2').style.display='none';
document.getElementById('c3').style.display='none';
document.getElementById('c4').style.display='none';
document.getElementById('c5').style.display='none';

function random_bin(){
    r = Math.random()
    if (r<.4)
        return 0;
    if (r<.7)
        return 1;
    if (r<.9)
        return 2;
    return 3;
}
var langdict = rusl;
var correct_card = 'c1';
var card_front_face = '';
var card_front_face_index = 0;
var bins = [];
var dictkeys = [];
var curr_bin =0;

function filter_invisible(langdict, ch){
    if (ch == 'Ⱐ' || ch == 'Ь'){
        return 'mehki znak'
    }
    if (ch == 'Ⱏ' || ch == 'Ъ'){
        return 'trdi znak'
    }
    return langdict[ch]
}
function cards(){
    var base = document.getElementsByName('seldict')[0].value;
    switch(base) {
        case 'slru':
            langdict=slru;
            break;
        case 'slsr':
            langdict=slsr;
            break;
        case 'slgl':
            langdict=slgl;
            break;
        case 'rusl':
            langdict=rusl;
            break;
        case 'srsl':
            langdict=srsl;
            break;
        case 'glsl':
            langdict=glsl;
            break;
        case 'slo_zastave':
            langdict=slo_zastave;
            break;
        case 'zastave_slo':
            langdict=zastave_slo;
            break;
        case 'slo_znakovna':
            langdict=slo_znakovna;
            break;
        case 'znakovna_slo':
            langdict=znakovna_slo;
            break;
        case 'slo_grska':
            langdict=slo_grska;
            break;
        case 'grska_slo':
            langdict=grska_slo;
            break;
        default:
            langdict = rusl;
    }

    //console.log(base);
    dictkeys=Object.keys(langdict);
    bins = [dictkeys.slice(), [], [], []];
    //console.log(bins);
    document.getElementById('card').style.display='block';
    document.getElementById('c1').style.display='inline';
    document.getElementById('c2').style.display='inline';
    document.getElementById('c3').style.display='inline';
    document.getElementById('c4').style.display='inline';
    document.getElementById('c5').style.display='inline';
    
    curr_bin =0;
    card_front_face_index=Math.floor(Math.random()*bins[curr_bin].length);
    card_front_face = bins[curr_bin][card_front_face_index];
    //console.log(card_front_face);
//    document.getElementById('card').innerHTML='<h1>'+ch+'</h1>';
    document.getElementById('card').innerHTML=prepareImage(card_front_face, 'card');
  
    document.getElementById('c1').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c2').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c3').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c4').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c5').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    correct_card = 'c'+Math.ceil(Math.random()*5);
    document.getElementById(correct_card).value = filter_invisible(langdict, card_front_face);

    document.getElementById('c1').innerHTML = '<h1>'+prepareImage(document.getElementById('c1').value, 'button')+'</h1>';
    document.getElementById('c2').innerHTML = '<h1>'+prepareImage(document.getElementById('c2').value, 'button')+'</h1>';
    document.getElementById('c3').innerHTML = '<h1>'+prepareImage(document.getElementById('c3').value, 'button')+'</h1>';
    document.getElementById('c4').innerHTML = '<h1>'+prepareImage(document.getElementById('c4').value, 'button')+'</h1>';
    document.getElementById('c5').innerHTML = '<h1>'+prepareImage(document.getElementById('c5').value, 'button')+'</h1>';
    document.getElementById('info').innerHTML='';
}

function prepareImage(value, tipSlike) {
    if (value.startsWith('slike')) {
        let style = '';
        switch (tipSlike) {
            case 'text':
                style = 'height: 50px';
                break;
            case 'button':
                style = 'height: 70px';
                break;
            case 'card':
                style = 'height: 100px';
                break;
        }
        return '<img src="'+value+'" style="'+style+'"></img>'
    }
    return value;
}

function evalueate(id){
    var val = document.getElementById(id).value
    if (val == filter_invisible(langdict,card_front_face)){
        bins[curr_bin].splice(card_front_face_index,1);
        bins[Math.min(3, curr_bin+1)].push(card_front_face);
        document.getElementById('info').innerHTML='<font color="MediumSeaGreen">Izbira JE bila pravilna.</font><br> Škatle: ' + [bins[0].length,bins[1].length,bins[2].length,bins[3].length].toString();

    }else{
        bins[curr_bin].splice(card_front_face_index,1);
        bins[Math.max(0, curr_bin-1)].push(card_front_face);
        document.getElementById('info').innerHTML='<font color="Tomato">Izbira NI bila pravilna</font>. Pravilna izbira za '+prepareImage(card_front_face, 'text')+' je '+prepareImage(filter_invisible(langdict,card_front_face), 'text')+'.<br> Škatle: ' + [bins[0].length,bins[1].length,bins[2].length,bins[3].length].toString();

    }

    curr_bin = random_bin();
    while(bins[curr_bin].length ==0){
        curr_bin = random_bin();
    }
    ////console.log('current bin sizes:', [bins[0].length,bins[1].length,bins[2].length,bins[3].length])

    card_front_face_index=Math.floor(Math.random()*bins[curr_bin].length);
    card_front_face = bins[curr_bin][card_front_face_index];
    ////console.log(card_front_face);

    document.getElementById('card').innerHTML=prepareImage(card_front_face, 'card');
  
    document.getElementById('c1').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c2').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c3').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c4').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c5').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    correct_card = 'c'+Math.ceil(Math.random()*5);
    document.getElementById(correct_card).value = filter_invisible(langdict, card_front_face);

    document.getElementById('c1').innerHTML = '<h1>'+prepareImage(document.getElementById('c1').value, 'button')+'</h1>';
    document.getElementById('c2').innerHTML = '<h1>'+prepareImage(document.getElementById('c2').value, 'button')+'</h1>';
    document.getElementById('c3').innerHTML = '<h1>'+prepareImage(document.getElementById('c3').value, 'button')+'</h1>';
    document.getElementById('c4').innerHTML = '<h1>'+prepareImage(document.getElementById('c4').value, 'button')+'</h1>';
    document.getElementById('c5').innerHTML = '<h1>'+prepareImage(document.getElementById('c5').value, 'button')+'</h1>';

}
