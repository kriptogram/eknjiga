//popravi napise
//popravi izpis cikla

window.addEventListener('load', function(){

	// global opacity value
	const opacity = 0.6;

	//global background color rgb
	const background_color_r = 255;
	const background_color_g = 255;
	const background_color_b = 255;

	//color menagement
	var rgb = function(r,g,b) {   
		var red = rgbToHex(r);
		var green = rgbToHex(g);
		var blue = rgbToHex(b);
		return red+green+blue;
	};
	var rgbColorToRgbaColor = function(rgb, colorName) {
		switch(colorName) {
			case 'r':
				return (1 - opacity) * rgb + opacity * background_color_r; 
			case 'g':
				return (1 - opacity) * rgb + opacity * background_color_g;
			case 'b':
				return (1 - opacity) * rgb + opacity * background_color_b;
			default:
				return;
		}
	}
	var rgbToHex = function (rgb) {
		var hex = Number(rgb).toString(16);
		if (hex.includes(".")) {
			splitHex = hex.split(".");
			hex = splitHex[0];
		}
		if (hex.length < 2) {
			hex = "0" + hex;
		}
  		return hex;
	};

	function calcColor(val){
		var half=Math.floor(n/2);
		var up=n-half;

		if(val==half){
			var r = rgbColorToRgbaColor(255, 'r');
			var g = rgbColorToRgbaColor(255, 'g');
			var b = rgbColorToRgbaColor(0, 'b');
		}else if(val<half){
			var green=Math.floor(val*255/half);
			var r = rgbColorToRgbaColor(255, 'r');
			var g = rgbColorToRgbaColor(green, 'g');
			var b = rgbColorToRgbaColor(0, 'b');
		}else{
			var red=Math.floor((up-(val-half))*255/up);
			var r = rgbColorToRgbaColor(red, 'r');
			var g = rgbColorToRgbaColor(255, 'g');
			var b = rgbColorToRgbaColor(0, 'b');
		}
		return rgb(r, g, b);
	}

	//vse drugo
	var n;
	var tab=document.getElementById("tab");
	var mult=document.getElementById("mult");
	var messSum=document.getElementById("messSum");
	var messMult=document.getElementById("messMult");
	var sumVisible;
	var multVisible;
	var cb=false;
	var mojRezultat;
	var forma=document.getElementById("forma");
	document.getElementById("cb").style.visibility="hidden";

	forma.addEventListener("submit", function(e){
		//zbrisem vse obstojece vrstice
		e.preventDefault();
		inizializiraj();
	});

	function inizializiraj(){
		document.getElementById("cb").style.visibility="visible";
		while(tab.rows[0])tab.deleteRow(0);
		while(mult.rows[0])mult.deleteRow(0);
		while(messSum.rows[0])messSum.deleteRow(0);
		while(messMult.rows[0])messMult.deleteRow(0);
		n=document.getElementById("n").value;
		cb=false;
		if(n=="")return;
		for(var i=0; i<3; i++){
			var row=messSum.insertRow(i);
			var cell=row.insertCell(0);
			row=messMult.insertRow(i);
			cell=row.insertCell(0);
		}
		n=parseInt(n, 10);
		messSum.rows[0].cells[0].innerHTML="Pritisni na neko celico, da dobiš inverzni element";
		messSum.rows[1].cells[0].innerHTML="Tukaj se izpiše operacija";
		messSum.rows[2].cells[0].innerHTML="Tukaj se izpiše cikel";
		messMult.rows[0].cells[0].innerHTML="Pritisni na neko celico, da dobiš inverzni element";
		messMult.rows[1].cells[0].innerHTML="Tukaj se izpiše operacija";
		messMult.rows[2].cells[0].innerHTML="Tukaj se izpiše cikel";

		sumVisible=true;
		multVisible=true;

		for(var i=0; i<=n; i++){
			var row=tab.insertRow(i);
			for(var j=0; j<=n; j++){
				var cell=row.insertCell(j);
				if(i==0 && j==0)cell.innerHTML="+";
				else if(i==0)cell.innerHTML=(j-1).toString(10);
				else if(j==0)cell.innerHTML=(i-1).toString(10);
				else{
					var val=((i-1)+(j-1))%n;
					cell.innerHTML=val.toString(10);
					cell.style.backgroundColor=calcColor(val);
					cell.style.width="15px"
				}
			}
		}

		for(var i=0; i<=n; i++){
			var row=mult.insertRow(i);
			for(var j=0; j<=n; j++){
				var cell=row.insertCell(j);
				if(i==0 && j==0)cell.innerHTML=" * ";
				else if(i==0)cell.innerHTML=(j-1).toString(10);
				else if(j==0)cell.innerHTML=(i-1).toString(10);
				else{
					var val=((i-1)*(j-1))%n;
					cell.innerHTML=val.toString(10);
					cell.style.backgroundColor=calcColor(val);
					cell.style.width="15px"
				}
			}
		}
	}
	inizializiraj();

	document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("reset").click();
		  }
	});

	//event listenerji za tabelo sestevanja
  	tab.addEventListener("click", function(event) {
    	if(cb==true)return;
    	if(sumVisible==true && event != null && event.target != null && event.target.innerHTML != null){

			//evidenciramo vse celice z istim rezultatom

			var td = event.target;
			if (td && td.cellIndex != 0 && td.parentNode && td.parentNode.rowIndex != 0 && event.target.innerHTML <= n && event.target.innerHTML >= 0) {
				while (td !== this && !td.matches("td")) {
					td = td.parentNode;
				}
				if (td === this) {
	
				} else {
					var val=td.innerHTML;
					if(val=='+')return;
					  for(var i=0; i<=n; i++){
						  for(var j=0; j<=n; j++){
							  if(tab.rows[i].cells[j].innerHTML==val || i==0 || j==0){
								  //do nothing
							  }else{
								tab.rows[i].cells[j].style.visibility='hidden';      					
							  }
						  }
					  }
				}
	
				//izpisemo inverzni element
				messSum.rows[0].cells[0].innerHTML="Inverzni element: "+(n-parseInt(td.innerHTML, 10))+" ( "+(n-parseInt(td.innerHTML, 10))+" + "+td.innerHTML+" = 0 mod "+ n+" )";
				messSum.rows[1].cells[0].innerHTML="Tukaj se izpiše operacija. Pritisni še enkrat na tabelo, če hočeš aktivirati to funkcionalnost.";
				messSum.rows[2].cells[0].innerHTML="Tukaj se izpiše cikel";
				document.getElementById("cb").style.visibility="hidden";
				sumVisible=false;
			}

    	}else{
    		for(var i=0; i<=n; i++){
	      		for(var j=0; j<=n; j++){
					tab.rows[i].cells[j].style.visibility='visible';      					
	      		}
	      	}	
	      	messSum.rows[0].cells[0].innerHTML="Pritisni na neko celico, da dobiš inverzni element";
	      	sumVisible=true;
			document.getElementById("cb").style.visibility="visible";
    	}
  	});

  	tab.addEventListener("mouseover", function(event){
  		if(cb==true)return;
  		if(sumVisible==false || cb==true)return;
  		var td = event.target;
  		while (td !== this && !td.matches("td")) {
	       	td = td.parentNode;
	    }
	    if (td === this) {

	    } else {
	    	var val=td.innerHTML;
	    	var celica=td.cellIndex;
	    	var vrstica=td.parentNode.rowIndex;
	    	celica--;
	    	vrstica--;
	    	if(celica>=0 && vrstica>=0){
	    		//izpisi vsoto
				messSum.rows[1].cells[0].innerHTML=vrstica+" + "+celica+ " mod "+n+" = "+val;
				messSum.rows[2].cells[0].innerHTML=vrniCikel(celica, "+");
	    	}


	    	//pobarvam se prve celice v rdece
	    	if(celica!=-1 && vrstica!=-1){
				var r = rgbColorToRgbaColor(255, 'r');
				var g = rgbColorToRgbaColor(0, 'g');
				var b = rgbColorToRgbaColor(0, 'b');
		    	tab.rows[0].cells[celica+1].style.backgroundColor= rgb(r,g,b);
		    	tab.rows[vrstica+1].cells[0].style.backgroundColor= rgb(r,g,b);
			}
			// else if(celica!=-1){
	    	// 	for(var i=1; i<=n; i++){
	    	// 		for(var j=1; j<=n; j++){
	    	// 			if(j!=celica+1)tab.rows[i].cells[j].style.visibility="hidden";
	    	// 		}
	    	// 	}
	    	// }else if(vrstica!=-1){
	    	// 	for(var i=1; i<=n; i++){
	    	// 		for(var j=1; j<=n; j++){
	    	// 			if(i!=vrstica+1)tab.rows[i].cells[j].style.visibility="hidden";
	    	// 		}
	    	// 	}
	    	// }
	    }
  	});

  	tab.addEventListener("mouseout", function(event){
  		if(cb==true)return;
  		if(sumVisible==false || cb==true)return;
  		messSum.rows[1].cells[0].innerHTML="Tukaj se izpiše operacija";
  		messSum.rows[2].cells[0].innerHTML="Tukaj se izpiše cikel";
  		for(var i=0; i<=n; i++){
  			tab.rows[0].cells[i].style.backgroundColor='white';
  			tab.rows[i].cells[0].style.backgroundColor='white';
  		}
  		for(var i=1; i<=n; i++){
  			for(var j=1; j<=n; j++){
  				tab.rows[i].cells[j].style.visibility="visible";
  			}
  		}
  	});


  	//event listenerji za tabelo mnozenja
  	mult.addEventListener("click", function(event) {
  		if(cb==true)return;
    	if(multVisible==true && event != null && event.target != null && event.target.innerHTML != null){

    		//evidenciramo vse celice z istim rezultatom
			var td = event.target;
			if (td && td.cellIndex != 0 && td.parentNode && td.parentNode.rowIndex != 0  && event.target.innerHTML <= n && event.target.innerHTML >= 0) {
				while (td !== this && !td.matches("td")) {
					td = td.parentNode;
				}
				if (td === this) {
	
				} else {
					var val=td.innerHTML;
					if(val==" * ")return;
					  for(var i=0; i<=n; i++){
						  for(var j=0; j<=n; j++){
							  if(mult.rows[i].cells[j].innerHTML==val || i==0 || j==0){
								  //do nothing
							  }else{
								mult.rows[i].cells[j].style.visibility='hidden';      					
							  }
						  }
					  }
				}
	
				//izpisemo inverzni element
				var inv=inverzni(parseInt(td.innerHTML, 10));
				if(inv>0) messMult.rows[0].cells[0].innerHTML="Inverzni element: "+inv+" ( "+inv+" * "+td.innerHTML+" = 1 mod "+ n+" )";
				else messMult.rows[0].cells[0].innerHTML="Inverzni element ne obstaja.";
				multVisible=false;
				messMult.rows[1].cells[0].innerHTML="Tukaj se izpiše operacija. Pritisni še enkrat na tabelo, če hočeš aktivirati to funkcionalnost.";
				messMult.rows[2].cells[0].innerHTML="Tukaj se izpiše cikel";
				document.getElementById("cb").style.visibility="hidden";
			}

    	}else{
    		for(var i=0; i<=n; i++){
	      		for(var j=0; j<=n; j++){
					mult.rows[i].cells[j].style.visibility='visible';      					
	      		}
	      	}	
	      	messMult.rows[0].cells[0].innerHTML="Pritisni na neko celico, da dobiš inverzni element";
	      	multVisible=true;
			document.getElementById("cb").style.visibility="visible";
    	}
  	});

  	mult.addEventListener("mouseover", function(event){
  		if(cb==true || multVisible==false)return;
  		if(multVisible==false)return;
  		var td = event.target;
  		while (td !== this && !td.matches("td")) {
	       	td = td.parentNode;
	    }
	    if (td === this) {

	    } else {
	    	var val=td.innerHTML;
	    	var celica=td.cellIndex;
	    	var vrstica=td.parentNode.rowIndex;
	    	celica--;
	    	vrstica--;
	    	if(celica>=0 && vrstica>=0){
	    		//izpisi vsoto
	    		messMult.rows[1].cells[0].innerHTML=vrstica+" <span>&#183;</span> "+ celica+ " mod "+n+" = "+val;
	    	}

	    	//pobarvam se prve celice v rdece
	    	if(celica!=-1 && vrstica!=-1){
				var r = rgbColorToRgbaColor(255, 'r');
				var g = rgbColorToRgbaColor(0, 'g');
				var b = rgbColorToRgbaColor(0, 'b');

		    	mult.rows[0].cells[celica+1].style.backgroundColor= rgb(r, g, b);
		    	mult.rows[vrstica+1].cells[0].style.backgroundColor= rgb(r, g, b);
			}
			// else if(celica!=-1){
	    	// 	for(var i=1; i<=n; i++){
	    	// 		for(var j=1; j<=n; j++){
	    	// 			if(j!=celica+1)mult.rows[i].cells[j].style.visibility="hidden";
	    	// 		}
	    	// 	}
	    	// }else if(vrstica!=-1){
	    	// 	for(var i=1; i<=n; i++){
	    	// 		for(var j=1; j<=n; j++){
	    	// 			if(i!=vrstica+1)mult.rows[i].cells[j].style.visibility="hidden";
	    	// 		}
	    	// 	}
	    	// }
	    	//izpisemo se cikel
	    	if(gcd(celica, n)!=1){
	    		if(celica != -1 && vrstica != -1) messMult.rows[2].cells[0].innerHTML="Cikel ne obstaja, ker gcd stevil "+n+" in "+celica+" je "+gcd(celica, n);
	    		return;
			}
	    	if(celica>=0 && vrstica>=0){
				messMult.rows[2].cells[0].innerHTML=vrniCikel(celica, "<span>&#183;</span>");
			}
	    }
  	});

  	mult.addEventListener("mouseout", function(event){
  		if(cb==true  || multVisible==false)return;
  		messMult.rows[1].cells[0].innerHTML="Tukaj se izpiše operacija";
  		messMult.rows[2].cells[0].innerHTML="Tukaj se izpiše cikel";

  		for(var i=0; i<=n; i++){
  			mult.rows[0].cells[i].style.backgroundColor='white';
  			mult.rows[i].cells[0].style.backgroundColor='white';
  		}
  		for(var i=1; i<=n; i++){
  			for(var j=1; j<=n; j++){
  				mult.rows[i].cells[j].style.visibility="visible";
  			}
  		}
  	});

	function gcd(x, y) {
		x = Math.abs(x);
		y = Math.abs(y);
		while(y) {
			var t = y;
			y = x % y;
			x = t;
		}
		return x;
	}

	document.getElementById("cb").addEventListener('click', function(){
		if(!mult.rows[0] || multVisible==false)return;
		if(cb==true){
			for(i=0; i<=n; i++){
				for(j=0; j<=n; j++){
					mult.rows[i].cells[j].style.visibility="visible";
				}
			}
			cb=false;
			document.getElementById("cb").value="Odstrani elemente brez inverza!";
		}else{
			for(var i=1; i<=n; i++){
				if(gcd(i-1, n)!=1){
					//zbrisem vrstico in stolpec
					for(var j=0; j<=n; j++){
						mult.rows[i].cells[j].style.visibility="hidden";
						mult.rows[j].cells[i].style.visibility="hidden";
					}
				}
			}
			cb=true;
			document.getElementById("cb").value="Pritisni še enkrat, če hočeš nazaj interakcijo.";
		}
	});

	function inverzni(a){
		for(var i=1; i<n; i++){
			if((a*i)%n==1)return i;
		}
		return -1;
	}

	function vrniCikel(st, op){
		var original=st;
		var str="";
		do{
			str=str+st.toString(10)+" "+op+" "+original+" = ";
			if(op=="+")st=(st+original)%n;
			else st=(st*original)%n;
			str=str+st.toString(10)+" (mod "+n+")\n";
		}while(st!=original)
		return str;
	}
});	