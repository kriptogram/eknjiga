window.addEventListener('load', function () {

  document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("reset").click();
      }
  });

  // nastavi privzete vrednosti:
  if (getParamValue("modul") && getParamValue("a") && getParamValue("b")) {
    document.getElementById("modul").value = getParamValue("modul");
    document.getElementById("a").value = getParamValue("a");
    document.getElementById("b").value = getParamValue("b");
    if (getParamValue("tip") == 0) {
      document.getElementById("input4").checked = true;
    }
    if (getParamValue("tip") == 2) {
      document.getElementById("input6").checked = true;
    }
  }

  var id = -1;
  var tab = document.getElementById("tab");
  var n;
  var forma = document.getElementById("forma");

  forma.addEventListener('submit', function (event) {
    event.preventDefault();
    document.getElementById("napaka").innerHTML = "";
    document.getElementsByClassName("clock")[0].style.transform = "rotate(0deg)";
    document.getElementsByClassName("clock")[1].style.transform = "rotate(0deg)";

    //pogledamo zeljeno hitrost
    var hitrostList = document.getElementsByName("hitrost");
    var hitrost = -1;
    document.getElementById("napaka").innerHTML = "";
    for (var i = 0; i < hitrostList.length; i++) {
      if (hitrostList[i].checked) {
        hitrost = hitrostList[i].value;
        break;
      }
    }
    if (hitrost == "hitro") hitrost = 400;
    else if (hitrost == "pocasi") hitrost = 2000;
    else hitrost = 1000;

    //zelena operacija
    var opList = document.getElementsByName("operacija");
    var op = 1;
    for (var i = 0; i < opList.length; i++) {
      if (opList[i].checked) {
        op = i;
        break;
      }
    }

    switch (op) {
      case 0:
        document.getElementById("sestevanje-ura").style.display = "block";
        document.getElementById("mnozenje-ura").style.display = "none";
        document.getElementById("mnozenje-ura").classList.remove("col-lg-6");
        document.getElementById("sestevanje-ura").classList.remove("col-lg-6");
        document.getElementById("sestevanje-ura").classList.add("col-lg-12");
        document.getElementById("mnozenje-ura").classList.add("col-lg-12");
        document.getElementById("nastavitve").classList.remove("col-md-6");
        document.getElementById("nastavitve").classList.add("col-md-12");
        break;
      case 1:
        document.getElementById("sestevanje-ura").style.display = "block";
        document.getElementById("mnozenje-ura").style.display = "block";
        document.getElementById("mnozenje-ura").classList.remove("col-lg-12");
        document.getElementById("mnozenje-ura").classList.add("col-lg-6");
        document.getElementById("sestevanje-ura").classList.remove("col-lg-12");
        document.getElementById("sestevanje-ura").classList.add("col-lg-6");
        document.getElementById("nastavitve").classList.remove("col-md-12");
        document.getElementById("nastavitve").classList.add("col-md-6");
        break;
      case 2:
        document.getElementById("sestevanje-ura").style.display = "none";
        document.getElementById("mnozenje-ura").style.display = "block";
        document.getElementById("sestevanje-ura").classList.remove("col-lg-6");
        document.getElementById("sestevanje-ura").classList.add("col-lg-12");
        document.getElementById("mnozenje-ura").classList.remove("col-lg-6");
        document.getElementById("mnozenje-ura").classList.add("col-lg-12");
        document.getElementById("nastavitve").classList.remove("col-md-6");
        document.getElementById("nastavitve").classList.add("col-md-12");
        break;
    }


    document.getElementById("blokRez").style.visibility = "visible";
    if (id != -1) {
      clearInterval(id);
      id = -1;
      //return;
    }


    n = document.getElementById('modul').value;
    var a = document.getElementById('a').value;
    var b = document.getElementById('b').value;
    n = parseInt(n, 10);
    a = parseInt(a, 10);
    b = parseInt(b, 10);
    var kot = 360 / n;

    if (n > 20 || n < 4) {
      document.getElementById("blokRez").style.visibility = "hidden";
      document.getElementById("napaka").innerHTML = "Izberi modul, ki je manjši ali enak 20 in večji od 3.";
      return;
    }

    if (isNaN(a) || isNaN(b) || isNaN(n) || n == 1) {
      document.getElementById("blokRez").style.visibility = "hidden";
      document.getElementById("napaka").innerHTML = "Nepravilen vhod.";
      return;
    }

    //zbrisemo tabelo in vstvarimo novo 
    nastaviTabele(b);


    var stl = ".clock:before {\
          content: '';\
          position: absolute;\
          top: calc(50% - 3px);\
          left: 50%;\
          width: 40%;\
          height: 4px;\
          background: #262626;\
          border-radius: 3px;\
          transform: rotate("+ 270 + "deg);\
          transform-origin: left;\
        }";

    stl = stl + ".text {\
        position: absolute;\
        left: -100%;\
        width: 200%;\
        height: 200%;\
        text-align: center;\
        transform: skewY(60deg) rotate("+ kot / 2 + "deg);\
      }";

    stl += ".trail {\
      z-index: -1;\
    }";

    // pocistimo sledi
    var trail = document.getElementById("trail");
    trail.innerHTML = '';
    var trailMult = document.getElementById("trailMult");
    trailMult.innerHTML = '';

    //nastavim ul
    var items = document.getElementById("items");
    items.innerHTML = '';
    var itemsMult = document.getElementById("itemsMult");
    itemsMult.innerHTML = '';

    // nastavi enacbo
    document.getElementById("enacba").innerHTML = 0 + " = " + (Math.floor((0) / n)) + " * " + n + " + " + ((0) % n)
    document.getElementById("enacbaMult").innerHTML = 0 + " = " + (Math.floor((0) / n)) + " * " + n + " + " + ((0) % n)

    for (var i = 1; i <= n; i++) {
      var newDiv = document.createElement("div");
      newDiv.setAttribute("class", "text");
      newDiv.innerHTML = (i - 1).toString(10);
      var li = document.createElement("li");
      li.appendChild(newDiv);
      items.appendChild(li);
    }

    for (var i = 1; i <= n; i++) {
      newDiv = document.createElement("div");
      newDiv.setAttribute("class", "text");
      newDiv.innerHTML = (i - 1).toString(10);
      li = document.createElement("li");
      li.appendChild(newDiv);
      itemsMult.appendChild(li);
    }


    //nastavim zacetne kote
    var neki = 0;
    var nekiMult = 0;

    items = items.getElementsByTagName("li");
    itemsMult = itemsMult.getElementsByTagName("li");
    document.getElementsByClassName("clock")[0].style.transform = "rotate(" + neki + "deg)";
    for (var i = 0; i < n; i++) {
      var absolute = i * 360 / n;
      var trenutni = absolute - neki - (kot / 2);
      items[i].style.transform = "rotate(" + trenutni + "deg) skewY(-60deg)";
      itemsMult[i].style.transform = "rotate(" + trenutni + "deg) skewY(-60deg)";
    }

    var styleSheet = document.createElement("style");
    styleSheet.type = "text/css";
    styleSheet.innerText = stl;
    document.head.appendChild(styleSheet);

    if (isNaN(a) || isNaN(b)) return;

    neki = 0;
    var wait4it = 0;
    var waitMult = 0;
    var zamenjaj = false;

    var trenutnoSt = 0;
    var trenutnoStMult = 0;
    id = setInterval(function () {
      //prvo pogledamo sestevanje
      if (op == 0 || op == 1) {

        if (wait4it == a + b) {

        } else if (wait4it == a && cakaj == true) {
          cakaj = false;
        } else if (wait4it < a || wait4it < a + b) {
          neki += kot;
          trenutnoSt++;
          neki = neki % 360;
          document.getElementsByClassName("clock")[0].style.transform = "rotate(" + neki + "deg)";
          for (var i = 0; i < n; i++) {
            var absolute = i * 360 / n - (kot / 2);
            var trenutni = absolute - neki;
            items[i].style.transform = "rotate(" + trenutni + "deg) skewY(-60deg)";
          }
          if (wait4it < a) {
            povecaj(1, tab);
            povecaj(3, tab);
            wait4it++;
            cakaj = true;
          } else {
            povecaj(2, tab);
            povecaj(3, tab);
            wait4it++;
          }

          // ul za pobarvane celice
          document.getElementById("stObhodov").innerHTML = "Obhodi: " + (Math.floor((trenutnoSt) / n))
          document.getElementById("enacba").innerHTML = trenutnoSt + " = " + (Math.floor((trenutnoSt) / n)) + " * " + n + " + " + ((trenutnoSt) % n)
          var trail = document.getElementById("trail");
          trail.innerHTML = '';
          // trail
          for (var i = 0; i < trenutnoSt; i++) {
            var barvaSledi = ''
            if (i < Math.floor((trenutnoSt - 1) / n) * n) {
              // ne narisemo
              barvaSledi = "rgba(255, 255, 255, 0.0)";
            } else if (i < a) {
              barvaSledi = "rgba(102, 153, 255, 0.3)";
            } else {
              barvaSledi = "rgba(255, 102, 102, 0.3)";
            }
            var absolute = (i) * 360 / n;
            var trenutni = absolute - neki;
            var li = document.createElement("li");
            li.setAttribute("class", "trail");
            li.style['background-color'] = barvaSledi;
            li.style.transform = "rotate(" + trenutni + "deg) skewY(-" + (90 - (360 / n)) + "deg)";
            trail.appendChild(li);
          }
        }
      }

      //potem pogledamo se mnozenje
      if (op == 1 || op == 2) {

        if (waitMult == a * b && zamenjaj == false) {
          //do nothing
        } else if (waitMult % b == 0 && zamenjaj == true) {
          var temp = tabMult.rows[1].cells[1].innerHTML;
          temp = parseInt(temp, 10);
          temp++;
          tabMult.rows[1].cells[1].innerHTML = temp.toString(10);
          temp = temp % n;
          tabMult.rows[1].cells[2].innerHTML = temp.toString(10);
          tabMult.rows[3].cells[1].innerHTML = '0';
          tabMult.rows[3].cells[2].innerHTML = '0';
          //povecaj(1, tabMult); zkaj minkia ne rabe ta vrstica??
          zamenjaj = false;
        } else {
          povecaj(3, tabMult);
          povecaj(4, tabMult);
          waitMult++;

          //popravimo uro
          nekiMult += kot;
          trenutnoStMult++;
          nekiMult = nekiMult % 360;
          document.getElementsByClassName("clock")[1].style.transform = "rotate(" + nekiMult + "deg)";
          for (var i = 0; i < n; i++) {
            var absolute = i * 360 / n - (kot / 2);
            var trenutni = absolute - nekiMult;
            itemsMult[i].style.transform = "rotate(" + trenutni + "deg) skewY(-60deg)";
          }

          if (waitMult % b == 0) zamenjaj = true;

          // ul za pobarvane celice
          var trailMult = document.getElementById("trailMult");
          trailMult.innerHTML = '';
          // trailMult
          document.getElementById("stObhodovMult").innerHTML = "Obhodi: " + (Math.floor((trenutnoStMult) / n))
          document.getElementById("enacbaMult").innerHTML = trenutnoStMult + " = " + (Math.floor((trenutnoStMult) / n)) + " * " + n + " + " + ((trenutnoStMult) % n)
          for (var i = 0; i < trenutnoStMult; i++) {
            var barvaSledi = ''
            if (i < Math.floor((trenutnoStMult - 1) / n) * n) {
              // ne narisemo
              barvaSledi = "rgba(255, 255, 255, 0.0)";
            } else if (Math.floor(i / b) % 2 == 0) {
              barvaSledi = "rgba(102, 153, 255, 0.3)";
            } else {
              barvaSledi = "rgba(255, 102, 102, 0.3)";
            }
            var absolute = (i) * 360 / n;
            var trenutni = absolute - nekiMult;
            var li = document.createElement("li");
            li.setAttribute("class", "trail");
            li.style['background-color'] = barvaSledi;
            li.style.transform = "rotate(" + trenutni + "deg) skewY(-" + (90 - (360 / n)) + "deg)";
            trailMult.appendChild(li);
          }
        }
      }

      if (waitMult == a * b && wait4it == a + b && zamenjaj == false) clearInterval(id);
    }, hitrost);
    return;
  });

  function povecaj(kaj, tb) {
    var abs = tb.rows[kaj].cells[1].innerHTML;
    var mod = tb.rows[kaj].cells[2].innerHTML;
    abs = parseInt(abs, 10);
    mod = parseInt(mod, 10);
    abs++;
    mod++;
    mod = mod % n;
    tb.rows[kaj].cells[1].innerHTML = abs.toString(10);
    tb.rows[kaj].cells[2].innerHTML = mod.toString(10);
  }

  function nastaviTabele(b) {
    while (tab.rows[0]) tab.deleteRow(0);
    while (tabMult.rows[0]) tabMult.deleteRow(0);
    for (var i = 0; i <= 3; i++) {
      var row = tab.insertRow(i);
      var cell = row.insertCell(0);
      cell.style.width = '30px';
      cell = row.insertCell(1);
      cell.style.width = '30px';
      cell = row.insertCell(2);
      cell.style.width = '70px';
    }
    tab.rows[0].cells[1].innerHTML = 'Absolutna vrednost';
    tab.rows[0].cells[2].innerHTML = 'Vrednost po modulu';
    tab.rows[1].cells[0].innerHTML = 'a';
    tab.rows[1].cells[1].innerHTML = '0';
    tab.rows[1].cells[2].innerHTML = '0';
    tab.rows[2].cells[0].innerHTML = 'b';
    tab.rows[2].cells[1].innerHTML = '0';
    tab.rows[2].cells[2].innerHTML = '0';
    tab.rows[3].cells[0].innerHTML = 'vsota';
    tab.rows[3].cells[1].innerHTML = '0';
    tab.rows[3].cells[2].innerHTML = '0';

    for (var i = 0; i <= 4; i++) {
      row = tabMult.insertRow(i);
      cell = row.insertCell(0);
      cell.style.width = '100px';
      cell = row.insertCell(1);
      cell.style.width = '30px';
      cell = row.insertCell(2);
      cell.style.width = '70px';
    }
    tabMult.rows[0].cells[1].innerHTML = 'Absolutna vrednost';
    tabMult.rows[0].cells[2].innerHTML = 'Vrednost po modulu';
    tabMult.rows[1].cells[0].innerHTML = 'a';
    tabMult.rows[1].cells[1].innerHTML = '0';
    tabMult.rows[1].cells[2].innerHTML = '0';
    tabMult.rows[2].cells[0].innerHTML = 'b';
    tabMult.rows[3].cells[1].innerHTML = '0';
    tabMult.rows[3].cells[2].innerHTML = '0';
    tabMult.rows[3].cells[0].innerHTML = '<u>c</u>';
    tabMult.rows[2].cells[1].innerHTML = b;
    tabMult.rows[2].cells[2].innerHTML = b % n;
    tabMult.rows[4].cells[0].innerHTML = 'Trenutni zmnožek</br>a<span>&#183;</span>b + c';
    tabMult.rows[4].cells[1].innerHTML = '0';
    tabMult.rows[4].cells[2].innerHTML = '0';
  }

  document.getElementById("tabMult").addEventListener("mouseover", function (e) {
    var td = e.target;
    while (td !== this && !td.matches("td")) {
      td = td.parentNode;
    }
    if (td === this) {

    } else {
      if (td.parentNode.rowIndex == 3 && td.cellIndex == 0) {
        var div = document.createElement("div");
        div.style.width = "360px";
        div.style.height = "65px";
        div.style.borderRadius = "5px";
        div.style.background = "#11171F";
        div.style.fontSize = "13px";
        div.style.textAlign = "center";
        div.style.color = "white";
        div.style.position = "fixed";
        div.style.top = (event.clientY + 10) + "px";
        div.style.left = (event.clientX + 20) + "px";
        div.id = "kajJeC";
        div.innerHTML = "c je pomožna spremenljivka, ki smo uvedli, zato da bo spodnja enačba vedno držala, \
                  se pravi da je trenutni zmnožek enak vrednosti v tabeli. Predstavlja ostanek deljenja\
                   trenutnega zmnožka (a <span>&#183;</span> b + c ) s številom b.";

        document.body.appendChild(div);
      }
    }
  });

  document.getElementById("tabMult").addEventListener("mouseout", function () {
    if (document.getElementById("kajJeC")) document.getElementById("kajJeC").remove();
  })
});

function getParamValue(paramName) {
    var url = window.location.search.substring(1); //get rid of "?" in querystring
    var qArray = url.split('&'); //get key-value pairs
    for (var i = 0; i < qArray.length; i++) {
        var pArr = qArray[i].split('='); //split key and value
        if (pArr[0] == paramName) 
            return pArr[1]; //return value
    }
}