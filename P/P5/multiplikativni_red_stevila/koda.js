window.addEventListener('load', function () {
   function gcd(a, b) {
      if (b == 0) return a;
      return gcd(b, a % b);
   }

   var a, n, k, i;
   var reset = document.getElementById("reset");
   var ns = document.getElementById("ns");
   var table = document.getElementById('table');
   var mss = document.getElementById('message');
   var prvi;
   // var list=document.getElementById('list');
   var stevec = -1;
   var stev1;
   var red = 0;
   var redSt;

   reset.addEventListener('click', resetiraj);

   document.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
          event.preventDefault();
          document.getElementById("ns").click();
        }
  });

   a = document.getElementById("a");
   n = document.getElementById("n");
   a.addEventListener('input', resetiraj);
   n.addEventListener('input', resetiraj);

   //stackoverflow.com/questions/29006214/how-to-divide-a-circle-into-12-equal-parts-with-color-using-css3-javascript
   ns.addEventListener('click', function (e) {
      if (i > 20) {
         if (table.rows[1].bgColor != 'green') {
            i--;
            table.deleteRow(1);
         } else {
            i--;
            table.deleteRow(2);
         }
      }
      if (stevec == -1 || stevec == red) stevec = 0;
      var neki = document.querySelectorAll('.text');
      for (var q = 0; q < neki.length && redSt != 1 && redSt != 2; q++) {
         neki[q].style.background = 'tomato';
         if (q == stevec) neki[q].style.background = 'green';
      }
      stevec++;
      if (i > 0) {
         var row = table.insertRow(i);
         var c = row.insertCell(0);
         c.innerHTML = stev1;
         c = row.insertCell(1);
         c.innerHTML = k + " &#215; " + a + " mod " + n;
         c = row.insertCell(2);
         c.innerHTML = k * a + " mod " + n + " = " + (k * a) % n;
         k = (k * a) % n;
         zaporedje.push(k);
         drawClock();
         if (k != 1) row.bgColor = '#f8f9fa';
         else {
            if (prvi == true) {
               prvi = false;
               document.getElementById("computedMessage").innerText = "Multiplikativni red števila je " + i;
               // c=row.insertCell(3);
               // c.innerHTML="Dobili smo multiplikativni red števila!";
               // c.bgColor="white";
               row.bgColor = '#28a745';
            } else {
               row.bgColor = 'silver';
            }
         }
         // row.scrollIntoView();
         i++;
         stev1++;
      }
   });

   resetiraj(null);

   function resetiraj(e) {
      zaporedje = [1];
      drawClock();
      // list.innerHTML="";
      for (var t = 0; t < i; t++)table.deleteRow(0);
      i = 0;
      mss.innerHTML = "";
      a = parseInt(document.getElementById("a").value, 10);
      n = parseInt(document.getElementById("n").value, 10);
      if (!a || a <= 0) {
         return;
      }
      if (!n || n <= 0) {
         return;
      }
      if (a >= n) {
         return;
      }

      var g = gcd(a, n);
      if (g != 1) {
         mss.innerHTML = "Najvecji skupni deljitelj stevil je " + g + ". Potreben pogoj, pa da obstaja multiplikativni red stevila je da sta si stevili tuji, to je da je nejcecji skupni deljitelj 1";
         i = -1;
         stev1 = -1;
         return;
      } else {
         var row = table.insertRow(0);
         var c = row.insertCell(0);
         // c.style.width='100px';
         c.innerHTML = "Iteracija";
         c = row.insertCell(1);
         // c.style.width='300px';
         c.innerHTML = "Prešnji_ostanek &#215; a mod n";
         c = row.insertCell(2);
         // c.style.width='300px';
         c.innerHTML = 'Produkt';
         i++;
         stev1++;
         k = 1;
         i = 1;
         stev1 = 1;
         prvi = true;
      }
   }
})

let centerX = 300;
let centerY = 300;
let zacetniRadij = 240;
let radij = 240;

let zaporedje = [1];

function drawJump(k, k2, vnos, bold, color) {
   let nElement = document.getElementById("n");
   let clockCanvas = document.getElementById('clockCanvas');
   var ctx = clockCanvas.getContext("2d");
   let n = nElement.value;
   if (n) {
      let kot = 2 * Math.PI / n;
      let kotZac = kot * k - Math.PI / 2;
      let kotKon = kot * k2 - Math.PI / 2;
      ctx.beginPath();
      if (bold) {
         ctx.lineWidth = 3;
      } else {
         ctx.lineWidth = 2;
      }
      ctx.strokeStyle = color;
      puscica_lok(ctx, centerX, centerY, kotZac, kotKon, radij * (1 + (vnos) / 30), radij * (1 + (vnos + 1) / 30));
      ctx.stroke();
      ctx.closePath();
   }
}



function drawClock() {
   radij = zacetniRadij * 1 / (1 + zaporedje.length / 30);
   let nElement = document.getElementById("n");
   let velikostBesedila = 20;
   let clockCanvas = document.getElementById('clockCanvas');
   var ctx = clockCanvas.getContext("2d");
   ctx.lineWidth = 1;
   ctx.clearRect(0, 0, clockCanvas.width, clockCanvas.height);
   ctx.beginPath();
   ctx.strokeStyle = "#000000";
   ctx.arc(centerX, centerY, radij, 0, 2 * Math.PI);
   ctx.stroke();
   ctx.closePath();
   // draw numbers
   ctx.beginPath();
   let n = nElement.value;
   if (n) {
      let kot = 2 * Math.PI / n;
      for (let i = 0; i < n; i++) {
         let xCrke = centerX + (radij * 0.9) * Math.cos(kot * i - Math.PI / 2);
         let yCrke = centerY + (radij * 0.9) * Math.sin(kot * i - Math.PI / 2);
         ctx.fillStyle = "#000000";
         ctx.font = velikostBesedila + "px Arial";
         ctx.fillText("" + i, xCrke - 0.33 * velikostBesedila, yCrke + 0.33 * velikostBesedila);
      }
   }
   ctx.closePath();


   // ali smo ze nasli red
   red = -1;
   for (let i = 1; i < zaporedje.length; i++) {
      if (zaporedje[i] === 1) {
         red = i;
         break;
      }
   }

   for (let i = 1; i < zaporedje.length; i++) {
      if (red === -1) {
         drawJump(zaporedje[i - 1], zaporedje[i], i, i === zaporedje.length - 1, "#000000");
      } else {
         drawJump(zaporedje[i - 1], zaporedje[i], i, i === zaporedje.length - 1, (i <= red) ? "#28a745" : "#6c757d");
      }
   }

   // kazalec
   let kot = 2 * Math.PI / n;
   let xKazalca = centerX + (radij * 0.8) * Math.cos(kot * zaporedje[zaporedje.length - 1] - Math.PI / 2);
   let yKazalca = centerY + (radij * 0.8) * Math.sin(kot * zaporedje[zaporedje.length - 1] - Math.PI / 2);
   ctx.lineWidth = 3;
   ctx.beginPath();
   ctx.strokeStyle = "#000000";
   ctx.moveTo(centerX, centerY);
   ctx.lineTo(xKazalca, yKazalca);
   ctx.stroke();
   ctx.closePath();
   ctx.lineWidth = 1;
}

function puscica_lok(ctx, x, y, kot1, kot2, r1, r2) {
   while (kot1 < 0) {
      kot1 += 2 * Math.PI;
   }
   while (kot2 < kot1) {
      kot2 += 2 * Math.PI;
   }
   let stKorakov = Math.max(20, 40 * (kot2 - kot1) / Math.PI);
   xs = [];
   ys = [];
   for (let i = 0; i <= stKorakov; i++) {
      let trenutniKot = kot1 + i * (kot2 - kot1) / stKorakov;
      let trenutniRadij = r1 + i * (r2 - r1) / stKorakov;
      let xTocke = x + trenutniRadij * Math.cos(trenutniKot);
      let yTocke = y + trenutniRadij * Math.sin(trenutniKot);
      xs.push(xTocke);
      ys.push(yTocke);
   }
   lomljenaPuscica(ctx, xs, ys);
}

function canvas_arrow(ctx, fromx, fromy, tox, toy) {
   var headlen = 10; // length of head in pixels
   var dx = tox - fromx;
   var dy = toy - fromy;
   var angle = Math.atan2(dy, dx);
   ctx.moveTo(fromx, fromy);
   ctx.lineTo(tox, toy);
   ctx.lineTo(tox - headlen * Math.cos(angle - Math.PI / 6), toy - headlen * Math.sin(angle - Math.PI / 6));
   ctx.moveTo(tox, toy);
   ctx.lineTo(tox - headlen * Math.cos(angle + Math.PI / 6), toy - headlen * Math.sin(angle + Math.PI / 6));
}

function lomljenaPuscica(ctx, xs, ys) {
   for (let i = 1; i < xs.length - 1; i++) {
      ctx.moveTo(xs[i - 1], ys[i - 1]);
      ctx.lineTo(xs[i], ys[i]);
   }
   canvas_arrow(ctx, xs[xs.length - 2], ys[ys.length - 2], xs[xs.length - 1], ys[ys.length - 1]);
}