window.addEventListener('load', function() {

  var next_step=document.getElementById("next_step");
  var collapse=document.getElementById("collapse");
  var helpSum=document.getElementById("helpSum");
  var tabOut=document.getElementById("myTable");  
  var tab; //tabela, kjer so shrajene vse vrednosti
  var stVidnih=0; //koliko vrstic je trenutno vidnih v tabeli
  var stVidnihSum=0; //v zadnji vrstici dodajamo eno stevko po drugi. Ta parameter nam pove, koliko jih je vidnih
  var prvo=document.getElementById("prvo_st").value;
  var drugo=document.getElementById("drugo_st").value;
  var baza; //baza ki uporabljamo
  var lenPrvo; //dolzina prvega stevila
  var lenDrugo; //dolzina drugega stevila
  var kolikoKoll=0; //koliko zdruzevanj je bilo narejenih
  var stCollapse=0; //koliko vrstic moramo sesteti pri naslednjemu zzdruzevanju
  var veljavenVhod=false;
  var oflow;//za print_help, drzi koliko je trenuten overflow
  
  document.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
          event.preventDefault();
          document.getElementById("next_step").click();
        }
  });

  collapse.addEventListener('click', function(event) {
    if(veljavenVhod==false)return;
	  if(stVidnih<3 || stCollapse==0)return;
    kolikoKoll++;
    var vrstica = new Array(lenDrugo+lenPrvo+2);
    var overflow=0;
    for(var i=tab[0].length-1; i>=0; i--){
      var sum=-1;
      for(var j=1; j<stCollapse; j++){
        if(tab[j][i]==-1) continue;
        if(sum==-1)sum=0;
        sum += parseInt(tab[j][i], baza);
      }
      if(sum==-1){
        vrstica[i]=' ';
        continue;
      }
      sum=sum+overflow;
      overflow=Math.floor(sum/baza);
      sum=sum%baza;
      if(sum==-1)vrstica[i]=' ';
      else vrstica[i]=sum.toString(baza);
    }
    if(overflow)vrstica[0]=overflow.toString(baza);
    
    tabOut.deleteRow(1);
    tabOut.deleteRow(1);
    var row=tabOut.insertRow(1);
    for(i=0; i<tab[0].length; i++){
      var cell=row.insertCell(i);
      if(i==0)cell.innerHTML=vrstica[i];
      else cell.innerHTML=vrstica[i];
    }
    stCollapse++;
    stVidnih--; 
    if(stVidnih<3)collapse.disabled=true;

    //popravimo po potrebi prenos
    if(oflow==0)return;
    oflow=0;
    for(var i=1; i<stVidnih; i++){
      var temp=tabOut.rows[i].cells[stVidnihSum+1].innerHTML;
      if(temp==" ")continue;
      oflow+=parseInt(temp, baza);
    }
    oflow=Math.floor(oflow/baza);
  });

  document.getElementById("reset").addEventListener("submit", function(event){
    
    //housekeeping
    oflow=0;
    collapse.disabled=true;
    next_step.disabled=false;
    document.getElementsByClassName("racunaj")[1].style.visibility="hidden";
    event.preventDefault();
    helpSum.innerHTML="";
    tabOut.parentNode.insertBefore(no_overline, tabOut);
    tabOut.parentNode.insertBefore(underline, tabOut);
    kolikoKoll=0;
    next_step.style.visibility="visible";
    collapse.style.visibility="visible";
    while(tabOut.rows[0])tabOut.deleteRow(0);

    //podatki
    prvo=document.getElementById("prvo_st").value;
    drugo=document.getElementById("drugo_st").value;
    while(prvo.charAt(0)=="0")prvo=prvo.substring(1);
    while(drugo.charAt(0)=="0")drugo=drugo.substring(1);
    lenPrvo=prvo.length;
    lenDrugo=drugo.length;
    stVidnihSum=lenDrugo+lenPrvo-1;
    
    var error=document.getElementById("error");
    baza=document.getElementById("rs-range-line").value;
    
    //preverimo pravilnost vhoda
    if(pravilenVhod(prvo, baza)==true && pravilenVhod(drugo, baza)==true){
      //pravilen vhod
      error.innerHTML="";
      next_step.style.visibility="visible";
      collapse.style.visibility="visible";
    }else{
      error.innerHTML="Nepravilen vhod!";
      veljavenVhod=false;
      next_step.style.visibility="hidden";
      collapse.style.visibility="hidden";
      document.getElementsByClassName("racunaj")[0].style.visibility="hidden";
      return;
    }
    document.getElementsByClassName("racunaj")[0].style.visibility="visible";
    veljavenVhod=true;
    
    prvo=parseInt(prvo, baza);
    drugo=parseInt(drugo, baza);
    
    stVidnih=0;
    stCollapse=0;
    
    tab=null;
    tab=new Array(lenDrugo+2);
    for(i=0; i<100; i++)tab[i]=new Array(lenDrugo+lenPrvo+2);
    
    for(i=0; i<tab.length; i++){
      for(var j=0; j<tab[0].length; j++)tab[i][j]=-1;
    }
    
    for(i=0; i<lenPrvo; i++)tab[0][i+1]=prvo.toString(baza).charAt(i);
    tab[0][lenPrvo+1]='<span>&#183;</span>';
    for(i=0; i<drugo.toString(baza).length; i++)tab[0][i+lenPrvo+2]=drugo.toString(baza).charAt(i);
    
    for(i=0; i<lenDrugo; i++)
      reqMnozi(tab, i, baza, lenPrvo);
    setVisibleRow(tab, 0, tabOut, 0);
    sum(tab, baza, lenDrugo+lenPrvo-1, lenDrugo+1);
    stVidnih=1;
  });
  
  function printHelpSum(num){
    var sporocilo="prenos = " + oflow+"</br>";
    sporocilo=sporocilo+" ("+ oflow +") ";
    var sum=oflow;
    for(var i=1; i<stVidnih; i++){
      var data=tabOut.rows[i].cells[num].innerHTML;
      if(data.charAt(0)=='+')data=data.substr(1);
      if(data==' ')continue;
      sum=sum+parseInt(data, baza);
      if(sporocilo.length==0 && data!=' ')sporocilo=sporocilo + data;
      else if(data!=' '){
        sporocilo=sporocilo+' + '+data;
        if(isNaN(parseInt(data, 10)))sporocilo=sporocilo+" (="+parseInt(data, 16)+") ";
      }
    }
    sporocilo=sporocilo+" = " + sum+"</br>";
    sporocilo=sporocilo+sum+" % "+baza+" = " + (sum%baza)+"</br>";
    oflow=Math.floor(sum/baza);
    sporocilo=sporocilo+sum+" / "+baza+" = " + (oflow)+"</br>";
    if(sum!=0 || num!=0)helpSum.innerHTML=sporocilo;
  }
  
  function sum(tab, modul, first, visina){
    var add=0;
    for(var i=first; i>=0; i--){
      tab[0][i]=add;
      var colSum=add;
      for(var j=1; j<visina; j++){
        var data=parseInt(tab[j][i], modul);
        if(data!=-1)colSum+=data;
      }
      tab[visina][i]=(colSum%modul).toString(modul);
      add=Math.floor(colSum/modul);
    }
  }
  
  next_step.addEventListener("click", function(event){
    if(veljavenVhod==false)return;
  	var stSum=lenDrugo+1;
    var overflow=tab[0][stVidnihSum];
    overflow=parseInt(overflow, 10);
    if(stVidnih+kolikoKoll<=lenDrugo){ //nismo se izpisali vseh vrstic
      var nRow=stVidnih+kolikoKoll;
      setVisibleRow(tab, nRow, tabOut, stVidnih);
      stVidnih++;
      if(stVidnih>=3)collapse.disabled=false;
      tabOut.rows[0].cells[lenPrvo+nRow].style.color="black";
      tabOut.rows[0].cells[lenPrvo+nRow+1].style.color="red";
      if(stVidnih==3 && kolikoKoll==0)stCollapse=3;
    }else if(stVidnihSum==lenDrugo+lenPrvo-1){ 
      //smo izpisali vse vrstice razen zadnjo (kumulatino). Nastavimo zacetnne vrednositi te vrstice na ' '.
      tabOut.rows[0].cells[lenPrvo+stVidnih+kolikoKoll].style.color="black";
      tabOut.rows[stVidnih-1].style.color="black";
      var vrstica=tabOut.insertRow(stVidnih);
      for(var i=0; i<lenDrugo+lenPrvo; i++){
        var cell=vrstica.insertCell(i);
        cell.innerHTML=" ";
        cell.style.textAlign="right";
      }
      printHelpSum(stVidnihSum);
      document.getElementsByClassName("racunaj")[1].style.visibility="visible";
      tabOut.parentNode.insertBefore(overline, tabOut);
      tabOut.rows[stVidnih].cells[stVidnihSum].innerHTML=tab[stSum][stVidnihSum];
      setColor(stVidnihSum, "red");
      stVidnihSum--;
    }else if(stVidnihSum>0 || (stVidnihSum==0 && (overflow>0 || tabOut.rows[1].cells[0].innerHTML!=" "))){ 
      //zaporedno dodajamo rezultat v celice zadnje vrstice
      printHelpSum(stVidnihSum);
      if(stVidnihSum==0 && tab[stSum][stVidnihSum]==0){
        setColor(stVidnihSum+1, "black");
      }else{
        tabOut.rows[stVidnih].cells[stVidnihSum].innerHTML=tab[stSum][stVidnihSum];
        setColor(stVidnihSum, "red");
        setColor(stVidnihSum+1, "black");
      }
      stVidnihSum--;
    }else{
      //end of job. Do nothing
      document.getElementsByClassName("racunaj")[1].style.visibility="hidden";
      next_step.disabled=true;
      setColor(1, "black");
      setColor(0, "black")
      helpSum.innerHTML="";
    }
  });
  
  function setColor(st, col){
    for(var i=1; i<tabOut.rows.length; i++){
      tabOut.rows[i].cells[st].style.color=col;
    }
  }
  
  function reqMnozi(tab, n, modul, lenNum1){
    var add=0;
    var coeff=parseInt(tab[0][lenNum1+n+2], modul);
    var ind=lenNum1+n;
    for(var i=lenNum1; i>0; i--, ind--){
      var c=coeff*parseInt(tab[0][i], modul)+add;
      var vr=c%modul;
      tab[n+1][ind]=vr.toString(modul);
      add=Math.floor(c/modul);
    }
    if(add)tab[n+1][ind]=add.toString(modul);
  }
    
  function setVisibleRow(tab, nRow, tabOut, nInTab){
    var row=tabOut.insertRow(nInTab);
    for(var i=0; i<tab[0].length; i++){
      var cell=row.insertCell(i);
      var data=tab[nRow][i];
      cell.style.width="12px";
      if(data!=-1)cell.innerHTML=data;
      else cell.innerHTML=" ";
      if(i==0 && nRow!=0)cell.innerHTML=cell.innerHTML;
      else if(i==0)cell.innerHTML=' '+cell.innerHTML;
    }
    row.style.color="red";
    if(nRow==0)row.style.color="black";
    else tabOut.rows[stVidnih-1].style.color="black";
  }
  
  function stevila(n){
	  var rez="0";
	  for(var i=1; i<10 && i<n; i++){
		  rez=rez+", " + i;
	  }
	  if(n<10)return rez+".";
	  if(n>=11)rez=rez + ", a";
	  if(n>=12)rez=rez + ", b";
	  if(n>=13)rez=rez + ", c";
	  if(n>=14)rez=rez + ", d";
	  if(n>=15)rez=rez + ", e";
	  if(n>=16)rez=rez + ", f";
	  return rez+".";
  }
  
  function pravilenVhod(st, b){
    for(var i=0; i<st.length; i++){
      if(isNaN(parseInt(st.charAt(i), baza)) && parseInt(st.charAt(i), baza)!=0)return false;
    }
    return true;
  }
  
  var no_overline = document.createElement('style');
  no_overline.innerHTML='table tr:last-child td {border-top: 0; }';  
  
  var overline = document.createElement('style');
  overline.innerHTML='table tr:last-child td {border-top: 1px solid black; }';
  
  var underline=document.createElement('style');
  underline.innerHTML='table tr:first-child td {border-top: 0; }';

  
  var rangeSlider = document.getElementById("rs-range-line");
  var rangeBullet = document.getElementById("rs-bullet");
  rangeSlider.addEventListener("input", showSliderValue, false);

  function showSliderValue() {
    rangeBullet.innerHTML = rangeSlider.value;
    var bulletPosition = ((rangeSlider.value-2) /rangeSlider.max);
    rangeBullet.style.left = (bulletPosition * 281) + "px";
  }

    //izpis modula
  document.getElementById("modul").addEventListener("mouseover", function(e){
    if(document.getElementById("tempModul"))return;
    var div = document.createElement("div");
    div.style.width = "300px";
    div.style.height = "70px";
    div.style.borderRadius="5px";
    div.style.background = "#11171F";
    div.style.fontSize="13px";
    div.style.textAlign="center";
    div.style.color = "white";
    div.style.position="fixed";
    div.style.top=(e.clientY+10)+"px";
    div.style.left=(e.clientX+20)+"px";
    div.id="tempModul";
    var m=document.getElementById("rs-range-line").value;
    m=parseInt(m, 10);
    var sporocilo="Modul določa, katere števke lahko uporabljaš. Trenutno kaže modul na \
      "+m+", torej lahko uporabljaš naslednje števke:</br>"+stevila(m);
    div.innerHTML = sporocilo;

    document.body.appendChild(div);
  });

  document.getElementById("modul").addEventListener("mouseout", function(e){
    if(document.getElementById("tempModul"))document.getElementById("tempModul").remove();
  });

  function stevila(n){
    var rez="0";
    for(var i=1; i<10 && i<n; i++){
      rez=rez+", " + i;
    }
    if(n<10)return rez+".";
    if(n>=11)rez=rez + ", a";
    if(n>=12)rez=rez + ", b";
    if(n>=13)rez=rez + ", c";
    if(n>=14)rez=rez + ", d";
    if(n>=15)rez=rez + ", e";
    if(n>=16)rez=rez + ", f";
    return rez+".";
  }
});