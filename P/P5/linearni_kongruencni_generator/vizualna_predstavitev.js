let tocke = [];
var a_testiranje,c_testiranje,m_testiranje,x0_testiranje,max_testiranje, st_vzorcev;

let robGrafaVodoravno = 20;
let robGrafaNavpicno = 50;


var rtime;
var timeout = false;
var delta = 200;
window.onresize = spremembaVelikosti;
function spremembaVelikosti() {
  rtime = new Date();
    if (timeout === false) {
        timeout = true;
        setTimeout(konecSpreminjanja, delta);
    }
}
function konecSpreminjanja() {
  if (new Date() - rtime < delta) {
      setTimeout(konecSpreminjanja, delta);
  } else {
      timeout = false;
      testiraj();
  }               
}

function testiraj() {
  a_testiranje = Number(document.getElementById("input-a-testiranje").value);
  c_testiranje = Number(document.getElementById("input-c-testiranje").value);
  m_testiranje = Number(document.getElementById("input-m-testiranje").value);
  x0_testiranje = Number(document.getElementById("input-x-testiranje").value);
  max_testiranje = Number(document.getElementById("input-max-testiranje").value);
  st_vzorcev = Number(document.getElementById("input-stevilo-vzorcev").value);
  trenutno = x0_testiranje;
  if (max_testiranje > m_testiranje) {
    document.getElementById("opozorilo").innerText = "Največja generirana vrednost ne sme biti večja od modula."
    document.getElementById("grafi").style.display = "none";
  } else {  
    document.getElementById("opozorilo").innerText = "";
    document.getElementById("grafi").style.display = "block";
    narisiGraf();
    narisiHistogram();
  }
}

let trenutno;
function generirajStevilo() {
  let vrednost = trenutno;
  trenutno = (a_testiranje * trenutno + c_testiranje) % m_testiranje;
  return vrednost % max_testiranje;
}

function resetirajGenerator() {
  trenutno = x0_testiranje;
}

function narisiHistogram() {
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(generirajHistogram);
}

// dokumentacija: https://developers.google.com/chart/interactive/docs/gallery/histogram
function generirajHistogram() {
  resetirajGenerator();
  // generiraj podatke
  let podatki = [['Vrednost']];
  for (let i = 0; i < st_vzorcev; i++) {
    podatki.push([generirajStevilo()]);
  }

  var data = google.visualization.arrayToDataTable(podatki);

  var options = {
    title: 'Generirane vrednosti',
    legend: { position: 'none' },
    histogram: { maxNumBuckets: Math.min(max_testiranje, 10) }
  };

  var chart = new google.visualization.Histogram(document.getElementById('histogram'));
  chart.draw(data, options);
}

function narisiGraf() {
  generirajTocke();
  var c = document.getElementById("graf-canvas");
  var ctx = c.getContext("2d");
  // pocisti risalno povrsino
  ctx.fillStyle = "#ffffff";
  ctx.fillRect(0,0,c.width, c.height);
  narisiKoordinatniSistem(ctx, c.width - robGrafaVodoravno*2, c.height - robGrafaNavpicno*2);
  narisiTocke(ctx, c.width - robGrafaVodoravno*2, c.height - robGrafaNavpicno*2);
}

function generirajTocke() {
  tocke = [];
  for (let i = 0; i < st_vzorcev; i++) {
    let x = generirajStevilo();
    let y = generirajStevilo();
    tocke.push([x, y]);
  }
}

function koordinateVPixle(sirina, visina, x, y) {
  let velikost = max_testiranje - 1;
  return [robGrafaVodoravno + sirina * x / velikost, robGrafaNavpicno + visina * (velikost - y) / velikost];
}

function narisiTocke(ctx, sirina, visina) {
  ctx.fillStyle = "#000000";
  for (let i = 0; i < tocke.length; i++) {
    ctx.beginPath();
    let tocka = koordinateVPixle(sirina, visina, tocke[i][0], tocke[i][1]);
    ctx.arc(tocka[0], tocka[1], 3, 0, 2 * Math.PI);
    ctx.fill();
    ctx.closePath();
  }
}

function narisiKoordinatniSistem(ctx, sirina, visina) {
  let velikost = max_testiranje - 1;
  let korak = Math.max(Math.floor(velikost / 5), 1);
  ctx.beginPath();
  ctx.strokeStyle = "#d3d3d3";
  // navpicne crtes
  for (let i = 0; i <= velikost; i += korak) {
    let zacetek = koordinateVPixle(sirina, visina, i, 0);
    let konec = koordinateVPixle(sirina, visina, i, velikost);
    ctx.moveTo(zacetek[0], zacetek[1]);
    ctx.lineTo(konec[0], konec[1]);
    ctx.font = "16px Arial";
    ctx.fillStyle = "#a3a3a3";
    ctx.fillText("" + i, zacetek[0] + 3, zacetek[1]-3);
  }

  // vodoravne crte
  for (let i = 0; i <= velikost; i += korak) {
    let zacetek = koordinateVPixle(sirina, visina, 0, i);
    let konec = koordinateVPixle(sirina, visina, velikost, i);
    ctx.moveTo(zacetek[0], zacetek[1]);
    ctx.lineTo(konec[0], konec[1]);
    ctx.font = "16px Arial";
    ctx.fillStyle = "#a3a3a3";
    ctx.fillText("" + i, zacetek[0] + 3, zacetek[1]-3);
  }
  ctx.stroke();
  ctx.closePath();
}