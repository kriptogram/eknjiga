---
title: "Racunala - pregled"
author: "Klemen Klanjscek"
date: "5 april 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
\newcommand{\R}{\mathbb{R}}
\newcommand{\P}{\mathbb{P}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\gf}{\text{GF}}


## Uvod v artmetiko nove dobe
### Iniciativa

* motivacija (digitalni računalniki)

### Množice števil in aritmetične operacije
* Neskončne množice
    + naravna števila: $\N$, praštevila $\P$
    + cela števila: $\Z$, $n\Z$
    + racionalna $\Q$, realna $\R$ in kompleksna $\C$ števila (v danem kontekstu manj zanimiva)

* Končne množice
    + glej grupe


* Predstavitev in zapis števil
    + eniški, dvojiški, osmiški, šestnajstiški zapis [interaktivno: paralelna primerjava]
    + rimska števila [interaktivno: paralelna primerjava]
    + ekonomija baze [interaktivno: paralelna primerjava]
 
* Operacije
    + seštevanje +, odštevanje -
    + množenje *, deljenje /
    + lastnosti seštevanja in množenja nad  $\N$ (zakoni, izreki, omejitve)
    + cela števila in lastnosti deljivosti (izreki) [interaktivno: avtomatsko preverjanje deljivosti]
    + lastnosti aritmetičnih operacij nad $\Q$ (zakoni, izreki, omejitve)
  

* Algoritmi za navadno množenje in seštevanje
    + šolsko množenje (dolgo množenje) [interaktivno: paralelna primerjava]
    + Ruska metoda  [interaktivno: paralelna primerjava]
    + Kitajska metoda [interaktivno: paralelna primerjava]
    + (Karatsubov algoritem)
    + vpliv različnih baz [interaktivno]
    + tabele (kako lahko izkoristimo lastnosti operacij) [interaktivno: izpolnjevanje s pomočjo lastnosti]

## Grupe in obsegi
### Grupe
* Lastnosti
    + Abelova grupa
    + grupe nad že znanimi množicami z že znanimi operacijami $(\Q, +)$, $(\Z, +)$, $(\Q_{*}=\Q-\{0\}, *)$, $(\Z_{*}=\Z-\{0\}, *)$


* Uvod v končne grupe
    + računanje z ostanki
    + princip računanja z analogno uro [interaktivno]
    + Cayleyeve table [interaktivno: podobno kot prej]
    + najmanjše grupe
    + računaje s lihimi in sodimi števili ($\Z_{2}$)
    + računaje do 10 ($\Z_{11}$)
  	+ računaje do 100 ($\Z_{101}$)
    + red elementa, generatorji [interaktivno]
    + pripadajoci algoritmi [interaktivno]


* Grupe v različnih preoblekah (izomorfizem grup)
  	+ ciklične grupe - $C_n$
  	+ grupa simetrij (zrcaljenja in rotacije) pravilnih $n$-kotnikov - $D_n$ [interaktivno: grafična zrcaljenja in rotacije]
		

		
* Število grup z $n$ elementi, zaporedje [A000001](http://oeis.org/A000001)

* Aritmetična logična enota (ALE) v procesorjih [interaktivno: samo osnovne operacije]
    + preliv pri množenju in seštevanju
  	+ 8 bitni registri ($\Z_{256}$)
  	+ 64 bitni registri ($\Z_{2^{64}}$)
  	+ odštevanje s seštevanjem - dvojiški komplement
  	+ deljenje ($\lfloor a / b \rfloor$) brez deljenja (z uporabo logičnih operatorjev in tabel)


* deljenje v multiplikativnih grupah
  	+ diofantska enačba
  	+ razširjen Evklidov algoritem [interaktivno: po korakih]

* (Direkten poduk in direktna vsota grup)
	
### Obsegi
* lastnosti (polkolobar, kolobar, komutativni obseg - polje $(\Q, +, *)$, $(\R, +, *)$
* končni obsegi $\gf(p)$ $\gf(p^n)$ [interaktivno: kalkulator s postopkom]
* končni obsegi in polinomi $\gf(p^n)$ [interaktivno: kalkulator s postopkom]
* afina šifra
		

## Eliptične krivulje
* definicija
* singularnost
* konstrukcija grupe nad točkami eliptične krivulje poljubnega obsega karakteristike različne od 2 in 3 [interaktivno: zvezen diskreten primer]

## Viri

1. [Modularna aritmetika](https://en.wikipedia.org/wiki/Modular_arithmetic) 
2. [Elipticne krivulje](https://en.wikipedia.org/wiki/Elliptic_curve)
3. [Grupe](https://en.wikipedia.org/wiki/Group_(mathematics))
4. [Koncni obsegi](https://en.wikipedia.org/wiki/Finite_field)
5. [Presek - Računala nove dobe 1](http://lkrv.fri.uni-lj.si/popularizacija/presek/racunala.nove.dobe.1.pdf)
6. [Presek - Računala nove dobe 2](http://lkrv.fri.uni-lj.si/popularizacija/presek/racunala.nove.dobe.2.pdf)
7. [Teta kriptografija v kratkih hlacah](https://www.dropbox.com/s/foasurrp2bzdbm5/k1.pdf)
8. [Ekonomija baze](https://en.wikipedia.org/wiki/Radix_economy)
9. [Kitajska metoda](https://en.wikipedia.org/wiki/Lattice_multiplication)
10. [Ruska metoda](https://en.wikipedia.org/wiki/Ancient_Egyptian_multiplication#Russian%20peasant%20multiplication)
11. [Cayleyeve tabele](https://en.wikipedia.org/wiki/Cayley_table)
12. [ECC]