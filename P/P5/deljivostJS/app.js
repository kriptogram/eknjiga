
function div1(N) {
  return 'JE. ' + N + ' = 1 × ' + N
}

function div2(N) {
  lastd = N % 10

  if (N % 2 == 0) {
    return 'JE. ' + lastd + ' je sodo.'
  }
  return 'NI. ' + lastd + ' je liho.'
}


function div3(N) {
  strnum = N.toString().split('')
  sum = strnum.reduce((a, b) => a + parseInt(b), 0)
  strsum = strnum.join(' + ')

  if (N % 3 == 0) {
    return 'JE. ' + strsum + ' = ' + sum + ' = 3 × ' + Math.floor(sum / 3)
  }
  return 'NI. ' + strsum + ' = ' + sum + ' = 3 × ' + Math.floor(sum / 3) + ' + ' + sum % 3
}

function div4(N) {
  lasttwo = N % 100

  if (N % 4 == 0) {
    return 'JE. ' + lasttwo + ' = 4 × ' + Math.floor(lasttwo / 4)
  }
  return 'NI. ' + lasttwo + ' = 4 × ' + Math.floor(lasttwo / 4) + ' + ' + lasttwo % 4
}
function div5(N) {
  last = N % 10

  if (N % 5 == 0) {
    return 'JE. Zadnja števka je ' + last + '.'
  }
  return 'NI. Zadnja števka je ' + last + '.'
}

function div6(N) {
  if (N % 6 == 0) {
    return 'JE. Deljivo s 2 in 3.'
  }
  if (N % 2 != 0) {
    return 'NI. Ni deljivo s 2.'
  }
  return 'NI. Ni deljivo s 3.'
}

function div7(N) {
  digits = N.toString().split('')
  sum = 0
  strsum = []
  blocks = []
  alter = true
  digits = ['0', '0'].concat(digits)
  nd = digits.length
  for (var i = 0; i <= Math.ceil((nd - 2) / 3) - 1; i++) {
    blk = parseInt((digits.slice(nd - (i + 1) * 3, nd - i * 3)).join(''))
    blocks.push(blk)
    if (alter) {
      alter = !alter
      strsum += blk + ' - '
      sum += parseInt(blk)
    } else {
      alter = !alter
      strsum += blk + ' + '
      sum -= parseInt(blk)
    }
  }

  if (N % 7 == 0) {
    return 'JE. ' + strsum.slice(0, -3) + ' = ' + sum + ' = 7 × ' + Math.floor(sum / 7)
  }
  return 'NI. ' + strsum.slice(0, -3) + ' = ' + sum + ' = 7 × ' + Math.floor(sum / 7) + ' + ' + (sum % 7 + 7) % 7
}


function div8(N) {
  lastthree = N % 1000
  if (N % 8 == 0) {
    return 'JE. ' + lastthree + ' = 8 × ' + Math.floor(lastthree / 8)
  }
  return 'NI. ' + lastthree + ' = 8 × ' + Math.floor(lastthree / 8) + ' + ' + lastthree % 8
}

function div9(N) {
  strnum = N.toString().split('')
  sum = strnum.reduce((a, b) => a + parseInt(b), 0)
  strsum = strnum.join(' + ')

  if (N % 9 == 0) {
    return 'JE. ' + strsum + ' = ' + sum + ' = 9 × ' + Math.floor(sum / 9)
  }
  return 'NI. ' + strsum + ' = ' + sum + ' = 9 × ' + Math.floor(sum / 9) + ' + ' + sum % 9
}


function div10(N) {
  last = N % 10
  if (N % 10 == 0) {
    return 'JE. Zadnja števka je 0.'
  }
  return 'NI. Zadnja števka je ' + last + '.'
}

function div11(N) {
  digits = (N.toString().split('')).reverse()
  strsum = digits[0]
  sum = parseInt(digits[0])
  alter = true
  for (var i = 1; i < digits.length; i++) {
    if (alter) {
      alter = !alter
      sum -= parseInt(digits[i])
      strsum += ' - ' + digits[i]
    } else {
      alter = !alter
      sum += parseInt(digits[i])
      strsum += ' + ' + digits[i]
    }
  }

  if (N % 11 == 0) {
    return 'JE. ' + strsum + ' = ' + sum + ' = 11 × ' + Math.floor(sum / 11)
  }
  return 'NI. ' + strsum + ' = ' + sum + ' = 11 × ' + Math.floor(sum / 11) + ' + ' + (sum % 11 + 11) % 11
}


var deljitelji = [...Array(11).keys()]
var opis = [
  'Brez pogoja.',
  'Zadnja števka je soda.',
  'Vsota števk je deljiva s 3.',
  'Zadnji dve števki sta deljivi s 4.',
  'Zadnja števka je 5 ali 0.',
  'Deljivo z 2 in 3.',
  'Alternirajoča vsota treh zaporednih števk od zadaj je deliva s 7.',
  'Zadje tri števke so deljive z 8.',
  'Vsota števk je deljiva z 9.',
  'Zadnja števka je 0.',
  'Alternirajoča vsota števk je deliva s 11.'
]


izracunaj_tabelo()

function izracunaj_tabelo() {
  var N = parseInt(document.getElementsByName('stevilo')[0].value);
  if (Number.isInteger(N) && N >= 0) {

    var pogoji = [div1(N), div2(N), div3(N), div4(N), div5(N), div6(N), div7(N), div8(N), div9(N), div10(N), div11(N)]

    let table_data = []
    for (var i = 1; i <= 11; i++) {
      table_data.push({ deljitelj: i, pogoj_za_deljivost: opis[i - 1], veljavnost_pogoja: pogoji[i - 1], ostanek: N % i })
    }



    var Parent = document.querySelector("table");
    while (Parent.hasChildNodes()) {
      Parent.removeChild(Parent.firstChild);
    }


    function generateTableHead(table, data) {
      let thead = table.createTHead();
      let row = thead.insertRow();
      for (let key of data) {
        let th = document.createElement("th");
        th.scope="col";
        let text = document.createTextNode(key);
        th.appendChild(text);
        row.appendChild(th);
      }
    }
    function generateTable(table, data) {
      for (let element of data) {
        let row = table.insertRow();
        for (key in element) {
          let cell = row.insertCell();
          let text = document.createTextNode(element[key]);
          cell.appendChild(text);
        }
      }
    }
    let table = document.querySelector("table");
    let data = ['deljitelj', 'pogoj za deljivost', 'veljavnost pogoja', 'ostanek'];
    generateTableHead(table, data);
    generateTable(table, table_data);

    var rows, index_ostatek = 3
    rows = document.querySelectorAll("tr");

    for (i = 0, len = rows.length; i < len; i++) {
      var cols = rows[i].querySelectorAll('td')
      if (cols.length > 0) {
        if (cols[index_ostatek].innerHTML == 0) {
          //http://colorsafe.co/
          rows[i].style.background = 'rgba(59, 153, 113, .5)';
        } else {
          rows[i].style.background = 'rgba(231, 98, 104, .5)';
        }
      } else {
        rows[0].style.background = '#008180';
      }
    }
    head = document.querySelectorAll("th");
    for (j = 0; j < head.length; j++) {
      head[j].innerHTML = '<a style="color:#FFFFFF;">' + head[j].innerHTML + '<a>';
    }
  } else {
    var Parent = document.querySelector("table");
    while (Parent.hasChildNodes()) {
      Parent.removeChild(Parent.firstChild);
    }

  }
}



