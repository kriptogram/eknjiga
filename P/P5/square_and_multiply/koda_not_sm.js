window.addEventListener('load', function(){
	console.log("Welcome!");
	document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("ns").click();
		  }
	});

	//globalne spremenljivkke
	var bin=document.getElementById("binarnaPredstavitev");
	var mess1=document.getElementById("mess1");
	var tab=document.getElementById("potence");
	var reset=document.getElementById("reset");
	var ns=document.getElementById("ns");
	var a, b, n;
	var stevec; //stevec pove, kateri korak naj se izvede naslednji

	reset.addEventListener('click', function(){
		stevec=0;
		mess1.innerHTML="";
		while(tab.rows[0])tab.deleteRow(0);
		a=parseInt(document.getElementById("a").value);
		b=parseInt(document.getElementById("b").value);
		n=parseInt(document.getElementById("n").value);
		
		binarno=b.toString(2);
		var temp="Najprej izracunamo binarno predstavitev eksponenta b:";
		temp=temp+"</br>    "+b+"<sub>10</sub> = "+binarno+"<sub>2</sub>"
		bin.innerHTML=temp;	
	});

	ns.addEventListener('click', function(){
		if(stevec==0){
			//izracunamo tabelo
			var presnji;
			var potenca=1;
			var count=0;
			while(potenca<=b){
				var row=tab.insertRow(count);
				
				var cell=row.insertCell(0);
				cell.innerHTML=potenca;
				cell.style.width='70px';
				
				cell=row.insertCell(1);
				cell.style.width='130px';
				cell.innerHTML=a+"<sup>"+potenca+"</sup> mod "+n;
				
				cell=row.insertCell(2);
				cell.style.width='130px'
				if(potenca>1)cell.innerHTML="("+a+"<sup>"+potenca/2+"</sup> ) <sup>2</sup> mod "+n;
				else cell.innerHTML="";
				
				cell=row.insertCell(3);
				cell.style.width='130px';
				if(count!=0)cell.innerHTML=presnji+"<sup>2</sup> mod "+n;
				else cell.innerHTML="";
				
				cell=row.insertCell(4);
				cell.style.width='130px';
				if(count==0){
					cell.innerHTML=a%n;		
					presnji=a%n;
				}else{
					cell.innerHTML=presnji*presnji%n;
					presnji=presnji*presnji%n;
				}
				potenca*=2;
				count++;
			}
			stevec=1;
		}else if(stevec==1){
			//obarvamo potrebne vrstice
			var binarno=b.toString(2);
			for(var i=0; tab.rows[i]; i++){
				if(binarno.charAt(binarno.length-i-1)==1){
					tab.rows[i].classList.add('bg-success');
					tab.rows[i].classList.add('text-white');
				}
			}
			stevec=2;
		}else if(stevec==2){
			//prvo sporocilo
			var temp="Torej velja:</br>";
			temp=temp+" "+a+"<sup>"+b+"</sup> mod "+n+" = " +a+"<sup>"+izpisiPotence()+"</sup> mod "+n+" = "
			temp=temp+"("+izpisiFaktorjeNaDolgo()+ ") mod "+n+" = ("+izpisiFaktorje()+") mod "+n+" = "+izracunajRezultat();
			mess1.innerHTML=temp;
			stevec=3;
		}else{
			//do nothing - work done!
		}
	});
	
	function izpisiPotence(){
		var potenca=1;
		var temp="";
		for(var i=0; tab.rows[i]; i++){
			if(tab.rows[i].classList.contains('bg-success')){
				if(temp.length==0){
					temp=temp+potenca.toString(10);
				}else{
					temp=temp+" + "+potenca.toString(10);
				}
			}
			potenca=potenca*2;
		}
		return temp;
	}

	function izpisiFaktorje(){
		var temp="";
		for(var i=0; tab.rows[i]; i++){
			if(tab.rows[i].classList.contains('bg-success')){
				if(temp.length==0){
					temp=temp+tab.rows[i].cells[4].innerHTML;
				}else{
					temp=temp+" * "+tab.rows[i].cells[4].innerHTML;
				}
			}
		}
		return temp;
	}

	function izpisiFaktorjeNaDolgo(){
		var str=izpisiPotence();
		var faktorji=str.split("+");
		temp="";
		for(var i=0; i<faktorji.length; i++){
			faktorji[i]=parseInt(faktorji[i], 10);
			temp=temp+""+a+"<sup>"+faktorji[i]+"</sup> ";
		}
		return temp;
	}

	function izracunajRezultat(){
		var temp=1;
		for(var i=0; tab.rows[i]; i++){
			if(tab.rows[i].classList.contains('bg-success')){
				temp=temp*parseInt(tab.rows[i].cells[4].innerHTML, 10);
				temp=temp%n;
			}
		}
		return temp.toString(10);	
	}
	
});