window.addEventListener('load', function(){
	console.log("Welcome!");

	document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("ns").click();
		  }
	});

	//globalne spremenljivkke
	var bin=document.getElementById("binarnaPredstavitev");
	var mess1=document.getElementById("mess1");
	var tab=document.getElementById("potence");
	var reset=document.getElementById("reset");
	var ns=document.getElementById("ns");
	var a, b, n;
	var stevec; //stevec pove, koliko vrstic je vidnih
	var niz; // niz  znakov KM

	reset.addEventListener('click', function(){
		stevec=0;
		mess1.innerHTML="";
		while(tab.rows[0])tab.deleteRow(0);
		a=parseInt(document.getElementById("a").value);
		b=parseInt(document.getElementById("b").value);
		n=parseInt(document.getElementById("n").value);
		niz="";

		binarno=b.toString(2);
		var temp="Najprej izracunamo binarno predstavitev eksponenta b:";
		temp=temp+"</br>    "+b+"<sub>10</sub> = "+binarno+"<sub>2</sub></br>\
				Sestavimo nek pomožni niz tako, da za vsako 1 napišemo KM, za vsako 0\
				pa K:</br>"+binarno+"  <span>&#8594;</span>  ";
		for(var i=0; i<binarno.length; i++){
			if(binarno.charAt(i)=='0'){
				temp=temp+"K ";
				niz=niz+"K";
			}else{
				temp=temp+"K M ";
				niz=niz+"KM";	
			} 
		}
		bin.innerHTML=temp;	
	});

	ns.addEventListener('click', function(){
		if(stevec==niz.length){
			mess1.innerHTML="V zadnji vrstici preberemo rezultat: "+a.toString(10)+"\
							<sup>"+b.toString(10)+"</sup> mod "+n.toString(10)+" = \
							"+tab.rows[stevec-1].cells[2].innerHTML;
			return;
		}
		var row=tab.insertRow(stevec);
		var cell=row.insertCell(0);
		cell.style.width="30px";
		cell.innerHTML=niz.charAt(stevec);
		cell=row.insertCell(1);
		cell.style.width='150px'
		if(stevec==0){
			//prvi znak bo vedno kvadriraj K
			cell.innerHTML=1+"<sup>2</sup> mod "+n.toString(10);
			cell=row.insertCell(2);
			cell.style.width='70px'
			cell.innerHTML=1;
		}else{
			presnji=tab.rows[stevec-1].cells[2].innerHTML;
			presnji=parseInt(presnji, 10);
			if(niz.charAt(stevec)=='K'){
				cell.innerHTML=presnji.toString(10)+"<sup>2</sup> mod "+n.toString(10);
				cell=row.insertCell(2);
				cell.innerHTML=((presnji*presnji)%n).toString(10);	
			}else{
				cell.innerHTML=a.toString(10)+"<span>&#183;</span>"+presnji.toString(10)+" mod "+n.toString(10);
				cell=row.insertCell(2);
				cell.innerHTML=((presnji*a)%n).toString(10);
			}
		}
		stevec++;

	});
	
	
	
});