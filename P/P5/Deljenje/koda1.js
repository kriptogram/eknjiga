$(function() {
	var rangePercent = $('[type="range"]').val();
	$('[type="range"]').on('change input', function() {
		rangePercent = $('[type="range"]').val();
		$('[type="range"]').css('filter', 'hue-rotate(-' + rangePercent + 'deg)');
	});
});
const submitButtonOkValue = "Začni!";
const submitButtonResetValue = "Ponastavi podatke"
var isError = false;
window.addEventListener('load', function(){
	var tab=document.getElementById("tab");
	var a, b, n, id;
	var working=false;
	var err=document.getElementById("err");

	document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("submitButton").click();
		  }
	});

	document.getElementById("reset").addEventListener('submit', function(e){
		e.preventDefault();

		//ustavi cikel, ce pritisnemo reset
		if(working==true){
			clearInterval(id)
			working=false;
		}

		//vhodni podatki
		a=document.getElementById("a").value;
		b=document.getElementById("b").value;
		n=parseInt(document.getElementById("rs-range-line").value, 10);

		if(!isError) {
			if(!preveri(a, n) || !preveri(b, n)){
				document.getElementById("submitButton").value = submitButtonResetValue;
				isError = true;

				err.innerHTML="Nepravilen vhod";
				document.getElementsByClassName("rezBox")[0].style.visibility="hidden";
				document.getElementById("rezultat").innerHTML="";
				document.getElementById("rezultat").innerHTML="";
				if(document.getElementById("tabrez").rows[0]){
					document.getElementById("tabrez").deleteRow(0);
				}
				return;
			} else {
				document.getElementById("submitButton").value = submitButtonOkValue;
				isError = false;
			}

			//preverimo ce so vhodni podatki enaki 0
			if(b=="0"){
				err.innerHTML="Deliti z 0 ni mogoče."
				document.getElementsByClassName("rezBox")[0].style.visibility="hidden";
				document.getElementById("rezultat").innerHTML="";
				if(document.getElementById("tabrez").rows[0]){
					document.getElementById("tabrez").deleteRow(0);
				}
			
				return;	
			}

			// While dividing disable changing the base
			document.getElementById("rs-range-line").disabled = true;
			
			//zbrisemo vodilne nicle
			while(a.charAt(0)=="0" && a!="0")a=a.substring(1);
			while(b.charAt(0)=="0")b=b.substring(1);
			var len_a=a.length;
			var len_b=b.length;

			//preverim pravilnost podatkov
			n=parseInt(document.getElementById("rs-range-line").value, 10);
			//pobrisemo vse stare vrednosti
			reset();
			
			var b_desetisko=parseInt(b, n);
			var speed=document.getElementById("speed").value;
			speed=parseInt(speed, 10);
			speed=speed*20;
			//napisemo prvo vrstico
			createRow(0, len_rez()+len_a+len_b+3);//popravi tuki
			insertIntoTab(0, 0,  a);
			tab.rows[0].cells[len_a].innerHTML=":";
			insertIntoTab(0, len_a+1, b);
			tab.rows[0].cells[len_a+len_b+1].innerHTML="=";
			tab.rows[0].cells[0].style.width='20px';

			var indeksDeljenje=len_a+len_b+2; //cifro rezultat deljenja napisemo v tab[0][indDeljenje]
			var indeksVrstica=1; //v katero vrstico pisemo
			var dividend=0; //desetiska predstavitev stevila ki trenutno delimo
			var indNovaCifra;
			var postaviNaNic=true; //
			var stop=false; // ko je stop=true bomo koncali	
			var trenutniZmnozek=0;
			var dodajNovo=false;
			var napisiMinus=false; //ko je postavljena na true, napisemo minus na spodnje stevilo
			document.getElementsByClassName("rezBox")[0].style.visibility="visible";

			function callback() {
				// When dividing stops enable changing the base
				document.getElementById("rs-range-line").disabled = false;
			}
			
			//zacnemo deliti
			id=setInterval(function(){
				if(indNovaCifra==a.length && stop==true){
					document.getElementById("boxRez").style.visibility="visible";
					document.getElementById("rezultat").innerHTML="Torej velja:";
					document.getElementById("rezultat").style.margin="10px";
					var rez=document.getElementById("tabrez");

					var row=rez.insertRow(0);
					var i;
					for(i=0; i<indeksDeljenje; i++){
						var cell=row.insertCell(i);
						cell.innerHTML=tab.rows[0].cells[i].innerHTML;
						cell.style.width="12px";
						if(i==a.length)cell.innerHTML=" = ";
						else if(tab.rows[0].cells[i].innerHTML=="=")cell.innerHTML=" <span>&#183</span> ";
						//if(i<a.length)cell.style.backgroundColor='green';
						//else if(i<=a.length+b.length && i!=a.length)cell.style.backgroundColor='blue';
						//else if(i!=a.length && i!=a.length+b.length+1)cell.style.backgroundColor='orange';
					}
					var cell=row.insertCell(i);
					cell.style.width="12px";
					cell.innerHTML=" + ";
					i++;
					for(var j=0; j<a.length+1; j++){
						if(!isNaN(parseInt(tab.rows[indeksVrstica].cells[j].innerHTML, n))){
							var cell=row.insertCell(i);
							cell.style.width="13px";
							cell.innerHTML=tab.rows[indeksVrstica].cells[j].innerHTML;
							i++;
							//cell.style.backgroundColor='red';
						}	
					}
					working=false;
					clearInterval(id);
					callback();
					return;
				}
				if(dividend<b_desetisko && indeksVrstica==1){
					for(var i=1; ; i++){
						if(i==a.length+1){
							//deljenje da 0
							createRow(1, a.length+1);
							tab.rows[0].cells[indeksDeljenje].innerHTML=0;
							for(var j=0; j<a.length; j++){
								tab.rows[1].cells[j].innerHTML=a.charAt(j);
							}
							stop=true;
							indNovaCifra=a.length;
							indeksDeljenje++;
							callback();
							return;
						}
						dividend=a.substring(0, i);
						dividend=parseInt(dividend, n);
						if(dividend>=b_desetisko){
							indNovaCifra=i;
							break;
						}
					}
				}

				if(dodajNovo==true){
					//dodamo se zgornjo cifro v trenutni zmnozek
					indNovaCifra++;
					dividend=dividend*n+parseInt(a.charAt(indNovaCifra-1), n);
					dodajNovo=false;
					insertNumber(dividend, n, indeksVrstica, indNovaCifra);
				}else if(postaviNaNic==true){
					//dodamo novo vrstico, ki se bo povecala. trenutniZmnozek je 0
					if(indeksVrstica>1)indeksVrstica++;
					createRow(indeksVrstica, len_a+len_b+10);
					tab.rows[0].cells[indeksDeljenje].innerHTML=0;
					insertNumber(0, n, indeksVrstica, indNovaCifra);
					postaviNaNic=false;
				}else if(dividend>=trenutniZmnozek+b_desetisko){
					//trenutniZmnozek povacamo za b_desetisko
					tab.rows[0].cells[indeksDeljenje].innerHTML=(parseInt(tab.rows[0].cells[indeksDeljenje].innerHTML, n)+1).toString(n);
					trenutniZmnozek=trenutniZmnozek+b_desetisko;
					insertNumber(trenutniZmnozek, n, indeksVrstica, indNovaCifra);
					if(trenutniZmnozek+b_desetisko>dividend)napisiMinus=true;
				}else if(napisiMinus==true || (trenutniZmnozek==0 && tab.rows[indeksVrstica].cells[0].innerHTML.charAt(0)!='-')){
					//zapisemo minuc
					tab.rows[indeksVrstica].cells[0].innerHTML='-'+tab.rows[indeksVrstica].cells[0].innerHTML;
					napisiMinus=false;
				}else{
					//ne moramo vec povecati trenutnega zmnozka 
					//prepisemo razliko presnjih dveh vrstic
					tab.rows[indeksVrstica].cells[0].innerHTML=tab.rows[indeksVrstica].cells[0].innerHTML.substring(1);
					indeksVrstica++;
					indeksDeljenje++;
					createRow(indeksVrstica, len_b+len_a+10);
					dividend=dividend-trenutniZmnozek;
					insertNumber(dividend, n, indeksVrstica, indNovaCifra);
					dodajNovo=true;
					postaviNaNic=true;
					trenutniZmnozek=0;
					if(indNovaCifra==a.length)stop=true;
				}
			}, speed);
			
		} else {
			document.getElementById("submitButton").value = submitButtonOkValue;
			reset();

			document.getElementById("a").value = null;
			document.getElementById("b").value = null;
			
			isError = false;
		}
	});

	this.document.getElementById("pretvoriButton").addEventListener("click", (e) => {
		e.preventDefault();
		reset();

		n=parseInt(document.getElementById("rs-range-line").value, 10);

		this.console.log(((Number) (document.getElementById("a").value)).toString(n));
		document.getElementById("a").value = ((Number) (document.getElementById("a").value)).toString(n); //Number.parseInt(document.getElementById("a").value, n).toString();
		document.getElementById("b").value = ((Number) (document.getElementById("b").value)).toString(n); //Number.parseInt(document.getElementById("b").value, n).toString();


		document.getElementById("submitButton").value = submitButtonOkValue;

		isError = false;
	})

	function insertNumber(num, b, i, j){
		j--;
		var numberString=num.toString(b);
		var len=numberString.length;
		var ind=len-1;
		for(var c=j; c>j-len; c--){
			tab.rows[i].cells[c].innerHTML=numberString.charAt(ind);
			ind--;
		}
	}

	function len_rez(){
		var tempa=parseInt(a, n);
		var tempb=parseInt(b, n);
		var tempc=Math.floor(tempa/tempb);
		tempc=tempc.toString(n);
		return tempc.length;
	}

	function insertIntoTab(i, j, str){
		for(var s=0; s<str.length; s++){
			tab.rows[i].cells[j+s].innerHTML=str.charAt(s);
		}
		return;
	}

	function createRow(i, n){
		var row=tab.insertRow(i);
		for(var j=0; j<n; j++){
			row.insertCell(j);
		}
	}

	function fillEmpty(row, start,  end){
		for(var i=start; i<=end; i++){
			tab.rows[row].cells[i].innerHTML=" ";
		}
	}

	var rangeSlider = document.getElementById("rs-range-line");
  	var rangeBullet = document.getElementById("rs-bullet");

  	rangeSlider.addEventListener("input", showSliderValue, false);

  	function showSliderValue() {
	    rangeBullet.innerHTML = rangeSlider.value;
	    var bulletPosition = ((rangeSlider.value-2) /rangeSlider.max);
	    rangeBullet.style.left = (bulletPosition * 281-25) + "px";
  	}

  	function reset(){
  		document.getElementById("rezultat").innerHTML="";
  		document.getElementById("racunamo").innerHTML="Računamo [modulo "+n+"]";
			err.innerHTML="";
			working=true;
			while(tab.rows[0])tab.deleteRow(0);
			if(document.getElementById("tabrez").rows[0])document.getElementById("tabrez").deleteRow(0);
			document.getElementById("boxRez").style.visibility="hidden";
  	}

  	  //izpis modula
  document.getElementById("modul").addEventListener("mouseover", function(e){
    if(document.getElementById("tempModul"))return;
    var div = document.createElement("div");
    div.style.width = "300px";
    div.style.height = "70px";
    div.style.borderRadius="5px";
    div.style.background = "#11171F";
    div.style.fontSize="13px";
    div.style.textAlign="center";
    div.style.color = "white";
    div.style.position="fixed";
    div.style.top=(e.clientY+10)+"px";
    div.style.left=(e.clientX+20)+"px";
    div.id="tempModul";
    var m=document.getElementById("rs-range-line").value;
    m=parseInt(m, 10);
    var sporocilo="Modul določa, katere števke lahko uporabljaš. Trenutno kaže modul na \
      "+m+", torej lahko uporabljaš naslednje števke:</br>"+stevila(m);
    div.innerHTML = sporocilo;

    document.body.appendChild(div);
  });

  document.getElementById("modul").addEventListener("mouseout", function(e){
    if(document.getElementById("tempModul"))document.getElementById("tempModul").remove();
  });

	function stevila(n){
	  var rez="0";
	  for(var i=1; i<10 && i<n; i++){
		  rez=rez+", " + i;
	  }
	  if(n<10)return rez+".";
	  if(n>=11)rez=rez + ", a";
	  if(n>=12)rez=rez + ", b";
	  if(n>=13)rez=rez + ", c";
	  if(n>=14)rez=rez + ", d";
	  if(n>=15)rez=rez + ", e";
	  if(n>=16)rez=rez + ", f";
	  return rez+".";
  }

  function preveri(a, n){
  	//n je ze parsan
  	for(var i=0; i<a.length; i++){
  		var temp=a.charAt(i);
  		temp=parseInt(temp, n);
		if(isNaN(temp))return false;
  	}
  	return true;
  }
  document.getElementById("a").addEventListener("change", function(e){
	document.getElementById("submitButton").value = submitButtonOkValue;
  });
  document.getElementById("b").addEventListener("change", function(e){
	document.getElementById("submitButton").value = submitButtonOkValue;
  });
});