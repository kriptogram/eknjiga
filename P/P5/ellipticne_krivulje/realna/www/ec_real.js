var a = -2;
var b = 6;
var P = [-2.155,0.55];
var Q = [0.32,2.322];
var R = [];
var Rminus = [];
var existsP = true;
var existsQ = true;
var Px = -2.155;
var Qx = 0.32;
var k = 0.716;
var n = 2.093;


var rangeaConfig = {
    polyfill: false,
    onSlide: function(position, value){
        a = value;
        $("#label_a").text("a = " + a);

        changeDomain();

        plot();
    }
  }

var rangebConfig = {
    polyfill: false,
    onSlide: function(position, value){
        b = value;
        $("#label_b").text("b = " + b);

        changeDomain();
        plot();
    }
  }

var rangePConfig = {
    polyfill: false,
    onSlide: function(position, value){
        Px = value;
        plot();
    }
  }

  var rangeQConfig = {
    polyfill: false,
    onSlide: function(position, value){
        Qx = value;
        plot();
    }
  }

function changeDomain() {
  var min = minDomain();
  if (!isNaN(min)){
    
    $("#range_P").attr("min", min);
    /*console.log($("#range_P").value)
    if($("#range_P").value < min) {
      console.log("change")
      $("#range_P").val(min);
    }*/
    $("#range_P").rangeslider('update', true);

    $("#range_Q").attr("min", min);
    $("#range_Q").rangeslider('update', true);
  }
  
}




function recalculateP() {
  Py = getYEC(Px);
  if($("#selectP").val() === "down" ) {
    Py = -Py;
  }
  
  
  if(isNaN(Py)) {
    P = [-100, -100];
    existsP = false;
    $("#label_P").text("Izbrana točka pri x = " + Px + " ne leži na eliptični krivulji");
    $("#label_P").css("color", "red");
  } else {
    Py = Math.round(Py*1000)/1000
    P = [Px, Py];
    existsP = true;
    $("#label_P").html("P = (x<sub>1</sub>, y<sub>1</sub>) =(" + P[0] + "," + P[1] + ")");
    $("#label_P").css("color", "black");
  }
  

}

function recalculateQ() {
  Qy = getYEC(Qx);
  if($("#selectQ").val() === "down" ) {
    Qy = -Qy;
  }
  if(isNaN(Qy)) {
    Q = [-100, -100];
    existsQ = false;
    $("#label_Q").text("Izbrana točka pri x = " + Qx + " ne leži na eliptični krivulji");
    $("#label_Q").css("color", "red");
  } else {
    Qy = round(Qy);
    Q = [Qx, Qy];
    existsQ = true;
    $("#label_Q").html("Q = (x<sub>2</sub>, y<sub>2</sub>) = (" + Q[0] + "," + Q[1] + ")");
    $("#label_Q").css("color", "black");
  }
}

function calculateF() {
  if (existsP && existsQ) {
    //if it interesects in infinity
    if(P[0] == Q[0] && P[1] == -Q[1]) {
      $("#equationExplanation").text("Premica skozi točki P in Q ima enačbo:");
      $("#equationPQ").text("x = " + P[0]);
      $("#equationPQ").css("color", "black");
      $("#button_explanation").hide();
      $("#intersection_div").hide();
      $("#explanation").hide();
      $("#infinity_text").text("Presečišče premice in krivulje je v neskončnosti");
      $("#infinity_equation").html("R = P + Q = &infin;");
      $("#infinity").show();

    } else {
        // if it is the same point
        $("#button_explanation").show();
        $("#intersection_div").show();
        $("#infinity").hide();
        
        if (P[0] == Q[0] && P[1] == Q[1]) {
          var x = P[0];
          var y = P[1];
          k = (3*x*x + a)/(2*y);
          $("#equationExplanation").text("Tangenta skozi točko P ima enačbo:");
          $("#k_equation").text("$$k = {3*x^2 + a \\over 2 * y} = { 3*" + round(x*x) + " + " + addParantheses(a) + " \\over 2* " + addParantheses(y) + "} = " + round(k) + " $$");
          
        } else if(P[0] == Q[0] && P[1] == -Q[1]) {
          $("#equationExplanation").text("Premica skozi točki P in Q ima enačbo:");
          $("#k_equation").text("$$ = " + P[0]);

        } else {
          k = (P[1] - Q[1])/(P[0]-Q[0]);
          $("#equationExplanation").text("Premica skozi točki P in Q ima enačbo:");
          $("#k_equation").text("$$k = {y_1 - y_2 \\over x_1 - x_2} = {" + addParantheses(P[1]) + " - " + addParantheses(Q[1]) + " \\over " + addParantheses(P[0]) + " - " + addParantheses(Q[0]) + " } = " + round(k) + " $$");
        }
        MathJax.Hub.Queue(["Typeset",MathJax.Hub, 'k_equation']);

        n = P[1]-k*P[0];
        $("#equationPQ").text("y = " + round(k) + "*x + " + round(n));
        $("#equationPQ").css("color", "black");
        $("#n_equation").text("$$n = {y_1 - k*x_1} = " + addParantheses([1]) + " - " + addParantheses(round(k)) + "*" + addParantheses(P[0]) + " = " + addParantheses(round(n)) + " $$");
        MathJax.Hub.Queue(["Typeset",MathJax.Hub, 'n_equation']);
    }
    
    
  } else {
    $("#equationPQ").text("Izberite P in Q na krivulji");
    $("#equationPQ").css("color", "red");
    $("#k_equation").text("");
    $("#n_equation").text("");
  }

  
  
  
}

function calculateR() {
  if(existsP && existsQ && !(P[0] == Q[0] && P[1] == -Q[1])) {
    var x = k*k - P[0] - Q[0];
    var y = k*x + n;
    Rminus = [x, y];
    R = [x, -y];
    $("#intersection_text").text("Presečišče premice in krivulje je v točki -R");
    $("#intersection").html("-R =  (x<sub>3</sub>, y<sub>3</sub>) = (" + round(x) + ", " + round(y) + ")");
    $("#addition_text").text("Če seštejemo točko P in Q dobimo točko R:");
    $("#addition").text("P + Q = R = (" + round(x) + ", " + round(-y) + ")");

    $("#button_intersection").show();
    $("#button_addition").show();

    $("#x_equation").text("$$x_3 = k^2 - x_1 - x_2 = " + round(k*k) + " - " + addParantheses(P[0]) + " - " + addParantheses(Q[0]) + "$$");
    MathJax.Hub.Queue(["Typeset",MathJax.Hub, 'x_equation']);

    $("#y_equation").text("$$y_3 = k*x_3 + n = " + addParantheses(round(k)) + " * " + addParantheses(round(x)) + " + " + addParantheses(round(n)) + "$$");
    MathJax.Hub.Queue(["Typeset",MathJax.Hub, 'y_equation']);

    $("#R_explanation").text("R dobimo tako, da presečišče premice in krivulje zrcalimo čez x os");
    $("#R_equation").text("$$R = (x_3, -y_3) = (" + round(x) + ", " + round(-y) + ")$$");
    MathJax.Hub.Queue(["Typeset",MathJax.Hub, 'R_equation']);

    /*$("#addition_explanation").show();
    $("#intersection_explanation").show();
    $("#explanation").show();*/

  } else {
    Rminus = [-100, -100];
    R = [-100, -100];
    $("#intersection_text").text("");
    $("#intersection").text("");
    $("#addition_text").text("");
    $("#addition").text("");

    $("#button_intersection").hide();
    $("#button_addition").hide();

    $("#x_equation").text("");
    $("#y_equation").text("");

    $("#R_explanation").text("");
    $("#R_equation").text("");

    /*$("#addition_explanation").hide();
    $("#intersection_explanation").hide();
    $("#explanation").hide();*/
  }
}

function round(x) {
  return Math.round(x*1000)/1000;
}

function addParantheses(x) {
  if(x < 0) {
    return "("+x+")";
  }
  else {
    return x;
  }
}

function pointsOnEC(from=-10, to=10, step=1) {
  var data = [];
  for(var x=from; x<=to; x+=step) {
    y_square = x*x*x + a*x + b;
    if(y_square >= 0) {
      y = Math.sqrt(y_square);
      data.push({x: x,y: y});
    }
    
  }
  console.log(data);
  return data;
}

function getYEC(x) {
  y_square = x*x*x + a*x + b;
  y = Math.sqrt(y_square);
  return y;
  
}

function minDomain(){
  for(var x=-5; x<=5; x+=0.001) {
    y_square = x*x*x + a*x + b;
    if(y_square >= 0) {
      return x;
    }
    
  }
  return NaN;
}

function checkValidCurve() {
  var D = 4*a*a*a + 27*b*b;
  if (D == 0) {
      $("#curve_equation").html("Ta eliptična krivulja ne ustreza pogoju, da mora biti 4*a<sup>3</sup>+27*b<sup>2</sup> različen od 0");
      $("#curve_equation").css("color", "red");
      $("#curve_exists").hide();
  } else {
      $("#curve_equation").html("y<sup>2</sup> = x<sup>3</sup> + " + addParantheses(a) + "*x + " + addParantheses(b));
      $("#curve_equation").css("color", "black");
      $("#curve_exists").show();
  }
}

function pointsStyling() {
  $("circle").attr("r", 8);

  var fills = ["#40fabb", "#b1aaee", "#F15E75", "#B8B8B8"];
  var strokes = ["05b378", "#8276e4", "#903846", "#B8B8B8"];

  $.each($('circle'), function (index, item) {


      $(item).attr('fill', fills[index]);
      $(item).attr('stroke', strokes[index]);
      if(index == 3) {
        $(item).attr("r", 4);
      }

    
    
  });
}

function linesStyling() {

  var colors = ["orange", "orange", "black"]
  $.each($('.line'), function (index, item) {
    $(item).css('stroke', colors[index]);
  });

  $.each($('.annotations'), function (index, item) {
    
    $(item.firstChild).css('stroke', "black");

    // if line through R and -R
    if(index == 0) {
      $(item).css('stroke-dasharray', "4");
    }
  });
}

function drawText() {
  var label = ["P", "Q", "R", "-R"];
  var fills = ["#40fabb", "#b1aaee", "#F15E75", "#B8B8B8"];

  $.each($('.my-label'), function (index, item) {
    $(item).remove();
  });

  var svg = $("svg");
  $.each($('circle'), function (index, item) {
    // because there is an extra circle for the non-existent tip
    if(index < 4) {
      
      var cx = $(item)[0].attributes.cx.value;
      var cy = $(item)[0].attributes.cy.value;
      var data = $(item)[0].__data__;

      var text = document.createElement("text");
      var t = document.createTextNode(label[index] + " (" + round(data[0]) + ", " + round(data[1]) + ")");
      text.appendChild(t);
      $(text).attr("x", cx);
      $(text).attr("y", cy);
      $(text).attr("text-anchor", "middle");
      $(text).attr("class", "my-label");
      $(text).attr("fill", fills[index]);
      svg[0].appendChild(text);
    }

  });
  //ugly but so that text actually shows
    svg.html(svg[0].innerHTML);
    //console.log(svg[0].innerHTML);
    
}




function plot() {
  checkValidCurve();
  recalculateP();
  recalculateQ();
  calculateF();
  calculateR();
  
  functionPlot({
    target: '#chart',
    disableZoom: true,
    width: 725,
    height: 500,
    xAxis: {
      label: 'x', 
      domain: [-8, 8]
    }, 
    yAxis: {
      label: 'y',
      domain: [-8, 8]
    },
    grid: true,
    data: [
      { fn: 'sqrt(x*x*x + ' + a + '*x + ' + b + ')', skipTip: true},
      { fn: '-sqrt(x*x*x + ' + a + '*x + ' + b + ')', skipTip: true },
      { fn: k + '*x+' + n, skipTip: true},
      {
        points: [
          P, Q, R, Rminus
        ],
        fnType: 'points',
        graphType: 'scatter'
      },
      
    ],
    annotations: [{
      x: R[0]
    },
    {
      x: P[0]
    }
  ],
  });

  if (!existsP || !existsQ) {
    $(".line-2").first().remove();

  }

  if(!(P[0] == Q[0] && P[1] == -Q[1])){
    $(".annotations")[1].remove();
  } else {
    $(".line-2").first().remove();
  }
  
  drawText();
  pointsStyling();
  linesStyling();
}

  
window.addEventListener("load", function(event) {
  $("#range_a").rangeslider(rangeaConfig);
  $("#range_b").rangeslider(rangebConfig);
  $("#range_P").rangeslider(rangePConfig);
  $("#range_Q").rangeslider(rangeQConfig);

  $("#selectP").change(function() {
    plot();
  });

  $("#selectQ").change(function() {
    plot();
  });

  changeDomain();

  plot();

  $("#explanation").hide();
  $("#button_explanation").click(function() {
    $('#explanation').toggle('1000');
    $(this).toggleClass("fa-angle-double-down fa-angle-double-up");
  });

  $("#intersection_explanation").hide();
  $("#button_intersection").click(function() {
    $('#intersection_explanation').toggle('1000');
    $(this).toggleClass("fa-angle-double-down fa-angle-double-up");
  });

  $("#addition_explanation").hide();
  $("#button_addition").click(function() {
    $('#addition_explanation').toggle('1000');
    $(this).toggleClass("fa-angle-double-down fa-angle-double-up");
  });
  
});