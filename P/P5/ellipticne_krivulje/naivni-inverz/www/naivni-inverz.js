var n;
var p;
var i;

function calculateInverse() {
    n = parseInt($("#n").val(), 10);
    p = parseInt($("#p").val(), 10);

    $(".explanation_step").each(function() {
        $(this).remove();
    });

    if(!isNaN(n) && !isNaN(p)) {
        $("#input_mistake").text("");
        $("#result").show();
        
        var inverse;
        for(i=0; i<p; i++) {
            var ostanek = (n*i)%p;
            
            if(ostanek == 1) {
                inverse = i;  
                $("#explanation_naive").append("<p class='explanation_step' style='color: #7f7fff;'>" + n + " * " + i + " mod " + p + " = " + (n*i) +  " mod " + p + " = " + + ostanek + "</p>");
            } else {
                $("#explanation_naive").append("<p class='explanation_step' >" + n + " * " + i + " mod " + p + " = " + (n*i) +  " mod " + p + " = " + ostanek + "</p>");
            }
        }
        
        $("#explain_icon").show();
        /*$("#inverse_result").text("$$" + n + "^{-1} \mod " + p + "= " + inverse + "$$");*/
        if(inverse === undefined) {
            $("#inverse_result").text("Ker je največji skupni delitelj števil " + n + " in " + p + " večji od 1, modularni inverz ne obstaja");
            $("#inverse_result").css("color", "red");
        } else {
            /*$("#inverse_result").html("m = " + n + "<sup>-1</sup> mod " + p + " = " + inverse);*/
            $("#inverse_result").text("$$n^{-1} = " + n + "^{-1} \\mod " + p + "= " + inverse + "$$");
            $("#inverse_result").css("color", "black");
            MathJax.Hub.Queue(["Typeset",MathJax.Hub, 'inverse_result']);
        }
        
    } else {
        $("#input_mistake").text("Prosim, vnesite števili n in p.");
        $("#input_mistake").css("color", "red");
        $("#result").hide();
    }
}


window.addEventListener("load", function(event) {
    $("#button_inverse").click(calculateInverse);
    
    $("#explanation_naive").hide();
    $("#explain_icon").hide();
    $("#explain_icon").click(function() {
        $('#explanation_naive').toggle('1000');
        $(this).toggleClass("fa-angle-double-down fa-angle-double-up");
    });

    document.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("button_inverse").click();
          }
    });

});