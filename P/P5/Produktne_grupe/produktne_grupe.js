// podprta tipa sta 'sestevanje' in 'mnozenje', oba rabita parameter n, ki doloca modul
let izbraneGrupe = []
let omejitevVelikostiTabele = 15;

let barve = ["#fc5858", "#58b8fc", "#3bf93f", "#f9cd3b", "#f93bed"];

window.addEventListener('load', function () {
  document.getElementById("stevilo-grup").onchange = izberiSteviloGrup;
  izberiSteviloGrup();
});

function izberiSteviloGrup() {
  let stGrup = document.getElementById("stevilo-grup").selectedIndex + 1;
  let vsebinaElementa = "";
  izbraneGrupe = [];
  for (let i = 0; i < stGrup; i++) {
    izbraneGrupe.push({ tip: 'sestevanje', n: 3, vrednost: 0, prejsnjaVrednost: 0 });
    vsebinaElementa += `
    <div class="row">
    <div class="form-group col-xs-6 col-sm-6 clo-md-6">
      <label style="color: ` + barve[i % barve.length] + `">Grupa ` + (i + 1) + `</label>
      <select id="vrsta-grupe-`+ (i + 1) + `" class="form-control" onchange="nastaviVrstoGrupe(` + (i + 1) + `)">
        <option>Seštevanje</option>
        <option>Množenje</option>
      </select>
    </div>

    <div class="form-group col-xs-6 col-sm-6">
      <label>Modul</label>
      <input type="number" class="form-control" id="modul-`+ (i + 1) + `" value="3" min="1" max="100" onchange="nastaviVrstoGrupe(` + (i + 1) + `)">
    </div>
    </div>
    `;
  }
  document.getElementById("izbrane-gurpe").innerHTML = vsebinaElementa;
  posodobiPodatke();
}

function nastaviVrstoGrupe(index) {
  let izbraniTipIndeks = document.getElementById(`vrsta-grupe-` + (index)).selectedIndex;
  if (izbraniTipIndeks === 0) {
    izbraneGrupe[index - 1].tip = 'sestevanje';
  } else if (izbraniTipIndeks === 1) {
    izbraneGrupe[index - 1].tip = 'mnozenje';
  }
  izbraneGrupe[index - 1].n = parseInt(document.getElementById(`modul-` + (index)).value);
  console.log(izbraneGrupe[index - 1])
  posodobiPodatke();
}

function posodobiPodatke() {
  let prevelikModul = false;
  for (let i = 0; i < izbraneGrupe.length; i++) {
    if (izbraneGrupe[i].n > 100) {
      prevelikModul = true;
    }
  }
  let moc = izracunajMocProduktneGrupe();
  if (prevelikModul || moc > 1000) {
    document.getElementById("napaka").innerText = "Produktna grupa ima preveč elementov";
  } else {
    document.getElementById("napaka").innerText = "";
    document.getElementById("moc-grupe").innerText = moc;
    document.getElementById("caylay-tabela").innerHTML = generirajCaylayTabelo();
    document.getElementById("enota-grupe").innerText = '(' + enotaGrupe() + ')';
    let ciklicna = aliJeProduktnaGrupaCiklicna()[0];
    document.getElementById("ciklicna").innerText = (ciklicna ? 'JE' : 'NI');
    if (ciklicna) {
      document.getElementById("razlaga").innerText = "";
      document.getElementById("generatorjiP").style.display = "inline";
      let stGen = steviloGeneratorjevProduktneGrupe();
      console.log(stGen);
      document.getElementById("generatorji").innerText = elementiVBesedilo(generatorjiProduktneGrupe()) + (stGen > omejitevVelikostiTabele ? ', ... (skupaj ' + stGen + ' generatorjev)' : '');
    } else {
      document.getElementById("razlaga").innerText = " (" + aliJeProduktnaGrupaCiklicna()[1] + ".)";
      document.getElementById("generatorjiP").style.display = "none";
    }
    document.getElementById("izracun").innerHTML = sestaviEnacbo();

    let element = [];
    let vsiSoSestevanje = true;
    for (let i = 0; i < izbraneGrupe.length; i++) {
      izbraneGrupe[i].vrednost = 0;
      izbraneGrupe[i].prejsnjaVrednost = 0;
      element.push(0);
      if (izbraneGrupe[i].tip != 'sestevanje') {
        vsiSoSestevanje = false;
      }
    }
    generirano = [element];
    if (vsiSoSestevanje) {
      document.getElementById("planeti").style.display = 'block';
      narisiPlanete();
      document.getElementById("generirani-elementi").innerHTML = napisiElemente();
      document.getElementById("zacetniElement").innerHTML = generirajIzbiroZacetnegaElementa();
    } else {
      document.getElementById("planeti").style.display = 'none';
    }
  }
}

function generirajIzbiroZacetnegaElementa() {
  let html = '(';
  for (let i = 0; i < izbraneGrupe.length; i++) {
    if (i > 0) {
      html += `, `;
    }
    html += `
    <select class="" id="zacetnaVrednost`+ i + `">`;
    let osnovniElementi = elementiOsnovneGrupe(izbraneGrupe[i]);
    for (let j = 0; j < osnovniElementi.length; j++) {
      html += `<option>` + osnovniElementi[j] + `</option>`;
    }
    html += `</select>`;
  }
  html += `)`;
  return html;
}

function sestaviEnacbo() {
  let enacba = "(";
  for (let i = 1; i <= izbraneGrupe.length; i++) {
    if (i > 1) {
      enacba += `, `;
    }
    enacba += `x<span class="sub">` + i + `</span>`;
  }
  enacba += ") ∘ (";
  for (let i = 1; i <= izbraneGrupe.length; i++) {
    if (i > 1) {
      enacba += `, `;
    }
    enacba += `y<span class="sub">` + i + `</span>`;
  }
  enacba += ") = (";
  for (let i = 1; i <= izbraneGrupe.length; i++) {
    if (i > 1) {
      enacba += `, `;
    }
    enacba += `x<span class="sub">` + i + `</span>`;
    if (izbraneGrupe[i - 1].tip === 'sestevanje') {
      enacba += `&nbsp;+<span class="sub">` + izbraneGrupe[i - 1].n + `</span>`;
    } else {
      enacba += `&nbsp;∗<span class="sub">` + izbraneGrupe[i - 1].n + `</span>`;
    }
    enacba += `&nbsp;y<span class="sub">` + i + `</span>`;
  }
  enacba += ")";
  return enacba;
}

function elementiVBesedilo(elementi) {
  let besedilo = "";
  for (let i = 0; i < elementi.length; i++) {
    if (i > 0) besedilo += ", ";
    besedilo += "(" + elementi[i] + ")";
  }
  return besedilo;
}

function generatorjiProduktneGrupe() {
  let generatorji = [];
  if (aliJeProduktnaGrupaCiklicna()[0]) {
    generatorji = generirajGeneratorjeRekruzivno(0);
  }
  return generatorji;
}

function generirajGeneratorjeRekruzivno(n) {
  let generatorji = []
  let osnovniGen = generatorjiOsnovneGrupe(izbraneGrupe[n]);
  if (n === izbraneGrupe.length - 1) {
    for (let i = 0; i < osnovniGen.length; i++) {
      generatorji.push([osnovniGen[i]]);
      if (generatorji.length >= omejitevVelikostiTabele) {
        break;
      }
    }
    return generatorji;
  }
  for (let i = 0; i < osnovniGen.length; i++) {
    let generatorjiNaprej = generirajGeneratorjeRekruzivno(n + 1);
    for (let j = 0; j < generatorjiNaprej.length; j++) {
      generatorji.push([osnovniGen[i]].concat(generatorjiNaprej[j]));
      if (generatorji.length >= omejitevVelikostiTabele) {
        break;
      }
    }
    if (generatorji.length >= omejitevVelikostiTabele) {
      break;
    }
  }

  return generatorji;
}

function steviloGeneratorjevProduktneGrupe() {
  let st = 1;
  for (let i = 0; i < izbraneGrupe.length; i++) {
    st *= generatorjiOsnovneGrupe(izbraneGrupe[i]).length;
  }
  return st;
}

function aliJeProduktnaGrupaCiklicna() {
  for (let i = 0; i < izbraneGrupe.length; i++) {
    for (let j = i + 1; j < izbraneGrupe.length; j++) {
      if (gcd(mocOsnovneGrupe(izbraneGrupe[i]), mocOsnovneGrupe(izbraneGrupe[j])) > 1) {
        return [false, "Moči grup niso tuje"];
      }
    }
  }

  for (let i = 0; i < izbraneGrupe.length; i++) {
    if (!aliJeOsnovnaGrupaCiklicna(izbraneGrupe[i])) {
      return [false, "Grupa " + (i+1) + " ni ciklična"];
    }
  }
  return [true, ""];
}

function aliJeOsnovnaGrupaCiklicna(grupa) {
  if (grupa.tip === 'sestevanje') {
    return true
  } else if (grupa.tip === 'mnozenje') {
    return generatorjiOsnovneGrupe(grupa).length > 0;
  }
}

function generatorjiOsnovneGrupe(grupa) {
  let elementiGrupe = elementiOsnovneGrupe(grupa);
  let generatorji = [];
  for (let i = 0; i < elementiGrupe.length; i++) {
    if (aliJeGeneratorOsnovneGrupe(grupa, elementiGrupe[i])) {
      generatorji.push(elementiGrupe[i]);
    }
  }
  return generatorji;
}

function aliJeGeneratorOsnovneGrupe(grupa, gen) {
  if (grupa.tip === 'sestevanje') {
    return gcd(grupa.n, gen) === 1
  }

  let moc = mocOsnovneGrupe(grupa);
  let trenutniEl = gen;
  for (let i = 1; i < moc; i++) {
    if (grupa.tip === 'mnozenje') {
      trenutniEl = gen * trenutniEl % grupa.n
    }
    if (trenutniEl === gen) {
      return false;
    }
  }
  return true;
}

function enotaGrupe() {
  let enota = [];
  for (let i = 0; i < izbraneGrupe.length; i++) {
    if (izbraneGrupe[i].tip === 'sestevanje') {
      enota.push(0);
    } else if (izbraneGrupe[i].tip === 'mnozenje') {
      enota.push(1);
    }
  }
  return enota;
}

function izracunajMocProduktneGrupe() {
  let moc = 1;
  for (let i = 0; i < izbraneGrupe.length; i++) {
    moc *= mocOsnovneGrupe(izbraneGrupe[i]);
  }
  return moc;
}

function mocOsnovneGrupe(grupa) {
  if (grupa.tip === 'sestevanje') {
    return grupa.n
  } else if (grupa.tip === 'mnozenje') {
    return mocGrupeZMnozenjem(grupa.n)
  }
}

function mocGrupeZMnozenjem(n) {
  let moc = 0;
  for (let i = 1; i < n; i++) {
    if (gcd(i, n) === 1) {
      moc++;
    }
  }
  return moc;
}

function gcd(a, b) {
  if (a <= 0 || b <= 0) {
    return 0;
  }
  if (a > b) {
    return gcd(b, a);
  }
  if (b % a === 0) {
    return a;
  } else {
    return gcd(b % a, a);
  }
}

function elementiOsnovneGrupe(grupa) {
  let elementi = []
  if (grupa.tip === 'mnozenje') {
    for (let i = 1; i < grupa.n; i++) {
      if (gcd(i, grupa.n) === 1) {
        elementi.push(i)
      }
    }
  } else if (grupa.tip === 'sestevanje') {
    for (let i = 0; i < grupa.n; i++) {
      elementi.push(i)
    }
  }

  return elementi;
}

function generirajVseProduktneElemente(zacetnaGrupa) {
  let vsiElementi = [];
  let elementiTrenutneGrupe = elementiOsnovneGrupe(izbraneGrupe[zacetnaGrupa]);
  for (let i = 0; i < elementiTrenutneGrupe.length; i++) {
    if (zacetnaGrupa === izbraneGrupe.length - 1) {
      vsiElementi.push([elementiTrenutneGrupe[i]])
    } else {
      let elementiOdTuNaprej = generirajVseProduktneElemente(zacetnaGrupa + 1);
      for (let j = 0; j < elementiOdTuNaprej.length; j++) {
        vsiElementi.push([elementiTrenutneGrupe[i]].concat(elementiOdTuNaprej[j]));
      }
    }
  }

  return vsiElementi;
}

function operacija(el1, el2) {
  let rez = [];
  for (let i = 0; i < izbraneGrupe.length; i++) {
    if (izbraneGrupe[i].tip === 'mnozenje') {
      rez.push(el1[i] * el2[i] % izbraneGrupe[i].n);
    } else if (izbraneGrupe[i].tip === 'sestevanje') {
      rez.push(el1[i] + el2[i] % izbraneGrupe[i].n);
    }
  }
  return rez;
}

function generirajCaylayTabelo() {
  let vsiElementiGrupe = generirajVseProduktneElemente(0);
  let html = `
    <table class="table table-bordered">
    <thead>
    <tr>`;

  html += `<th scope="col" style="text-align: center;">∘</th>`;

  for (let i = 0; i < vsiElementiGrupe.length; i++) {
    if (i >= omejitevVelikostiTabele) {
      html += `<th scope="col" style="text-align: center;">...</th>`;
      break;
    }
    html += `<th scope="col" style="text-align: center;">(` + zapisElementaZBarvami(vsiElementiGrupe[i]) + `)</th>`;
  }
  html += `
    </tr>
    </thead>
    <tbody>
    `;
  for (let i = 0; i < vsiElementiGrupe.length; i++) {
    if (i >= omejitevVelikostiTabele) {
      html += `<tr>`;
      html += `<th style="text-align: center;">...</th>`;
      for (let j = 0; j < vsiElementiGrupe.length; j++) {
        html += `<td style="text-align: center;">...</td>`;
        if (j >= omejitevVelikostiTabele) {
          break;
        }
      }
      break;
    }
    html += `<tr>`;
    html += `<th style="text-align: center;">(` + zapisElementaZBarvami(vsiElementiGrupe[i]) + `)</th>`;
    for (let j = 0; j < vsiElementiGrupe.length; j++) {
      if (j >= omejitevVelikostiTabele) {
        html += `<td style="text-align: center;">...</td>`;
        break;
      }
      html += `<td style="text-align: center;">(` + zapisElementaZBarvami(operacija(vsiElementiGrupe[i], vsiElementiGrupe[j])) + `)</td>`;
    }
    html += `</tr>`;
  }
  html += `
    </tbody>
    </table>
    `;
  return html;
}

function zapisElementaZBarvami(element) {
  let html = ``;
  for (let i = 0; i < element.length; i++) {
    if (i > 0) {
      html += `,`;
    }
    html += `<span style="color: ` + barve[i % barve.length] + `">` + element[i] + `</span>`;
  }
  return html;
}

let animacija = 0;

function narisiPlanete() {
  var c = document.getElementById("planeti-canvas");
  var ctx = c.getContext("2d");
  ctx.fillStyle = "#ffffff";
  ctx.fillRect(0, 0, c.width, c.height)
  narisiSonce(ctx, c.width, c.height);
  narisiOrbite(ctx, c.width, c.height);
}

function narisiSonce(ctx, sirina, visina) {
  let radijSonca = 0.2;
  let radijZarkov = 0.35;
  let steviloZarkov = 15;
  ctx.beginPath();
  ctx.fillStyle = "#fcde58";
  let velikostTocke = sirina * radijSonca / 2;
  ctx.arc(sirina / 2, visina / 2, velikostTocke, 0, 2 * Math.PI);
  ctx.fill();
  ctx.closePath();

  for (let i = 0; i < steviloZarkov; i++) {
    let x = sirina / 2 + sirina * radijZarkov / 2 * Math.sin(2 * Math.PI * i / steviloZarkov)
    let y = visina / 2 - sirina * radijZarkov / 2 * Math.cos(2 * Math.PI * i / steviloZarkov)
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.strokeStyle = "#fcde58";
    ctx.moveTo(sirina / 2, visina / 2);
    ctx.lineTo(x, y);
    ctx.stroke();
    ctx.closePath();
    ctx.lineWidth = 1;
  }
}

function narisiOrbite(ctx, sirina, visina) {
  let stPlanetov = izbraneGrupe.length;
  let najmanjsiKrog = 0.4
  let najvecjiKrog = 0.9
  for (let i = 0; i < stPlanetov; i++) {
    let velikostTocke = Math.max(4, Math.min(20, Math.round(Math.log10(izbraneGrupe[i].n) * 10)));
    let odstotek = najvecjiKrog - i * ((najvecjiKrog - najmanjsiKrog) / stPlanetov);
    ctx.beginPath();
    ctx.strokeStyle = "#000000";
    ctx.arc(sirina / 2, visina / 2, sirina * odstotek / 2, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.closePath();

    // narisi tocko
    let razlikaVrednosti = izbraneGrupe[i].vrednost - izbraneGrupe[i].prejsnjaVrednost;
    if (razlikaVrednosti < 0) {
      razlikaVrednosti += izbraneGrupe[i].n;
    }
    let trenutnaVrednost = izbraneGrupe[i].prejsnjaVrednost + (animacija * razlikaVrednosti);
    let x = sirina / 2 + sirina * odstotek / 2 * Math.sin(2 * Math.PI * trenutnaVrednost / izbraneGrupe[i].n)
    let y = visina / 2 - sirina * odstotek / 2 * Math.cos(2 * Math.PI * trenutnaVrednost / izbraneGrupe[i].n)
    ctx.beginPath();
    ctx.fillStyle = barve[i % barve.length];
    ctx.arc(x, y, velikostTocke, 0, 2 * Math.PI);
    ctx.fill();
    ctx.closePath();
    // narisi vrednost
    ctx.font = "16px Arial";
    ctx.fillStyle = "#000000";
    ctx.fillText(izbraneGrupe[i].vrednost + "", x + velikostTocke, y - velikostTocke);
  }
}

let interval = null;
let generirano = [];
let zacetniElement;
let predvajanje = false;

function predvajaj() {
  if (!predvajanje) {
    document.getElementById("predvajajGumb").innerText = "Ustavi";
    zacetniElement = [];
    for (let i = 0; i < izbraneGrupe.length; i++) {
      zacetniElement.push(izbraneGrupe[i].vrednost);
    }
    generirano = [zacetniElement];
    predvajanje = true;
    setTimeout(naslednjaVrednost, 250);
  } else {
    document.getElementById("predvajajGumb").innerText = "Predvajaj";
    predvajanje = false;
  }
}

function nastaviZacetniElement() {
  document.getElementById("predvajajGumb").innerText = "Predvajaj";
  predvajanje = false;
  zacetniElement = [];
  for (let i = 0; i < izbraneGrupe.length; i++) {
    izbraneGrupe[i].vrednost = parseInt(document.getElementById("zacetnaVrednost" + i).value);
    izbraneGrupe[i].prejsnjaVrednost = izbraneGrupe[i].vrednost
    zacetniElement.push(izbraneGrupe[i].vrednost);
  }
  generirano = [zacetniElement];
  document.getElementById("generirani-elementi").innerHTML = napisiElemente();
  narisiPlanete();
}

function aliSmoNaZacetnem() {
  for (let i = 0; i < izbraneGrupe.length; i++) {
    if (izbraneGrupe[i].vrednost != zacetniElement[i]) {
      return false;
    }
  }
  return true;
}

function naslednjaVrednost() {
  clearInterval(interval);
  interval = 0;
  animacija = 0;
  let element = [];
  for (let i = 0; i < izbraneGrupe.length; i++) {
    izbraneGrupe[i].prejsnjaVrednost = izbraneGrupe[i].vrednost;
    izbraneGrupe[i].vrednost = (izbraneGrupe[i].vrednost + 1) % izbraneGrupe[i].n;
    element.push(izbraneGrupe[i].vrednost);
  }
  generirano.push(element);

  animacija = 0;
  interval = setInterval(function () {
    animacija += 0.1
    if (animacija >= 1) {
      animacija = 1;
      clearInterval(interval);

      // avtomatsko predvajanje
      console.log(predvajanje)
      if (predvajanje) {
        if (!aliSmoNaZacetnem()) {
          setTimeout(naslednjaVrednost, 250);
        } else {
          document.getElementById("predvajajGumb").innerText = "Predvajaj";
          predvajanje = false;
        }
      }
    }
    narisiPlanete();
  }, 50);
  narisiPlanete();
  document.getElementById("generirani-elementi").innerHTML = napisiElemente();
}

function napisiElemente() {
  let elementi = "";
  for (let i = 0; i < generirano.length; i++) {
    if (i > 0) {
      elementi += ", ";
    }
    elementi += "(";
    elementi += zapisElementaZBarvami(generirano[i]);
    elementi += ")";
  }
  return elementi;
}