document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("generirajBtn").click();
      }
});

function exp_by_squaring(g, x, p){
    if(x == 0)
        return 1;
    else if(x == 1)
        return g;
    else if(x % 2 == 0)
        return exp_by_squaring((g * g) % p, x / 2, p) % p;
    else if(x % 2 == 1)
        return g * exp_by_squaring((g * g) % p, (x - 1) / 2, p) % p;
}

function pow(x, y, z){
    var number = 1;
    while(y){
        if(y & 1)
            number = number * x % z;
        y >>= 1
        x = x * x % z;
    }
    return number
}

function generiraj(){
    g = parseInt(document.getElementById("prvoStevilo").value);
    p = parseInt(document.getElementById("drugoStevilo").value);
    n = parseInt(document.getElementById("cetrtoStevilo").value);
    if(!parseInt(document.getElementById("tretjeStevilo").value)){
        x0 = + new Date;
        document.getElementById("tretjeStevilo").value = x0;
    }
    else
        x0 = parseInt(document.getElementById("tretjeStevilo").value);

    var xi = x0;

    var rezultatVsebnik = document.getElementById("rezultat-vsebnik");
    while(rezultatVsebnik.hasChildNodes()) {
        rezultatVsebnik.firstChild.remove();
    }
    var rezultat = document.createElement("div");
    rezultat.classList.add("m-auto");
    rezultatVsebnik.appendChild(rezultat);

    for(var i=1; i<n; i++){
        rezultat.innerHTML += `$$ x_{${i}} = g^{x_${i-1}} \\bmod p = ${g}^{${xi}} \\bmod ${p} = `;
        xi = pow(g, xi, p);
        rezultat.innerHTML += xi + "$$";
    }
    // document.getElementById("rezultat").innerHTML += ""
    MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
}

function generateNBits(n, g, x0, p){
    console.log(`n = ${n}, g = ${g}, x0 = ${x0}, p = ${p}`);

    var bits = "";
    var x = x0;

    for(var i=0; i<n; i++){  
        console.log(x);      
        if(x % 2)
            bits += "1";
            else
            bits += "0";
            
        x = exp_by_squaring(g, x, p);
    }
    return bits;
}

var g;
var p;
var x0;
var n;
