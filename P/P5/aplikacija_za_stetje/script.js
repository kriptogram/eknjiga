function generirajTabelo() {
  var table = document.getElementById("table");
  const mod1 = parseInt(document.getElementById("prvoStevilo").value);
  const mod2 = parseInt(document.getElementById("drugoStevilo").value);
  if(!mod1 || !mod2) {
    table.innerHTML = "Prosim, vnesi števili v vnosni polji.";
    return;
  }
  const tabVals = calcTableValues(mod1, mod2); 

  tableString = `\\[
    \\begin{array}{cc|` + 'c'.repeat(mod1 + 1) + `}
    \\ & & `
  for (let i = 0; i < mod1; i++) {
    tableString += i + ` &`;
  }
  
  tableString += ` \\text{(mod ` + mod1 + `)} \\\\ 
  \\hline
  `;

  for (let j = 0; j < mod2; j ++) {
    for (let i = 0; i < mod1 + 3; i++) {
      if(i == 0) {
        if (j == Math.floor(mod2 / 2)) {
          tableString += `\\ \\text{(mod ` + mod2 + `)} & `;
        } else {
          tableString += `\\ & `;
        }
      } else if (i == 1) {
        tableString += j; 
      } else if (i == mod1 + 2) {
        tableString += ` & \\\\
        `;
      } else {
        console.log(i - 2,j)
        tableString += `& ` + (tabVals[i - 2][j] != null ? tabVals[i - 2][j] : '') + ` `; 
      }
    }
  }
  tableString += `\\end{array}
  \\]`
  table.innerHTML = tableString;
  console.log(tableString);
  MathJax.Hub.Queue(["Typeset", MathJax.Hub, table]);
}

function calcTableValues(mod1, mod2) {
  let i = 0;
  let j = 0;
  let table = [];
  for (let ix = 0; ix < mod1; ix++) {
    table.push([]);
    for(let ix2 = 0; ix2 < mod2; ix2++) {
      table[ix].push(null);
    }
  }
  console.log(table);
  let count = 0;
  while(true) {
    if (i >= mod1) {
      i = 0;
    } 
    if (j >= mod2) {
      j = 0;
    }
    if(table[i][j] != null) {
      break;
    }
    table[i][j] = count;

    i ++;
    j ++;
    count ++;
  }
  return table;
}