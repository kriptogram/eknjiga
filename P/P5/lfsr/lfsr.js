function createElement(str) {
    var template = document.createElement('template');
    template.innerHTML = str.trim();
    return template.content.firstChild;
}

function moveRight() {
    var btn = document.getElementById("next-btn");
    btn.disabled = true;

    var numbers = document.getElementById("numbers");

    var next = nextValue();

    var output = document.getElementById("output");
    output.value = numbers.lastChild.value + output.value;
    numbers.lastChild.remove();

    numbers.insertBefore(
        createElement(`<input class="register-val" type="text" value="` + next + `" style="width: 0px;"/>`),
        numbers.firstChild
    );

    var count = 0;
    var interval = setInterval(function() {
        numbers.firstChild.style.width = count + "px";
        if(count == 44) {
            clearInterval(interval);
            btn.disabled = false;
        }
        count += 2;
    }, 6);
}

function moveLeft() {
    var btn = document.getElementById("prev-btn");
    btn.disabled = true;

    var numbers = document.getElementById("numbers");

    var output = document.getElementById("output");
    var prev = output.value.charAt(0);
    if(!prev) {
        btn.disabled = false;
        return;
    }
    output.value = output.value.substring(1);

    numbers.appendChild(
        createElement(`<input class="register-val" type="text" value="` + prev + `" style="width: 0px;"/>`)
    );

    var count = 0;
    var interval = setInterval(function() {
        numbers.firstChild.style.width = (44 - count) + "px";
        numbers.lastChild.style.width = count + "px";
        if(count == 44) {
            clearInterval(interval);
            btn.disabled = false;
            numbers.firstChild.remove();
        }
        count += 2;
    }, 6);
}

function nextValue(numbers) {
    if(connections.length == 0) {
        return 0;
    }

    var numbers = document.getElementById("numbers");
    var val = 0;
    for(var i = 0; i < connections.length; i++) {
        if(connections[i] == 1) {
            val ^= Number(numbers.children[i].value);
        }
    }

    return val;
}

var cellCounter = 0;
function insert_cell() {
    var register = document.getElementById("register-row");
    register.innerHTML += `<td class="register-cell"></td>`;

    var numbers = document.getElementById("numbers");
    numbers.innerHTML += `<input class="register-val" id="val-` + cellCounter + `" type="text" value="0"/>`;
    cellCounter++;
}

function insert_XOR() {
    var row = document.getElementById("wire-row");
    row.innerHTML += 
    `<svg class="wire-block">
        <path d="M 25 5 L 25 48" stroke-width="3" stroke="black"/>
        <path d="M 0 15 L 48 15" stroke-width="3" stroke="black"/>
        <circle cx="25" cy="15" r="10" stroke="black" stroke-width="3" fill="none"/>
    </svg>`;

    insert_cell();
}

function insert_line() {
    var row = document.getElementById("wire-row");
    row.innerHTML += 
    `<svg class="wire-block">
        <path d="M 0 15 L 48 15" stroke-width="3" stroke="black"/>
    </svg>`;

    insert_cell();
}

function insert_XORend() {
    var row = document.getElementById("wire-row");
    row.innerHTML += 
    `<svg class="wire-block">
        <path d="M 25 5 L 25 48" stroke-width="3" stroke="black"/>
        <path d="M 0 15 L 35 15" stroke-width="3" stroke="black"/>
        <circle cx="25" cy="15" r="10" stroke="black" stroke-width="3" fill="none"/>
    </svg>`;

    insert_cell();
}

function insert_end() {
    var row = document.getElementById("wire-row");
    row.innerHTML += 
    `<svg class="wire-block">
        <path d="M 25 14 L 25 48" stroke-width="3" stroke="black"/>
        <path d="M 0 15 L 25 15" stroke-width="3" stroke="black"/>
    </svg>`;

    insert_cell();
}

window.onload = function() {
    //insert_XOR();
    //insert_line();
    //insert_end();
}

var connections = [];

function setPolynomial() {
    var row = document.getElementById("wire-row");
    row.innerHTML = "";
    var register = document.getElementById("register-row");
    register.innerHTML = "";
    var numbers = document.getElementById("numbers");
    numbers.innerHTML = "";
    document.getElementById("back-arrow").style.display = "none";


    var polynomial = document.getElementById("characteristic-polynomial").value;
    var regex = /^x([1-9][0-9]*|[2-9]?)( ?\+ ?x([1-9][0-9]*|[2-9]?))*( ?\+ ?1)$/g;
    var matches = polynomial.match(regex);

    var warning = document.getElementById("polynomial-warning-1");
    if(matches == null) {
        warning.style.display = "block";
        return;
    }
    warning.style.display = "none";

    polynomial = polynomial.replaceAll(" ", "");
    polynomial = polynomial.replace(/1$/g, "0");
    var terms = polynomial.split("+");
    var constants = terms.map(t => {
        t = t.replaceAll("x", "");
        if(t == "") {
            return 1;
        }
        return Number(t);
    });
    var lfsr_degree = Math.max(...constants);
    constants = constants.map(c => lfsr_degree - c);
    constants.sort((a, b) => {
        return a - b;
    });
    var warning = document.getElementById("polynomial-warning-2");
    for(var i = 1; i < constants.length; i++) {
        if(constants[i] == constants[i-1]) {
            warning.style.display = "block";
            return;
        }
    }
    warning.style.display = "none";
    var num = constants[constants.length - 1];

    connections = [];
    for(var i = 0; i < num; i++) {
        connections.push(0);
    }
    constants.forEach(element => {
        connections[element-1] = 1;
    });

    for(var i = 0; i < connections.length; i++) {
        if(i == connections.length-1) {
            insert_end();
        } else if(connections[i] == 1) {
            insert_XOR();
        } else {
            insert_line();
        }
    }
    document.getElementById("register-tbl").style.width = connections.length * 48 + "px";
    document.getElementById("back-arrow").style.display = "block";
}
