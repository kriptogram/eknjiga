#!/bin/sh

#cd /Users/mfilic/Documents/FRI20182019/SIPK/crypto-e-book/ebook-SIPK/P

#Rscript -e "install.packages("knitr", repos = c("https://xran.yihui.name", "https://cran.r-project.org"))"

set -x

clear
echo
echo "START BUILDING THE SCRIPT."
echo

if [ $# -gt 0 ]; then
    echo "Building linijske kode"
    rootDir=$(pwd)
    cd ./P3/linijske_kode/
    npm i && npm run build
    cd $rootDir

    echo "Building OpenSSH kljuci"
    rootDir=$(pwd)
    cd ./P6/generiranjeRSA/
    npm i && npm run build
    cd $rootDir

    echo "Building RSA interaction"
    rootDir=$(pwd)
    cd ./P6/rsaCryptoSystemCode
    npm i && npm run build
    cd $rootDir
fi


export LANG=sl_SI.UTF-8

echo
echo "Preparing an environment for building a script for the artists."
echo

Rscript -e "bookdown::clean_book(TRUE)"
Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')"

echo
echo "Updating links and cleaning."
echo


Rscript -e "source('./code/helper.R'); cleanScript()"

rm -rf *.md
rm -rf *.rds

#cp -r ./Menu/* ./_crypto_eknjiga/
cp -r ./P* ./_crypto_eknjiga/
cp -r ./js-libs* ./_crypto_eknjiga/
cp -r ./static* ./_crypto_eknjiga/
rm -rf ./_crypto_eknjiga/*.md

echo
echo "Copy sources."
echo

echo
echo "THE END of build."
echo
