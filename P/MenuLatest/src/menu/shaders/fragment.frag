#ifdef GL_ES
  precision mediump float;
#endif

struct DirectionalLight {
    vec3 direction;
    vec3 intensity;
};

varying vec2 frag_texture;
varying vec3 frag_normals;

uniform vec3 ambient_light_int;
uniform DirectionalLight sun;
uniform bool offScreen;

uniform sampler2D samplerUniform;

void main(void) {


  vec3 surface_normal = normalize(frag_normals);
  vec3 light_int = ambient_light_int + sun.intensity * max(dot(surface_normal, normalize(sun.direction)), 0.0);

  vec4 texel =  texture2D(samplerUniform, vec2(frag_texture.s, frag_texture.t));

  if (offScreen) {
    gl_FragColor = texture2D(samplerUniform, vec2(frag_texture.s, frag_texture.s));
  } else {
    gl_FragColor = vec4(texel.rgb * light_int, texel.a);
  }

}