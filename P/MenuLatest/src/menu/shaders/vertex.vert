
precision mediump float;

attribute vec3 attr_position;
attribute vec2 attr_texture;
attribute vec3 attr_normals;

varying vec2 frag_texture;
varying vec3 frag_normals;

uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProj;
uniform mat4 mModel;

void main(void) {
  frag_normals = (mWorld * vec4(attr_normals,0.0)).xyz;
  frag_texture = attr_texture;
  gl_Position = mProj * mView * mModel * mWorld * vec4(attr_position, 1.0);
  
}