export function getRelativeMousePosition(event, target) {
    let mouseX = -1;
    let mouseY = -1;
    target = target || event.target;
    const rect = canvas.getBoundingClientRect();
    mouseX = event.clientX - rect.left;
    mouseY = event.clientY - rect.top;
    return {
        x: mouseX,
        y: mouseY,
    }
}

export function getNoPaddingNoBorderCanvasRelativeMousePosition(event, target) {
    target = target || event.target;
    var pos = getRelativeMousePosition(event, target);

    pos.x = pos.x * target.width / target.clientWidth;
    pos.y = pos.y * target.height / target.clientHeight;

    return pos;
}

export function convertMousePositionIntoWebGLCoordinates(event, target, gl) {
    var pos = getRelativeMousePosition(event, target);
    const x = pos.x / gl.canvas.width * 2 - 1;
    const y = pos.y / gl.canvas.height * -2 + 1;
    return {
        x: x,
        y: y,
    }
}
