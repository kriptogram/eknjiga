export function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? '0' + hex : hex;
}

export function rgbToHex(r, g, b) {
    return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

export const cubeColors = [
    rgbToHex(8, 163, 219),
    rgbToHex(79, 194, 197),
    rgbToHex(59, 186, 141),

    rgbToHex(17, 107, 142),
    rgbToHex(37, 129, 128),
    rgbToHex(27, 122, 88),

    rgbToHex(8, 59, 78),
    rgbToHex(19, 63, 64),
    rgbToHex(12, 56, 41),

];
