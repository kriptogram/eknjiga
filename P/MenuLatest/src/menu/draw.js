const {mat4, glMatrix} = require('gl-matrix');
const {rgbToHex, cubeColors} = require('./color-helpers');
const {getRelativeMousePosition} = require('./mouse-helpers');
const {popMatrix, pushMatrix} = require('./matrix-stack');
const {createIdentityMatrix} = require('./webgl-utils');
const {links} = require('./navigation');

glMatrix.setMatrixArrayType(Float32Array);

// Variables
let allCubes = {
    angle: 0,
    rotate: true,
    maxFactor: 2.5,
    factor: 2.4,
    cubesPerLines: null,
};

let rotatingCube = [];
let currentlyClickedCube = -1;
for (let i = 0; i < 7; i++) {
   rotatingCube.push({
       stopTurn: true,
       cubeRotAngle: 0,
   });
}

// Can be adjusted if preffered
const performInitialRotation = false;
// Timing info
let then = 0;

// Matrices
let worldMatrix = createIdentityMatrix();
let viewMatrix = createIdentityMatrix();
let modelMatrix = createIdentityMatrix();
let projectionMatrix = createIdentityMatrix();
let identityMatrix = createIdentityMatrix();


const draw = (gl, now, state) => {

    const deltaTime = now - then;   // deltaTime is now number of seconds since last frame
    then = now;

    gl.clearColor(0.0, 0.0, 0.0, 1);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    let diagonal = Math.sqrt(Math.pow(allCubes.factor, 2) + Math.pow(allCubes.factor, 2));
    let distance = Math.sqrt(Math.pow(allCubes.factor, 2) + Math.pow(allCubes.factor, 2));


    mat4.lookAt(viewMatrix, [0, 0, -(3 * diagonal + 2 * distance) / Math.tan(45)], [0, 0, 0], [0, 1, 0])
    mat4.perspective(projectionMatrix, glMatrix.toRadian(45), gl.canvas.width / gl.canvas.height, 0.1, 1000.0)


    if (gl.canvas.height <= gl.canvas.width) {
        allCubes.cubesPerLines = 3;
        mat4.translate(viewMatrix, viewMatrix, [-1 * allCubes.factor, 2 * allCubes.factor, -allCubes.factor * 0.5])
    } else {
        allCubes.cubesPerLines = 2;
        mat4.translate(viewMatrix, viewMatrix, [-0.5 * allCubes.factor, 3.25 * allCubes.factor, allCubes.factor * 3.25])
    }

    for (let i = 0; i < 7; i++) {

        if (i % allCubes.cubesPerLines == 0) {
            mat4.translate(viewMatrix, viewMatrix, [allCubes.factor * allCubes.cubesPerLines, -allCubes.factor, 0])
        }

        mat4.translate(viewMatrix, viewMatrix, [-allCubes.factor, 0, 0]);

        if (i != -1 && i == currentlyClickedCube) {

            pushMatrix(modelMatrix);

            let initialAngle = Math.PI;
            let maxAngle = initialAngle;

            let speedReduction = 100;
            mat4.identity(identityMatrix);

            rotatingCube[i].cubeRotAngle += deltaTime / speedReduction;


            if (rotatingCube[i].cubeRotAngle < maxAngle - 0.1 && !rotatingCube[i].stopTurn) {
                mat4.rotate(modelMatrix, modelMatrix, rotatingCube[i].cubeRotAngle, [1, 0, 1]);
            } else {
                rotatingCube[i].stopTurn = true;
                mat4.rotate(modelMatrix, modelMatrix, maxAngle, [1, 0, 1]);
                gl.canvas.onclick = (event, target) => {
                    window.location.href = links[currentlyClickedCube];
                };
            }


            drawCube(i + 1);

            modelMatrix = popMatrix();

        } else {
            rotatingCube[i].cubeRotAngle = 0;
            drawCube(i + 1);

        }

    }


    // Handle click event
    gl.canvas.onmousemove = (event, target) => {

        var pos = getRelativeMousePosition(event, target, gl)
        let mouseX = pos.x;
        let mouseY = pos.y;
        const pixelX = mouseX * gl.canvas.width / gl.canvas.clientWidth;
        const pixelY = gl.canvas.height - mouseY * gl.canvas.height / gl.canvas.clientHeight - 1;

        var pixels = new Uint8Array(4 * 1 * 1);

        gl.bindFramebuffer(gl.FRAMEBUFFER, state.frameBuffer);
        if (gl.checkFramebufferStatus(gl.FRAMEBUFFER) == gl.FRAMEBUFFER_COMPLETE) {
            gl.readPixels(pixelX, pixelY, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixels);
            let cubeIndexFromClick = cubeColors.indexOf(rgbToHex(pixels[0], pixels[1], pixels[2]));

            if (cubeIndexFromClick != -1) {
                rotatingCube[cubeIndexFromClick].stopTurn = false;
            }
            currentlyClickedCube = cubeIndexFromClick;


        }
        gl.bindFramebuffer(gl.FRAMEBUFFER, null)

    };

    // Functions
    function drawCube(cubeIndex) {

        // Perform initial rotation
        if (allCubes.rotate) {
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, state.sectionTextures[4])


        } else {
            // Bind appropriate texture
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, state.sectionTextures[cubeIndex - 1])

        }


        let initialAngle = Math.PI;
        let maxAngle = initialAngle + 4 * Math.PI;

        let percentage = allCubes.angle / maxAngle;

        allCubes.factor = percentage * allCubes.maxFactor;


        if (allCubes.angle >= maxAngle) {

            allCubes.rotate = false;

        } else {

            let speedReduction = 700;
            mat4.identity(identityMatrix);

            if (performInitialRotation) {
                allCubes.angle += deltaTime / speedReduction;

                if (allCubes.angle <= 3 * Math.PI) {
                    mat4.rotate(worldMatrix, identityMatrix, allCubes.angle, [1, 1, 0]);
                } else {
                    mat4.rotate(worldMatrix, identityMatrix, allCubes.angle, [0, 1, 0]);
                }

            } else {
                allCubes.angle = maxAngle
                mat4.rotate(worldMatrix, identityMatrix, maxAngle, [1, 1, 0]);
                mat4.rotate(worldMatrix, identityMatrix, maxAngle, [0, 1, 0]);
            }

        }


        gl.uniformMatrix4fv(state.uniforms.world, gl.FALSE, worldMatrix);
        gl.uniformMatrix4fv(state.uniforms.view, gl.FALSE, viewMatrix);
        gl.uniformMatrix4fv(state.uniforms.projection, gl.FALSE, projectionMatrix);
        gl.uniformMatrix4fv(state.uniforms.model, gl.FALSE, modelMatrix);
        gl.drawElements(gl.TRIANGLES, 132, gl.UNSIGNED_SHORT, 0);
    }

};

const render = (gl, now, state) => {

    // Off screen
    gl.bindFramebuffer(gl.FRAMEBUFFER, state.frameBuffer);
    gl.uniform1i(state.uniforms.off_screen, true);

    draw(gl, now, state);

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    gl.uniform1i(state.uniforms.off_screen, false);

    draw(gl, now, state);

    requestAnimationFrame(now => render(gl, now, state));
};

export default render;
