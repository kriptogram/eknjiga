import fragmentShaderSrc from './shaders/fragment.frag';
import vertexShaderSrc from './shaders/vertex.vert';
import resizeCanvas from './resize-canvas';
import createProgram from './create-program';
import buffer from './create-buffer';
import render from './draw';

// Section images
import p1Gesla from '../assets/images/gesla&virusi_tekstura.jpg'
import p2Okrajsave from "../assets/images/okrajsave_tekstura.jpg";
import p3Kode from "../assets/images/kode_tekstura.jpg";
import p4Sifre from "../assets/images/sifre_tekstura.jpg"
import p5Racunala from "../assets/images/racunala_tekstura.jpg";
import p6Podpisi from "../assets/images/podpisi_tekstura.jpg";
import p7Protokoli from "../assets/images/protokoli_tekstura.jpg";
import p8loto from "../assets/images/loto_tekstura.jpg";
import p9Veriga from "../assets/images/verige_tekstura.jpg";

let sectionImages = [p1Gesla, p2Okrajsave, p3Kode, p4Sifre, p5Racunala, p6Podpisi, p7Protokoli, p8loto, p9Veriga];
let sectionTextures = [];

// Initialize a texture and load an image. https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Using_textures_in_WebGL
// When the image finished loading copy it into the texture.
function loadTexture(gl, url) {

    const texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);

    // Put a single pixel in the texture so we can
    // use it immediately. When the image has finished downloading
    // we'll update the texture with the contents of the image.
    const level = 0;
    const internalFormat = gl.RGBA;
    const width = 1;
    const height = 1;
    const border = 0;
    const srcFormat = gl.RGBA;
    const srcType = gl.UNSIGNED_BYTE;

    function isPowerOf2(value) {
        return (value & (value - 1)) == 0;
    }

    const pixel = new Uint8Array([255, 255, 255, 255]);  // black
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
        width, height, border, srcFormat, srcType,
        pixel);

    const image = new Image();
    image.onload = function () {
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);


        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, image);

        // WebGL1 has different requirements for power of 2 images
        // vs non power of 2 images so check if the image is a
        // power of 2 in both dimensions.
        if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
            // Yes, it's a power of 2. Generate mips.
            gl.generateMipmap(gl.TEXTURE_2D);
        } else {
            // No, it's not a power of 2. Turn off mips and set
            // wrapping to clamp to edge
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

        }
    };
    image.src = url;

    return texture;

}

async function initTextures(gl) {
    for (let i = 0; i < sectionImages.length; i++) {
        sectionTextures.push(loadTexture(gl, sectionImages[i]));
    }
    return sectionTextures;
}


const menu = async () => {
    // Create a program
    const canvas = document.getElementById('canvas');

    const gl = canvas.getContext('webgl2');

    initTextures(gl);

    const shaders = [
        {src: fragmentShaderSrc, type: gl.FRAGMENT_SHADER},
        {src: vertexShaderSrc, type: gl.VERTEX_SHADER}
    ];


    const program = createProgram(gl, shaders);


    // Set up attributes and uniforms
    const attributes = {
        position: gl.getAttribLocation(program, 'attr_position'),
        frag_texture: gl.getAttribLocation(program, 'attr_texture'),
        frag_normals: gl.getAttribLocation(program, 'attr_normals'),
    };

    const uniforms = {
        world: gl.getUniformLocation(program, 'mWorld'),
        view: gl.getUniformLocation(program, 'mView'),
        model: gl.getUniformLocation(program, 'mModel'),
        projection: gl.getUniformLocation(program, 'mProj'),
        ambient_light_int: gl.getUniformLocation(program, 'ambient_light_int'),
        sunlight_int: gl.getUniformLocation(program, 'sun.intensity'),
        sunlight_dir: gl.getUniformLocation(program, 'sun.direction'),
        off_screen: gl.getUniformLocation(program, 'offScreen'),
    };

    // Set WebGL program here (we have only one)
    gl.useProgram(program);

    // Resize canvas and viewport
    const resize = () => {
        resizeCanvas(gl.canvas);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    };

    // Setup canvas
    window.onresize = resize;
    resize();

    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    gl.cullFace(gl.BACK);


    gl.enableVertexAttribArray(attributes.position);
    const vertexBuffer = buffer.createVertexBuffer(gl);
    gl.vertexAttribPointer(attributes.position, 3, gl.FLOAT, false, 3 * Float32Array.BYTES_PER_ELEMENT, 0);


    gl.enableVertexAttribArray(attributes.frag_normals);
    const normalsBuffer = buffer.createNormalsBuffer(gl);
    gl.vertexAttribPointer(attributes.frag_normals, 3, gl.FLOAT, true, 3 * Float32Array.BYTES_PER_ELEMENT, 0);

    gl.enableVertexAttribArray(attributes.frag_texture);
    const textureBuffer = buffer.createTextureBuffer(gl);
    gl.vertexAttribPointer(attributes.frag_texture, 2, gl.FLOAT, false, 2 * Float32Array.BYTES_PER_ELEMENT, 0);


    const indexBuffer = buffer.createIndexBuffer(gl);

    gl.uniform3f(uniforms.ambient_light_int, 0.2, 0.2, 0.2);
    gl.uniform3f(uniforms.sunlight_int, 0.9, 0.9, 0.9);
    gl.uniform3f(uniforms.sunlight_dir, 3.0, 4.0, -10.0);

    let offScreenTexture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, offScreenTexture);
    // Allocate storage for colors for offscreen framebuffer
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.canvas.width, gl.canvas.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

    // Renderbuffer will provide storage for individual buffers used in frameBuffers
    let renderBuffer = buffer.createRenderBuffer(gl);
    let frameBuffer = buffer.createFrameBuffer(gl);

    // Once bound we can attach the texture and renderBuffer
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, offScreenTexture, 0);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderBuffer);

    // Unbind
    gl.bindTexture(gl.TEXTURE_2D, null);
    gl.bindRenderbuffer(gl.RENDERBUFFER, null);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

    requestAnimationFrame(now => {

        render(gl, now, {
            vertexBuffer,
            normalsBuffer,
            indexBuffer,
            textureBuffer,
            attributes,
            uniforms,
            sectionTextures,
            frameBuffer,
        });
    })
};

export default menu;
