// Function creates and returns new HTML title.
function new_title(type, text) {
  var title = document.createElement(type);
  title.innerHTML = text;
  return title;
}

// Function creates and returns new HTML input field.
function new_input(type, name) {
  var input = document.createElement('input');
  input.setAttribute("type", type);
  input.id = name;
  return input;
}

// Function creates and returns new HTML input field.
function new_textarea(id, name) {
  var input = document.createElement('textarea');
  input.id = name;
  return input;
}

// Function creates and returns new HTML button.
function new_button(value, onclick, id) {
  var button = document.createElement("input");
  button.type = "button";
  button.value = value;
  button.onclick = onclick;
  button.setAttribute("id", id);
  return button;
}

// Function that creates new div representing one person in communication.
function create_new_person(name, id) {
  var alice = document.createElement('div');
  alice.setAttribute("id", id);
  alice.setAttribute("class", "user");
  alice.setAttribute("counter", 0);
  
  // Append all elements.
  alice.appendChild(new_title("h2", name));
  alice.appendChild(new_title("h3", "Ključ:"));
  key_div = document.createElement('div');
  key_div.setAttribute("id", id+"_key");
  key_div.setAttribute("class", "key");
  alice.appendChild(key_div);

  alice.appendChild(new_title("h3", "Sporočilo:"));
  alice.appendChild(new_textarea("user", id+"_message_input"));
  alice.appendChild(new_button("Zašifriraj", function(){encrypt(id);}, id+"_encrypt"));
  alice.appendChild(new_title("h3", "Kriptogram:"));
  alice.appendChild(new_textarea("user", id+"_ciphertext_input"));
  alice.appendChild(new_button("Odšifriraj", function(){decrypt(id);}, id+"_decrypt"));
  alice.appendChild(document.createElement("br"));
  alice.appendChild(new_button("Pošlji", function(){send(id);}, id+"_send"));
  
  return alice;
}


function generate_key() {
  var len = 100; // length of key is 100
  var alphabet = document.getElementById("alphabet").value;
  var output = "";
  var n = alphabet.length;
  for(var i=0; i<len; i++) {
    var r = Math.floor(Math.random() * n);
    output += alphabet.charAt(r);
  }
  document.getElementById("alice_key").innerHTML = output;
  document.getElementById("alice_key").setAttribute("key", output);
  document.getElementById("alice").setAttribute("counter", 0);
  document.getElementById("bob_key").innerHTML = output;
  document.getElementById("bob_key").setAttribute("key", output);
  document.getElementById("bob").setAttribute("counter", 0);
}

function color_key(id, start, end) {
  var key = document.getElementById(id + "_key").getAttribute("key");
  new_key = key.substring(0,start) + "<b>" + key.substring(start,end) + "</b>" + key.substring(end,key.length);
  document.getElementById(id+"_key").innerHTML = new_key;
}

function encrypt(id) {
  var plaintext = document.getElementById(id+"_message_input").value;
  var alphabet = document.getElementById("alphabet").value;
  var n = plaintext.length;
  var counter = parseInt(document.getElementById(id).getAttribute("counter"));
  var key = document.getElementById(id+"_key").getAttribute("key");
  var ciphertext = "";
  if(n == '0') {
    window.alert("Sporočilo je prazno.");
    return;
  }
  if(n > key.length - counter) {
    window.alert("Neuporabljen del ključa je prekratek.");
    return;
  }
  for(var i=0; i<n; i++) {
    if(alphabet.indexOf(plaintext.charAt(i)) == -1) {
      window.alert("Znak '" + plaintext.charAt(i) + "' ni v abecedi");
      return;
    }
    var index = (alphabet.indexOf(plaintext.charAt(i)) + alphabet.indexOf(key.charAt(counter + i))) % alphabet.length;
    ciphertext += alphabet.charAt(index);
  }
  color_key(id, counter, counter + n);
  document.getElementById(id).setAttribute("counter", counter + n);
  document.getElementById(id+"_ciphertext_input").value = ciphertext;
}

// Function for decryption of cyphertext.
function decrypt(id) {
  var ciphertext = document.getElementById(id+"_ciphertext_input").value;
  var alphabet = document.getElementById("alphabet").value;
  var n = ciphertext.length;
  if(n == '0') {
    window.alert("Kriptogram je prazen.");
  }
  var counter = parseInt(document.getElementById(id).getAttribute("counter"));
  var key = document.getElementById(id+"_key").getAttribute("key");
  if(key.length - counter < n) {
    window.alert("Neuporabljen del ključa je prekratek.");
    return;
  }
  var plaintext = "";
  for(var i=0; i<n; i++) {
    if(alphabet.indexOf(ciphertext.charAt(i)) == -1) {
      window.alert("Znak '" + ciphertext.charAt(i) + "' ni v abecedi.");
      return;
    }
    var index = (alphabet.indexOf(ciphertext.charAt(i)) - alphabet.indexOf(key.charAt(counter + i)) + alphabet.length) % alphabet.length;
    plaintext += alphabet.charAt(index);
  }
  color_key(id, counter, counter + n);
  document.getElementById(id).setAttribute("counter", counter + n);
  document.getElementById(id+"_message_input").value = plaintext;
}

// It sends ciphertext from one person to another.
function send(from) {
  var to = "alice";
  if(from == "alice") to = "bob";
  var ciphertext = document.getElementById(from+"_ciphertext_input").value;
  document.getElementById(to+"_ciphertext_input").value = ciphertext;

  // send the position of sender's key
  var counter = parseInt(document.getElementById(from).getAttribute("counter"));
  var n = ciphertext.length;
  document.getElementById(to).setAttribute("counter", counter-n);
  color_key(to, counter-n, counter)
}

window.addEventListener("load", function(event) {
  var wrapper = document.getElementById('cchiperWrapper');
  //wrapper.appendChild(new_title("h1", "One Time Pad"));

  // create div common to set alphabet and generate key
  var common = document.createElement('div');
  common.setAttribute("id", "common");

  common.appendChild(new_title("h3", "Abeceda:"));
  var alpha = new_textarea("alphabet");
  alpha.setAttribute("id", "alphabet");
  alpha.value = "abcdefghijklmnopqrstuvwxyz";
  common.appendChild(alpha);
  common.appendChild(document.createElement("br"));
  common.appendChild(new_button("Generiraj ključ", generate_key, "generate_key"));
  wrapper.appendChild(common);

  
  wrapper.appendChild(create_new_person("Ana", "alice"));
  wrapper.appendChild(create_new_person("Bor", "bob"));

  document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("generate_key").click();
      }
});
});
