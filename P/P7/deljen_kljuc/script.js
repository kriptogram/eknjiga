praveVrednosti = []
izbraniKljuci = []
uporabljeniKljuci = []
polinom = []
tockeNaPolinomu = []

velikostBesedila = 16
n = 5;
stopnja = 5;

function resetiraj() {
  dolociKljuc();

  // sestavi izbiro kljucev
  let izbiraVhoda = document.getElementById("izbiraVhoda");
  let innerHTML = "";
  for (let i = 0; i < n; i++) {
    innerHTML += `
    <div>
      <div style="margin-top: 1ex;">Vrednost `+ (i + 1) + `:</div>
      <div class="input-group">
        <div class="form-check">
          <div>
            <input class="form-check-input" type="radio" name="izbira`+ (i) + `" id="izbira` + (i) + `-0" value="neUporabimo" onclick="posodobiRadio(` + (i) + `, 'neUporabimo')">
            <label class="form-check-label" for="izbira`+ (i + 1) + `-1">
              Izračunamo
            </label>
          </div>
        </div>
        <div class="form-check" style="margin-left: 1em;">
          <div>
            <input class="form-check-input" type="radio" name="izbira`+ (i) + `" id="izbira` + (i) + `-1" value="poznamo" checked onclick="posodobiRadio(` + (i) + `, 'poznamo')">
            <label class="form-check-label" for="izbira`+ (i + 1) + `-1">
              Poznamo
            </label>
          </div>
        </div>
        <div class="form-check" style="margin-left: 1em;">
          <div>
            <input class="form-check-input" type="radio" name="izbira`+ (i) + `" id="izbira` + (i) + `-2" value="ugibamo" onclick="posodobiRadio(` + (i) + `, 'ugibamo')">
            <label class="form-check-label" for="izbira`+ (i) + `-2">
              Ugibamo
            </label>
          </div>
        </div>
        <input type="range" class="custom-range" disabled style="margin-left: 1em;" min="-100" max="100" id="ugibanje`+ (i) + `" oninput="posodobiVrednost(` + (i) + `, this.value)">
        <span style="margin-left: 1em;" id="vrednost`+ (i) + `">0</span>
      </div>
    </div>
    `;
  }
  izbiraVhoda.innerHTML = innerHTML;

  // nastavi vrednosti
  for (let i = 0; i < n; i++) {
    nastaviVrednostKljuca(i, praveVrednosti[i]);
  }
  nastaviVeljavnoStanje(-1);
  izracunajPolinom();
}

function dolociKljuc() {
  // izberi kljuce
  let kljuc = 0
  maxDelKljuca = 0;
  minDelKljuca = 0;
  do {
    praveVrednosti = []
    izbraniKljuci = []
    tockeNaPolinomu = []
    for (let i = 0; i < stopnja; i++) {
      let vrednost = Math.floor((Math.random() * 200) - 100);
      praveVrednosti.push(vrednost);
      izbraniKljuci.push(vrednost);
      tockeNaPolinomu.push(vrednost);
    }
    izracunajPraviPolinom();
    maxDelKljuca = 0;
    minDelKljuca = 0;
    for (let i = stopnja; i < n; i++) {
      let del = izracunajY(i + 1);
      if (del > maxDelKljuca) {
        maxDelKljuca = del;
      }
      if (del < minDelKljuca) {
        minDelKljuca = del;
      }
      praveVrednosti.push(del);
      izbraniKljuci.push(del);
    }
    kljuc = izracunajY(0);
    if (kljuc > maxDelKljuca) {
      maxDelKljuca = kljuc;
    }
    if (kljuc < minDelKljuca) {
      minDelKljuca = kljuc;
    }
  } while (maxDelKljuca > 100 || minDelKljuca < -100)
  napisiPodatkeOKljucu(kljuc);
}

function napisiPodatkeOKljucu(vrednostKljuca) {
  document.getElementById("praviKljuc").innerText = Math.round(vrednostKljuca);
  let zapisPolinoma = "";
  for (let i = polinom.length - 1; i >= 0; i--) {
    zapisPolinoma += Math.round(polinom[i]*100)/100;
    if (i > 0) {
      if (i == 1) {
        zapisPolinoma += " x";
      } else {
        zapisPolinoma += " x" + superscript(i);
      }
      zapisPolinoma += " + ";
    }
  }
  document.getElementById("polinom").innerText = zapisPolinoma;
}

function superscript(n) {
  switch(n) {
    case 2:
      return "²";
    case 3:
      return "³"
    case 4:
      return "⁴"
    case 5:
      return "⁵"
    default:
      return n;
  } 
}

function izberiStopnjo(s) {
  if (s > n) {
    document.getElementById("izbiraDelov").value = s;
    n = s
  }
  stopnja = parseInt(s);
  resetiraj();
  document.getElementById("napisStDelov").innerText = n;
  document.getElementById("napisStopnja").innerText = (stopnja-1);
  document.getElementById("napisPotrebnihTock").innerText = (stopnja);
}

function izberiStDelov(stDelov) {
  n = stDelov;
  if (stopnja > n) {
    document.getElementById("izbiraStopnje").value = n;
    stopnja = n
  }
  document.getElementById("napisStDelov").innerText = n;
  document.getElementById("napisStopnja").innerText = (stopnja-1);
  document.getElementById("napisPotrebnihTock").innerText = (stopnja);
  resetiraj();
}

function nastaviSlider(id, vrednost) {
  let vrednostElement = document.getElementById("vrednost" + (id));
  vrednostElement.innerText = vrednost;
  document.getElementById("ugibanje" + (id)).value = Math.round(vrednost);
}

function nastaviVrednostKljuca(id, vrednost) {
  izbraniKljuci[id] = vrednost;
  nastaviSlider(id, vrednost);

  izracunajPolinom();
}

function uporabljamoVrednost(i) {
  return !(document.getElementById("izbira" + (i) + "-0") && document.getElementById("izbira" + (i) + "-0").checked);
}

function posodobiRadio(id, vrednost) {
  document.getElementById('ugibanje' + id).disabled = (vrednost !== 'ugibamo');
  if (vrednost == 'poznamo') {
    nastaviVrednostKljuca(id, praveVrednosti[id]);
  }
  nastaviVeljavnoStanje(id);
  izracunajPolinom();
}

function stUporabljenih() {
  let stUporabljenih = 0;
  for (let i = 0; i < n; i++) {
    if (uporabljamoVrednost(i)) {
      stUporabljenih++;
    }
  }
  return stUporabljenih;
}

function nastaviVeljavnoStanje(neSpreminjaj) {
  for (let i = n - 1; i >= 0 && stUporabljenih() > stopnja; i--) {
    if (i != neSpreminjaj && uporabljamoVrednost(i)) {
      document.getElementById("izbira" + (i) + "-0").checked = true;
    }
  }
}

function posodobiVrednost(id, vrednost) {
  nastaviVrednostKljuca(id, vrednost);
}

function izracunajPraviPolinom() {
  let tocke = [];
  for (let i = 0; i < tockeNaPolinomu.length; i++) {
    tocke.push({
      x: i + 1,
      y: tockeNaPolinomu[i]
    });
  }
  polinom = Lagrange(tocke);
  document.getElementById("kljuc").innerText = Math.round(izracunajY(0));
}

function izracunajPolinom() {
  let tocke = [];
  for (let i = 0; i < izbraniKljuci.length; i++) {
    if (uporabljamoVrednost(i)) {
      tocke.push({
        x: i + 1,
        y: izbraniKljuci[i]
      });
    }
  }
  polinom = Lagrange(tocke);
  document.getElementById("kljuc").innerText = Math.round(izracunajY(0));
  for (let i = 0; i < izbraniKljuci.length; i++) {
    if (!uporabljamoVrednost(i)) {
      nastaviSlider(i, Math.round(izracunajY(i+1)));
    }
  }
  narisiGraf();
}

function izracunajY(x) {
  let rezultat = 0;
  let potencaX = 1;
  for (let i = 0; i < polinom.length; i++) {
    rezultat += potencaX * polinom[i];
    potencaX *= x;
  }
  return rezultat;
}

function narisiGraf() {
  let graph = document.getElementById('graph');
  graph.width = window.innerWidth;
  // graph.height = window.innerHeight;
  let ctx = graph.getContext("2d");
  ctx.lineWidth = 1;
  ctx.clearRect(0, 0, graph.width, graph.height);
  narisiPolinom(ctx, graph, -0.1, parseInt(n) + 0.1, -105, 105);
  narisiOsi(ctx, graph, -0.1, parseInt(n) + 0.1, -105, 105);
  narisiTocke(ctx, graph, -0.1, parseInt(n) + 0.1, -105, 105);
}

function narisiOsi(ctx, graph, minX, maxX, minY, maxY) {
  ctx.beginPath();
  ctx.strokeStyle = "#000000";
  ctx.fillStyle = "#000000";
  ctx.font = velikostBesedila + "px Arial";
  let zacetekX = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, minX, 0);
  let konecX = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, maxX, 0);
  ctx.moveTo(zacetekX[0], zacetekX[1]);
  ctx.lineTo(konecX[0], konecX[1]);

  // oznake X
  for (let x = Math.floor(minX); x <= maxX; x++) {
    let oznaka = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, x, 0);
    ctx.moveTo(oznaka[0], oznaka[1] - 2);
    ctx.lineTo(oznaka[0], oznaka[1] + 2);
    ctx.fillText("" + x, oznaka[0] - (x == 0 ? -velikostBesedila / 3 : velikostBesedila / 3), oznaka[1] + velikostBesedila + 3);
  }

  let zacetekY = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, 0, minY);
  let konecY = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, 0, maxY);
  ctx.moveTo(zacetekY[0], zacetekY[1]);
  ctx.lineTo(konecY[0], konecY[1]);

  // oznake Y
  for (let y = Math.floor(-100); y <= 100; y += Math.floor((100 - (-100)) / 10)) {
    let oznaka = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, 0, y);
    ctx.moveTo(oznaka[0] - 2, oznaka[1]);
    ctx.lineTo(oznaka[0] + 2, oznaka[1]);
    if (y != 0) {
      ctx.fillText("" + y, oznaka[0] + velikostBesedila / 3, oznaka[1] + velikostBesedila / 3);
    }
  }

  ctx.stroke();
  ctx.closePath();
}

function pretvoriVPixle(minX, maxX, minY, maxY, sirina, visina, x, y) {
  return [sirina * (x - minX) / (maxX - minX), visina - visina * (y - minY) / (maxY - minY)];
}

function narisiTocke(ctx, graph, minX, maxX, minY, maxY) {
  for (let i = 0; i < izbraniKljuci.length; i++) {
    let trenutniPixel = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, i + 1, izracunajY(i + 1));
    let pixelNaXOsi = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, i + 1, 0);
    // crta do tocke
    let ugibamo = document.getElementById("izbira" + (i) + "-2") && document.getElementById("izbira" + (i) + "-2").checked;

    ctx.beginPath();
    if (ugibamo) {
      ctx.strokeStyle = "#dc3545";
      ctx.fillStyle = "#dc3545";
    } else if (!uporabljamoVrednost(i)) {
      // te vrednosti ne uporabimo
      ctx.strokeStyle = "#6c757d";
      ctx.fillStyle = "#6c757d";
    } else {
      ctx.strokeStyle = "#28a745";
      ctx.fillStyle = "#28a745";
    }
    ctx.moveTo(trenutniPixel[0], trenutniPixel[1]);
    ctx.lineTo(pixelNaXOsi[0], pixelNaXOsi[1]);
    ctx.stroke();
    // besedilo na sredi crte
    ctx.font = velikostBesedila + "px Arial";
    ctx.fillText("" + Math.round(izracunajY(i + 1)), (trenutniPixel[0] + pixelNaXOsi[0]) / 2 + 5, (trenutniPixel[1] + pixelNaXOsi[1]) / 2 + velikostBesedila / 3);
    ctx.closePath();

    // tocka
    ctx.beginPath();
    ctx.fillStyle = "#000000";
    ctx.arc(trenutniPixel[0], trenutniPixel[1], 5, 0, 2 * Math.PI);
    ctx.fill();
    ctx.closePath();
  }

  let trenutniPixel = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, 0, izracunajY(0));
  ctx.beginPath();
  ctx.fillStyle = "#dc3545";
  ctx.arc(trenutniPixel[0], trenutniPixel[1], 5, 0, 2 * Math.PI);
  ctx.fill();
  ctx.closePath();
}

function narisiPolinom(ctx, graph, minX, maxX, minY, maxY) {
  ctx.beginPath();
  let prejsnjiX = minX;
  let prejsnjiY = izracunajY(minX);
  let zacetekPixel = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, prejsnjiX, prejsnjiY);
  ctx.moveTo(zacetekPixel[0], zacetekPixel[1]);
  for (let x = minX + 0.1; x <= maxX + 0.1; x += 0.05) {
    let y = izracunajY(x);
    let trenutniPixel = pretvoriVPixle(minX, maxX, minY, maxY, graph.width, graph.height, x, y);
    ctx.lineTo(trenutniPixel[0], trenutniPixel[1]);
  }
  ctx.stroke();
  ctx.closePath();
}

window.addEventListener('load', function () {
  resetiraj();
  document.getElementById("izbiraDelov").value = n;
  document.getElementById("izbiraStopnje").value = stopnja;
});