
function mainDe(event){
  //shift == x
  var shift = -3;
  
  var submitelement = document.querySelector('#ceaserDecrypt .do'); // Append Submit Button
  submitelement.addEventListener("click", function(event) {
    
    var input = document.querySelector('#ceaserDecrypt #chipertext');
    textDec=input.value.split('');

    var div = document.createElement('div');
    div.classList.add('output');
    
    i = 0;
    while (i < textDec.length){
      if(textDec[i].trim())
        div.innerHTML += caesarShift(textDec[i],shift);
      else
        div.innerHTML += textDec[i];
      i = i+1;
    }
    var container = document.getElementById("ceaserDecrypt");
    var elements = container.getElementsByClassName("output");

    while (elements[0]) {
      elements[0].parentNode.removeChild(elements[0]);
    }
    div.classList.add('blue');
    document.getElementById('ceaserDecrypt').appendChild(div);
  });

  var restartelement = document.querySelector('#ceaserDecrypt .restart'); // Restart Button
    restartelement.addEventListener("click", function(event) {
      
      var container = document.getElementById("ceaserDecrypt");
      var elements = container.getElementsByClassName("output");

      while (elements[0]) {
          elements[0].parentNode.removeChild(elements[0]);
      }
    });
}

textDec = [];

//include common js file
window.addEventListener("load", function(eventW) {
  //$.getScript("js/ceaserChiper.js", function(){ mainDe(eventW); });
  document.getElementById('ceaserDecrypt').innerHTML = '<div id="wrapper">'+
  '<div class="main">'+
  '<div class="container"><h2>Obrazec za odšifriranje</h2>'+
  '<hr>'+
  '<label>Vnesi tajnopis za odšifriranje: </label>'+
  '<textarea class="form-control" name="chipertext" id="chipertext"></textarea>'+
  '<input class="btn btn-secondary do" type = "submit" id="button" value="Začni" dname="Start">'+
  '<input class="btn btn-secondary restart do" type = "submit" id="button" value="Briši" dname="Restart">'+
  '</div>'+
'</div>'
  mainDe(eventW);
});
