textDec = [];

function mainEn(event){
  //shift == x
    var shift = 3;
    
    var submitelement = document.querySelector('#ceaserEncrypt .do'); // Submit Button
    submitelement.addEventListener("click", function(event) {
      
      var input = document.querySelector('#ceaserEncrypt #plaintext');
      textEnc=input.value.split('');
  
      var div = document.createElement('div');
      div.classList.add('output');
      
      i = 0;
      while (i < textEnc.length){
        if(textEnc[i].trim())
          div.innerHTML += caesarShift(textEnc[i],shift);
        else
          div.innerHTML += textEnc[i];
        i = i+1;
      }
      var container = document.getElementById("ceaserEncrypt");
      var elements = container.getElementsByClassName("output");

      while (elements[0]) {
          elements[0].parentNode.removeChild(elements[0]);
      }
      div.classList.add('blue');
      document.getElementById('ceaserEncrypt').appendChild(div);
    });

    var restartelement = document.querySelector('#ceaserEncrypt .restart'); // Restart Button
    restartelement.addEventListener("click", function(event) {
      
      var container = document.getElementById("ceaserEncrypt");
      var elements = container.getElementsByClassName("output");

      while (elements[0]) {
          elements[0].parentNode.removeChild(elements[0]);
      }
    });
}


//https://dillinger.io/
window.addEventListener("load", function(eventW) {
  //$.getScript("js/ceaserChiper.js", function(){ mainEn(eventW);});
  document.getElementById('ceaserEncrypt').innerHTML = '<div id="wrapper">'+
  '<div class="main">'+
  '<div class="container"><h2>Obrazec za šifriranje</h2>'+
  '<hr>'+
  '<label>Vnesi čistopis za šifriranje: </label>'+
  '<textarea class="form-control" name="plaintext" id="plaintext"></textarea>'+
  '<input class="btn btn-secondary do" type="submit" value="Začni" dname="Start" id="button">'+
  '<input class="btn btn-secondary restart do" type="submit" value="Briši" dname="Restart" id="button">'+
  '</div>'+
'</div>'
  mainEn(eventW);
});
