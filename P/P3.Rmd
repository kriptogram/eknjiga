# Kode

```{r filtriranjeVsebine, echo=F}
source('params.R')
```

## Uvod {-}
TO-DO

## Napake niso za vedno
<!--
Presek, zgoščenke, planeti in kode 10. apr., 2003 -->

<!-- =========================== 1se =========================== -->
V času informacijske tehnologije (računalniki, mobilni telefoni,
bančne kartice, internet) se vsi dobro zavedamo pomena hitrega in natančnega
prenosa, obdelovanja in hranjenja informacij.
<!-- -->
Še tako popolne naprave delajo napake, le-te pa lahko hitro spremenijo
sicer izredno koristno programsko in strojno opremo v ničvredno ali
celo nevarno orodje.
<!-- -->
Dolgo časa so se ljudje trudili izdelati računalnike in pomnilnike,
ki bodo naredili oziroma vsebovali, kar se da malo napak. Seveda so
bile zato cene takih izdelkov vedno višje. Potem pa so se domislili,
da bi raje računalnike same naučili iskati in odpravljati napake.
<!-- Tako je postala proizvodnja veliko učinkovitejša in cenejša. -->
Za povečanje zanesljivosti prenosa in obdelave informacij smo dolgo
časa uporabljali kontrolne bite (angl. parity-check bits), kot
npr. pri številki bančnega čeka, ki pa so služili le za odkrivanje napak.
<!-- -->
Ko je matematik Richard Hamming vnašal v računalnik programe
z luknjačem kartic in mu je nato računalnik večkrat zavrnil
paket kartic zaradi napak, se je zamislil:
<br>

<blockquote class='quote'>
"Če zna računalnik sam odkriti napako, zakaj ne zna najti tudi
njenega mesta in je odpraviti."
</blockquote> <br>

```{r slikaCD, out.width=1000, echo=FALSE, fig.cap="CD", fig.align="center"}
include_graphics("static/img/scd.png")
```

Resnično nas pogosto bolj zanima, kaj lahko storimo, če pride do
tovrstnih napak, saj so računalniki začeli prevzemati večino dela na
področju obdelovanja informacij in pri telekomunikacijah.
<!-- -->
Na začetku so bili računalniški programi dovolj enostavni, tako da so
tehnične napake (ponavadi je odpovedala elektronka) hitro postale očitne.
Toda z razvojem strojne opreme so postajali programi vse obsežnejši
in bolj zapleteni, s tem pa je postalo upanje,
da bi lahko hitro opazili majhne napake, ki spremenijo delovanje naprave,
zanemarljivo in zato tudi resna skrb.
<!-- -->
Možnost, da se nam izmuzne kakšna napaka, je vse večja tudi zato, ker
so elektronska vezja iz dneva v dan manjša, računalniki pa vse hitrejši.
<!-- -->
Tudi če je možnost napake ena sama milijardinka (npr. industrijski
standard za trde diske je ena napaka na 10 milijard bitov),
se bo računalnik, ki opravi 2 milijardi osnovnih operacij na sekundo,
in tak računalnik je prav dolgočasno počasen glede na superračunalnike,
zmotil približno dvakrat na sekundo. Glede na količino podatkov, ki jih
obdelujemo dandanes, je to pravšnji recept za vsakodnevne nevšečnosti.

*Kaj lahko torej storimo?*

<!-- -->
Raziskovalci so našli odgovor v kodah za odpravljanje napak.
**Koda** je skupina simbolov, ki predstavlja informacijo.
Kode obstajajo že tisočletja. To so npr. hieroglifi, grška abeceda, rimske
številke ali pa genetska koda za sestavljanje ribonukleinskih kislin.
Nastale so za različne potrebe: za zapis govora ali glasbe,
Morsejeva abeceda za prenos informacij, za shranjevanje podatkov itd.
<!-- -->
Matematična teorija kod, ki se je razvila v zadnjih petdesetih letih,
je računalnikarjem in inženirjem omogočila, da sestavijo sisteme, ki
so kar se da zanesljivi (kolikor pač dopušča strojna oprema).
<!-- -->
Tako teorija kodiranja lahko predstavlja varnostno mrežo, svojevrstno
matematično zavarovanje pred muhastim materialnim svetom, v katerem
živimo.
<br>
<br>

<blockquote class='quote'>
Tehnologija kod za popravljanje napak je danes tako razširjena kot
diski/zgoščenke (CD), glej sliko \@ref(fig:slikaCD).
Omogoča nam, da poslušamo priljubljeni Mozartov ali Madonnin CD
brez kakršnih koli motenj, četudi nam ga mačka prav pošteno spraska.
</blockquote>
<br>
<br>
Enako tehnologijo uporabljajo za komunikacijo tudi vesoljske ladje in
sonde,
<!-- (deep-space probes) -->
ki raziskujejo naše osončje. Kode za odpravljanje napak omogočajo,
da kljub elektromagnetnim motnjam pridejo na Zemljo kristalno jasni
posnetki oddaljenih planetov, pri tem pa za prenos porabimo
manj energije kot hladilnikova žarnica (gre torej za šepetanje,
ki mora prepotovati več milijard kilometrov).

V nadaljevanju tega poglavja bomo najprej predstavili enostavnejše kode
za odpravljanje napak, npr. kode s ponavljanjem in Hammingove kode,
nato pa bomo spoznali še poenostavljeno varianto t.i. Reed-Salomonovih
kod. Nazadnje si bomo ogledali osnove kodiranja na zgoščenkah.
<!-- SASA Zaključimo pa z nekaj nalogami.
Org. je samo za Presekov članek in jo je potrebno dopolniti -->


<!-- =========================== 2se =========================== -->
### Preproste kode za odpravljanje napak {-}

<div class="container-fluid">
<div class="row">
<div class="col-sm-8" id="nopadcol">

<!-- SASA poisci ref za Shannonovo teorijo: -->
Claude Shannon je kmalu po koncu druge svetovne vojne postavil
teoretične osnove teoriji informacij in zanesljivemu prenosu digitalnih
podatkov.
<!-- -->
Leta 1948 pa je Richard Hamming izumil metodo za
*popravljanje* ene napake in *odkrivanje* dveh napak.
<!-- -->
Bistvo vseh metod za odpravljanje napak je dodajanje kontrolnih bitov.
Najenostavnejša koda za odpravljanje napak je zasnovana na
*ponavljanju*.
Če npr. pričakujemo, da pri prenosu ne bo prišlo do več kot
ene same napake, potem je dovolj, da vsak bit ponovimo trikrat in pri
sprejemu uporabimo "večinsko pravilo" (zelo kratko "simfonijo"
1101 zakodiramo v
111 111 000 111). Če prejmemo
111 011 000 111, popravimo sporočilo v
111 111 000 111 in ga nazadnje še odkodiramo v 1101.
<!-- % $$
% 1011 \tosliko   (kodiranje)
% 111 000 111 111 \to (prenos)
% 111 000 011 111 \to (odkodiranje)
% 111 000 111 111 \to
% 1011
% $$
-->
V splošnem lahko odpravimo $n$ napak z $(2n+1)$-kratnim ponavljanjem
in uporabo večinskega pravila.
Toda ta metoda je preveč potratna. V času, ko si želimo hitrega
prenosa čim večje količine podatkov, je takšno napihovanje večinoma
nesprejemljivo.
<!-- -->
Namesto tega si želimo dodati manjše število kontrolnih bitov,
ki pa naj bodo ravno tako, ali pa morda še bolj, učinkoviti.

Najpreprostejši primer Hammingove kode za odpravljanje napak
predstavimo kar z Vennovim diagramom za tri množice,
<!-- s pomočjo Presekovega znamenja, -->
glej sliko \@ref(fig:slikaPresek).

<!-- SASASL pres1.eps,height=8cm (a) (b) -->

<!-- ```{r out.width=800, echo=FALSE}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/pres1.png"))
``` -->

<!--
<figure>
<img src="./static/img/pres1.png" style="width:50%">
<figcaption><small>
-->

<!-- </small></figcaption></figure> -->

<!-- SASA
The information rate $R=k/n$, where $n$ symbols are transmitted and
$k$ is the lenght of the message ($r=n-k$ are the check bits).
-->
S Hammingovo kodo nam je uspelo zmanjšati število kontrolnih bitov z
8 na 3, dobili smo torej kodo z *informacijsko stopnjo* 4/7 namesto
prejšnjih 4/12=1/3.
<!-- -->
Pravkar opisano kodo lahko seveda posplošimo. Običajno to storimo
z nekaj linearne algebre (matrike), glej [3].
<!-- -->
Hammingova koda odkrije, da je prišlo do napake pri prenosu tudi,
kadar je prišlo do dveh napak, saj ne morejo vsi trije krogi vsebovati
obeh polj, na katerih je prišlo do napake.
<!-- -->
Če pa na dveh mestih zaznamo, da simbola manjkata - npr. da ju ne
moremo prebrati (angl. erasure), potem znamo ti dve mesti celo
popraviti.
<!--
Če bi tekst samo podvojili, bi dobili kodo z informacijsko stopnjo
1/2, ki pa lahko odkriva samo samostojne napake, ne zna pa jih
odpravljati. -->

V grobem lahko rečemo, da je cilj teorije kodiranja najti primerno
ravnovesje med skromno metodo nadzora z nekaj kontrolnimi biti in
razsipno metodo s ponavljanji.
<!-- -->
Hammingova koda predstavlja prvi korak v to smer.
</div>
<div class="col-sm-4" style="background-color:rgba(225, 225, 225, 0.3);">
<br>
```{r slikaPresek, out.width=800, echo=FALSE, fig.cap="Vennov diagram za tri množice", fig.align="center"}
include_graphics("static/img/pres1.png")
```
<font color="dimgray">
<!-- SAS2NA: Quote seveda ni pravi stil - bolje bi bilo, če bi nam uspelo to škatlo spraviti poleg slike
in v njej pomanjšati velikost fonta -->
(a) Našo kratko "simfonijo"
$1101$ spravimo tokrat zaporedoma na
rjavo (1), zeleno (2), oranžno (3) in vijoličasto polje (4), preostala polja
pa dopolnimo tako, da bo v vsakem krogu vsota števil *soda*.
Dobimo kodo $1101001$, kjer zadnja tri mesta predstavljajo zaporedoma
rumeno (5), rdeče (6) in modro polje (7).
<!-- -->
Naštejmo vseh $16$ kodnih besed, ki jih lahko dobimo na ta način:
$0000000$, $0001011$, $0010110$, $0011101$,
$0100101$, $0101110$, $0110011$, $0111000$,
$1000111$, $1001100$, $1010001$, $1011010$,
$1100010$, $1101001$, $1110100$, $1111111$.
<br>
(b) Recimo, da je prišlo do ene same napake in da smo prejeli zaporedje
$1111001$ . Potem bo prejemnik lahko ugotovil, da je
napaka v rumenem (levem) in rdečem (desnem) krogu, ne pa v modrem
(zgornjem), kar pomeni, da je potrebno popraviti oranžno polje (3).
<!-- -->
Ni se težko prepričati, da je mogoče na tak način odpraviti napako
na poljubnem mestu (tudi kontrolnem), seveda ob pogoju, da je to edina
napaka.
</font>
</div>
</div>
</div>

<!-- =========================== 3se =========================== -->
### Reed-Salomonove kode {-}

<div class="container-fluid">
<div class="row">
<div class="col-sm-9" id="nopadcol">

Po odkritju Hammingove kode je sledilo obdobje številnih poskusov s
kodami za odpravljanje napak. Ko je bila teorija kod stara deset let,
sta Irving Reed in Gustave Salomon (takrat zaposlena v Lincolnovem
laboratoriju na MIT) zadela v polno.
<!-- -->
Namesto ničel in enic sta uporabila skupine bitov, ki jim tudi v
računalništvu pravimo kar *besede*. Ta izbira je pripomogla k
odpravljanju grozdnih napak, tj. napak, pri katerih se pokvari več
zaporednih bitov. Če delamo npr. s skupinami, sestavljenimi iz osmih
bitov, potem celo devet zaporednih napak lahko pokvari največ dve
besedi. Reed-Salomonova koda (na kratko RS-koda) za odpravljanje dveh
napak torej predstavlja že precej dobro zaščito.
<!--
Današnje implementacije RS-kod v CD tehnologiji lahko
odpravijo grozdne napake dolžine do celo 4000 bitov. -->


Naj bo $(a_0,a_1,\dots,a_{10})$, $a_i\in \mathbb{Z}_{13}$, sporočilo,
ki ga želimo prenesti. Nadomestimo ga s 13-terico
<!-- % (a_0,a_1,\dots,a_{10}) \to (c_0,c_1,\dots,c_{10},c_{11},c_{12})  -->
$(c_0,c_1,\dots,c_{10},c_{11},c_{12})$,
tako da je $c_i=a_i$ za $i\in \{0,\dots,10\}$, besedi
$c_{11},c_{12}\in \mathbb{Z}_{13}$ pa izračunamo
<!-- (kot smo se naučili v prejšnjem razdelku)  -->
iz enačb

<!-- \begin{equation} \label{cc} -->
$$
c_0+c_1+\cdots+c_{12} \mod 13  = 0\ \ \ \ \mbox{in} \ \ \ \ \
c_1+2c_2+\cdots+12c_{12} \mod 13 = 0,
$$
in pošljemo.
<!-- s pomočjo zgornjih tabel. -->
Predpostavimo, da smo prejeli zaporedje
$(r_0,r_1,\dots,r_{10},r_{11},r_{12})$, pri čemer je prišlo kvečjemu
do ene napake. Če je do napake prišlo pri prenosu besede $c_i$, potem
velja $r_j = c_j$ za vse $j \in \{0,1,\dots,12\}\backslash \{i\}$ in
$r_i = c_i+e$ za neki $e\in \{1,\dots,12\}$.
<!-- Iz $e \equiv\sum_{i=0}^{11} r_i\equiv \!\!\!\!\!\!\! / \ \ 0 (\mod 13)$ -->
Iz
$$
e = r_0+r_1+\cdots+r_{12} \mod{13}\ne 0
$$
ugotovimo, da je prišlo do napake.
Nato pa iz
$$
r_1+2r_2+\cdots+12r_{12} \equiv ie \pmod{13}
$$
z deljenjem z $e$ izračunamo še $i$, ki nam pove, na katerem mestu
moramo odšteti $e$, da odpravimo napako. Prišli smo do [13,11]-kode
z informacijsko stopnjo $11/13$, ki zna popraviti eno napačno besedo.


*Primer.* Naj bo $a=(1,3,8,2,7,0,1,9,11,4,12)$. Potem iz
<!-- (\ref{cc}) -->
zgornje enačbe dobimo $6+c_{11}+c_{12}=0 \mod 13$ in
$2-2c_{11}-c_{12}=0 \mod 13$ oziroma
$c_{11}=8 \mod 13$ in $c_{12}=12 \mod 13$, torej je
$c=(1,3,8,2,7,0,1,9,11,4,12,8,12)$.
<!-- -->
Recimo, da smo dobili $r=(1,3,8,2,1,0,1,9,11,4,12,8,12)$.
<!-- -->
Iz $r_0+r_1+\cdots+r_{12}= 7$ zaključimo, da je $e=7$,
nato pa iz $r_1+2r_2+\cdots+12r_{12} = 2$ še $2/7 \equiv 4\pmod{13}$.
<!-- -->
Slednje deljenje smo seveda izvedli v končnem obsegu, glej [2].
<!-- -->
Pravzaprav moramo številu 2 v števcu prištevati 13, vse dokler se
deljenje ne izide. Torej, ker 2+13=15 še ni deljivo s 7, poskusimo z
2+13+13=28 in dobimo 4, kar pomeni, da je potrebno na mestu z indeksom
4 odšteti napako 7.

Reed-Salomonove kode lahko obravnavamo veliko bolj splošno in pridemo
tudi do tistih, ki odpravljajo dve in več napak. Vendar pa za to
potrebujemo še več matematičnega znanja, ki presega naš okvir,
pa tudi računanje v praštevilskem obsegu $\mathbb{Z}_p$ je potrebno
nadomestiti z računanjem v končnem obsegu GF$(2^n)$. Glej [5].
<!-- -->
Poleg zgoščenk se RS-kode uporabljajo tudi v telekomunikacijah.
<!-- -->
Ena izmed najpomembnejših uporab je bila kodiranje digitalnih slik, ki
sta nam jih na Zemljo pošiljala vesolski sondi  Voyager 1 in 2.
</div>
<div class="col-sm-3" style="background-color:rgba(225, 225, 225, 0.3);">
<br>
```{r slikaVoyager, out.width=400, echo=FALSE, fig.cap="(a) Voyager", fig.align="center"}
include_graphics("static/img/voyager.png")
```

```{r slikaJupiter, out.width=400, echo=FALSE, fig.cap="(b) Jupiter", fig.align="center"}
include_graphics("static/img/jupiter.png")
```

<font color="dimgray">
(a) Voyagerja sta leta 1979 poslikala Jupiter, leta 1980/81
Saturn, nato pa je Voyager 2 poslikal leta 1981 Uran in leta 1989 Neptun,
http://nssdc.gsfc.nasa.gov/planetary/voyager.html. \
<br>
(b) Jupiter in njegovi Galilejski sateliti, Io, Evropa,
Ganimed in Kalisto (montaža),
http://nssdc.gsfc.nasa.gov/photo_gallery/photogallery-jupiter.html.
</font>
</div>
</div>
</div>

<!--
%                    Voyager 1                            Voyager 2
%          Distance       Date/Time        Distance         Date/Time
% Jupiter 4.89 Rj March 5, 1979 12:05:26 10.11 Rj July  9, 1979 22:29:51
% Saturn  3.09 Rs Nov. 12, 1980 23:46:30  2.67 Rs Aug. 26, 1981 03:24:57
% Uranus    ---            ---            4.19 Ru Jan. 24, 1986 17:59:47
% Neptune   ---            ---            1.18 Rn Aug. 25, 1989 03:56:36
-->

<br>

<!-- =========================== 4se =========================== -->
### Zgoščenke - CD {-}

Zapisovanje glasbe na CD-je je prevzelo ljubitelje glasbe
tako rekoč čez noč. Visoko kvaliteto predvajanja
zvoka je poleg laserske tehnike v veliki
meri pripisati kodam za odpravljanje napak.
Oglejmo si način zapisa CD nekoliko pobližje,
slika \@ref(fig:slikaHD).

<!-- SASASL slike/GN-CD-LandAndPits-A.ps,width=5cm}
hd.eps,width=12cm                                -->

<div class="container-fluid">
<div class="row">
<div class="col-sm-9" style="background-color:rgba(225, 225, 225, 0.3);">
<br>
```{r slikaHD, out.width=800, echo=FALSE, fig.cap="CD ima premer 12 cm in debelino 1.2 mm, njegova osnova pa je iz prozorne plastike.", fig.align="center"}
include_graphics("static/img/hd.png")
```
</div>
<div class="col-sm-3" style="background-color:rgba(225, 225, 225, 0.3);">
<font color="dimgray">
<br>
Na popisanem CD-ju se nahaja spirala, ki se začenja iz notranjosti in
je sestavljena iz hribčkov in dolinic. Branje poteka z laserjem (žarek
ima premer 1 mirkon), ki zazna spremembo višine spirale (preko jakosti
svetlobe, ki se odbije od diska, odboj svetlobe je ali svetlejši ali
temnejši, saj je razlika v višini približno 1/4 valovne dolžine
svetlobe v disku). Na ta način dobimo dvojiško zaporedje, vsaka
sprememba ustreza številu 1, odsotnost spremembe višine pa ustreza
številu 0.
</font>
</div>
</div>
</div>
<br>
<!-- **trije žarki namesto enega**
(za prilagajanje zrcala, če krogi niso čisto koncentrični
 in za prilagajanje razdalje laserja do diska). -->

1. Proces razdelimo na faze: digitalno/analogni konverter,
kode in modulacija, glej sliko \@ref(fig:slikaKod).

2. Pri kodiranju bomo na bajte gledali kot na elemente iz obsega
GF$(2^8)$, glej [2].
<!-- -->
V stereotehniki posnamemo 4 bajte
<!-- $m_{4t}$, $m_{4t+1}$, $m_{4t+2}$ in $m_{4t+3}$ -->
na vsak "tik", ki pomeni 1/44.100-ti del sekunde.
Meritev 6-ih zaporednih tikov
<!-- $m_{24t},m_{24t+1},\dots ,m_{24t+23}$ -->
združimo v sporočilo
<!-- $M_t$  -->
dolžine $24$ bajtov,
<!--
Naj bo $C$ Reed-Salomonova koda RS$(2^8,5)$. Potem zakodiramo $M_t$ v
kodno besedo $c_t$, z uporabo kode $C_1=C(227)$, tj. skrajšane
RS-kode nad obsegom GF$(2^8)$ s parametri $(n_1,k_1,d_1)=(28,24,5)$. -->
ki ga v dveh korakih zakodiramo v 32 bajtov.  Najprej uporabimo
[28,24]-RS kodo, ki zna popraviti dve napaki, in nato še
[32,28]-RS kodo, ki tudi zna popraviti dve napaki.
<!-- -->
Tem 32-im bajtom dodamo še 33. bajt, ki je števec pesmi.
Le-tega običajno vidimo na zaslonu predvajalnika.

3. Do sedaj so bili vsi omenjeni bajti bodisi nosilci informacije bodisi
so bili dodani kot kontrola za odkrivanje in popravljanje napak.
Z besedami smo računali za potrebe kodiranja, sedaj pa se
zaradi fizikalnih lastnosti materialov spustimo na nivo bitov.
<!-- -->
Potrebno je paziti, da zaporedne enice niso ne preveč blizu (med njima
morata biti vsaj dve ničli) in ne preveč narazen (med njima je lahko
največ deset ničel).
<!-- -->
RS-kode seveda nimajo takih lastnosti, vendar pa ima to lastnost natanko
267 dvojiških besed dolžine 14 (preverjeno z računalnikom).
Zato preslikamo 256 elementov končnega obsega s pomočjo dobro izbrane
tabele v 256 besed, preostalih 11 besed pa zavržemo.
Ta postopek se imenuje EFM (angl. eight to fourteen modulation).
Da pa bi zgoraj omenjena lastnost veljala tudi med besedami dolžine 14,
mednje postavimo še tri dodatne bite (ničle ali enice; pri tem pazimo
še, da sta števili doslej prebranih ničel in enic čim bolj skupaj)
in dobimo kodne besede dolžine $33\cdot 17=561$ bitov.
<!-- -->
Končno, da zabeležimo začetek novega niza, priključimo vsaki kodni
besedi še 27 sinhronizacijskih bitov, ki imajo zgornjo lastnost in so
izbrani tako, da nikakor ne morejo sestavljati kodirnega podzaporedja.
<!-- -->
Ena sekunda glasbe se torej pretvori v
$(561+27)\cdot 44.100/6= 4.321.800$ bitov glasbe na spirali.
<!-- 74 minut pa v $74\cdot 60 \cdot 4.321.800=19.188.792.000$ bitov. -->

<!-- SASASL -->

<!-- img src='./static/img/kod.png' -->
<!-- out.width='80%' -->
<!-- https://poldham.github.io/minute_website/images.html -->

<div class="container-fluid">
<div class="row">
<div class="col-sm-9" style="background-color:rgba(225, 225, 225, 0.3);">
<br>
```{r slikaKod, out.width=800, echo=FALSE, fig.cap="Postopek kodiranja in odkodiranja v treh korakih", fig.align="center"}
include_graphics("static/img/kod.png")
```
</div>
<div class="col-sm-3" style="background-color:rgba(225, 225, 225, 0.3);">
<font color="dimgray">
Med snemanjem izmerimo glasbo 44.100-krat na sekundo,
po enkrat na levi in enkrat na desni.
Amplitudo zvoka opiše naravno število med 0 in $2^{16}\!-\!1$,
ki ga predstavlja v dvojiškem sistemu 16 bitov.
Ker gre za stereo glasbo, dobimo pri vsaki meritvi 32 bitov
(t.i. audio-biti) oziroma 4 bajte podatkov.
</font>
</div>
</div>
</div>
<br>
<!-- -->
Pri branju zgoščenke se lahko pojavi tudi čez 100.000 napak.
Le-te lahko povzročajo nezaželeni delci, mikroskopski mehurčki v
plastiki, prstni odtisi, praske ali celo manjše luknje v CD-ju ipd.
<!-- -->
Za primerjavo vzemimo knjigo z 200 stranmi, izpisano v pisavi, ki
dopušča 3000 znakov na stran.
<!-- % ($1=a,2=b,\dots, 26=z,
%  0=\mbox{"$\ $"},
% 27=\mbox{","},
% 28=\mbox{"."},
% 29=\mbox{":"},
% 30=\mbox{"?"}$).
-->
Recimo, da je tiskalnik le $99,9\%$ zanesljiv.
V povprečju lahko torej pričakujemo do 3 napake na stran.
<!-- -->
Vrnimo se k CD-jem.
Če bi bila verjetnost, da predvajalnik prebere napačen bit, enaka
$10^{-4}$, bi še vedno imeli na stotine napak vsako sekundo.
<!-- -->
Kvaliteto zvoka seveda izboljšajo kode. Napake se običajno
pojavijo v gručah (t.i. grozdne napake).
Da zmanjšamo njihov vpliv, je kodiranje narejeno v dveh korakih.
Pri tem druga koda kot vhod uporabi nekoliko prepleten izhod prve kode,
tako da bajti, ki so sosedni v kodnih besedah (glasbi), niso sosedni
tudi na disku. Kodirna shema za CD-je se imenuje CIRC (Cross-Interleaved
Reed-Salomon Scheme Code) in pravilno odpravi vse grozdne napake do
dolžine 8.871 bitov, kar ustreza približno 2,5 mm spirale na CD-ju.

Ogledali smo si le nekaj najbolj osnovnih vidikov kodirnega procesa,
praksa pa je seveda še nekoliko bolj zapletena reč.

<!-- =========================== 5se =========================== -->
**Viri in dodatno branje**

[1] B. Cipra, The Ubiquitous Reed-Solomon Codes,
           *SIAM News* **26**/1 (1993)
<!-- %http://www.cs.utk.edu/~shuford/terminal/reed_solomon_codes.html -->
<!-- http://www.siam.org/siamnews/mtc/mtc193.htm. -->

[2] A. Jurišić, Računala nove dobe, 2. del,
*Presek* **30** (2002-03), str. 291-296.

[3] S. Klavžar, O teoriji kodiranja, linearnih kodah in slikah
z Marsa, *Obzornik mat. fiz.* **45** (1998) 4, str. 97-106.

[4] Več avtorjev,
           For all practical purposes: introduction to contemporary
           mathematics, 5. izdaja, New York, W. H. Freeman, 2000.

[5] A. Jurišić in A. Žitnik, Reed Solomonove kode, *OMF* **51** (2004),
129-143
(http://lkrv.fri.uni-lj.si/popularizacija/omf/reed_solomonove_kode.pdf).
<!-- SASASASA -->

## Barkode
<!-- (Denis) -->

Program za delovanje potrebuje vpis 12 meste številke. Po vpisu števlke pritisnemu tipko nariši, ki nam na to nariše na zaslon
bar kodo, ki je sestavljena iz 95 črtic. Posamezna črtica je lahko 1 (črne barve) in 0 bele barve.
Po pritisku na tipko uporabnik lahko s pomočjo premika miške po bar kodi spremlja kako se je ta del bar kode narisal.
Z desne strani sta prikazene dve tabeli, ki se sproti spreminjata pri premiku miške ali vpisu nove bar kode.
Od spodej sta dva tekstovna okna. V prvem je napisan pomen prve tabele in kako pridemo do check številke, ki je zadnja številka oz.
13 številka vpisane bar kode. V drugem pa je opisan točno določen del bar kode, kjer trenutno kaže miška.
<iframe src="./P3/BarKodaV2/test.html" id = 'barKoda0-iframe'></iframe>

<iframe src="./P3/barKoda/barKoda1/barKoda1.html" id = 'barKoda1-iframe'></iframe>
<iframe src="./P3/barKoda/barKoda2/barKoda2.html" id = 'barKoda2-iframe'></iframe>
<iframe src="./P3/barKoda/barKoda3/barKoda3.html" id = 'barKoda3-iframe'></iframe>

<!--
## Abecede

### Učenje novih adeced

Start writing in one of the fields. Clicking on the enter button,
conversion is done. In case of the problem, please contact us.

<iframe src="./P3/pretvornik_med_abecedami/pretvornik.html" id = 'pretvornik-iframe'></iframe>
<iframe src="./P3/ucenje_abeced_s_kartami/karte.html" id = 'karte-iframe'></iframe>
-->

## Kodiranje binarnih datotek z besedilom
<iframe src="./P3/ascii_kodiranja/ascii_kodiranja.html" id='asci-kodiranja-iframe'></iframe>

## Kodiranja za odpravljanje napak
<iframe src="./P3/kode_za_odpravljanje_napak/kode_za_odpravljanje_napak.html" id='odpravljanje-napak-iframe'></iframe>

## Linijske kode
<iframe src="./P3/linijske_kode/dist/index.html" id='linijske-kode-iframe' style="height: 110em;"></iframe>

<script>
  iFrameResize({ log: true, warningTimeout: 10000}, '#barKoda0-iframe, #barKoda1-iframe, #barKoda2-iframe, #barKoda3-iframe, #pretvornik-iframe, #karte-iframe, #asci-kodiranja-iframe, #odpravljanje-napak-iframe, #linijske-kode-iframe')
</script>
