## Automatic deployment
    
*  defined in `.gitlab-ci.yml`
*  https://kriptogram.gitlab.io/eknjiga/
    

## Style - colors
Depending on the Chapter in which your interaction fits, use the color fro m the list below as a base/main color of the application. For all color issues contact @NinaHostnik

- P1 `#01A3D9`
- P2 `#4CC3C6`
- P3 `#3CBC8D`
- P4 `#056B8D`
- P5 `#268080`
- P6 `#1F7857`
- P7 `#003A4D`
- Loto / Generator naključnih števil `#134040`
- Verige / Bločne verige `#0F3829`

## Build the ebook 

```
# Get the current git directory from source (if not having one).
git clone https://gitlab.com/kriptogram/eknjiga.git
cd eknjiga/
git checkout -b my-new-build-branch # checkout new branch 
cd P/
# vi P2.Rmd # optional: make changes.
./build.command 

# for npm build run
./build.command --npm

# Observe the ebook built: Open index.html from _crypto_eknjiga/
cd _crypto_eknjiga/
#firefox index.html  #insted of firefox, use yor own browser. Or open in Finder/File manager.
```
