Some interesting links:

1.  [C++ interactivity within RStudio](https://support.rstudio.com/hc/en-us/articles/200486088-Using-Rcpp-with-RStudio)
2.  [Debugging Shiny apps](https://shiny.rstudio.com/articles/debugging.html)
3.  [Python in javascript](https://stackoverflow.com/questions/32288722/call-python-function-from-js?noredirect=1&lq=1)