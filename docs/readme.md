Some useful links:

1. GIT:
    1. [Git Rebasing with example](https://git-scm.com/book/en/v2/Git-Branching-Rebasing)
    2. [Style you RMarkdown](https://github.com/holtzy/)
    3. [Git Merge](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
 
2. RMarkdown + Shiny:
    1. [Using html templates with Shiny](https://shiny.rstudio.com/articles/templates.html)
    2. Get the current dir inside embeded shiny app, e.g. fixing the problem of source not found when
running it from RMarkdown file, [source](https://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script?noredirect=1&lq=1)
```
sourceDir <- getSrcDirectory(function(dummy) {dummy}) 
setwd(sourceDir)
```
3. Cryptography:

    a. High school level:
      1. [Introduction to cryptography](https://www.khanacademy.org/computing/computer-science/cryptography/crypt/v/intro-to-cryptography)
    
    b. Master level:
      * Sasa - http://lkrv.fri.uni-lj.si/~ajurisic/kitk2-10/folije/3x2kitk09.pdf
      * Stanford course on cryptography I, available on [coursera](https://www.coursera.org/learn/crypto)
      * [A Graduate Course in Applied Cryptography](https://toc.cryptobook.us/) - I recomend this textbook: [Download](https://crypto.stanford.edu/~dabo/cryptobook/BonehShoup_0_4.pdf)
      * Stanford master course in Cryptography content: [cs255](http://crypto.stanford.edu/~dabo/cs255/)
      * Example01 Shiny application published on [shinyapps](https://mathtravel.shinyapps.io/main/)
      * D. R. Stinson and M. Paterson, Cryptography - Theory and Practice, CRC Press, 4th. Ed., 2018 ([3rd ed. 2006](http://www.ksom.res.in/files/RCCT-2014-III-CM/CryptographyTheoryandpractice(3ed).pdf), 2nd ed. 2002, 1st. ed. 1995: SIG 11996/3) (kripto2)
      * [FRI Cryptography course](https://ucilnica.fri.uni-lj.si/course/view.php?id=93)
      * https://medium.com/algorand/algorand-releases-first-open-source-code-of-verifiable-random-function-93c2960abd61
    
    c. General:
        1. [Gary Kessler](https://www.garykessler.net/library/crypto.html) online book
        1. https://web.williams.edu/Mathematics/sjmiller/public_html/tas2011/book/chap3_publickey.pdf