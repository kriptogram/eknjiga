//funkcija opreveri če je abeceda sestavljena iz črk in če ni podvojenih črk
function isABC(abeceda){
    var unique = abeceda.split('').filter(function(item, i, ar) {
        return ar.indexOf(item) === i;
    });
    if((unique.length===abeceda.split('').length)){
        return 'ok';
    }
    else{
        return 'Vpisana abeceda ne sme vsebovati podvojenih črk.';
    }

}

//gcd
function gcd_mod(a, n){
while (n !== 0){
  var t = n
  n = a % n
  a = t
}
return a;
}

//modularni inverz:
function modInverse(a, n) {
a %= n;
for (var x = 1; x < n; x++) {
    if ((a*x)%n == 1) {
        return x;
    }
}
return 'ni';
}

//funkcija za generiranje header tabele
function generateTableHead(table, data) {
let thead = table.createTHead();
let row = thead.insertRow();
for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);  
}
}

//funkcija za generiranje tabele
function generateTable(table, data) {
for (let element of data) {
  let row = table.insertRow();
  for (key in element) {
    let cell = row.insertCell();
    let text = document.createTextNode(element[key]);
    cell.appendChild(text);
  }
}
}

// funkcija za vračanje index-a črke
function findCrka(abeceda,crka){
for(i=0;i<abeceda.length;i++){
    if(abeceda[i] == crka){
        return i;
    }
}
return null;
}

function sifriranje(a,b,x,N){
return ((((a*x)+b)%N)+N)%N;

}

function izrisiTabelo(){
//abeceda = document.getElementById('abeceda').value.toUpperCase();
abeceda = document.getElementById('abeceda').value;
a1 = document.getElementById('aKljuc').value;
b1 = document.getElementById('bKljuc').value;
table = document.querySelector('table');
data = [];
d = [];
data.push('abeceda');
for(i=0;i<abeceda.length;i++){
    data.push(abeceda.split('')[i]);
}
N = abeceda.length;
a=( (a1%N) +N )%N;
b=( (b1%N) +N )%N;
//NAPAKE
napaka = [];
napaka[0] = '';napaka[1] = '';napaka[2] = '';napaka[3] = '';

//izbrišem napake in tabelo (če obstaja)
var x = document.getElementById('messageError');
if(x!=null){
    x.parentNode.removeChild(x);
}
if(table!=null){
    table.parentNode.removeChild(table);
}



if(isABC(abeceda)=='ok' && a1.length!=0 && b1.length!=0 && N!=0){
    
    table = document.createElement('table');
    table.setAttribute('border','1|0');
    table.setAttribute('style','border-collapse: collapse');
    document.getElementById('firstDiv').appendChild(table); 
    
    generateTableHead(table, data);

    table_data = [];
    indexi = [];
    indexi.push('oštevilčenje: $i$');
    for(i=0;i<abeceda.split('').length;i++){
        indexi.push(i);
    }
    table_data.push(indexi);
    generateTable(table, table_data);

    table_data2 = [];
    E = [];
    E.push('$ a \\cdot i + b \\mod \\text{ št. znakov} $');
    for(i=0;i<abeceda.split('').length;i++){
        E.push(sifriranje(parseInt(a),parseInt(b),i,N));
    }
    table_data2.push(E);
    generateTable(table, table_data2);

    table_data3 = [];
    crke = [];
    crke.push('šifrirana abeceda');
    for(i=1;i<E.length;i++){
        crke[i]=data[E[i]+1]
    }
    table_data3.push(crke);
    generateTable(table, table_data3);
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, table]);

    if(a.length!=0 && gcd_mod(a,N)!=1){
        napaka[1] = 'Šifrirana abeceda vsebuje podvojene znake ( gcd(a,število znakov v abecedi) = '+gcd_mod(a,N)+')';
    }

}
else{
    if (a1.length ==0 ){
        napaka[2] = 'Število a ni vpisano. ';
    }
    
    if (b1.length ==0 ){
        napaka[3] = 'Število b ni vpisano. ';
    }
    if(isABC(abeceda)!='ok'){
        napaka[0] = 'Vpisana abeceda vsebuje podvojene znake. ';
    }
    if(N == 0){
        napaka[0] = 'Niste vpisali abecede. ';
    }
      
}
    
    napake = '';
    for(n=0;n<napaka.length;n++){
        if(napaka[n].length != 0){
            napake+=napaka[n];
            
        }
    }
    if(napake !=''){
        p = document.createElement('p');
        p.setAttribute('id','messageError');
        text = document.createTextNode(napake);
        p.appendChild(text);
        document.getElementById('firstDiv').appendChild(p);
    }
    

}



function sifrirejBesedilo(){
abeceda = document.getElementById('abeceda').value;
besedilo = document.getElementById('besedilo').value;
a1 = document.getElementById('aKljuc').value;
b1 = document.getElementById('bKljuc').value;
data = abeceda.split('');
data2 = besedilo.split('');
N = data.length;
a=( (a1%N) +N )%N;
b=( (b1%N) +N )%N;
aInv = 0;
if(a1.length !=0){
    aInv = modInverse(a,N); 
}
D = 0;


sifBesedilo = '';

var x = document.getElementById('messageError');
if(x!=null){
    x.parentNode.removeChild(x);
}

//NAPAKE
napaka = [];
napaka[0] = '';napaka[1] = '';napaka[2] = '';napaka[3] = '';

if(isABC(abeceda)=='ok' && a1.length!=0 && b1.length!=0 && N!=0){
    /*for(j=0;j<data2.length;j++){                      //DEŠIFRIRANJE
        possition = findCrka(data,data2[j]);
        if(possition != null){
            p = aInv*(possition-b);                            //p = aInv * (index - b)
            D =  p % N;                                         //dešifriranje
            desBesedilo+=data[D];
        }
        else{
            desBesedilo+=data2[j];
        }
        
    }*/
    //ŠIFRIRANJE
    for(j=0;j<data2.length;j++){
        possition = findCrka(data,data2[j]);
        if(possition != null){
            sifBesedilo += data[sifriranje(parseInt(a),parseInt(b),possition,N)];
        }
        else{
            sifBesedilo+=data2[j];
        }
    }
    document.getElementById('sifBesedilo').value = sifBesedilo;
    if(a.length!=0 && gcd_mod(a,N)!=1){
        napaka[1] = 'Šifrirana abeceda vsebuje podvojene znake ( gcd(a,število znakov v abecedi) = '+gcd_mod(a,N)+')';
    }
} 
else{
    if (a1.length ==0 ){
        napaka[2] = 'Število a ni vpisano. ';
    }
    
    if (b1.length ==0 ){
        napaka[3] = 'Število b ni vpisano. ';
    }
    if(isABC(abeceda)!='ok'){
        napaka[0] = 'Vpisana abeceda vsebuje podvojene znake. ';
    }
    if(N == 0){
        napaka[0] = 'Niste vpisali abecede. ';
    }
}
            
napake = '';
for(n=0;n<napaka.length;n++){
    if(napaka[n].length != 0){
        napake+=napaka[n];
        
    }
}
if(napake !=''){
    p = document.createElement('p');
    p.setAttribute('id','messageError');
    text = document.createTextNode(napake);
    p.appendChild(text);
    document.getElementById('firstDiv').appendChild(p);
}
} 


function desifrirniKljuc(){

var x = document.getElementById('messageError3');
if(x!=null){
    x.parentNode.removeChild(x);
}

a1 = parseInt(document.getElementById('a2Kljuc').value);
b1 = parseInt(document.getElementById('b2Kljuc').value);
N = parseInt(document.getElementById('N').value);
a=( (a1%N) +N )%N;
b=( (b1%N) +N )%N;
console.log(a);
a2 = modInverse(a,N); 
text = '';                          
if(a1.length != 0 && b1.length != 0 && a2!='ni'){
    text += 'S pomočjo Eklidovega algoritma dobimo inverz števila a = '+a2+'.\n';
    text += 'Drugi del ključa pa dobimo s pomočjo formule \\(-a^{-1}*b=\\) '+(((-a2*b)%N) +N )%N+'.\n';
    text += 'Ključ za dešifriranje: ('+a2+','+(((-a2*b)%N) +N )%N+').';
    
    
}
else{
    if(Number.isNaN(a1)){
        text+='Števila a ni vpisano.\n';
    }
    if(Number.isNaN(b1)){
        text+='Števila b ni vpisano.\n';
    }
    if(Number.isNaN(N)){
        text+='Števila N ni vpisano.\n';
    }
    if(a2=='ni'){
        text+='Ne obsaja inverz za število a.\n';
    }
}
if(text!=''){
    p = document.createElement('p');
    p.setAttribute('id','messageError3');
    t = document.createTextNode(text);
    p.appendChild(t);
    document.getElementById('t2Div').appendChild(p);
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById('messageError3')]);
}




}

function izracunajKljuc(){
besedilo = document.getElementById('besedilo2').value.split('');
sifBesedilo = document.getElementById('sifBesedilo2').value.split('');
abeceda = document.getElementById('abeceda2').value;
tabABC = abeceda.split('');
N = abeceda.length;
najden = false;
sporocilo = '';

crki = [];
scrki = [];


var x = document.getElementById('messageError2');
if(x!=null){
    x.parentNode.removeChild(x);
}

if(isABC(abeceda)=='ok' && besedilo.length >= 2 && sifBesedilo.length >= 2 && N!=0 && sifBesedilo.length==besedilo.length){
    //za prvo črko/znak se vedno izbere prva obstoječa v abecedi črka/znak šifriranega in navadnega besedila
    crki[0]='';
    scrki[0]='';
    for(j=0;j<besedilo.length;j++){
        if(findCrka(tabABC,besedilo[j])!=null && findCrka(tabABC,sifBesedilo[j])!=null){
            crki[0]=besedilo[j];
            scrki[0]=sifBesedilo[j];
            break;
        }
    }
    //iskanje nasljednja dva znaka
    if(crki[0]!='' && scrki[0]!=''){
        for(j=0;j<besedilo.length;j++){
            if(besedilo[j]!=crki[0] && sifBesedilo[j]!=scrki[0] && findCrka(tabABC,besedilo[j])!=null && findCrka(tabABC,sifBesedilo[j])!=null && najden == false){
                crki[1]=besedilo[j];
                scrki[1]=sifBesedilo[j];
                najden = true;

                sporocilo+= '$x_1 = ' + abeceda.indexOf(crki[0])    + '\\ (\\text{'+ crki[0] +  '}), \\ ' +
                            'x_2 = ' +  abeceda.indexOf(crki[1])    + '\\ (\\text{'+ crki[1] +  '}), \\ ' +
                            'y_1 = ' +  abeceda.indexOf(scrki[0])   + '\\ (\\text{'+ scrki[0] + '}), \\ ' +
                            'y_2 = ' +  abeceda.indexOf(scrki[1])   + '\\ (\\text{'+ scrki[1] + '})$ \n'

                crki[0]=findCrka(tabABC,crki[0]);
                crki[1]=findCrka(tabABC,crki[1]);   
                scrki[0]=findCrka(tabABC,scrki[0]); 
                scrki[1]=findCrka(tabABC,scrki[1]);
                //y  = ax +b        jih odštejemo (y->cipher text; x->plain text)
                //y2 = ax + b       y->scrki; x->crki b se pokrajša
                // če je število negativno se uporabi formula (((x)%N)+N)%N Primer x=-5; N=26 -> novi x -> 21
                //y
                x = (((crki[0]-crki[1])%N)+N)%N;
                y = (((scrki[0]-scrki[1])%N)+N)%N;
                if(modInverse(x,N)!='ni'){
                    a = (((y*modInverse(x,N))%N)+N)%N;
                    //b = y-ax
                    b=(((scrki[0]-a*crki[0])%N)+N)%N;
                    
                    sporocilo +=    'Ključ dobimo po formuli: $y_1-y_2 \\equiv a x_1 - a x_2 \\pmod{' + N + ' }$.\n';
                    sporocilo +=    'Iz tega sledi:\n $ a = (y_1 - y_2) (x_1 - x_2)^{-1} \\implies a = ('+ y +'\\cdot' + modInverse(x,N) +') \\mod{' + N +
                                    '} = '+((( y * modInverse(x,N))%N)+N)%N + '$ in\n';

                    sporocilo+= '$b = y_1 - a x_1 \\implies b = ('+scrki[0]+ '-' + a + '\\cdot' + crki[0]+') \\mod{' + N + '} = ' + b + '$.\n';
                    sporocilo += 'Ključ $(a,b) = (' + a +',' + b + ').$ \n';
                    //sporocilo += 'Pod pogojem, da ste celotno besedilo in šifrirano besedilo vneseli pravilno (program preverja le 4 znake, ki vstrezajo pogojem), lahko probate vnesti ključ in abecedo v prvi del naloge.';
                    
                } else {
                    sporocilo = 'Ne obstaja inverz števila x, zato uporabimo napad z grobo silo. Pregledati moramo vse možnosti.\n';
                    sporocilo += '$a$ je lahko katerokoli število, ki je tuje številu $N$.\n';
                    var sporociloBruteForce = 'Ključ $(a, b)$ je lahko:';
                    var stKljucev = 0;
                    for (var i = 1; i <= N; i++) {
                        //check if a is coprime with N
                        if (gcd_mod(i, N) == 1) {
                            for(var j = 1; j <= N; j++) {
                                encodedX1 = (((i * crki[0] + j)%N)+N)%N;
                                encodedX2 = (((i * crki[1] + j)%N)+N)%N;
                                if(scrki[0] == encodedX1 && scrki[1] == encodedX2) {
                                    if (stKljucev != 0) {
                                        sporociloBruteForce += ','; 
                                    }
                                    sporociloBruteForce += ' $('+ i + ', ' + j +')$';

                                    stKljucev++;
                                }
                            }
                        }
                    }
                    if (stKljucev == 0) {
                        sporocilo += 'Ne obstaja dešifrirni ključ za podano kombinacijo.\n';
                    }  else {
                        sporocilo += sporociloBruteForce + '.\n';
                    }

                }  
            }
        }
    }
}

//napake1
if(N==0 || besedilo.length < 2 || sifBesedilo.length < 2){
    if(N==0){
        sporocilo += 'Niste vpisali abecede\n';
    }
    if(besedilo.length < 2){
        sporocilo += 'Vpisano besedilo ni dolgo vsaj 2 znaka.\n';
    }
    if(sifBesedilo.length < 2){
        sporocilo += 'Vpisano šifrirano besedilo ni dolgo vsaj 2 znaka.\n';
    }
}
//napake2
else if(isABC(abeceda)!='ok' || (besedilo.length >=2 && sifBesedilo.length >= 2 && besedilo.length != sifBesedilo.length) || najden == false){
    if(isABC(abeceda)!='ok'){
        sporocilo += 'Vpisana abeceda vsebuje podvojene znake.\n';
    }
    if(besedilo.length >=2 && sifBesedilo.length >= 2 && besedilo.length != sifBesedilo.length){
        sporocilo += 'Vpisano besedilo in šifrirano besedilo nista enako dolga.\n';
    }
    if(najden == false){
        sporocilo += 'Nista najdena dva različna x-a, ki se preslikata v dva različna y-a (x - index znaka besedila, y - index znaka šifriranega besedila) obstoječih v vpisani abecedi.';
    }
}




if(sporocilo!=''){
    text = document.createTextNode(sporocilo);
    p = document.createElement('p');
    p.setAttribute('style', 'white-space:pre-line;')
    p.setAttribute('id','messageError2');
    p.appendChild(text);
    document.getElementById('fifthDiv').appendChild(p);
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById('messageError2')]);
}


}


document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("sifButton").click();
      }
});

document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("desifrirniBtn").click();
      }
});

document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("izracunajBtn").click();
      }
});