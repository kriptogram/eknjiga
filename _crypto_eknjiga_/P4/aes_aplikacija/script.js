function is_hexadecimal(str){
    var regexp = /^[0-9a-fA-F]+$/;
    if (regexp.test(str))
        return true;
    else
        return false;
}

function preveriHex(id){
    if(!is_hexadecimal(document.getElementById(id).value[document.getElementById(id).value.length - 1])){
        document.getElementById(id).value = document.getElementById(id).value.substring(0, document.getElementById(id).value.length - 1)
    }
}

function preveriDolzino(id){
    var key = document.getElementById(id).value;
    if(id == 'kljuc' && key.length < document.getElementById('kljuc').maxLength){
        var dodaneNicle = "";
        for(i=key.length; i<document.getElementById('kljuc').maxLength; i++){
            dodaneNicle += "0";
        }
        document.getElementById(id).value = key + dodaneNicle;
    }
    else if(key.length < 32){
        var dodaneNicle = "";
        for(i=key.length; i<32; i++){
            dodaneNicle += "0";
        }
        document.getElementById(id).value = key + dodaneNicle;
    }
}

function spremeniVelikostKljuca(){
    if(vrstaAES.value == 'AES128'){
        document.getElementById('kljuc').maxLength = "32";
        document.getElementById('kljuc').style.width = "320px";
    }
    else if(vrstaAES.value == 'AES192'){
        document.getElementById('kljuc').maxLength = "48";
        document.getElementById('kljuc').style.width = "460px";
    }
    else if(vrstaAES.value == 'AES256'){
        document.getElementById('kljuc').maxLength = "64";
        document.getElementById('kljuc').style.width = "600px";
    }
}

function sifriraj(){
    preveriDolzino('kljuc');
    var key = document.getElementById("kljuc").value;
    var keyBytes = aesjs.utils.hex.toBytes(key);
    
    preveriDolzino('vhodniBlok');
    var text = document.getElementById('vhodniBlok').value;
    var textBytes = aesjs.utils.hex.toBytes(text);

    var aesEcb = new aesjs.ModeOfOperation.ecb(keyBytes);
    var encryptedBytes = aesEcb.encrypt(textBytes);

    var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
    document.getElementById('izhodniBlok').value = encryptedHex;

}

function odsifriraj(){
    preveriDolzino('kljuc');
    var key = document.getElementById("kljuc").value;
    var keyBytes = aesjs.utils.hex.toBytes(key);
    
    preveriDolzino('izhodniBlok');
    var encryptedHex = document.getElementById('izhodniBlok').value;
    var encryptedBytes = aesjs.utils.hex.toBytes(encryptedHex);

    var aesEcb = new aesjs.ModeOfOperation.ecb(keyBytes);
    var decryptedBytes = aesEcb.decrypt(encryptedBytes);

    var decryptedText = aesjs.utils.hex.fromBytes(decryptedBytes);
    document.getElementById('vhodniBlok').value = decryptedText;
}


window.addEventListener('load', function(){
    this.document.getElementById("sifriraj").addEventListener("click", sifriraj);
    this.document.getElementById("odsifriraj").addEventListener("click", odsifriraj);
});

document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("sifriraj").click();
      }
});