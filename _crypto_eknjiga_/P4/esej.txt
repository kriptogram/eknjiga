Šifriranje

Kadar želimo podatke zaščititi, da jih ne morejo preprati osebe katerim sporočilo ni namenjeno, jih moramo na nek način zaščititi
Eden od postopkov za dosega tega cilja je šifriranje (angl. cipher), izhaja pa iz hebrejske besede saphar, ki pomeni šteti/označiti.
S problemo kako zaščititi te sporočila, da jih ljudje, ki jim ta niso namenjena, ne morejo razvozlati, se ukvarja kriptografija.
Ta izraz je sestavljen iz grških besed kryptos, ki pomeni skrivnost, in logos, ki pomeni beseda.

Cezarjeva šifra

Cezarjeva šifra je ena od nastarejših in najpogosteje uporabljenih oblik šifriranja. 
Poznali so jo že v rimskih časih in ga je med drugimi uporabljal tudi Julij Cezar po komer se ta šifra imenuje.
Šifriranje poteka tako, da vsako črko abecede zamaknemo za določeno število znakov. Šifro je zato možno razbiti precej preprosto kajti na voljo je le 24 kombinacij, ki pa jih lahko razbijemo kar "na roke".

Postopek

Postopek lahko ponazorimo tako, da vsako črko označimo po vrsti, s številko od 1 do 25.
Nato izberemo za koliko mest bomo zamaknili naš čistopis. Naše besedilo nato pretvorimo v številke ki pripadajo posamični črki in tem potem prištejemo zamik.
Vsem številkam ki so višje od 25 oštejemo 25 in nato v tabeli pogledamo katera številka je katera črka, si čimer pretvorimo besedilo nazaj v črke.

*alternativa:*

Namesto tabele lahko uporabimo tudi krog črk.

<app_circle>

Pri njemu najprej ugotovimo katero črko predstavlja naš zamik. Ta je črka ki je n-ta (zamik + 1) po vrsti vseh črk. Nato A zunanjega kroga z našo n-to črko notranjega kroga.
Nato črke eno ter po eno začnemo prepisovati iz čistopisa v tajnopis, vendar namesto da zapišemo črko samo, jo poiščemo na zunanjem delu kroga in črko ki se na tem nahaja na notranjem krogu zapišemo v tajnopis.


Vigenerjeva šifra

Vigenerjeva šifraje različica Cezarjeve šifre pri katere za vsako številko uporabimo drugo cezarjevo šifro, ki jo določa geslo.
Šifra je bila prvič zabeležena leta 1553 v knjigi Giovana Battista Bellasa ampak je bila v 19. stoletju napačno pripisana Blaiseju de Vigenèrju od koder je tudi dobila svoje ime.
Šifra je bila izredno preprosta za implementacijo in pojasnilo, vendar je učenjaki niso uspeli razbiti več kot 300 let, ko je Friedrich Kasiski uspel razviti splošno metodo za razbijanje.
Zaradi tega razloga se jo je v tem času prijelo ime le chiffre indéchiffrable (francosko za šifra, ki se je ne da razbiti).

Postopek

Ponovno bomo uporabili tabelo, ki smo jo uporabili pri Cezarjevi šifri. Prvi korak je, da pretvorimo posamične črke gesla v številke in nato od vsake odštejemo 1.
Nato ta postopek naredimo še s našim čistopisom. Prvo številko čistopisa seštejemo s prvo številko iz gesla, drugo čistopisa z drugo gesla in tako naprej.
Ko nam zmanjka gesla začnemo geslo ponovno zprvo številko medtem ko čistopis nadaljujemo eno črko za drugo.
Ko pridemo do konca vsem številkam večjim od 25 odštejemo 25 in nato vsako številko poiščemo v tameli ter jih po vrsti zabeležimo, kar nam predstavlja tajnopis.

*alternativa*

Za ta postopek šifriranja lahko uporabimo tudi krog črk, ki smo ga uporabili pri cezarjevi šifri, kar naredi ta postopek še lažji.
Na zunanjem krogu A poravnamo s prvo črko gesla. Nato na zunanjem krogu poiščemo prvo črke čistopisa. Črka ki se pod njo nahaja pod njo predstavlja prvo črko tajnopisa. To naredimo za vsako črko.
Če nam med tem zmanjka gesla preprosto pričnemo z njim od začetka.