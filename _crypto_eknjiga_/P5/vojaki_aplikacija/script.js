const r = 15;
var x = r * 2;
var y = r * 2;
var pad = r * 3;

var prvoSt;
var drugoSt;
var tretjeSt;
var cetrtoSt;

var canvas;
var ctx;

function narisi(ctx, c, a, b, x, y){
    for(i=0; i<b; i++){
        ctx.fillStyle = c;
        if(i % a == 0 && i != 0){
            x += pad;
            y -= (a * pad);
        }
        ctx.beginPath();
        ctx.arc(x, y, r, 0, 2 * Math.PI);
        ctx.fill();
        ctx.closePath();
        ctx.fillStyle = "#FFFFFF"
        ctx.font = "15px courier";
        if((i + 1) >= 10 && (i + 1) < 100){
            ctx.fillText("" + (i + 1), x - 9, y + 3);
        }
        else if((i + 1) >= 100){
            ctx.fillText("" + (i + 1), x - 13, y + 3);
        }
        else{
            ctx.fillText("" + (i + 1), x - 5, y + 3);
        }
        y += pad;
    }
}

function izracunajMere(){
    var max_width = 0;
    var max_height = (prvoSt + drugoSt + tretjeSt + 1) * pad;

    if((Math.floor(cetrtoSt / prvoSt) <= cetrtoSt) && ((Math.floor(cetrtoSt / prvoSt)) * pad) > max_width){
        max_width = (Math.floor(cetrtoSt / prvoSt) + 1) * pad + r;
    }
    if((Math.floor(cetrtoSt / drugoSt) <= cetrtoSt) && ((Math.floor(cetrtoSt / drugoSt)) * pad) > max_width){
        max_width = (Math.floor(cetrtoSt / drugoSt) + 1) * pad + r;
    }
    if((Math.floor(cetrtoSt / tretjeSt) <= cetrtoSt) && ((Math.floor(cetrtoSt / tretjeSt)) * pad) > max_width){
        max_width = (Math.floor(cetrtoSt / tretjeSt) + 1) * pad + r;
    }
    
    return [max_width, max_height];
}

function posodobiStevila(){
    prvoSt = parseInt(document.getElementById("prvoStevilo").value);
    drugoSt = parseInt(document.getElementById("drugoStevilo").value);
    tretjeSt = parseInt(document.getElementById("tretjeStevilo").value);
    cetrtoSt = parseInt(document.getElementById("cetrtoStevilo").value);
    narisiVse();
}

function narisiVse(){
    canvas = this.document.getElementById('canvas');
    ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    
    canvas.width = izracunajMere()[0];
    canvas.height = izracunajMere()[1];

    x = r * 2;
    y = r * 2;
    pad = r * 3;
    
    narisi(ctx, "#2a4d69", prvoSt, cetrtoSt, x, y);
    y = pad * prvoSt + pad;
    narisi(ctx, "#4b86b4", drugoSt, cetrtoSt, x, y);
    y = (pad * prvoSt + pad) + (pad * drugoSt + pad/4);
    narisi(ctx, "#63ace5", tretjeSt, cetrtoSt, x, y);
}

window.addEventListener('load', function(){
    prvoSt = parseInt(document.getElementById("prvoStevilo").value);
    drugoSt = parseInt(document.getElementById("drugoStevilo").value);
    tretjeSt = parseInt(document.getElementById("tretjeStevilo").value);
    cetrtoSt = parseInt(document.getElementById("cetrtoStevilo").value);
    narisiVse();
});