var a,c,m,x0,prev;
window.onload = function() {
    reset();
}

function reset() {
    a = Number(document.getElementById("input-a").value);
    c = Number(document.getElementById("input-c").value);
    m = Number(document.getElementById("input-m").value);
    x0 = Number(document.getElementById("input-x").value);

    cycle = 0;
    oldArrowAng = null;
    oldArrow1Ang = null;
    oldArrow2Ang = null;
    oldArrow3Ang = null;

    prev = x0;
    populateCircle(m);
    pointTo(x0);
}

function calc() {
    next = (a * prev + c) % m;
    pointTo(next);
    prev = next;
}

total = 0;
function populateCircle(num) {
    total = num;
    d = 2 * Math.PI / num;
    clock = document.getElementById("clock");
    rect = clock.getBoundingClientRect();
    height = rect.height;
    width = rect.width;

    halfW = width / 2;
    halfH = height / 2;
    offset = 24;

    clock.innerHTML = "";
    for(var i = 0; i < num; i++) {
        ang = - Math.PI / 2 + i * d;
        var sub;
        if(i < 10) {sub = 5;} else {sub = 9;}
        x = halfW + Math.cos(ang) * (halfW - offset) - sub;
        y = halfH + Math.sin(ang) * (halfH - offset) + 7;
        clock.innerHTML += '<text x="' + x + '" y="' + y + '" fill="black">' + i + '</text>';
    }
    clock.innerHTML += `
        <line id="arrow3" x1="250" y1="250" x2="250" y2="250" stroke="rgba(0,0,255,0)" stroke-width="3"></line>
        <line id="arrow2" x1="250" y1="250" x2="250" y2="250" stroke="rgba(0,255,0,0)" stroke-width="3"></line>
        <line id="arrow1" x1="250" y1="250" x2="250" y2="250" stroke="rgba(255,0,0,0)" stroke-width="3"></line>
        <line id="arrow"  x1="250" y1="250" x2="250" y2="250" stroke="rgba(0,0,0,0)"   stroke-width="3"></line>
        
        <circle id="circle3" cx="0" cy="0" r="0" stroke="rgba(0,0,255,0)" stroke-width="3" fill="rgba(0,0,255,0)"></circle>
        <circle id="circle2" cx="0" cy="0" r="0" stroke="rgba(0,255,0,0)" stroke-width="3" fill="rgba(0,255,0,0)"></circle>
        <circle id="circle1" cx="0" cy="0" r="0" stroke="rgba(255,0,0,0)" stroke-width="3" fill="rgba(255,0,0,0)"></circle>
        <circle id="circle"  cx="0" cy="0" r="0" stroke="rgba(0,0,0,0)"  stroke-width="3"  fill="rgba(0,0,0,0)"></circle>

        <path id="arc" stroke-width="2" fill="rgba(0,0,0,0)"/>
        <path id="arc1" stroke-width="2" fill="rgba(0,0,0,0)"/>
        <path id="arc2" stroke-width="2" fill="rgba(0,0,0,0)"/>
    `;
}

cycle = 0;

function pointTo(num) {
    d = 2 * Math.PI / total;
    clock = document.getElementById("clock");
    rect = clock.getBoundingClientRect();
    height = rect.height;
    width = rect.width;

    halfW = width / 2;
    halfH = height / 2;
    arrowOffset = 55;
    circleOffset = -24;

    arrow = document.getElementById("arrow");
    arrow1 = document.getElementById("arrow1");
    arrow2 = document.getElementById("arrow2");
    arrow3 = document.getElementById("arrow3");
    circle = document.getElementById("circle");
    circle1 = document.getElementById("circle1");
    circle2 = document.getElementById("circle2");
    circle3 = document.getElementById("circle3");
    arc = document.getElementById("arc");
    arc1 = document.getElementById("arc1");
    arc2 = document.getElementById("arc2");

    if(cycle == 0) {
        arrow.setAttribute("stroke", "black");
        arrow.setAttribute("marker-end", "url(#head)");
        circle.setAttribute("r", 20);
        circle.setAttribute("stroke", "black");
        cycle++;
    } else if(cycle == 1) {
        arrow1.setAttribute("stroke", "rgb(255, 0, 0, 100)");
        arrow1.setAttribute("marker-end", "url(#head-r)");
        circle1.setAttribute("r", 20);
        circle1.setAttribute("stroke", "rgb(255, 0, 0, 100)");
        arc.setAttribute("stroke", "rgb(255, 0, 0, 100)");
        arc.setAttribute("marker-end", "url(#head-r)");
        cycle++;
    } else if(cycle == 2) {
        arrow2.setAttribute("stroke", "rgb(0, 255, 0, 100)");
        arrow2.setAttribute("marker-end", "url(#head-g)");
        circle2.setAttribute("r", 20);
        circle2.setAttribute("stroke", "rgb(0, 255, 0, 100)");
        arc1.setAttribute("stroke", "rgb(0, 255, 0, 100)");
        arc1.setAttribute("marker-end", "url(#head-g)");
        cycle++;
    } else if(cycle == 3) {
        arrow3.setAttribute("stroke", "rgb(0, 0, 255, 100)");
        arrow3.setAttribute("marker-end", "url(#head-b)");
        circle3.setAttribute("r", 20);
        circle3.setAttribute("stroke", "rgb(0, 0, 255, 100)");
        arc2.setAttribute("stroke", "rgb(0, 0, 255, 100)");
        arc2.setAttribute("marker-end", "url(#head-b)");
        cycle++;
    }

    ang = - Math.PI / 2 + num * d;
    setArrowAngle(ang);
}

oldArrowAng = null;
oldArrow1Ang = null;
oldArrow2Ang = null;
oldArrow3Ang = null;

arcRad = 130;
arc1Rad = 100;
arc2Rad = 70;

function setArrowAngle(angle) {
    d = 2 * Math.PI / total;
    clock = document.getElementById("clock");
    rect = clock.getBoundingClientRect();
    height = rect.height;
    width = rect.width;

    halfW = width / 2;
    halfH = height / 2;
    arrowOffset = 55;
    circleOffset = -24;

    arrow = document.getElementById("arrow");
    circle = document.getElementById("circle");
    arrow1 = document.getElementById("arrow1");
    circle1 = document.getElementById("circle1");
    arrow2 = document.getElementById("arrow2");
    circle2 = document.getElementById("circle2");
    arrow3 = document.getElementById("arrow3");
    circle3 = document.getElementById("circle3");

    arc = document.getElementById("arc");
    arc1 = document.getElementById("arc1");
    arc2 = document.getElementById("arc2");

    if(oldArrowAng != null) {
        if(oldArrow1Ang != null) {
            if(oldArrow2Ang != null) {
                if(oldArrow3Ang != null) {
                    angles = makeIntervals(oldArrowAng, angle, 100);
                    angles1 = makeIntervals(oldArrow1Ang, oldArrowAng, 100);
                    angles2 = makeIntervals(oldArrow2Ang, oldArrow1Ang, 100);
                    angles3 = makeIntervals(oldArrow3Ang, oldArrow2Ang, 100);
                    i = 0;
                    document.getElementById("calc-btn").disabled = true;
                    document.getElementById("reset-btn").disabled = true;
                    interval = setInterval(function(){
                        ang = angles[i];
                        x = halfW + Math.cos(ang) * (halfW - arrowOffset);
                        y = halfH + Math.sin(ang) * (halfH - arrowOffset);
                        cx = halfW + Math.cos(ang) * (halfW + circleOffset);
                        cy = halfH + Math.sin(ang) * (halfH + circleOffset);
                    
                        arrow.setAttribute("x2", x);
                        arrow.setAttribute("y2", y);
                        circle.setAttribute("cx", cx);
                        circle.setAttribute("cy", cy);
                    
                        ax_end = halfW + Math.cos(ang) * arcRad;
                        ay_end = halfW + Math.sin(ang) * arcRad;

                        ang1 = angles1[i];
                        x = halfW + Math.cos(ang1) * (halfW - arrowOffset);
                        y = halfH + Math.sin(ang1) * (halfH - arrowOffset);
                        cx = halfW + Math.cos(ang1) * (halfW + circleOffset);
                        cy = halfH + Math.sin(ang1) * (halfH + circleOffset);
                    
                        arrow1.setAttribute("x2", x);
                        arrow1.setAttribute("y2", y);
                        circle1.setAttribute("cx", cx);
                        circle1.setAttribute("cy", cy);
                    
                        ax_start = halfW + Math.cos(ang1) * arcRad;
                        ay_start = halfW + Math.sin(ang1) * arcRad;
                        a1x_end = halfW + Math.cos(ang1) * arc1Rad;
                        a1y_end = halfW + Math.sin(ang1) * arc1Rad;

                        ang2 = angles2[i];
                        x = halfW + Math.cos(ang2) * (halfW - arrowOffset);
                        y = halfH + Math.sin(ang2) * (halfH - arrowOffset);
                        cx = halfW + Math.cos(ang2) * (halfW + circleOffset);
                        cy = halfH + Math.sin(ang2) * (halfH + circleOffset);
                    
                        arrow2.setAttribute("x2", x);
                        arrow2.setAttribute("y2", y);
                        circle2.setAttribute("cx", cx);
                        circle2.setAttribute("cy", cy);

                        a1x_start = halfW + Math.cos(ang2) * arc1Rad;
                        a1y_start = halfW + Math.sin(ang2) * arc1Rad;
                        a2x_end = halfW + Math.cos(ang2) * arc2Rad;
                        a2y_end = halfW + Math.sin(ang2) * arc2Rad;

                        ang3 = angles3[i];
                        x = halfW + Math.cos(ang3) * (halfW - arrowOffset);
                        y = halfH + Math.sin(ang3) * (halfH - arrowOffset);
                        cx = halfW + Math.cos(ang3) * (halfW + circleOffset);
                        cy = halfH + Math.sin(ang3) * (halfH + circleOffset);
                    
                        arrow3.setAttribute("x2", x);
                        arrow3.setAttribute("y2", y);
                        circle3.setAttribute("cx", cx);
                        circle3.setAttribute("cy", cy);
                    
                        a2x_start = halfW + Math.cos(ang3) * arc2Rad;
                        a2y_start = halfW + Math.sin(ang3) * arc2Rad;

                        if(!isAngleSharp(ang, ang1)) {
                            arc.setAttribute("d", "M " + ax_start + " " + ay_start + " A " + arcRad + " " + arcRad + ", 180, 1, 1, " + ax_end + " " + ay_end);
                        } else {
                            arc.setAttribute("d", "M " + ax_start + " " + ay_start + " A " + arcRad + " " + arcRad + ", 180, 0, 1, " + ax_end + " " + ay_end);
                        }
    
                        if(!isAngleSharp(ang1, ang2)) {
                            arc1.setAttribute("d", "M " + a1x_start + " " + a1y_start + " A " + arc1Rad + " " + arc1Rad + ", 180, 1, 1, " + a1x_end + " " + a1y_end);
                        } else {
                            arc1.setAttribute("d", "M " + a1x_start + " " + a1y_start + " A " + arc1Rad + " " + arc1Rad + ", 180, 0, 1, " + a1x_end + " " + a1y_end);
                        }

                        if(!isAngleSharp(ang2, ang3)) {
                            arc2.setAttribute("d", "M " + a2x_start + " " + a2y_start + " A " + arc2Rad + " " + arc2Rad + ", 180, 1, 1, " + a2x_end + " " + a2y_end);
                        } else {
                            arc2.setAttribute("d", "M " + a2x_start + " " + a2y_start + " A " + arc2Rad + " " + arc2Rad + ", 180, 0, 1, " + a2x_end + " " + a2y_end);
                        }

                        i++;
                        if(i == angles.length) {
                            clearInterval(interval);
                            document.getElementById("calc-btn").disabled = false;
                            document.getElementById("reset-btn").disabled = false;
                        }
                    }, 2);
                    oldArrow3Ang = oldArrow2Ang;
                    oldArrow2Ang = oldArrow1Ang;
                    oldArrow1Ang = oldArrowAng;
                    oldArrowAng = angle;
                } else {
                    x = halfW + Math.cos(oldArrow2Ang) * (halfW - arrowOffset);
                    y = halfH + Math.sin(oldArrow2Ang) * (halfH - arrowOffset);
                    cx = halfW + Math.cos(oldArrow2Ang) * (halfW + circleOffset);
                    cy = halfH + Math.sin(oldArrow2Ang) * (halfH + circleOffset);
        
                    arrow3.setAttribute("x2", x);
                    arrow3.setAttribute("y2", y);
                    circle3.setAttribute("cx", cx);
                    circle3.setAttribute("cy", cy);
        
                    oldArrow3Ang = oldArrow2Ang;

                    a2x_start = halfW + Math.cos(oldArrow2Ang) * arc2Rad;
                    a2y_start = halfW + Math.sin(oldArrow2Ang) * arc2Rad;

                    angles = makeIntervals(oldArrowAng, angle, 100);
                    angles1 = makeIntervals(oldArrow1Ang, oldArrowAng, 100);
                    angles2 = makeIntervals(oldArrow2Ang, oldArrow1Ang, 100);

                    temp = oldArrow2Ang;
                    i = 0;
                    document.getElementById("calc-btn").disabled = true;
                    document.getElementById("reset-btn").disabled = true;
                    interval = setInterval(function(){
                        ang = angles[i];
                        x = halfW + Math.cos(ang) * (halfW - arrowOffset);
                        y = halfH + Math.sin(ang) * (halfH - arrowOffset);
                        cx = halfW + Math.cos(ang) * (halfW + circleOffset);
                        cy = halfH + Math.sin(ang) * (halfH + circleOffset);
                    
                        arrow.setAttribute("x2", x);
                        arrow.setAttribute("y2", y);
                        circle.setAttribute("cx", cx);
                        circle.setAttribute("cy", cy);
                    
                        ax_end = halfW + Math.cos(ang) * arcRad;
                        ay_end = halfW + Math.sin(ang) * arcRad;

                        ang1 = angles1[i];
                        x = halfW + Math.cos(ang1) * (halfW - arrowOffset);
                        y = halfH + Math.sin(ang1) * (halfH - arrowOffset);
                        cx = halfW + Math.cos(ang1) * (halfW + circleOffset);
                        cy = halfH + Math.sin(ang1) * (halfH + circleOffset);
                    
                        arrow1.setAttribute("x2", x);
                        arrow1.setAttribute("y2", y);
                        circle1.setAttribute("cx", cx);
                        circle1.setAttribute("cy", cy);
                    
                        ax_start = halfW + Math.cos(ang1) * arcRad;
                        ay_start = halfW + Math.sin(ang1) * arcRad;
                        a1x_end = halfW + Math.cos(ang1) * arc1Rad;
                        a1y_end = halfW + Math.sin(ang1) * arc1Rad;

                        ang2 = angles2[i];
                        x = halfW + Math.cos(ang2) * (halfW - arrowOffset);
                        y = halfH + Math.sin(ang2) * (halfH - arrowOffset);
                        cx = halfW + Math.cos(ang2) * (halfW + circleOffset);
                        cy = halfH + Math.sin(ang2) * (halfH + circleOffset);
                    
                        a1x_start = halfW + Math.cos(ang2) * arc1Rad;
                        a1y_start = halfW + Math.sin(ang2) * arc1Rad;
                        a2x_end = halfW + Math.cos(ang2) * arc2Rad;
                        a2y_end = halfW + Math.sin(ang2) * arc2Rad;

                        arrow2.setAttribute("x2", x);
                        arrow2.setAttribute("y2", y);
                        circle2.setAttribute("cx", cx);
                        circle2.setAttribute("cy", cy);

                        if(!isAngleSharp(ang, ang1)) {
                            arc.setAttribute("d", "M " + ax_start + " " + ay_start + " A " + arcRad + " " + arcRad + ", 180, 1, 1, " + ax_end + " " + ay_end);
                        } else {
                            arc.setAttribute("d", "M " + ax_start + " " + ay_start + " A " + arcRad + " " + arcRad + ", 180, 0, 1, " + ax_end + " " + ay_end);
                        }
    
                        if(!isAngleSharp(ang1, ang2)) {
                            arc1.setAttribute("d", "M " + a1x_start + " " + a1y_start + " A " + arc1Rad + " " + arc1Rad + ", 180, 1, 1, " + a1x_end + " " + a1y_end);
                        } else {
                            arc1.setAttribute("d", "M " + a1x_start + " " + a1y_start + " A " + arc1Rad + " " + arc1Rad + ", 180, 0, 1, " + a1x_end + " " + a1y_end);
                        }

                        if(!isAngleSharp(ang2, temp)) {
                            arc2.setAttribute("d", "M " + a2x_start + " " + a2y_start + " A " + arc2Rad + " " + arc2Rad + ", 180, 1, 1, " + a2x_end + " " + a2y_end);
                        } else {
                            arc2.setAttribute("d", "M " + a2x_start + " " + a2y_start + " A " + arc2Rad + " " + arc2Rad + ", 180, 0, 1, " + a2x_end + " " + a2y_end);
                        }

                        i++;
                        if(i == angles.length) {
                            clearInterval(interval);
                            document.getElementById("calc-btn").disabled = false;
                            document.getElementById("reset-btn").disabled = false;
                        }
                    }, 2);
                    oldArrow2Ang = oldArrow1Ang;
                    oldArrow1Ang = oldArrowAng;
                    oldArrowAng = angle;
                }
            } else {
                x = halfW + Math.cos(oldArrow1Ang) * (halfW - arrowOffset);
                y = halfH + Math.sin(oldArrow1Ang) * (halfH - arrowOffset);
                cx = halfW + Math.cos(oldArrow1Ang) * (halfW + circleOffset);
                cy = halfH + Math.sin(oldArrow1Ang) * (halfH + circleOffset);
    
                a1x_start = halfW + Math.cos(oldArrow1Ang) * arc1Rad;
                a1y_start = halfW + Math.sin(oldArrow1Ang) * arc1Rad;

                arrow2.setAttribute("x2", x);
                arrow2.setAttribute("y2", y);
                circle2.setAttribute("cx", cx);
                circle2.setAttribute("cy", cy);
    
                oldArrow2Ang = oldArrow1Ang;
    
                angles = makeIntervals(oldArrowAng, angle, 100);
                angles1 = makeIntervals(oldArrow1Ang, oldArrowAng, 100);
    
                temp = oldArrow1Ang;
                i = 0;
                document.getElementById("calc-btn").disabled = true;
                document.getElementById("reset-btn").disabled = true;
                interval = setInterval(function(){
                    ang = angles[i];
                    x = halfW + Math.cos(ang) * (halfW - arrowOffset);
                    y = halfH + Math.sin(ang) * (halfH - arrowOffset);
                    cx = halfW + Math.cos(ang) * (halfW + circleOffset);
                    cy = halfH + Math.sin(ang) * (halfH + circleOffset);
    
                    arrow.setAttribute("x2", x);
                    arrow.setAttribute("y2", y);
                    circle.setAttribute("cx", cx);
                    circle.setAttribute("cy", cy);
    
                    ax_end = halfW + Math.cos(ang) * arcRad;
                    ay_end = halfW + Math.sin(ang) * arcRad;

                    ang1 = angles1[i];
                    x = halfW + Math.cos(ang1) * (halfW - arrowOffset);
                    y = halfH + Math.sin(ang1) * (halfH - arrowOffset);
                    cx = halfW + Math.cos(ang1) * (halfW + circleOffset);
                    cy = halfH + Math.sin(ang1) * (halfH + circleOffset);
    
                    ax_start = halfW + Math.cos(ang1) * arcRad;
                    ay_start = halfW + Math.sin(ang1) * arcRad;
                    a1x_end = halfW + Math.cos(ang1) * arc1Rad;
                    a1y_end = halfW + Math.sin(ang1) * arc1Rad;

                    arrow1.setAttribute("x2", x);
                    arrow1.setAttribute("y2", y);
                    circle1.setAttribute("cx", cx);
                    circle1.setAttribute("cy", cy);

                    if(!isAngleSharp(ang, ang1)) {
                        arc.setAttribute("d", "M " + ax_start + " " + ay_start + " A " + arcRad + " " + arcRad + ", 180, 1, 1, " + ax_end + " " + ay_end);
                    } else {
                        arc.setAttribute("d", "M " + ax_start + " " + ay_start + " A " + arcRad + " " + arcRad + ", 180, 0, 1, " + ax_end + " " + ay_end);
                    }

                    if(!isAngleSharp(ang1, temp)) {
                        arc1.setAttribute("d", "M " + a1x_start + " " + a1y_start + " A " + arc1Rad + " " + arc1Rad + ", 180, 1, 1, " + a1x_end + " " + a1y_end);
                    } else {
                        arc1.setAttribute("d", "M " + a1x_start + " " + a1y_start + " A " + arc1Rad + " " + arc1Rad + ", 180, 0, 1, " + a1x_end + " " + a1y_end);
                    }

                    i++;
                    if(i == angles.length) {
                        clearInterval(interval);
                        document.getElementById("calc-btn").disabled = false;
                        document.getElementById("reset-btn").disabled = false;
                    }
                }, 2);
    
                oldArrow1Ang = oldArrowAng;
                oldArrowAng = angle;
            }
        } else {
            x = halfW + Math.cos(oldArrowAng) * (halfW - arrowOffset);
            y = halfH + Math.sin(oldArrowAng) * (halfH - arrowOffset);
            cx = halfW + Math.cos(oldArrowAng) * (halfW + circleOffset);
            cy = halfH + Math.sin(oldArrowAng) * (halfH + circleOffset);
    
            ax_start = halfW + Math.cos(oldArrowAng) * arcRad;
            ay_start = halfW + Math.sin(oldArrowAng) * arcRad;

            arrow1.setAttribute("x2", x);
            arrow1.setAttribute("y2", y);
            circle1.setAttribute("cx", cx);
            circle1.setAttribute("cy", cy);
    
            oldArrow1Ang = oldArrowAng;
    
            angles = makeIntervals(oldArrowAng, angle, 100);
            i = 0;
            document.getElementById("calc-btn").disabled = true;
            document.getElementById("reset-btn").disabled = true;
            interval = setInterval(function(){
                ang = angles[i];
                x = halfW + Math.cos(ang) * (halfW - arrowOffset);
                y = halfH + Math.sin(ang) * (halfH - arrowOffset);
                cx = halfW + Math.cos(ang) * (halfW + circleOffset);
                cy = halfH + Math.sin(ang) * (halfH + circleOffset);
    
                ax_end = halfW + Math.cos(ang) * arcRad;
                ay_end = halfW + Math.sin(ang) * arcRad;
                if(!isAngleSharp(ang, oldArrow1Ang)) {
                    arc.setAttribute("d", "M " + ax_start + " " + ay_start + " A " + arcRad + " " + arcRad + ", 45, 1, 1, " + ax_end + " " + ay_end);
                } else {
                    arc.setAttribute("d", "M " + ax_start + " " + ay_start + " A " + arcRad + " " + arcRad + ", 45, 0, 1, " + ax_end + " " + ay_end);
                }
                arrow.setAttribute("x2", x);
                arrow.setAttribute("y2", y);
                circle.setAttribute("cx", cx);
                circle.setAttribute("cy", cy);
                i++;
                if(i == angles.length) {
                    clearInterval(interval);
                    document.getElementById("calc-btn").disabled = false;
                    document.getElementById("reset-btn").disabled = false;
                }
            }, 2);
    
            oldArrowAng = angle;
        }
    } else {
        x = halfW + Math.cos(angle) * (halfW - arrowOffset);
        y = halfH + Math.sin(angle) * (halfH - arrowOffset);
        cx = halfW + Math.cos(angle) * (halfW + circleOffset);
        cy = halfH + Math.sin(angle) * (halfH + circleOffset);
    
        arrow.setAttribute("x2", x);
        arrow.setAttribute("y2", y);
        circle.setAttribute("cx", cx);
        circle.setAttribute("cy", cy);
    
        oldArrowAng = angle;
    }
}

function makeIntervals(start, end, amount) {
    ret = [start];
    if(end < start) {
        end += 2 * Math.PI;
    }
    d = (end - start) / (amount - 1);
    for(var i = 1; i < amount; i++) {
        ret.push((start + (d * i)) % (2*Math.PI));
    }
    return ret;
}

function isAngleSharp(ang0, ang1) {
    if(ang0 > ang1) {
        return (ang0 - ang1) < Math.PI;
    } else {
        return (ang1 - ang0) > Math.PI;
    }
}