var n;
var p;

function deleteExplanation() {
    $(".explanation_step").each(function() {
        $(this).remove();
    });
}

function calculateInverse() {
    n = parseInt($("#n").val(), 10);
    p = parseInt($("#p").val(), 10);

    deleteExplanation();

    if(!isNaN(n) && !isNaN(p)) {

        $("#input_mistake").text("");
        $("#result").show();
        
        var a = [0,1];
        var remainder = p;
        var myn = n;
        var myp = p;
        var iteration = 0;
        var inverse;
        while(remainder != 0) {
            
            var c = Math.floor(myp/myn);
            remainder = myp%myn;
            a.push(mod(a[iteration] - a[iteration+1]*c, p));
            
            if(iteration < 2) {
                $("#explanation_euclide").append("<p class='explanation_step'>" + myp + " = <span style='color:red'>" + c + "</span>*" + myn + " + " + remainder + ", &emsp; a<sub>" + iteration + "</sub> = <span style='color:#7f7fff'>"  + a[iteration] + "</span></p>");
            } else {
                $("#explanation_euclide").append("<p class='explanation_step'>" + myp + " = <span style='color:red'>" + c + "</span>*" + myn + " + " + remainder + ", &emsp; a<sub>" + iteration + "</sub> = <span style='color:#7f7fff'>" + a[iteration-2] + "</span> - <span style='color:#7f7fff'>" + a[iteration-1] + "</span>*<span style='color:red'>" + c + "</span> mod " + p + " = <span style='color:#7f7fff'>" + a[iteration] + "</span></p>");

            }

            myp = myn;
            myn = remainder;
            
            if(remainder == 1) {
                inverse = a[a.length-1];
            }
        
            iteration++;
        }
        
        $("#explanation_euclide").append("<p class='explanation_step'> &emsp; &ensp; &emsp; &ensp; &emsp; &emsp; a<sub>" + iteration + "</sub> = <span style='color:#7f7fff'>" + a[iteration-2] + "</span> - <span style='color:#7f7fff'>" + a[iteration-1] + "</span>*<span style='color:red'>" + c + "</span> mod " + p + " = <span style='color:#7f7fff'>" + a[iteration] + "</span></p>");
        
        if(inverse === undefined) {
            $("#inverse_result").text("Ker je največji skupni delitelj števil " + n + " in " + p + " večji od 1, modularni inverz ne obstaja");
            $("#inverse_result").css("color", "red");
            deleteExplanation();
            $("#explain_icon").hide();
            $("#explanation_euclide").hide();
        } else {
            /*$("#inverse_result").html("m = " + n + "<sup>-1</sup> mod " + p + " = " + inverse);*/
            $("#inverse_result").text("$$n^{-1} = " + n + "^{-1} \\mod " + p + "= " + inverse + "$$");
            $("#inverse_result").css("color", "black");
            MathJax.Hub.Queue(["Typeset",MathJax.Hub, 'inverse_result']);
            $("#explain_icon").show();
            
        }
    } else {
        $("#input_mistake").text("Prosim, vnesite števili n in p.");
        $("#input_mistake").css("color", "red");
        $("#result").hide();
    }
}

function mod(n, m) {
    return ((n % m) + m) % m;
  }


window.addEventListener("load", function(event) {
    $("#button_inverse").click(calculateInverse);
    
    $("#explanation_euclide").hide();
    $("#explain_icon").hide();
    $("#explain_icon").click(function() {
        $('#explanation_euclide').toggle('1000');
        $(this).toggleClass("fa-angle-double-down fa-angle-double-up");
    });

    document.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("button_inverse").click();
          }
    });

});