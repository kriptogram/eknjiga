window.addEventListener('load', function(){
    var a, n, st, cikel, indPrviCikel; //cikel:bool nam pove, ce se je ze pojavil cikel, indPrviCikel indeks, kjer se pojavi prvi cikel
    var vrednostCikla; // vrednost, pri kateri se pojavi cikel
    var color; //barva s katero barvamo
    var ns=document.getElementById("ns");
    var tab=document.getElementById("table");
    var urejena=document.getElementById("tableUrejena");
    var mess=document.getElementById("mess");
    var endOfCycleNotification=this.document.getElementById("cycle-end-notification");
    var opacity=1;
    var nepravilno=false;
    
    document.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("ns").click();
          }
    });

    document.getElementById("forma").addEventListener('submit', function(e){
        e.preventDefault();
        nepravilno=false;
        color="A6E1A1";
        vrednostCikla=-1;
        cikel=false;
        mess.innerHTML="";
        while(urejena.rows[0])urejena.deleteRow(0);
        while(tab.rows[0])tab.deleteRow(0);
        a=document.getElementById("a").value;
        n=document.getElementById("n").value;


        var reg = new RegExp('^[0-9]+$');
        if(reg.test(a)==false){
        	mess.innerHTML="Nepravilen vhod";
        	nepravilno=true;
        	return;
        }
        if(reg.test(n)==false){
        	mess.innerHTML="Nepravilen vhod";
        	nepravilno=true;
        	return;
        }
        st=1;
        var row=tab.insertRow(0);
        var c=row.insertCell(0);
        var kStr="k&equiv;log<sub>a</sub>c";
        str=kStr.bold();
        c.innerHTML=str;
        c.style.width='100px';
        c=row.insertCell(1);
        str="c";
        str=str.bold();
        c.innerHTML=str;
        c.style.width='100px';
        
        row=urejena.insertRow(0);
        c=row.insertCell(0);
        c.innerHTML='c';
        c.style.width='100px';
        c=row.insertCell(1);
        c.innerHTML=kStr;
        c.style.width='100px';

        for (let i = 0; i < n; i++) {
            row=urejena.insertRow(i+1);
            c=row.insertCell(0);
            c.style.width='100px';
            c.innerHTML=i;
            c=row.insertCell(1);
            c.style.width='100px';
        }

        endOfCycleNotification.classList.add("d-none");
    });
    
    ns.addEventListener('click', function(e){
        if(nepravilno==true)return;
        if(st==1){
            var row=tab.insertRow(1);
            var c=row.insertCell(0);
            c.style.width='100px';
            c.innerHTML='0';
            c=row.insertCell(1);
            c.innerHTML='1';
            c.style.width='100px';

            urejena.rows[2].cells[1].innerHTML = "0";
            st++;
        }else{  
            mess.innerHTML="";
            var row=tab.insertRow(st);
            
            //nastavimo prvo celico... presnja +1
            var presnji=tab.rows[st-1].cells[0].innerHTML;
            presnji=parseInt(presnji, 10);
            presnji++;
            var c=row.insertCell(0);
            c.style.width='100px';
            c.innerHTML=presnji;
            
            //nastavimo drugo celico
            presnji=tab.rows[st-1].cells[1].innerHTML;
            presnji=parseInt(presnji, 10);
            presnji=(presnji*a) % n;
            c=row.insertCell(1);
            c.style.width='100px';
            c.innerHTML=presnji;

            //po potrebi pobarvamo za oznaciti nov cikel
            if(cikel==true){
                if(presnji==vrednostCikla)toggleColor();
                row.style.backgroundColor=color;
            }

            //housekeeping
            if(cikel==true && st>=indPrviCikel+10){
                console.log("ekolo " + indPrviCikel);
                tab.deleteRow(indPrviCikel-1);
            }else
                st++;


            //popravimo se urejeno tabelo in preverimo, ce je prislo do cikla
            if(cikel==false){    

                //preverimo, ce je pri tem koraku prislo do cikla
                var trenutna=tab.rows[st-1].cells[1].innerHTML;
                trenutna=parseInt(trenutna, 10);
                for(var i=1; i<st-1; i++){
                    var vrednost=parseInt(tab.rows[i].cells[1].innerHTML, 10);
                    if(vrednost==trenutna){
                        vrednostCikla=vrednost;
                        indPrviCikel=st;
                        cikel=true;

                        endOfCycleNotification.classList.remove("d-none");
                        addUnexistingLogs()

                        tab.rows[i].classList.add("green");
                        tab.rows[st-1].classList.add("green");
                        for(var j=i+1; j<st-1; j++){
                            tab.rows[j].classList.add("lime");
                        }
                    }
                }

                if(cikel==false){
                    var data=parseInt(tab.rows[st-1].cells[1].innerHTML, 10);
                    urejena.rows[data + 1].cells[1].innerHTML = st-2;
                }
            }
            return;
        }
    });

    function addUnexistingLogs() {
        for (let i = 0; i < urejena.rows.length; i++) {
            const row = urejena.rows[i];
            const cell = row.cells[1];
            if (cell.innerHTML === "") {
                cell.innerHTML = "Ne obstaja!"; 
                row.classList.add("text-muted");
            }
        }
    }
    function toggleColor(){
        if(color=='A9F7A3'){
            color='A6E1A1'
        }else color='A9F7A3';
    }
})