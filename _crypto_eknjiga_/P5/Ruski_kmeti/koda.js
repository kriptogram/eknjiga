
window.addEventListener('load', function() {
  
  var next_step=document.getElementById("next_step");
  next_step.style.visibility="hidden";
 
  var tabOut=document.getElementById("myTable");  
  var tab;
  var stVidnih=0;
  var baza;
  var stSestej=0; //
  var sestej=0; //trenutna vsota
  var prvo, drugo;
  var rez = document.getElementById("rezultat");
  var veljavenVhod=false;

  document.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
          event.preventDefault();
          document.getElementById("next_step").click();
        }
  });

  document.getElementById("reset").addEventListener("submit", function(event){
    event.preventDefault();
    next_step.disabled=false;
    veljavenVhod=true;
    next_step.style.visibility="visible";
    while(tabOut.rows[0])tabOut.deleteRow(0);
    rez.innerHTML="";
    prvo=document.getElementById("prvo_st").value;
    drugo=document.getElementById("drugo_st").value;
    baza=document.getElementById("rs-range-line").value;

    if(prvo=="0"){
      rez.innerHTML="Če množimo z 0 neko število dobimo vedno 0."
      veljavenVhod=false;
      return;
    }
    
    //preverimo pravilnost podatkov
    var error=document.getElementById("error");
    if(pravilenVhod(prvo, baza)==true && pravilenVhod(drugo, baza)==true && prvo!="" && drugo!=""){
      //izpisi mozne tiste
      error.innerHTML="";
    }else{
      error.innerHTML="Nepravilen vhod!";
      document.getElementById("izpisiBazo").innerHTML="";
      veljavenVhod=false;
      return;
    }
    document.getElementById("izpisiBazo").innerHTML="Trenutno izbrana baza je <strong>"+baza+"</strong>";
    veljavenVhod=true;
    
    var prviNum = parseInt(prvo, baza);
    var drugiNum = parseInt(drugo, baza);
    stVidnih=0;
    stSestej=0; 
    sestej=0;
  
    tab=null;
    tab=new Array(100);
    for(i=0; i<100; i++)tab[i]=new Array(2);
    
    var k=0;
    while(prviNum>0){
      tab[k]=[prviNum.toString(baza), drugiNum.toString(baza)];
      prviNum=Math.floor(prviNum/2);
      drugiNum *= 2;
      k++;
    }
  });
  
  function pravilenVhod(st, b){
    for(var i=0; i<st.length; i++){
      if(!parseInt(st.charAt(i), baza) && parseInt(st.charAt(i), baza)!=0)return false;
    }
    return true;
  }
  
  next_step.addEventListener("click", function(event){
    if(veljavenVhod==false)return;
    if(tab[stVidnih][0]){
      var row=tabOut.insertRow(stVidnih);
      var cell=row.insertCell(0);
      cell.innerHTML=tab[stVidnih][0];
      cell.style.width = '80px';
      //cell.addEventListener("onmouseover", izpisiDesetisko(cell.innerHTML));
      
      cell=row.insertCell(1);
      cell.innerHTML=tab[stVidnih][1];
      cell.style.textAlign="right";
      cell.style.width = '80px';
      //cell.addEventListener("onmouseover", izpisiDesetisko(cell.innerHTML));
      
      cell=row.insertCell(2);
      cell.innerHTML=" ";
      cell.style.width="250px";
      stVidnih++;
    }else if(tab[stSestej][0]){
      if(stSestej>0)tabOut.rows[stSestej-1].style.color="black";
      tabOut.rows[stSestej].style.color="red";
      if(parseInt(tab[stSestej][0], baza) % 2 == 0){
          var prec = tab[stSestej][1].toString(baza);
          prec=prec.strike();
          tabOut.rows[stSestej].cells[1].innerHTML=prec;
          var celica=tabOut.rows[stSestej].cells[2];
          var str = "0 + " + sestej.toString(baza) +" = " + sestej.toString(baza);
          celica.innerHTML = str;
          celica.style.width="250px";
          celica.style.textAlign="right";
        }else{
          celica=tabOut.rows[stSestej].cells[2];
          var novSum = parseInt(tab[stSestej][1], baza)+sestej;
          celica.innerHTML = tab[stSestej][1]+" + "+sestej.toString(baza)+" = "  + novSum.toString(baza);
          sestej=novSum;
          celica.style.width="250px";
          celica.style.textAlign="right";
        }
        sestej.innerHTML="Trenuten rezultat je "+sestej;
        stSestej++;
        if(stSestej<stVidnih)rez.innerHTML="Trenutni rezultat je: " + sestej;
        else rez.innerHTML="Dokončen rezultat je: " + sestej.toString(baza);
      }else{
        tabOut.rows[stSestej-1].style.color="black";
        next_step.disabled=true;
      }
  });
  
  document.getElementById("myTable").addEventListener("mouseover", function(event) {
    var td = event.target;
    while (td !== this && !td.matches("td")) {
        td = td.parentNode;
    }
    if (td === this) {
    } else {
      izpisiDesetisko(td.innerHTML, event);
    }
  });
  
  document.getElementById("myTable").addEventListener("mouseout", function(event) {
    if(document.getElementById("tempDesetisko"))
      document.getElementById("tempDesetisko").remove();
  });
  
  function izpisiDesetisko(e, event) {
    if(e==" ")return;
    e=getData(e);

    if(document.getElementById("tempDesetisko"))return;
    var div = document.createElement("div");
    div.style.width = "360px";
    div.style.height = "16px";
    div.style.borderRadius="5px";
    div.style.background = "#11171F";
    div.style.fontSize="13px";
    div.style.textAlign="center";
    div.style.color = "white";
    div.style.position="fixed";
    div.style.top=(event.clientY+10)+"px";
    div.style.left=(event.clientX+20)+"px";
    div.id="tempDesetisko";
    var m=document.getElementById("rs-range-line").value;
    m=parseInt(m, 10);
    var sporocilo="Desetiška vrednost števila "+e+" v modulu "+baza+" je "+parseInt(e, baza);
    div.innerHTML = sporocilo;

    document.body.appendChild(div);
  }
  
  function getData(e){
    if(e.charAt(0)=="<"){
      while(e.charAt(0)!=">")e=e.substring(1);
      e=e.substring(1);
      var i=0;
      while(e.charAt(i)!="<") i++;
      e=e.substring(0, i);
      return e;
    }else if(e.includes("+")){
      var i=0;
      while(e.charAt(i)!="=")i++;
      i++;
      e=e.substring(i);
      return e; 
    }else{
      return e;
    }
  }

  function stevila(n){
	  var rez="0";
	  for(var i=1; i<10 && i<n; i++){
		  rez=rez+", " + i;
	  }
	  if(n<10)return rez+".";
	  if(n>=11)rez=rez + ", a";
	  if(n>=12)rez=rez + ", b";
	  if(n>=13)rez=rez + ", c";
	  if(n>=14)rez=rez + ", d";
	  if(n>=15)rez=rez + ", e";
	  if(n>=16)rez=rez + ", f";
	  return rez+".";
  }

  //izpis modula
  document.getElementById("modul").addEventListener("mouseover", function(e){
    if(document.getElementById("tempModul"))return;
    var div = document.createElement("div");
    div.style.width = "300px";
    div.style.height = "70px";
    div.style.borderRadius="5px";
    div.style.background = "#11171F";
    div.style.fontSize="13px";
    div.style.textAlign="center";
    div.style.color = "white";
    div.style.position="fixed";
    div.style.top=(e.clientY+10)+"px";
    div.style.left=(e.clientX+20)+"px";
    div.id="tempModul";
    var m=document.getElementById("rs-range-line").value;
    m=parseInt(m, 10);
    var sporocilo="Modul določa, katere števke lahko uporabljaš. Trenutno kaže modul na \
      "+m+", torej lahko uporabljaš naslednje števke:</br>"+stevila(m);
    div.innerHTML = sporocilo;

    document.body.appendChild(div);
  });

  document.getElementById("modul").addEventListener("mouseout", function(e){
    if(document.getElementById("tempModul"))document.getElementById("tempModul").remove();
  });

  function getPos(el) {
    for (var lx=0, ly=0;
         el != null;
         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
    return {x: lx,y: ly};
  }


  var rangeSlider = document.getElementById("rs-range-line");
  var rangeBullet = document.getElementById("rs-bullet");

  rangeSlider.addEventListener("input", showSliderValue, false);

  function showSliderValue() {
    rangeBullet.innerHTML = rangeSlider.value;
    var bulletPosition = ((rangeSlider.value-2) /rangeSlider.max);
    rangeBullet.style.left = (bulletPosition * 281) + "px";
  }
  
});