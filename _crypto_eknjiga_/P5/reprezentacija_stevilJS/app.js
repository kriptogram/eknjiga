

function toRoman(num) {
    var result = '';
    var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var roman = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
    for (var i = 0; i <= decimal.length; i++) {
        while (num % decimal[i] < num) {
            result += roman[i];
            num -= decimal[i];
        }
    }
    return result;
}

function fromRoman(str) {
    var result = 0;
    // the result is now a number, not a string
    var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var roman = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
    for (var i = 0; i <= decimal.length; i++) {
        while (str.indexOf(roman[i]) === 0) {
            result += decimal[i];
            str = str.replace(roman[i], '');
        }
    }
    return result;
}

function pretvori_v_dec(txt, stev_sistem) {
    if (stev_sistem == stev_sistemi[0]) {
        return parseInt(txt, 10)
    }
    if (stev_sistem == stev_sistemi[1]) {
        return parseInt(txt, 2)
    }
    if (stev_sistem == stev_sistemi[2]) {
        return txt.trim().length
    }
    if (stev_sistem == stev_sistemi[3]) {
        return parseInt(txt, 8)
    }
    if (stev_sistem == stev_sistemi[4]) {
        return parseInt(txt, 16)
    }
    if (stev_sistem == stev_sistemi[5]) {
        return fromRoman(txt)
    }
}

function radixEconomy(N, b) {
    if (N < 1) {
        return (b)
    }
    return b * Math.floor(1 + Math.log2(N) / Math.log2(b))
}


stev_sistemi = ['decimalni', 'dvojiški', 'eniški', 'osmiški', 'šestnajstiški', 'rimski']
header_table = ['st. sistem', 'zapis števila', 'ekonomija baze']

izracunaj_tabelo()


function izracunaj_tabelo() {
    var Parent = document.querySelector("table");
    while (Parent.hasChildNodes()) {
        Parent.removeChild(Parent.firstChild);
    }
    strN = document.getElementsByName('stevilo')[0].value;
    stsistem = document.getElementsByName('stsistem')[0].value;
    N = pretvori_v_dec(strN, stsistem)


    if (isNaN(N)) {
        document.getElementsByName('error')[0].innerHTML = 'Zapis števila ni pravi.'
    } else if (N < 0) {
        document.getElementsByName('error')[0].innerHTML = 'Število ni pozitivno.'
    } else {
        document.getElementsByName('error')[0].innerHTML = ''

        zapis_stevila = [N.toString(10),
        N.toString(2),
        (N <= 100) ? '|'.repeat(N) : '|'.repeat(100) + '...',
        N.toString(8),
        N.toString(16).toUpperCase(),
        (N <= 3999) ? toRoman(N) : 'ni predstavljivo z rimskimi števili']

        ekonomija_baze = [radixEconomy(N, 10),
        radixEconomy(N, 2),
            N,
        radixEconomy(N, 8),
        radixEconomy(N, 16),
            '']

        let table_data = []
        for (var i = 0; i < stev_sistemi.length; i++) {
            table_data.push({
                stevilski_sistem: stev_sistemi[i],
                zapis_st: zapis_stevila[i],
                ek_baze: ekonomija_baze[i]
            })
        }


        function generateTableHead(table, data) {
            let thead = table.createTHead();
            let row = thead.insertRow();
            for (let key of data) {
                let th = document.createElement("th");
                th.scope ="col";
                let text = document.createTextNode(key);
                th.appendChild(text);
                row.appendChild(th);
            }
        }
        function generateTable(table, data) {
            for (let element of data) {
                let row = table.insertRow();
                for (key in element) {
                    let cell = row.insertCell();
                    let text = document.createTextNode(element[key]);
                    cell.appendChild(text);
                }
            }
        }
        let table = document.querySelector("table");
        generateTableHead(table, header_table);
        generateTable(table, table_data);

        var rows = document.querySelectorAll("tr");

        for (i = 1, len = rows.length; i < len; i++) {
            var cols = rows[i].querySelectorAll('td')
            cols[0].style.background = 'rgba(0, 129, 128, .5)';
            cols[1].style.background = 'rgba(0, 129, 128, .3)';
            cols[2].style.background = 'rgba(0, 129, 128, .3)';
        }
        head = document.querySelectorAll("th");
        for (j = 0; j < head.length-1; j++) {
            head[j].style.background = '#008180';
            head[j].innerHTML = '<a style="color:#FFFFFF;">' + head[j].innerHTML + '<a>';
        }
        head[head.length-1].style.background = '#008180';
        head[head.length-1].innerHTML = '<a style="color:#FFFFFF;" href="https://en.wikipedia.org/wiki/Radix_economy">' +  head[head.length-1].innerHTML + '<a>';
    }
}