window.addEventListener('load', function(){

	//globalne spremenljivkke
	var bin=document.getElementById("binarnaPredstavitev");
	var mess1=document.getElementById("mess1");
	var tab=document.getElementById("tab");
	var reset=document.getElementById("reset");
	var forma=document.getElementById("forma");
	var ns=document.getElementById("ns");
	var a, b, n;
	var stevec; //stevec pove, koliko vrstic je vidnih
	var niz; // niz  znakov KM
	var binarno;
	var krat="<span>&#183;</span>";

	document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("ns").click();
		  }
	});

	forma.addEventListener('submit', function(event){
		event.preventDefault(); //Returning false will prevent that the form submission completes
		stevec=0;
		mess1.innerHTML="";
		while(tab.rows[0])tab.deleteRow(0);
		a=parseInt(document.getElementById("a").value);
		b=parseInt(document.getElementById("b").value);
		n=parseInt(document.getElementById("n").value);
		niz="";
		var temp;

		binarno=b.toString(2);
		if(b<n){
			temp="Najprej izračunamo binarno predstavitev operanda b:";
			temp=temp+"</br>    "+b+"<sub>10</sub> = "+binarno+"<sub>2</sub></br>";
		}else{
			temp="Ker velja b<span>&#8805;</span>n, lahko zreducijamo operand b z operacijo b=b mod n, torej:\
				</br>          "+b+" mod "+n+" = "+b%n;
			b=b%n;
			binarno=b.toString(2);
			temp=temp+"</br>Sedaj izračunamo binarno predstavitev operanda b:";
			temp=temp+"</br>    "+b+"<sub>10</sub> = "+binarno+"<sub>2</sub></br>";
		}
		bin.innerHTML=temp;	
		ns.disabled = false;
	});

	ns.addEventListener('click', function(){
		if(stevec==binarno.length+1){
			mess1.innerHTML="Torej velja </br>"+a+krat+b+" = "+presnji()+" mod "+n;	
			ns.disabled = true;	// Disable button if not needed.	
			return;
		}

		var row;
		row=tab.insertRow(stevec);
		var cell=row.insertCell(0);
		cell.style.width="50px";
		if(stevec==0){
			cell.innerHTML=binarno.charAt(stevec);
			cell=row.insertCell(1);
			cell.style.width="100px";
			cell.innerHTML="2 "+krat+" 0 = 0";
			cell=row.insertCell(2);
			cell.style.width="200px";
			if(binarno.charAt(0)=="1")cell.innerHTML=sestej(0);
			else cell.innerHTML="0";
			row=tab.insertRow(0);
			cell=row.insertCell(0);
			cell.innerHTML="Vrednost"
			cell=row.insertCell(1);
			cell.innerHTML="Podvoji"
			cell=row.insertCell(2);
			cell.innerHTML="Seštej, če je vrednost=1";
			stevec+=2;
		}else{
			cell.innerHTML=binarno.charAt(stevec-1);
			cell=row.insertCell(1);
			cell.style.width="100px";
			cell.innerHTML=podvoji();
			cell=row.insertCell(2);
			cell.style.width="200px";
			var pr=trenutni();
			if(binarno.charAt(stevec-1)=="0"){
				//log("0");
				cell.innerHTML=pr;
			}else{
				//console.log("ekolo");
				cell.innerHTML=sestej(pr);
			}
			stevec++;
		}
	});

	function sestej(st){
		var str=st.toString(10)+" + "+a.toString(10)+" mod "+n+" = ";
		var rez=(a+st)%n;
		str=str+rez.toString(10);
		return str;
	}

	function presnji(){
		var ind=stevec-1;
		var vsebina=tab.rows[ind].cells[2].innerHTML;
		var i=0;
	 	while(vsebina.charAt(i)!="=" && i<vsebina.length)i++;
	 	i++;
	 	if(i>=vsebina.length)
	 		return parseInt(vsebina, 10);
	 	vsebina=vsebina.substring(i);
	 	return parseInt(vsebina, 10);
	}

	function trenutni(){
		var ind=stevec;
		var vsebina=tab.rows[ind].cells[1].innerHTML;
		var i=0;
	 	while(vsebina.charAt(i)!="=" && i<vsebina.length)
	 		i++;
	 	i++;
	 	vsebina=vsebina.substring(i);
	 	return parseInt(vsebina, 10);
	}

	function podvoji(){
		var vrednost=presnji();
		var str="2 "+krat+vrednost+" = ";
		var rez=2*vrednost;
		rez=rez%n;
		str=str+rez.toString(10);
		return str;
	}
});