window.addEventListener('load', function(){

	document.getElementById("reset").addEventListener("submit", function(e){
		e.preventDefault();
		var datum=document.getElementById("datum").value;
		if(datum.length==0){
			document.getElementById("rez").innerHTML="Nepravilen vhod - vnesi veljaven datum.";
			return;
		}
		var emso=datum.substring(8);
		emso=emso+datum.substring(5, 7);
		emso=emso+datum.substring(1, 4);
		var e=document.getElementById("drzava");
		var drzava=e.options[e.selectedIndex].value;
		if(drzava!="-1"){
			emso+=drzava;
		}else{
			var drugo=document.getElementById("drzavaDrugo").value;	
			if(drugo.length==2 && parseInt(drugo.charAt(0)) && parseInt(drugo.charAt(1))){
				emso+=drugo;
			}else{
				//nepravilen vhod
				document.getElementById("rez").innerHTML="Nepravilen vhod - koda države ima vedno 2 cifri.";
				return;
			}
		}
		var zapo=document.getElementById("zapo").value;
		if(zapo.length==3 && isNaN(zapo)==false){
			emso+=zapo;
		}else{
			document.getElementById("rez").innerHTML="Nepravilen vhod - zaporedna številka rojstva ima vedno 3 cifre.";
			return;
		}

		var spol;
		var sp=document.getElementsByName("spol");
		if(sp[0].checked==true)spol="M";
		else spol="Z";
		var temp=parseInt(zapo, 10);
		if(temp>500 && spol=="M"){
			document.getElementById("rez").innerHTML="Nepravilen vhod. Zaporedna številka rojstva</br>se ne sklada¸\
														z vnesenim spolom.";
			return;
		}
		if(temp<500 && spol=="Z"){
			document.getElementById("rez").innerHTML="Nepravilen vhod. Zaporedna številka rojstva</br>se ne sklada¸\
														z vnesenim spolom.";
			return;
		}
		var utezi=[7, 6, 5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
		var sum=0;
		for(var i=0; i<utezi.length; i++){
			var cifra=emso.charAt(i);
			cifra=parseInt(cifra, 10); 
			sum+=cifra*utezi[i];
		}
		sum=sum%11;
		var kontrol;
		if(sum==0){
			kontrol="0";
			emso+=kontrol;
		}else if(sum==1){
			zapo=parseInt(zapo, 10);
			zapo++;
			emso=emso.substring(0, 10);
			emso=emso+zapo.toString(10);
			var sum=0;
			for(var i=0; i<utezi.length; i++){
				var cifra=emso.charAt(i);
				cifra=parseInt(cifra, 10); 
				sum+=cifra*utezi[i];
			}
			sum=sum%11;
			if(sum==0)kontrol="0";
			else kontrol=(11-sum).toString(10);
			emso=emso+kontrol;
		}else{
			kontrol=(11-sum).toString(10);
			emso+=kontrol;
		}
		var rez=document.getElementById("rez");
		rez.innerHTML="Kontrolna vsota je "+kontrol+"</br>Torej celotna EMŠO številka je </br>"+emso;
	});

	document.getElementById("drzavaDrugo").addEventListener("click", function(){
		document.getElementById("drzavaDrugo").value="";
	});
});