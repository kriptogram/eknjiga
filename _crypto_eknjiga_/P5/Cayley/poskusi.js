//popravi napise
//popravi izpis cikla

window.addEventListener('load', function(){
	var n;
	var mojRezultat;
	var forma=document.getElementById("forma");
	//document.getElementById("poskusi").style.visibility="hidden";

	function inizializiraj(){
		//zbrisem vse obstojece vrstice
		//document.getElementById("poskusi").style.visibility="visible";
		n=document.getElementById("n").value;
		document.getElementById("trenutniModul").innerHTML="Trenutni modul je "+n+"";
		n=parseInt(n, 10);

		generirajRacun();
		
	}
	inizializiraj();

	function gcd(x, y) {
		x = Math.abs(x);
		y = Math.abs(y);
		while(y) {
			var t = y;
			y = x % y;
			x = t;
		}
		return x;
	}

	function inverzni(a){
		for(var i=1; i<n; i++){
			if((a*i)%n==1)return i;
		}
		return -1;
	}

	//poskusi ti
	document.getElementById("rand").addEventListener("click", generirajRacun);

	function generirajRacun(){
		document.getElementById("vnesiRez").value="";
		document.getElementById("vnesiRez").style.background = "#FFFFFF";
		document.getElementById("rezultatNapis").innerHTML = "";
		var a=Math.floor(Math.random()*n);
		var b=Math.floor(Math.random()*n);
		var c=Math.floor(Math.random()*4);
		var racun=document.getElementById("racun");

		if(c==0){
			//+
			mojRezultat=(a+b)%n;
			racun.innerHTML=a+" + "+b+" po modulu " + n + " je ";
		}else if(c==1){
			//-
			mojRezultat=(a-b)%n;
			if(mojRezultat<0)mojRezultat+=n;
			racun.innerHTML=a+" - "+b+" po modulu  " + n + " je ";
		}else if(c==2){
			//*
			mojRezultat=(a*b)%n;
			racun.innerHTML=a+" <span>&#183;</span> "+b+" po modulu " + n + " je ";
		}else{
			//:
			var inv=inverzni(b);
			while(inv==-1){
				var b=Math.floor(Math.random()*n);
				inv=inverzni(b);		
			}
			mojRezultat=(a*inv)%n;
			racun.innerHTML=a+" : "+b+" po modulu " + n + " je ";
		}
	}

	document.getElementById("preveri1").addEventListener("click", function(e){
		e.preventDefault();
		var vneseno=document.getElementById("vnesiRez").value;
		vneseno=parseInt(vneseno, 10);
		while(vneseno<0)
			vneseno+=n;
		if(vneseno%n==mojRezultat){
			document.getElementById("vnesiRez").style.background = "#00FF00"

			document.getElementById("rezultatNapis").innerHTML = "✔"
			document.getElementById("rezultatNapis").style.color = "#00FF00"
			
		}
		else{
			document.getElementById("vnesiRez").style.background = "#FF0000"

			document.getElementById("rezultatNapis").innerHTML = "❌"
			document.getElementById("rezultatNapis").style.color = "#FF0000"
		}
	});

	document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("preveri1").click();
		  }
	});
	

});	