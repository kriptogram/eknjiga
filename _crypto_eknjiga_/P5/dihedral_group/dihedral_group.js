class Symetry {
    constructor(type, index) {
        this.type = type;
        this.index = index;
    }

    toString() {
        return `${this.type} <sub>${this.index}</sub>`;
    }
}

class Rotation extends Symetry {
    constructor(angle) {
        super("r", angle.toString());
        this.angle = angle;
    }
}

class Reflection extends Symetry {
    constructor(simetral, num) {
        super("z", simetral);
        this.num = num;
    }
}

class DihedralGroup {
    constructor(set, cayleyTable, nOfCorners) {
        this.set = set;
        this.cayleyTable = cayleyTable
        this.nOfCorners;
    }
}

var radios = document.getElementsByName('nOfCorners');

r0 = new Rotation(0     );
r1 = new Rotation(120   );
r2 = new Rotation(240   );
zp = new Reflection("p", 0);
zq = new Reflection("q", 1);
zr = new Reflection("r", 2);

dih3Tab = [
    [r0, r1, r2, zp, zq, zr],
    [r1, r2, r0, zq, zr, zp],
    [r2, r0, r1, zr, zp, zq],
    [zp, zr, zq, r0, r2, r1],
    [zq, zp, zr, r1, r0, r2],
    [zr, zq, zp, r2, r1, r0],
]

dih3 = new DihedralGroup([r0, r1, r2, zp, zq, zr], dih3Tab)

r0 = new Rotation(0  );
r1 = new Rotation(90 );
r2 = new Rotation(180);
r3 = new Rotation(270);
zp = new Reflection("p", 0);
zq = new Reflection("q", 1);
zr = new Reflection("r", 2);
zs = new Reflection("s", 3);

dih4tab = [
    [r0, r1, r2, r3, zp, zq, zr, zs],
    [r1, r2, r3, r0, zr, zs, zq, zp],
    [r2, r3, r0, r1, zq, zp, zs, zr],
    [r3, r0, r1, r0, zs, zr, zp, zq],
    [zp, zs, zq, zr, r0, r2, r3, r1],
    [zq, zr, zp, zs, r2, r0, r1, r3],
    [zr, zp, zs, zq, r1, r3, r0, r2],
    [zs, zq, zr, zp, r3, r1, r2, r0],
]

dih4 = new DihedralGroup([r0, r1, r2, r3, zp, zq, zr, zs], dih4tab)

const fifth = 360 / 5;

r0 = new Rotation(0 * fifth);
r1 = new Rotation(1 * fifth);
r2 = new Rotation(2 * fifth);
r3 = new Rotation(3 * fifth);
r4 = new Rotation(4 * fifth);
zp = new Reflection("p", 0);
zq = new Reflection("q", 1);
zr = new Reflection("r", 2);
zs = new Reflection("s", 3);
zt = new Reflection("t", 4);

dih5tab = [
    [r0, r1, r2, r3, r4, zp, zq, zr, zs, zt],
    [r1, r2, r3, r4, r0, zq, zr, zs, zt, zp],
    [r2, r3, r4, r0, r1, zr, zs, zt, zp, zq],
    [r3, r4, r0, r1, r2, zs, zt, zp, zq, zr],
    [r4, r0, r1, r2, r3, zt, zp, zq, zr, zs],
    [zp, zt, zs, zr, zq, r0, r4, r3, r2, r1],
    [zq, zp, zt, zs, zr, r1, r0, r4, r3, r2],
    [zr, zq, zp, zt, zs, r2, r1, r0, r4, r3],
    [zs, zr, zq, zp, zt, r3, r2, r1, r0, r4],
    [zt, zs, zr, zq, zp, r4, r3, r2, r1, r0],
]

dih5 = new DihedralGroup([r0, r1, r2, r3, r4, zp, zq, zr, zs, zt], dih5tab)

dihs = {
    3: dih3, 
    4: dih4,
    5: dih5
}

offsetLabels = 15;

for (let i = 0; i < radios.length; i++) {
    radios[i].onclick = generate;
}
window.onload = generate;

function generate() {
    var radios = document.getElementsByName('nOfCorners');

    for (let i = 0; i < radios.length; i++) {
        radios[i].onclick = generate;
    }
    let nOfCorners = getNumberOfCorners();
    
    if (nOfCorners == 5) {
        document.getElementById("animation").classList = "col-12 col-lg-12 col-md-12 col-sm-12";
        document.getElementById("tbl").classList = "col-12 col-lg-12 col-md-12 col-sm-12";
    } else {
        document.getElementById("animation").classList = "col-12 col-lg-6 col-md-12 col-sm-12";
        document.getElementById("tbl").classList = "col-12 col-lg-6 col-md-12 col-sm-12";
    }
    
    let dih = dihs[nOfCorners];
    header = ["",...dih.set];
    table = [[...header]];
    dih.cayleyTable.forEach((row, i) => {
        
        row.forEach((cell, j) => {
            
        });
        table.push([header[i + 1], ...row]);
    })
    createTable(table, dih.set);
    createButtons(dih.set);
    drawCanvas();
    document.getElementById("order").innerHTML = nOfCorners * 2;
}

function createTable(tableData, set) {
    document.getElementById("table").innerHTML = "";
    var table = document.createElement('table');
    table.classList.add("table");
    table.classList.add("table-bordered");
    var tableBody = document.createElement('tbody');

    tableData.forEach(function(rowData, i) {
        var row = document.createElement('tr');

        rowData.forEach(function(cellData, j) {
            var cell;
            if(i == 0 && j == 0) {
                cell = document.createElement('th');
                cell.innerHTML = '&#8728;';
            } else if(i <= 0 || j <= 0) {
                cell = document.createElement('th');
                cell.innerHTML = cellData.toString();
            } else {
                cell = document.createElement('td');
                var dropdown = document.createElement("div");
                dropdown.classList.add("dropdown");
                var dropdownBtn = document.createElement('button');
                dropdownBtn.classList.add("btn");
                dropdownBtn.classList.add("btn-primary");
                dropdownBtn.classList.add("dropdown-toggle");
                dropdownBtn.setAttribute("type", "button");
                dropdownBtn.setAttribute("data-toggle", "dropdown");
                dropdownBtn.setAttribute("aria-haspopup", "true");
                dropdownBtn.setAttribute("aria-expanded", "false");
                dropdownBtn.setAttribute("id", "dropdownMenuButton" + i + j);
                dropdownBtn.innerHTML = "?";
                
                var dropdownMenu = document.createElement("div");
                dropdownMenu.classList.add("dropdown-menu");
                dropdownMenu.setAttribute("aria-labelledby", "dropdownMenuButton" + i + j);

                set.forEach((elm)=>{
                    var option = document.createElement('div');
                    option.classList.add("dropdown-item");
                    option.innerHTML = elm.toString();
                    option.onclick = () => {
                        dropdownBtn.innerHTML = elm.toString();
                        if(elm == cellData) {
                            dropdownBtn.classList.remove("btn-primary");
                            dropdownBtn.classList.remove("btn-danger");
                            dropdownBtn.classList.add("btn-success");
                        } else {
                            dropdownBtn.classList.remove("btn-primary");
                            dropdownBtn.classList.remove("btn-success");
                            dropdownBtn.classList.add("btn-danger");
                        }
                    };
                    dropdownMenu.append(option);
                });

                
                dropdown.append(dropdownBtn);
                dropdown.append(dropdownMenu);    
                cell.append(dropdown);
            }
            row.appendChild(cell);
        });

        tableBody.appendChild(row);
    });

    table.appendChild(tableBody);
    document.getElementById("table").appendChild(table);
}


function createButtons(elms) {
    angle = 0;
    reflections = []
    for (let i = 0; i < elms.length; i++) {
        reflections.push(0); 
    }
    btns = document.getElementById("buttons");
    btns.innerHTML = "";
    elms.forEach((elm) => {
        btn = document.createElement("button");
        btn.classList.add("btn");
        btn.classList.add("btn-primary");
        btn.classList.add("m-1");
        btn.innerHTML = elm.toString();
        btn.onclick = () => {
            if (elm.type == "r") {
                angle += elm.angle;
            } else {
                reflections[elm.num]++;
            }

            simulate(angle, reflections);
        }
        btns.append(btn);
    });
}


// -------------------- SVG simulation -------------------- //

const widthViewBox = 500;
const heightViewBox = 350;
const width = widthViewBox;
const height = heightViewBox;
const xStart = -widthViewBox/2;
const yStart = -heightViewBox/2;
const viewBox = xStart + " " + yStart + " " + width + " " + height;
const awayFromCenter = height/2.5
const radiusCorner = height/150;


const svgns = "http://www.w3.org/2000/svg";

var nOfCorners = 3;
var corners = [];
var cornersValues = [];

function getNumberOfCorners() {
    for (var i = 0; i < radios.length; i++) {
        if (radios[i].checked) {
            return parseInt(radios[i].value);
        }
    }

    return 0;
}

function drawCanvas() {
    $("#canvas").empty();

    var canvas = document.getElementById("canvas");

    canvas.style.transition      = `all 0s ease-out`;
    canvas.style.webkitTransform = 'rotate(0deg)'; 
    canvas.style.mozTransform    = 'rotate(0deg)'; 
    canvas.style.msTransform     = 'rotate(0deg)'; 
    canvas.style.oTransform      = 'rotate(0deg)'; 
    canvas.style.transform       = 'rotate(0deg)'; 

    angle = 0;

    canvas.setAttribute("viewBox", viewBox);
    canvas.setAttribute("width", widthViewBox);
    canvas.setAttribute("height", heightViewBox);

    const adjustmentAngles = [30,45, 54]

    nOfCorners = getNumberOfCorners();

    const angleInDegrees = 360.0 / nOfCorners;
    
    corners = [];
    for(var i = 0; i < nOfCorners; i++) {
        corners.push({x: Math.cos((adjustmentAngles[nOfCorners - 3] * Math.PI / 180.0) + i * Math.PI / 180.0 * angleInDegrees) * awayFromCenter, y: Math.sin((adjustmentAngles[nOfCorners - 3] * Math.PI / 180.0) + i * Math.PI / 180.0 * angleInDegrees) * awayFromCenter});
    }

    const extraLenOverSquare = width / 50;
    
    // Draw polygon background
    var polygonPoints = "";
    corners.forEach(corner => {
        // Add polygon point
        polygonPoints += " " + corner.x + "," + corner.y;
    });

    var polygon = document.createElementNS(svgns, 'polygon');
    polygon.setAttributeNS(null, 'points', polygonPoints);
    polygon.setAttributeNS(null, 'style', "fill:rgba(217, 217, 217, 0.8);");
    canvas.appendChild(polygon);
    
    const labels = ['A', 'B', 'C', 'D', 'E'];
    labelBisections = [];
    if(nOfCorners == 3) {
        labelBisections = ['p', 'q', 'r'];
    } else if(nOfCorners == 4) {
        labelBisections = ['p', 'r', 's', 'q']
    } else {
        labelBisections = ['p', 's', 'q', 't', 'r'];
    }
    var ixBisection = 0;

    const labelDistance = -25;
    for (var i = 0; i < corners.length; i++) {

        const corner = corners[i];

        // Add symmetry line
        const coefficient = Math.abs(corner.x) < 1e-10 ? null : parseFloat(corner.y) / parseFloat(corner.x)

        const xQuadrantCoefficient = corner.x > 0 ? -1 : 1;

        // Add circle in the corner
        var circle = document.createElementNS(svgns, 'circle');
        circle.setAttributeNS(null, 'cx', corner.x);
        circle.setAttributeNS(null, 'cy', corner.y);
        circle.setAttributeNS(null, 'r', radiusCorner);
        circle.setAttributeNS(null, 'style', "fill:rgba(255,0,0,0.8);");
        canvas.appendChild(circle);


        var yText = corner.y - xQuadrantCoefficient * (coefficient != null ? coefficient : 1) * 1.5 * extraLenOverSquare;
        if (nOfCorners == 4) {
            yText = corner.y;
        }

        // Add label to the corner
        var text = document.createElementNS(svgns, 'text');
        text.classList.add('text-360')
        text.setAttributeNS(null, 'x', corner.x - (coefficient != null ? xQuadrantCoefficient * 2.5 * extraLenOverSquare: extraLenOverSquare / 4 ));
        text.setAttributeNS(null, 'y', yText);
        text.setAttributeNS(null, 'style', 'font-size:22px; font-weight: bold;');
        text.setAttributeNS(null, 'fill', 'rgba(0,0,0,0.8)');
        text.innerHTML = labels[i];
        canvas.appendChild(text);

        if (nOfCorners == 4 && i >= 2) {
            // We dont want to repeat some bisections
            continue;
        }

        var line = document.createElementNS(svgns, 'line');
        line.setAttributeNS(null, 'x1', corner.x);
        line.setAttributeNS(null, 'x2', - corner.x + (coefficient != null ? xQuadrantCoefficient * extraLenOverSquare: 0 ));
        line.setAttributeNS(null, 'y1', corner.y);
        line.setAttributeNS(null, 'y2', - corner.y + xQuadrantCoefficient * (coefficient != null ? coefficient : 1) * extraLenOverSquare);
        line.setAttributeNS(null, 'style', "stroke:rgb(255,0,0);stroke-width:2");
        canvas.appendChild(line);

        // Label bisection
        var text = document.createElementNS(svgns, 'text');
        text.setAttributeNS(null, 'x', -offsetLabels - corner.x + (coefficient != null ? xQuadrantCoefficient * labelDistance : 0 ));
        text.setAttributeNS(null, 'y', -offsetLabels - corner.y + xQuadrantCoefficient * (coefficient != null ? coefficient : 1) * labelDistance);
        text.setAttributeNS(null, 'style', 'font-size:22px; font-weight: bold;');
        text.setAttributeNS(null, 'fill', 'rgba(0,0,0,0.8)');
        text.innerHTML = labelBisections[ixBisection];
        canvas.appendChild(text);

        ixBisection++;
    }

    if (nOfCorners == 4) {
        // Draw the rest of the bisections - squares have more
        // Vertical
        var lineVertical = document.createElementNS(svgns, 'line');
        lineVertical.setAttributeNS(null, 'x1', 0);
        lineVertical.setAttributeNS(null, 'x2', 0);
        lineVertical.setAttributeNS(null, 'y1', -(awayFromCenter + extraLenOverSquare));
        lineVertical.setAttributeNS(null, 'y2', (awayFromCenter + extraLenOverSquare));
        lineVertical.setAttributeNS(null, 'style', "stroke:rgb(255,0,0);stroke-width:2");
        canvas.appendChild(lineVertical);

        var textVertical = document.createElementNS(svgns, 'text');
        textVertical.setAttributeNS(null, 'x', offsetLabels/2 + 0);
        textVertical.setAttributeNS(null, 'y', offsetLabels/2 -(awayFromCenter + labelDistance));
        textVertical.setAttributeNS(null, 'style', 'font-size:22px; font-weight: bold;');
        textVertical.setAttributeNS(null, 'fill', 'rgba(0,0,0,0.8)');
        textVertical.innerHTML = labelBisections[ixBisection];
        canvas.appendChild(textVertical);

        ixBisection++;

        //Horizontal
        var lineHorizontal = document.createElementNS(svgns, 'line');
        lineHorizontal.setAttributeNS(null, 'x1', -(awayFromCenter + extraLenOverSquare));
        lineHorizontal.setAttributeNS(null, 'x2', (awayFromCenter + extraLenOverSquare));
        lineHorizontal.setAttributeNS(null, 'y1', 0);
        lineHorizontal.setAttributeNS(null, 'y2', 0);
        lineHorizontal.setAttributeNS(null, 'style', "stroke:rgb(255,0,0);stroke-width:2");
        canvas.appendChild(lineHorizontal);

        var textHorizontal = document.createElementNS(svgns, 'text');
        textHorizontal.setAttributeNS(null, 'x', offsetLabels/2 + (awayFromCenter + labelDistance));
        textHorizontal.setAttributeNS(null, 'y', offsetLabels + 0);
        textHorizontal.setAttributeNS(null, 'style', 'font-size:22px; font-weight: bold;');
        textHorizontal.setAttributeNS(null, 'fill', 'rgba(0,0,0,0.8)');
        textHorizontal.innerHTML = labelBisections[ixBisection];
        canvas.appendChild(textHorizontal);

        ixBisection++;

    }
}

function simulate(angle, reflections) {
    var canvas = document.getElementById('canvas');

    var degText = - angle;
    var animation = 'rotate('+angle+'deg) ';
    // var textAnimation = 'rotate('+degText+'deg) ';

    console.log(reflections);
    reflections.forEach((r, i) => {
        var reflectionAngle = 2 * Math.PI * (i + 1) / reflections.length;
        console.log(reflectionAngle);
        animation += `rotate3d(${Math.sin(reflectionAngle)}, ${Math.cos(reflectionAngle)}, 0, ${r * 180}deg) `;
        // textAnimation += `rotate3d(${Math.sin(reflectionAngle)}, ${Math.cos(reflectionAngle)}, 0, ${-r * 180}deg) `;
    });

    canvas.style.transition      = `all 2s ease-out`;
    canvas.style.webkitTransform = animation; 
    canvas.style.mozTransform    = animation; 
    canvas.style.msTransform     = animation; 
    canvas.style.oTransform      = animation; 
    canvas.style.transform       = animation; 
    
    // var texts = document.getElementsByClassName('text-360');

    // for(var i = 0; i < texts.length; i++) {
    //     var text = texts[i];
    //     var degText = - angle;
    //     text.style.transformOrigin = 'center';
    //     text.style.transformBox    = 'fill-box';
    //     text.style.transition      = `all 2s ease-out`;
    //     text.style.webkitTransform = textAnimation; 
    //     text.style.mozTransform    = textAnimation; 
    //     text.style.msTransform     = textAnimation; 
    //     text.style.oTransform      = textAnimation; 
    //     text.style.transform       = textAnimation; 
    // }
}