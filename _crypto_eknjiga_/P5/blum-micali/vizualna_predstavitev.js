let tocke = new Map();
var a_testiranje,c_testiranje,m_testiranje,x0_testiranje,max_testiranje, n_testiranje;

let robGrafaVodoravno = 20;
let robGrafaNavpicno = 50;

function testiraj() {
  g_testiranje = Number(document.getElementById("input-g-testiranje").value);
  p_testiranje = Number(document.getElementById("input-p-testiranje").value);
  x0_testiranje = Number(document.getElementById("input-x0-testiranje").value);
  a_testiranje = Number(document.getElementById("input-a-testiranje").value);
  b_testiranje = Number(document.getElementById("input-b-testiranje").value);
  n_testiranje = Number(document.getElementById("input-n-testiranje").value);
  
  if (a_testiranje > b_testiranje) {
    // swap a and b
    let temp_a_testiranje = a_testiranje;
    a_testiranje = b_testiranje;
    b_testiranje = temp_a_testiranje; 
  }

  trenutno = x0_testiranje;

  intervalLen = b_testiranje - a_testiranje + 1;

  if (intervalLen  > p_testiranje) {
    document.getElementById("opozorilo").innerText = "Velikost intervala ne sme biti večja od modula."
    document.getElementById("grafi").style.display = "none";
  } else {  
    document.getElementById("opozorilo").innerText = "";
    document.getElementById("grafi").style.display = "block";
    narisiGraf();
    narisiHistogram();
  }
}

let trenutno;
function generirajStevilo() {
  let vrednost = trenutno;
  trenutno = pow(g_testiranje, trenutno, p_testiranje);
  return a_testiranje + (vrednost % intervalLen);
}

function resetirajGenerator() {
  trenutno = x0_testiranje;
}

function narisiHistogram() {
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(generirajHistogram);
}

// dokumentacija: https://developers.google.com/chart/interactive/docs/gallery/histogram
function generirajHistogram() {
  resetirajGenerator();
  // generiraj podatke
  let podatki = [['Vrednost']];
  for (let i = 0; i < n_testiranje; i++) {
    podatki.push([generirajStevilo()]);
  }

  var data = google.visualization.arrayToDataTable(podatki);

  var options = {
    title: 'Generirane vrednosti',
    legend: { position: 'none' },
    histogram: { maxNumBuckets: Math.min(intervalLen, 10) }
  };

  var chart = new google.visualization.Histogram(document.getElementById('histogram'));
  chart.draw(data, options);
}

function narisiGraf() {
  generirajTocke();
  var c = document.getElementById("graf-canvas");
  var ctx = c.getContext("2d");
  // pocisti risalno povrsino
  ctx.fillStyle = "#ffffff";
  ctx.fillRect(0,0,c.width, c.height);
  narisiKoordinatniSistem(ctx, c.width - robGrafaVodoravno*2, c.height - robGrafaNavpicno*2);
  narisiTocke(ctx, c.width - robGrafaVodoravno*2, c.height - robGrafaNavpicno*2);
}

function generirajTocke() {
  tocke.clear();
  for (let i = 0; i < n_testiranje; i++) {
    let x = generirajStevilo();
    let y = generirajStevilo();
    let key = [x,y].toString();
    if (tocke.has(key)) {
      tocke.set(key, tocke.get(key) + 1);
    } else {
      tocke.set(key, 1);
    }
    console.log(`x = ${x}, y = ${y}, n = ${tocke.get(key)}`);
  }
}

function koordinateVPixle(sirina, visina, x, y) {
  let velikost = intervalLen - 1;
  return [robGrafaVodoravno + sirina * x / velikost, robGrafaNavpicno + visina * (velikost - y) / velikost];
}

function narisiTocke(ctx, sirina, visina) {
  ctx.fillStyle = "#000000";
  for (let [k, n] of tocke.entries()) {
    let coordinates = k.split(",");
    x = parseInt(coordinates[0]);
    y = parseInt(coordinates[1]);
    ctx.beginPath();
    let tocka = koordinateVPixle(sirina, visina, x - a_testiranje, y - a_testiranje);
    ctx.arc(tocka[0] - a_testiranje, tocka[1] - a_testiranje, 1.5 * n, 0, 2 * Math.PI);
    ctx.fill();
    ctx.closePath();
  }
}

function narisiKoordinatniSistem(ctx, sirina, visina) {
  let velikost = intervalLen - 1;
  let korak = Math.max(Math.floor(velikost / 5), 1);
  ctx.beginPath();
  ctx.strokeStyle = "#d3d3d3";
  // navpicne crtes
  for (let i = 0; i <= velikost; i += korak) {
    let zacetek = koordinateVPixle(sirina, visina, i, 0);
    let konec = koordinateVPixle(sirina, visina, i, velikost);
    ctx.moveTo(zacetek[0], zacetek[1]);
    ctx.lineTo(konec[0], konec[1]);
    ctx.font = "16px Arial";
    ctx.fillStyle = "#a3a3a3";
    ctx.fillText("" + (i + a_testiranje), zacetek[0] + 3, zacetek[1]-3);
  }

  // vodoravne crte
  for (let i = 0; i <= velikost; i += korak) {
    let zacetek = koordinateVPixle(sirina, visina, 0, i);
    let konec = koordinateVPixle(sirina, visina, velikost, i);
    ctx.moveTo(zacetek[0], zacetek[1]);
    ctx.lineTo(konec[0], konec[1]);
    ctx.font = "16px Arial";
    ctx.fillStyle = "#a3a3a3";
    ctx.fillText("" + (i + a_testiranje), zacetek[0] + 3, zacetek[1]-3);
  }
  ctx.stroke();
  ctx.closePath();
}
