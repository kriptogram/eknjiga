const SIZE = 250, DOT_SIZE = 5, SIZE_IN_DOTS = SIZE / DOT_SIZE;
var timeout;

function naslikaj() {
    let g_slikanje = Number(document.getElementById("input-g-slikanje").value);
    let p_slikanje = Number(document.getElementById("input-p-slikanje").value);
    let x0_slikanje = Number(document.getElementById("input-x0-slikanje").value);

    document.getElementById("slike").style.display = "block";

    randomBits = generateNBits(SIZE_IN_DOTS * SIZE_IN_DOTS, g_slikanje % p_slikanje, x0_slikanje % p_slikanje, p_slikanje);

    let canvas = document.getElementById("canvas");
    let diagonalCanvas = document.getElementById("diagonalCanvas");

    canvas.width = SIZE
    canvas.height = SIZE;
    canvas.style.width  = SIZE + "px";
    canvas.style.height = SIZE + "px";

    diagonalCanvas.width = SIZE;
    diagonalCanvas.height = SIZE;
    diagonalCanvas.style.width  = SIZE + "px";
    diagonalCanvas.style.height = SIZE + "px";

    let ctx = canvas.getContext("2d");
    let ctxDiagonal = diagonalCanvas.getContext("2d");

    ctx.fillStyle = "#ffffff";
    ctx.fillRect(0, 0, SIZE, SIZE);
    ctx.fillStyle = "#000000";

    ctxDiagonal.fillStyle = "#ffffff";
    ctxDiagonal.fillRect(0, 0, SIZE, SIZE);
    ctxDiagonal.fillStyle = "#000000";

    let diagonalCoordinates = {x: 0, y: 0};
    up = true;

    var i = 0;

    clearTimeout(timeout);

    timeout = window.setInterval(() => {
        if(up) {
            if(diagonalCoordinates.y == 0) {
                diagonalCoordinates.x ++;
                up = !up;
            } else if(diagonalCoordinates.x >= SIZE_IN_DOTS - 1) {
                diagonalCoordinates.y ++;
                up = !up;
            } else {
                diagonalCoordinates.x ++;
                diagonalCoordinates.y --;
            }
        } else {
            if(diagonalCoordinates.x == 0) {
                diagonalCoordinates.y ++;
                up = !up;
            } else if(diagonalCoordinates.y >= SIZE_IN_DOTS - 1) {
                diagonalCoordinates.x ++;
                up = !up;
            } else {
                diagonalCoordinates.x --;
                diagonalCoordinates.y ++;
            }
        }

        if(randomBits.charAt(i) == "1") {
            drawPixel(ctx, {x: i % (SIZE / DOT_SIZE), y: Math.floor(DOT_SIZE * i / SIZE)});
            drawPixel(ctxDiagonal, diagonalCoordinates);
        }

        if(i++ >= randomBits.length) {
            clearTimeout(timeout);
        }
    }, 1);
}

function drawPixel(ctx, point) {
    ctx.fillRect(DOT_SIZE * point.x, DOT_SIZE * point.y, DOT_SIZE, DOT_SIZE);
}