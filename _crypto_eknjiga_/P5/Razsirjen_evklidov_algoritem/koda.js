window.addEventListener('load', function() {
	console.log("Welcome");
	var tab=document.getElementById("tab");

	document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("zacniBtn").click();
		  }
	});

	document.getElementById("form").addEventListener('submit', function(e){
		e.preventDefault();
		document.getElementById("err").innerHTML="";
		while(tab.rows[0]){
			tab.deleteRow(0);
		}
		var a=document.getElementById("a").value;
		var b=document.getElementById("b").value;
		
		//preverimo pravilnost podatkov
		//preverimo da vsako stevilo ima kvecjemu en znak minus
		if(a.split('-').length!=1 && (a.split('-').length!=2 || a.charAt(0)!="-")){
			document.getElementById("err").innerHTML="Nepravilni podatki.";
			document.getElementById("rezultat").innerHTML="";
			return;
		}
		if(b.split('-').length!=1 && (b.split('-').length!=2 || b.charAt(0)!="-")){
			document.getElementById("err").innerHTML="Nepravilni podatki.";
			document.getElementById("rezultat").innerHTML="";
			return;
		}
		a=parseInt(a, 10);
		b=parseInt(b, 10);


		if(a==0){
			document.getElementById("err").innerHTML+="Število 0 ne more biti modul.";
			document.getElementById("rezultat").innerHTML="";
			return;	
		}
		if(a<0){
			document.getElementById("err").innerHTML="Modul mora biti celo pozitivno število.";
			document.getElementById("rezultat").innerHTML="";
			return;
		}
		if(b>a){
			var temp=b;
			while(temp>=a)temp-=a;
			document.getElementById("err").innerHTML="V modulu "+a+" je število "+b+" kongruentno številu "+temp+".";
			b=temp;
		}
		if(b<0){
			var temp=b;
			while(temp<0)temp+=a;
			document.getElementById("err").innerHTML="V modulu "+a+" je število "+b+" kongruentno številu "+temp+".";
			b=temp;
		}
		if(b==0){
			document.getElementById("err").innerHTML+="</br>Število 0 nima inverza.";
			document.getElementById("rezultat").innerHTML="";
			return;	
		}
		var krat="<span>&#183;</span>";

		//prva vrstica
		var row=tab.insertRow(0);
		var cell=row.insertCell(0);
		cell.innerHTML='index i';
		cell.style.width="50px";
		cell=row.insertCell(1);
		cell.innerHTML="kvocient";
		cell.style.width="150px";
		cell=row.insertCell(2);
		cell.innerHTML="Ostanek";
		cell.style.width="150px";
		cell=row.insertCell(3);
		cell.innerHTML="s<sub>i</sub>";
		cell.style.width="150px";
		cell=row.insertCell(4);
		cell.innerHTML="t<sub>i</sub>";
		cell.style.width="150px";

		//Druga vrstica
		row=tab.insertRow(1);
		cell=row.insertCell(0);
		cell.innerHTML='0';
		cell=row.insertCell(1);
		cell.innerHTML="  ";
		cell=row.insertCell(2);
		cell.innerHTML=a.toString(10);
		cell=row.insertCell(3);
		cell.innerHTML="1";
		cell=row.insertCell(4);
		cell.innerHTML="0";

		//tretja vrstica
		row=tab.insertRow(2);
		cell=row.insertCell(0);
		cell.innerHTML='1';
		cell=row.insertCell(1);
		cell.innerHTML="  ";
		cell=row.insertCell(2);
		cell.innerHTML=b.toString(10);
		cell=row.insertCell(3);
		cell.innerHTML="0";
		cell=row.insertCell(4);
		cell.innerHTML="1";

		//zanka za vse ostale vrstice
		var ind=3;
		var stop=false;
		while(true){
			row=tab.insertRow(ind);
			cell=row.insertCell(0);
			cell.innerHTML=(ind-1).toString(10);
			
			cell=row.insertCell(1);
			var vr1=vrednost(tab.rows[ind-2].cells[2].innerHTML);
			var vr2=vrednost(tab.rows[ind-1].cells[2].innerHTML);
			console.log(vr1+" "+vr2);
			var kolicnik=Math.floor(vr1/vr2);
			cell.innerHTML=vr1.toString(10)+" / "+vr2.toString(10)+" = "+kolicnik.toString(10);
			
			cell=row.insertCell(2);
			if(vr2>=0)cell.innerHTML=vr1.toString(10)+" - "+kolicnik.toString(10)+" "+krat+" "+vr2.toString(10)+" = "+(vr1%vr2).toString(10);
			else cell.innerHTML=vr1.toString(10)+" - "+kolicnik.toString(10)+" "+krat+" ("+vr2.toString(10)+") = "+(vr1%vr2).toString(10);
			if(vr1%vr2==0)stop=true;
			
			cell=row.insertCell(3);
			vr1=vrednost(tab.rows[ind-2].cells[3].innerHTML);
			vr2=vrednost(tab.rows[ind-1].cells[3].innerHTML);
			var rez=vr1-kolicnik*vr2;
			if(vr2>=0)cell.innerHTML=vr1.toString(10)+" - "+kolicnik.toString(10)+" "+krat+" "+vr2.toString(10)+" = "+rez.toString(10);
			else cell.innerHTML=vr1.toString(10)+" - "+kolicnik.toString(10)+" "+krat+" ("+vr2.toString(10)+") = "+rez.toString(10);

			cell=row.insertCell(4);
			vr1=vrednost(tab.rows[ind-2].cells[4].innerHTML);
			vr2=vrednost(tab.rows[ind-1].cells[4].innerHTML);
			var rez=vr1-kolicnik*vr2;
			if(vr2>=0)cell.innerHTML=vr1.toString(10)+" - "+kolicnik.toString(10)+" "+krat+" "+vr2.toString(10)+" = "+rez.toString(10);
			else cell.innerHTML=vr1.toString(10)+" - "+kolicnik.toString(10)+" "+krat+" ("+vr2.toString(10)+") = "+rez.toString(10);
			ind++;

			if(stop==true)break;
		}
		var rezultat=document.getElementById("rezultat");
		var str="Števila si nista tuja, torej ne obstaja inverzni element števila "+b+" v modulu "+a+".";
		var predzadnji=vrednost(tab.rows[ind-2].cells[2].innerHTML);
		console.log("pred "+predzadnji);
		if(parseInt(predzadnji, 10)==1){
			ind--;
			var str="Velja torej zveza: </br> "+vrednost(tab.rows[ind-1].cells[3].innerHTML)+" "+krat+" "+a.toString(10);
			var temp=vrednost(tab.rows[ind-1].cells[4].innerHTML);
			temp=parseInt(temp, 10);
			console.log(temp);
			if(temp>0){
				str=str+" + "+vrednost(temp.toString(10))+" "+krat+" "+b.toString(10);
			}else{
				str=str+" - "+vrednost(temp.toString(10).substring(1))+" "+krat+" "+b.toString(10);
			}
			str=str+" = 1 </br>Iz tega zaključimo da je "+temp.toString(10);
			if(temp<0)str=str+"(= "+a+" " +temp.toString(10)+" = "+ (parseInt(a)+temp).toString(10) +") ";
			str=str+" inverzni element števila "+b +" v modulu " + a+".";
		}else{

		}
		rezultat.innerHTML=str;
	});

	function vrednost(izraz){
		for(var i=0; izraz.charAt(i)!='=' && i<izraz.length; i++){}
		if(i==izraz.length)return parseInt(izraz, 10);
		i+=2;
		izraz=izraz.substring(i);
		return parseInt(izraz, 10);
	}
});