module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    'eslint:recommended'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': 'warn',//process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': 'warn',//process.env.NODE_ENV === 'production' ? 'warn' : 'off'
  }
}
