//https://en.wikipedia.org/wiki/Glagolitic_script
//https://en.wikipedia.org/wiki/Slovene_alphabet
//https://en.wikipedia.org/wiki/Russian_alphabet
//https://en.wikipedia.org/wiki/Serbian_Cyrillic_alphabet
//https://en.wikipedia.org/wiki/Cyrillic_numerals <-- TODO
//srbska glagolica
var rusl = {
    'А': 'A',
    'Б': 'B',
    'В': 'V',
    'Г': 'G',
    'Д': 'D',
    'Е': 'JE',
    'Ё': 'JO',
    'Ж': 'Ž',
    'З': 'Z',
    'И': 'I',
    'Й': 'J',
    'К': 'K',
    'Л': 'L',
    'М': 'M',
    'Н': 'N',
    'О': 'O',
    'П': 'P',
    'Р': 'R',
    'С': 'S',
    'Т': 'T',
    'У': 'U',
    'Ф': 'F',
    'Х': 'H',
    'Ц': 'C',
    'Ч': 'Č',
    'Ш': 'Š',
    'Щ': 'Š',
    'Ъ': '',
  //  'Ъ': '\'',
    'Ы': 'I',
     'Ь': '',
   // 'Ь': '\"',
    'Э': 'E',
    'Ю': 'JU',
    'Я': 'JA',
};


var slru = {
   'A' : 'А' ,
   'B' : 'Б' ,
   'V' : 'В' ,
   'G' : 'Г' ,
   'D' : 'Д',
   'JE' : 'Е',
   'JO' : 'Ё',
   'Ž' : 'Ж' ,
   'Z' : 'З' ,
   'I' : 'И' ,
   'J' : 'Й' ,
   'K' : 'К' ,
   'L' : 'Л' ,
   'M' : 'М' ,
   'N' : 'Н' ,
   'O' : 'О' ,
   'P' : 'П' ,
   'R' : 'Р' ,
   'S' : 'С' ,
   'T' : 'Т' ,
   'U' : 'У' ,
   'F' : 'Ф' ,
   'H' : 'Х' ,
   'C' : 'Ц' ,
   'Č' : 'Ч' ,
   'Š' : 'Ш' ,
   'E' : 'Э' ,
   'JU' : 'Ю',
   'JA' : 'Я'
};


var srsl = {
    'А': 'A',
    'Б': 'B',
    'В': 'V',
    'Г': 'G',
    'Д': 'D',
    'Ђ': 'Đ',
    'Е': 'E',
    'Ж': 'Ž',
    'З': 'Z',
    'И': 'I',
    'Ј': 'J',
    'К': 'K',
    'Л': 'L',
    'Љ': 'LJ',
    'М': 'M',
    'Н': 'N',
    'Њ': 'NJ',
    'О': 'O',
    'П': 'P',
    'Р': 'R',
    'С': 'S',
    'Т': 'T',
    'Ћ': 'Ć',
    'У': 'U',
    'Ф': 'F',
    'Х': 'H',
    'Ц': 'C',
    'Ч': 'Č',
    'Џ': 'DŽ',
    'Ш': 'Š'
};

var slsr = {
    'A':'А',
    'B':'Б',
    'V':'В',
    'G':'Г',
    'D':'Д',
    'Đ':'Ђ',
    'E':'Е',
    'Ž':'Ж',
    'Z':'З',
    'I':'И',
    'J':'Ј',
    'K':'К',
    'L':'Л',
    'M':'М',
    'N':'Н',
    'NJ':'Њ',
    'O':'О',
    'P':'П',
    'R':'Р',
    'S':'С',
    'T':'Т',
    'Ć':'Ћ',
    'U':'У',
    'F':'Ф',
    'H':'Х',
    'C':'Ц',
    'Č':'Ч',
    'Š':'Ш',
    'DŽ': 'Џ',
    'LJ':'Љ'
};


var glsl ={
    'Ⰰ': 'A', 
    'Ⰱ': 'B', 
    'Ⰲ': 'V', 	
    'Ⰳ': 'G', 	
    'Ⰴ': 'D', 	
    'Ⰵ': 'E', 
    'Ⰶ': 'Ž', 
    'Ⰷ': 'DŽ', 
    'Ⰸ': 'Z', 
    'Ⰹ': 'I', 
    'Ⰺ': 'J',
    'Ⰻ': 'I', 
    'Ⰼ': 'Đ', 
    'Ⰽ': 'K', 
    'Ⰾ': 'L', 
    'Ⰿ': 'M', 
    'Ⱀ': 'N', 	
    'Ⱁ': 'O', 	
    'Ⱂ': 'P', 	
    'Ⱃ': 'R', 	
    'Ⱄ': 'S', 	
    'Ⱅ': 'T', 	
    'Ⱆ': 'U', 	
    'Ⱇ': 'F', 	
    'Ⱈ': 'H', 	
    'Ⱉ': 'O', 	
    'Ⱋ': 'Š', 	
    'Ⱌ': 'C', 	
    'Ⱍ': 'Č', 	
    'Ⱎ': 'Š', 	
    'Ⱏ': '', 
    'Ⱐ': '',
    'ⰟⰊ': 'I', 
    'Ⱐ': '',
    'Ⱑ': 'JA',
    'Ⱖ': 'JO',
    'Ⱓ':'JU',
    'Ⱔ':'E',
    'Ⱗ':'JE',
    'Ⱘ':'O',
    'Ⱙ':'JO',
    'Ⱚ':'TH',
    'Ⱛ':'I'
}

var slgl ={
    'A': 'Ⰰ', 
    'B': 'Ⰱ', 
    'V': 'Ⰲ', 	
    'G': 'Ⰳ', 	
    'D': 'Ⰴ', 	
    'E': 'Ⰵ', 
    'Ž': 'Ⰶ', 
    'Z': 'Ⰸ', 
    'I': 'Ⰹ',
    'J': 'Ⰺ',
    'K': 'Ⰽ', 
    'L': 'Ⰾ', 
    'M': 'Ⰿ', 
    'N': 'Ⱀ', 	
    'O': 'Ⱁ', 	
    'P': 'Ⱂ', 	
    'R': 'Ⱃ', 	
    'S': 'Ⱄ', 	
    'T': 'Ⱅ', 	
    'U': 'Ⱆ', 	
    'F': 'Ⱇ', 	
    'H': 'Ⱈ', 	
    'C': 'Ⱌ', 	
    'Č': 'Ⱍ', 	
    'Š': 'Ⱎ', 
    'DŽ': 'Ⰷ', 
    'JA': 'Ⱑ',
    'JO': 'Ⱖ',
    'JU': 'Ⱓ',
    'Đ': 'Ⰼ'
}




function translate(besedilo, langdict){
    crke = besedilo.split('');
    var output= "";
   
    for (var i=0; i<crke.length; i++){
        
        if (i+1<crke.length && (crke[i]+crke[i+1]).toUpperCase() in langdict){
            output +=langdict[(crke[i]+crke[i+1]).toUpperCase()].toLowerCase();
            i++;
        }else if (crke[i].toUpperCase() in langdict){
            if (crke[i]==crke[i].toLowerCase()){
                output +=langdict[crke[i].toUpperCase()].toLowerCase();
            }else{
                output +=langdict[crke[i].toUpperCase()];
            }
            
        }else{
            output +=crke[i];
        }
    }
    return output;
}

function sl() {
    var base = document.getElementsByName('sl')[0].value;
    document.getElementsByName('ru')[0].value= translate(base, slru);
    document.getElementsByName('sr')[0].value= translate(base, slsr);
    document.getElementsByName('gl')[0].value= translate(base, slgl);
}


function ru() {
    var base = document.getElementsByName('ru')[0].value;
    document.getElementsByName('sl')[0].value= translate(base, rusl);
    base = document.getElementsByName('sl')[0].value;
    document.getElementsByName('sr')[0].value= translate(base, slsr);
    document.getElementsByName('gl')[0].value= translate(base, slgl);
}

function sr() {
    var base = document.getElementsByName('sr')[0].value;
    document.getElementsByName('sl')[0].value= translate(base, srsl);
    base = document.getElementsByName('sl')[0].value;
    document.getElementsByName('ru')[0].value= translate(base, slru);
    document.getElementsByName('gl')[0].value= translate(base, slgl);
}

function gl() {
    var base = document.getElementsByName('gl')[0].value;
    document.getElementsByName('sl')[0].value= translate(base, glsl);
    base = document.getElementsByName('sl')[0].value;
    document.getElementsByName('ru')[0].value= translate(base, slru);
    document.getElementsByName('sr')[0].value= translate(base, slsr);
}

// initial conversion
sl()


