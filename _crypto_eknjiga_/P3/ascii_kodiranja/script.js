window.addEventListener('load', function () {
  let selectIn = document.getElementById("selectInput");
  selectIn.onchange = selectInput;

  let selectOut = document.getElementById("selectOutput");
  selectOut.onchange = selectOutput;

  selectInput();
  selectOutput();
  addInputListeners();
});

let base64map = {
  "A": "000000", "Q": "010000", "g": "100000", "w": "110000",
  "B": "000001", "R": "010001", "h": "100001", "x": "110001",
  "C": "000010", "S": "010010", "i": "100010", "y": "110010",
  "D": "000011", "T": "010011", "j": "100011", "z": "110011",
  "E": "000100", "U": "010100", "k": "100100", "0": "110100",
  "F": "000101", "V": "010101", "l": "100101", "1": "110101",
  "G": "000110", "W": "010110", "m": "100110", "2": "110110",
  "H": "000111", "X": "010111", "n": "100111", "3": "110111",
  "I": "001000", "Y": "011000", "o": "101000", "4": "111000",
  "J": "001001", "Z": "011001", "p": "101001", "5": "111001",
  "K": "001010", "a": "011010", "q": "101010", "6": "111010",
  "L": "001011", "b": "011011", "r": "101011", "7": "111011",
  "M": "001100", "c": "011100", "s": "101100", "8": "111100",
  "N": "001101", "d": "011101", "t": "101101", "9": "111101",
  "O": "001110", "e": "011110", "u": "101110", "+": "111110",
  "P": "001111", "f": "011111", "v": "101111", "/": "111111"
}

let base64reverseMap = {
  "000000": "A", "010000": "Q", "100000": "g", "110000": "w",
  "000001": "B", "010001": "R", "100001": "h", "110001": "x",
  "000010": "C", "010010": "S", "100010": "i", "110010": "y",
  "000011": "D", "010011": "T", "100011": "j", "110011": "z",
  "000100": "E", "010100": "U", "100100": "k", "110100": "0",
  "000101": "F", "010101": "V", "100101": "l", "110101": "1",
  "000110": "G", "010110": "W", "100110": "m", "110110": "2",
  "000111": "H", "010111": "X", "100111": "n", "110111": "3",
  "001000": "I", "011000": "Y", "101000": "o", "111000": "4",
  "001001": "J", "011001": "Z", "101001": "p", "111001": "5",
  "001010": "K", "011010": "a", "101010": "q", "111010": "6",
  "001011": "L", "011011": "b", "101011": "r", "111011": "7",
  "001100": "M", "011100": "c", "101100": "s", "111100": "8",
  "001101": "N", "011101": "d", "101101": "t", "111101": "9",
  "001110": "O", "011110": "e", "101110": "u", "111110": "+",
  "001111": "P", "011111": "f", "101111": "v", "111111": "/"
}

let base16map = {
  "0": "0000", "1": "0001", "2": "0010", "3": "0011",
  "4": "0100", "5": "0101", "6": "0110", "7": "0111",
  "8": "1000", "9": "1001", "A": "1010", "B": "1011",
  "C": "1100", "D": "1101", "E": "1110", "F": "1111"
}

let base16reverseMap = {
  "0000": "0", "0001": "1", "0010": "2", "0011": "3",
  "0100": "4", "0101": "5", "0110": "6", "0111": "7",
  "1000": "8", "1001": "9", "1010": "A", "1011": "B",
  "1100": "C", "1101": "D", "1110": "E", "1111": "F"
}

var base64selectedFile = "";

function selectInput() {
  let selectInput = document.getElementById("selectInput");
  let selectedInput = selectInput.options[selectInput.selectedIndex].value;
  document.getElementById("input").innerHTML = "";
  if (selectedInput === "datoteka") {
    document.getElementById("input").innerHTML = `
    <form>
    <label for="izberiDatoteko">Izberi datoteko:</label>
    <input type="file" id="fileUpload" name="izberiDatoteko"><br><br>
    </form>`;
  }
  if (selectedInput === "besedilo") {
    document.getElementById("input").innerHTML = `
    <textarea class="form-control" id="inputText" rows="3"></textarea>
    `;
  }
  if (selectedInput === "base64") {
    document.getElementById("input").innerHTML = `
    <textarea class="form-control" id="inputBase64" rows="3"></textarea>
    `;
  }
  if (selectedInput === "base16") {
    document.getElementById("input").innerHTML = `
    <textarea class="form-control" id="inputBase16" rows="3"></textarea>
    `;
  }
  if (selectedInput === "url") {
    document.getElementById("input").innerHTML = `
    <textarea class="form-control" id="inputUrl" rows="3"></textarea>
    `;
  }
  addInputListeners();
  updateOutput();
}

function selectOutput() {
  let selectOutput = document.getElementById("selectOutput");
  let selectedOutput = selectOutput.options[selectOutput.selectedIndex].value;
  document.getElementById("output").innerHTML = "";
  if (selectedOutput === "datoteka") {
    document.getElementById("output").innerHTML = `
    <button class="btn btn-default col-md-12" onclick="downloadFile()">Prenesi datoteko</button>
    <input type="checkbox" id="showImage" class="">  Prikaži kot sliko</input>
    <div id="imageContainer" class="col-md-12"></div>
    `;
  }
  if (selectedOutput === "besedilo") {
    document.getElementById("output").innerHTML = `
    <span id="text-output"></span>
    `;
  }
  if (selectedOutput === "base64") {
    document.getElementById("output").innerHTML = `
    <div>
      <h5>
        Razlaga pretvorbe (zadnjih 5 znakov):
      </h5>
      <span id="base64-explanation"></span>
    </div>
    <h5>
      Base64 besedilo:
    </h5>
    <span id="base64-output" style="word-break: break-all;"></span>
    `;
  }
  if (selectedOutput === "base16") {
    document.getElementById("output").innerHTML = `
    <div>
      <h5>
        Razlaga pretvorbe (zadnjih 5 znakov):
      </h5>
      <span id="base16-explanation"></span>
    </div>
    <h5>
      Šestnajstiško število:
    </h5>
    <span id="base16-output" style="word-break: break-all;"></span>
    `;
  }
  if (selectedOutput === "url") {
    document.getElementById("output").innerHTML = `
    <input type="checkbox" id="encodeAllCharacters" class=""> Kodiraj vse znake</input>
    <br/>
    <span id="url-output" style="word-break: break-all;"></span>
    `;
  }
  addInputListeners();
  updateOutput();
}

function generateExplanationTable(encodingName, encodedString, map, hasPadding) {
  let output = `
  <table class="table">
    <tbody>
      <tr>
        <th scope="row" style="border: 1px solid #dee2e6;">Biti</th>
  `;
  let padding = 0;
  if (hasPadding) {
    while (encodedString.slice(-1) === "=") {
      encodedString = encodedString.substring(0, encodedString.length - 1);
      padding++;
    }
  }

  let colors = [
    "#3399ff",
    "#ff6699",
    "#85e085",
    "#ff66ff",
    "#33cccc",
    "#3399ff",
    "#ff6699",
    "#85e085",
    "#ff66ff",
    "#33cccc"
  ]

  for (let i = 0; i < encodedString.length; i++) {
    let bits = map[encodedString[i]];
    let color = colors[i];
    for (let j = 0; j < bits.length; j++) {
      if (encodedString.length === i + 1 && j >= bits.length - (2 * padding)) {
        color = "#ff0000";
      }
      output += "<td style='text-align: center; padding-left: 0px; padding-right: 0px; border: 1px solid #dee2e6; color: " + color + ";'>";
      output += bits[j];
      output += "</td>";
    }
  }

  if (hasPadding) {
    output += '<td colspan="2" style="text-align: center; border: 1px solid #dee2e6; padding-left: 0px; padding-right: 0px; color: red">';
    output += 'Polnilo (' + (padding * 2) + ' b)';
    output += "</td>";
  }

  output += `
    </tr>
    <tr>
      <th scope="row" style="border: 1px solid #dee2e6;">` + encodingName + `</th>
  `;

  for (let i = 0; i < encodedString.length; i++) {
    let color = colors[i];
    output += '<td colspan="' + map[encodedString[i]].length + '" style="text-align: center; border: 1px solid #dee2e6; color: ' + color + ';">';
    output += encodedString[i];
    output += "</td>";
  }

  if (hasPadding) {
    output += '<td colspan="2" style="text-align: center; border: 1px solid #dee2e6; padding-left: 0px; padding-right: 0px; color: red">';
    for (let i = 0; i < padding; i++) {
      output += '=';
    }
    output += "</td>";
  }

  output += `
      </tr>
    </tbody>
  </table>
  `;

  return output;
}

function base64ToHex(str) {
  let base16String = "";
  let bitBuffer = "";
  let padding = 0;
  for (let i = 0; i < str.length; i++) {
    if (str[i] === "=") {
      padding++;
      continue;
    }
    bitBuffer += base64map[str[i]];
    while (bitBuffer.length >= 4) {
      base16String += base16reverseMap[bitBuffer.substring(0, 4)];
      bitBuffer = bitBuffer.substring(4);
    }
  }
  while (padding >= 2) {
    base16String = base16String.substring(0, base16String.length-1);
    padding-=2;
  }
  return base16String;
}

function hexToBase64(str) {
  let base64String = "";
  let bitBuffer = "";
  for (let i = 0; i < str.length; i++) {
    bitBuffer += base16map[str[i]];
    if (bitBuffer.length >= 6) {
      base64String += base64reverseMap[bitBuffer.substring(0, 6)];
      bitBuffer = bitBuffer.substring(6);
    }
  }
  if (bitBuffer.length !== 0) {
    let padding = 0;
    while (bitBuffer.length < 6) {
      bitBuffer += "00";
      padding++;
    }
    base64String += base64reverseMap[bitBuffer.substring(0, 6)];
    for (let i = 0; i < padding; i++) {
      base64String += "=";
    }
  }
  return base64String;
}

// function hexToBase64(str) {
//   let raw = String.fromCharCode.apply(null,
//     str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" "));
//   console.log(raw);
//   return btoa(raw);
// }

function downloadFile() {
  // Convert the Base64 string back to text.
  var byteString = atob(base64selectedFile);

  // Convert that text into a byte array.
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  // Blob for saving.
  var blob = new Blob([ia], { type: "*/*" });

  // Tell the browser to save as report.pdf.
  saveAs(blob, "izhod");

}

function addInputListeners() {
  let fileUpload = document.getElementById("fileUpload");
  if (fileUpload) {
    fileUpload.onchange = fileSelected;
  }

  let inputText = document.getElementById("inputText");
  if (inputText) {
    inputText.oninput = textChanged;
  }

  let inputBase64 = document.getElementById("inputBase64");
  if (inputBase64) {
    inputBase64.oninput = textChanged;
  }

  let inputBase16 = document.getElementById("inputBase16");
  if (inputBase16) {
    inputBase16.oninput = textChanged;
  }

  let inputUrl = document.getElementById("inputUrl");
  if (inputUrl) {
    inputUrl.oninput = textChanged;
  }

  let showImage = document.getElementById("showImage");
  if (showImage) {
    showImage.oninput = function (evt) {
      if (document.getElementById("showImage").checked) {
        let imageContainer = document.getElementById("imageContainer");
        if (imageContainer) {
          imageContainer.innerHTML = `
            <img id="imageDisplay" class="col-md-12"></img>
          `;
        }
        updateOutput();
      } else {
        let imageContainer = document.getElementById("imageContainer");
        if (imageContainer) {
          imageContainer.innerHTML = "";
        }
      }
    };
  }

  let encodeAllCharacters = document.getElementById("encodeAllCharacters");
  if (encodeAllCharacters) {
    encodeAllCharacters.oninput = function (evt) {
      updateOutput();
    };
  }
}

function updateOutput() {
  var base64Output = document.getElementById("base64-output");
  var base64Explanation = document.getElementById("base64-explanation");
  if (base64Output) {
    base64Output.innerText = base64selectedFile;
    base64Explanation.innerHTML = generateExplanationTable("Base64", base64selectedFile.substring(base64selectedFile.length - 5 - (base64selectedFile.split("=").length - 1)), base64map, true)
  }

  var base16Output = document.getElementById("base16-output");
  var base16Explanation = document.getElementById("base16-explanation");
  if (base16Output) {
    let base16Text = base64ToHex(base64selectedFile);
    base16Output.innerText = base16Text;

    base16Explanation.innerHTML = generateExplanationTable("Šestnajstiški sistem", base16Text.substring(base16Text.length - 5 - (base16Text.split("=").length - 1)), base16map, false)
  }

  var textOutput = document.getElementById("text-output");
  if (textOutput) {
    textOutput.innerText = Base64.decode(base64selectedFile);
  }

  var imageDisplay = document.getElementById("imageDisplay");
  if (imageDisplay) {
    imageDisplay.src = "data:image/png;base64, " + base64selectedFile;
  }

  var urlOutput = document.getElementById("url-output");
  if (urlOutput) {
    try {
      urlOutput.innerText = urlEncode(base64selectedFile);
    }
    catch (err) {
      urlOutput.innerHTML = `
        <span style="color: brown;">Vhoda ni mogoče kodirati z url kodiranjem.</span>
      `;
    }
  }

  document.getElementById("napaka").innerText = "";
}

function urlEncode(base64Input) {
  var encodeAllCharacters = document.getElementById("encodeAllCharacters");
  let text = Base64.decode(base64Input);
  if (encodeAllCharacters && encodeAllCharacters.checked) {
    return percentEncodeAllChars(encodeURIComponent(text));
  } else {
    return encodeURIComponent(text);
  }
}

function percentEncodeAllChars(str) {
  var strEncoded = '';
  for (var i = 0, ilen = str.length; i < ilen; i++) {
    if (str.charCodeAt(i) === '%'.charCodeAt(0)) {
      // dont encode already encoded characters
      strEncoded += str.substring(i, i + 3);
      i += 2;
    } else {
      var strHex = parseInt(str.charCodeAt(i)).toString(16);
      while (strHex.length < 2) {
        strHex = '0' + strHex;
      }
      strEncoded += '%' + strHex;
    }
  }
  return strEncoded.toUpperCase();
}

function textChanged(evt) {
  let inputText = document.getElementById("inputText");
  if (inputText) {
    base64selectedFile = Base64.encode(inputText.value);
    updateOutput();
  }

  let inputBase64 = document.getElementById("inputBase64");
  if (inputBase64) {
    if (/^[A-Za-z0-9+/=]*$/.test(inputBase64.value)) {
      base64selectedFile = inputBase64.value;
      updateOutput();
    } else {
      document.getElementById("napaka").innerText = "Neveljaven vhod. Štiriinšestdesetiški zapis lahko vsebuje velike in majhne črke, števila in ločila +, / ter =";
    }
  }

  let inputBase16 = document.getElementById("inputBase16");
  if (inputBase16) {
    if (/^[A-Fa-f0-9]*$/.test(inputBase16.value)) {
      base64selectedFile = hexToBase64(inputBase16.value.toUpperCase());
      updateOutput();
    } else {
      document.getElementById("napaka").innerText = "Neveljaven vhod. Štiriinšestdesetiški zapis lahko vsebuje samo znake 0123456789ABCDEF.";
    }
  }

  let inputUrl = document.getElementById("inputUrl");
  if (inputUrl) {
    if (/^[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.%]*$/.test(inputUrl.value)) {
      try {
        base64selectedFile = Base64.encode(decodeURIComponent(inputUrl.value));
        updateOutput();
      }
      catch (err) {
        document.getElementById("napaka").innerText = "Neveljaven vhod. Vsakemu % mora slediti dvomestno šestnajstiško število manjše od 128 (hex: 80).";
      }
    } else {
      document.getElementById("napaka").innerText = "Neveljaven vhod. Url zapis lahko vsebuje velike in majhne črke, števila in ločila -._~:/?#[]@!$&'()*+,;=.%.";
    }
  }
}

function fileSelected(evt) {
  if (window.FileReader) {
    let files = evt.target.files;
    if (files.length < 1) {
      return;
    }
    let reader = new FileReader();
    let f = files[0];
    // If we use onloadend, we need to check the readyState.
    reader.onloadend = function (event) {
      if (event.target.readyState == FileReader.DONE) { // DONE == 2
        console.log(event.target.result.byteLength);
        if (event.target.result.byteLength > 250000) {
          document.getElementById("napaka").innerText = "Datoteka je prevelika. (Največ 250 kB)";
        } else {
          let array = new Uint8Array(event.target.result);
          let string = String.fromCharCode.apply(null, array)
          var base64String = btoa(string);
          base64selectedFile = base64String;
          updateOutput();
        }
      }
    };

    reader.readAsArrayBuffer(f);
  } else {
    alert('Brskalnik ne podpira lokalnega branja datotek');
  }
}