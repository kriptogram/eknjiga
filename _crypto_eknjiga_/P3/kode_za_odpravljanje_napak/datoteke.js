function enableFileUpload() {
  let datoteka = document.getElementById("datoteka");
  if (datoteka && datoteka.checked) {
    document.getElementById("fileUploadDiv").innerHTML = `
    <form>
      <label for="izberiDatoteko">Izberi datoteko:</label>
      <input type="file" id="fileUpload" name="izberiDatoteko"><br><br>
    </form>
    <span id="napaka" style="color: brown;"></span>
    `;
    
    document.getElementById("nalaganjeKodiranegaIzhoda").innerHTML = `
    <button class="btn btn-default col-md-12" onclick="prenesiDatoteko(true, false, false)">Prenesi kodirano datoteko</button>
    `;
    document.getElementById("nalaganjeOdkodiranegaIzhoda").innerHTML = `
    <button class="btn btn-default col-md-12" onclick="prenesiDatoteko(true, true, true)">Prenesi odkodirano datoteko</button>
    `;

    // pobrisi tabele
    document.getElementById("bitni-vhod").innerHTML = ``;
    document.getElementById("kodiraniVhodOkvir").innerHTML = ``;
    document.getElementById("sumOkvir").innerHTML = ``;
    document.getElementById("izhodOkvir").innerHTML = ``;

    let fileUpload = document.getElementById("fileUpload");
    if (fileUpload) {
      fileUpload.onchange = fileSelected;
    }
  } else {
    document.getElementById("fileUploadDiv").innerHTML = ``;
    document.getElementById("nalaganjeOdkodiranegaIzhoda").innerHTML = ``;
    document.getElementById("bitni-vhod").innerHTML = `
    <br />
    <span id="klik-obvestilo">Klikni na vhodne celice, da jih spremeniš.</span>
    <div id="vhod" style="overflow: auto; overflow-y: hidden;">
    </div>`;
    document.getElementById("kodiraniVhodOkvir").innerHTML = `
    <div id="kodiraniVhod" style="overflow: auto; overflow-y: hidden;"></div> `;
    document.getElementById("sumOkvir").innerHTML = `<div id="sum" style="overflow: auto; overflow-y: hidden;"></div>`;
    document.getElementById("izhodOkvir").innerHTML = `
    <div id="prejeto" style="overflow: auto; overflow-y: hidden;">
    </div>
    <div id="izhod" style="overflow: auto; overflow-y: hidden;">
    </div>`;

    document.getElementById("nalaganjeKodiranegaIzhoda").innerHTML = ``;
    posodobi();
  }
}


base64selectedFile = "";

function prenesiDatoteko(kodiraj, sum, dekodiraj) {
  let binarnaDatoteka = base64toBinary(base64selectedFile);
  let kopija = "";
  if (dekodiraj) {
    kopija = binarnaDatoteka;
  }
  console.log('pred kodiranjem', binarnaDatoteka.length)

  if (kodiraj) {
    console.log('kodiranje')
    binarnaDatoteka = izracunajKodiraniVhod(binarnaDatoteka);
    console.log('konec kodiranja', binarnaDatoteka.length)
  }
  
  if (sum) {
    console.log('sum')
    binarnaDatoteka = dodajSum(binarnaDatoteka);
    console.log('konec suma', binarnaDatoteka.length)
  }

  if (dekodiraj) {
    console.log('odkodiranje')
    let prejeto = binarnaDatoteka;
    binarnaDatoteka = izracunajDekodiraniIzhod(binarnaDatoteka);
    console.log('konec odkodiranja', binarnaDatoteka.length)
    posodobiStatistikoDatoteke(kopija, binarnaDatoteka, prejeto);
    kopija = "";
  }

  while (binarnaDatoteka.length % 8 !== 0) {
    binarnaDatoteka += "0";
  }
  console.log('konec dodajanja paddinga')


  // Convert the Base64 string back to text.
  var byteString = atob(binarytoBase64(binarnaDatoteka));

  // Convert that text into a byte array.
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  // Blob for saving.
  var blob = new Blob([ia], { type: "*/*" });

  // Tell the browser to save as report.pdf.
  saveAs(blob, "izhod");
};

function posodobiStatistikoDatoteke(original, odkodirano, prejeto) {
  let stNapak = 0;
  for (let i = 0; i < original.length; i++) {
    if (odkodirano[i] !== original[i]) {
      stNapak++;
    }
  }

  let prepoznaniVhod = izracunajKodiraniVhod(odkodirano);
  let zaznanihNapak = 0;
  for (let i = 0; i < prejeto.length; i++) {
    if (prepoznaniVhod[i] !== prejeto[i]) {
      zaznanihNapak++;
    }
  }

  let statistika = "Statistika:<ul>";
  statistika += "<li>Zaznane napake: " + zaznanihNapak + "</li>";
  statistika += "<li>Napačno dekodirani biti: " + stNapak + " (" + (100 * stNapak / odkodirano.length) + " %)</li>";
  statistika += "</ul>";
  document.getElementById("statistika").innerHTML = statistika;

}

function dodajSum(vhodLokalno) {
  let sumTukaj = "";
  for (let i = 0; i < vhodLokalno.length; i++) {
    sumTukaj += (Math.random() < verjetnostNapake) ? '1' : '0';
  }
  let rezultatTukaj = "";
  for (let i = 0; i < vhodLokalno.length; i++) {
    if (vhodLokalno.charAt(i) === sumTukaj.charAt(i)) {
      rezultatTukaj += "0";
    } else {
      rezultatTukaj += "1";
    }
  }
  return rezultatTukaj;
}

function fileSelected(evt) {
  if (window.FileReader) {
    let files = evt.target.files;
    if (files.length < 1) {
      return;
    }
    let reader = new FileReader();
    let f = files[0];
    // If we use onloadend, we need to check the readyState.
    reader.onloadend = function (event) {
      if (event.target.readyState == FileReader.DONE) { // DONE == 2
        console.log(event.target.result.byteLength);
        if (event.target.result.byteLength > 100000) {
          document.getElementById("napaka").innerText = "Datoteka je prevelika. (Največ 100 kB)";
        } else {
          let array = new Uint8Array(event.target.result);
          let string = String.fromCharCode.apply(null, array)
          var base64String = btoa(string);
          base64selectedFile = base64String;
          console.log(base64String);
        }
      }
    };

    reader.readAsArrayBuffer(f);
  } else {
    alert('Brskalnik ne podpira lokalnega branja datotek');
  }
}

let base64map = {
  "A": "000000", "Q": "010000", "g": "100000", "w": "110000",
  "B": "000001", "R": "010001", "h": "100001", "x": "110001",
  "C": "000010", "S": "010010", "i": "100010", "y": "110010",
  "D": "000011", "T": "010011", "j": "100011", "z": "110011",
  "E": "000100", "U": "010100", "k": "100100", "0": "110100",
  "F": "000101", "V": "010101", "l": "100101", "1": "110101",
  "G": "000110", "W": "010110", "m": "100110", "2": "110110",
  "H": "000111", "X": "010111", "n": "100111", "3": "110111",
  "I": "001000", "Y": "011000", "o": "101000", "4": "111000",
  "J": "001001", "Z": "011001", "p": "101001", "5": "111001",
  "K": "001010", "a": "011010", "q": "101010", "6": "111010",
  "L": "001011", "b": "011011", "r": "101011", "7": "111011",
  "M": "001100", "c": "011100", "s": "101100", "8": "111100",
  "N": "001101", "d": "011101", "t": "101101", "9": "111101",
  "O": "001110", "e": "011110", "u": "101110", "+": "111110",
  "P": "001111", "f": "011111", "v": "101111", "/": "111111"
}

let base64reverseMap = {
  "000000": "A", "010000": "Q", "100000": "g", "110000": "w",
  "000001": "B", "010001": "R", "100001": "h", "110001": "x",
  "000010": "C", "010010": "S", "100010": "i", "110010": "y",
  "000011": "D", "010011": "T", "100011": "j", "110011": "z",
  "000100": "E", "010100": "U", "100100": "k", "110100": "0",
  "000101": "F", "010101": "V", "100101": "l", "110101": "1",
  "000110": "G", "010110": "W", "100110": "m", "110110": "2",
  "000111": "H", "010111": "X", "100111": "n", "110111": "3",
  "001000": "I", "011000": "Y", "101000": "o", "111000": "4",
  "001001": "J", "011001": "Z", "101001": "p", "111001": "5",
  "001010": "K", "011010": "a", "101010": "q", "111010": "6",
  "001011": "L", "011011": "b", "101011": "r", "111011": "7",
  "001100": "M", "011100": "c", "101100": "s", "111100": "8",
  "001101": "N", "011101": "d", "101101": "t", "111101": "9",
  "001110": "O", "011110": "e", "101110": "u", "111110": "+",
  "001111": "P", "011111": "f", "101111": "v", "111111": "/"
}

function base64toBinary(base64) {
  let output = "";
  for (let i = 0; i < base64.length; i++) {
    if (base64[i] === "=") {
      output = output.substring(0, output.length - 2);
    } else {
      output += base64map[base64[i]];
    }
  }
  return output;
}
function binarytoBase64(binary) {
  let output = "";
  let padding = 0;
  while (binary.length > 0) {
    while (binary.length < 6) {
      binary += "00";
      padding++;
    }
    let chunk = binary.substring(0, 6);
    output += base64reverseMap[chunk];
    binary = binary.substring(6, binary.length);
  }
  for (let i = 0; i < padding; i++) {
    output += "=";
  }
  return output;
}