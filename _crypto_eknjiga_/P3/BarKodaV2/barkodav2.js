var stevilka = "3838900946701";
var structure = {0:"LLLLLL", 1:"LLGLGG", 2 :"LLGGLG", 3:"LLGGGL", 4:"LGLLGG" ,5:"LGGLLG", 6:"LGGGLL", 7:"LGLGLG", 8:"LGLGGL", 9:"LGGLGL"};
var encoding = {0:"0001101 0100111 1110010", 1:"0011001 0110011 1100110", 2 :"0010011 0011011 1101100", 
                3:"0111101 0100001 1000010", 4:"0100011 0011101 1011100" ,5:"0110001 0111001 1001110", 
                6:"0101111 0000101 1010000", 7:"0111011 0010001 1000100", 8:"0110111 0001001 1001000", 9:"0001011 0010111 1110100"};

var lrGuard = "101";
var cGuard = "01010";


function returnBinaryCode(stevilka){
    var s = structure[stevilka.charAt(0)]; 
    var koda = lrGuard;
    //PRVIH 6
    for(i=0; i< s.length; i++){
        if(s.charAt(i)==="L"){
            koda = koda + encoding[parseInt(stevilka.charAt(1+i))].split(" ")[0];
        }
        else if(s.charAt(i)==="G"){
            koda = koda + encoding[parseInt(stevilka.charAt(1+i))].split(" ")[1];
        }
        else{
            koda = koda + encoding[parseInt(stevilka.charAt(1+i))].split(" ")[2];
        }

    }
    koda = koda + cGuard
    //DRUGIH 6
    for(i=0; i< 6; i++){
        koda = koda + encoding[parseInt(stevilka.charAt(7+i))].split(" ")[2];
    }
    koda = koda + lrGuard;

    return koda;
}
    


document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("narisiBtn").click();
      }
});




// The function gets called when the window is fully loaded
function narisi(stevilka,prvaStevilka) {
   
    
    var td = document.getElementById("structureTab").getElementsByTagName("td");
    td[prvaStevilka*3].style.backgroundColor = "#3CBC8D";
    td[prvaStevilka*3+1].style.backgroundColor = "#3CBC8D";
    td[prvaStevilka*3+2].style.backgroundColor = "#3CBC8D";

    // Get the canvas and context
    var canvas = document.getElementById("viewport"); 
    var context = canvas.getContext("2d");
    // Define the image dimensions
    var width = canvas.width;
    var height = canvas.height;
    // Create an ImageData object
    var imagedata = context.createImageData(width, height);

    //slovar pozicij pixelov za posamezno številko v bar kodi
    var x = width/95;
    var pozicije = new Object();
    pozicije[0] = "8-"+(3*x+8);
    console.log(pozicije[0]);
    for(i=0;i<6;i++){
        pozicije[i+1] = pozicije[i].split("-")[1]+"-"+(parseInt(pozicije[i].split("-")[1])+7*x);
        console.log(pozicije[i+1]);
    }
    pozicije[7]=pozicije[6].split("-")[1]+"-"+(parseInt(pozicije[6].split("-")[1])+5*x);
    console.log(pozicije[7]);
    for(i=7;i<14;i++){
        pozicije[i+1] = pozicije[i].split("-")[1]+"-"+(parseInt(pozicije[i].split("-")[1])+7*x);
        console.log(pozicije[i+1]);
    }
    pozicije[14] = pozicije[13].split("-")[1]+"-"+(parseInt(pozicije[13].split("-")[1])+3*x);
    console.log(pozicije[14]);






    // Create the image
    function createImage(odPix, doPix, num,prvaStevilka, position) {
        var koda = returnBinaryCode(num);
        var w = (width/95);
        var x = document.getElementById("number");
            if(x!=null){
                 x.parentNode.removeChild(x);
            }

        // Loop over all of the pixels
        var enkrat = true;
        for (var x=0; x<width; x++) {
            for (var y=0; y<height; y++) {
                // Get the pixel index
                var pixelindex = (y * width + x) * 4;
 

                if(koda.charAt(Math.floor(x/w))==="1"){
                    var red = 0;
                    var green = 0;
                    var blue = 0;
                    if(odPix!==0 || doPix!==0){
                        if(x+8>=odPix && x+8< doPix){
                            var red = 0;
                            var green = 0;
                            var blue = 0;

                            var number = document.createElement("p");
                            number.style.paddingLeft = odPix+"px";                  //TODO poravnava
                            number.setAttribute("id","number");

                            if(position!=0 && position!=7 && position!=14){
                                //resetiranje barve
                                if(enkrat){
                                    var td = document.getElementById("encodingTab").getElementsByTagName("td");
                                    for(i=0;i<40;i++){
                                        td[i].style.backgroundColor = "#fff";
                                    }

                                    if(position>7){
                                        number.innerHTML = num.charAt(position-1);  //zaradi center guarda
                                        td[Number(num.charAt(position-1))*4].style.backgroundColor = "#3CBC8D";
                                        td[Number(num.charAt(position-1))*4+3].style.backgroundColor = "#BD3D3D"; 
                                        document.getElementById("text2").style.backgroundColor="#BD3D3D";
                                        document.getElementById("text2").innerHTML = "Ker je številka "+num.charAt(position-1)+" na "+ (position-1)+" pozijici, velja da je v zaporedju črk na istem mestu črka R, potem sledi da je binarana vrednost te številke enaka "+encoding[parseInt(num.charAt(position-1))].split(" ")[2]+".";
                                        
                                    }
                                    else{
                                        number.innerHTML = num.charAt(position);

                                        //barvanje
                                        td[Number(num.charAt(position))*4].style.backgroundColor = "#3CBC8D";

                                        var crka = structure[prvaStevilka].charAt(position-1);
                                        if(crka==="L"){
                                            document.getElementById("text2").style.backgroundColor="#3CBC8D";
                                            document.getElementById("text2").innerHTML = "Ker je številka "+num.charAt(position)+" na "+ position+" pozijici, velja da je v zaporedju črk na istem mestu črka L, potem sledi da je binarana vrednost te številke enaka "+encoding[parseInt(num.charAt(position))].split(" ")[0]+".";
                                            td[Number(num.charAt(position))*4+1].style.backgroundColor = "#3CBC8D";
                                        }
                                            
                                        else if(crka==="G"){
                                            document.getElementById("text2").style.backgroundColor="#3DA8BD";
                                            document.getElementById("text2").innerHTML = "Ker je številka "+num.charAt(position)+" na "+ position+" pozijici, velja da je v zaporedju črk na istem mestu črka G, potem sledi da je binarana vrednost te številke enaka "+encoding[parseInt(num.charAt(position))].split(" ")[1]+".";
                                            td[Number(num.charAt(position))*4+2].style.backgroundColor = "#3DA8BD";
                                        }
                                            
                                        else{
                                            document.getElementById("text2").style.backgroundColor="#BD3D3D";
                                            document.getElementById("text2").innerHTML = "Ker je številka "+num.charAt(position)+" na "+ position+" pozijici, velja da je v zaporedju črk na istem mestu črka R, potem sledi da je binarana vrednost te številke enaka "+encoding[parseInt(num.charAt(position))].split(" ")[2]+".";
                                            td[Number(num.charAt(position))*4+3].style.backgroundColor = "#BD3D3D";
                                        }
                                        
                                    }
                                    document.getElementById("numberDiv").appendChild(number);
                                    enkrat = false;
                                }
                                else{
                                    var red = 255;
                                    var green = 255;
                                    var blue = 61;
                                    if(odPix!==0 || doPix!==0){
                                        if(position>7){
                                        var red = 189;
                                        var green = 61;
                                        var blue = 61;
                                    }
                                    else{
                                        var crka = structure[prvaStevilka].charAt(position-1);
                                        if(crka==="L"){
                                            var red = 60;
                                            var green = 188;
                                            var blue = 141;
                                        }
                                            
                                        else if(crka==="G"){
                                            var red = 60;
                                            var green = 168;
                                            var blue = 189;
                                        }
                                            
                                        else{
                                            var red = 189;
                                            var green = 61;
                                            var blue = 61;
                                        }
                                    }
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                            }

                        
                        }
                        if(x+8>=odPix && x+8< doPix && (position==0 || position==7 || position==14)){
                            var red = 0;
                            var green = 0;
                            var blue = 0;

                            //resetiranje barve
                            if(enkrat){
                                var td = document.getElementById("encodingTab").getElementsByTagName("td");
                                for(i=0;i<40;i++){
                                     td[i].style.backgroundColor = "#fff";
                                }
                                enkrat=false;
                            }
                            document.getElementById("text2").style.backgroundColor="gray";
                            if(position==0)
                                document.getElementById("text2").innerHTML = "To je left guard in je njegova binarna vrednost enaka "+lrGuard+".";
                            else if(position==14)
                                document.getElementById("text2").innerHTML = "To je right guard in je njegova binarna vrednost enaka "+lrGuard+".";
                            else
                                document.getElementById("text2").innerHTML = "To je center guard in je njegova binarna vrednost enaka "+cGuard+".";

                            
                        }
                    }
                    

                    
                }
                else{
                    var red = 255;
                    var green = 255;
                    var blue = 255;
                    
                    if(x+8>=odPix && x+8<doPix){
                        if(position>7){
                            var red = 225;
                            var green = 172;
                            var blue = 172;
                        }   
                    
                        else{
                            var crka = structure[prvaStevilka].charAt(position-1);
                            if(crka==="L"){
                                var red = 194;
                                var green = 253;
                                var blue = 226;
                            }
                                                
                            else if(crka==="G"){
                                var red = 214;
                                var green = 255;
                                var blue = 255;
                            }                
                            else{
                                var red = 189;
                                var green = 61;
                                var blue = 61;
                            }
                        }
                    if(position==0 || position==7 || position==14){
                        var red = 215;
                        var green = 215;
                        var blue = 215;
                    } 
                    }
                             
                }
                
                if((Math.floor(x/w)>=3 && Math.floor(x/w)<45) || (Math.floor(x/w)>=50 && Math.floor(x/w)<92)){
                    if(y>270){
                        var red = 255;
                        var green = 255;
                        var blue = 255;  
                         
                    }
                }

                
 
                
 
                // Set the pixel data
                imagedata.data[pixelindex] = red;     // Red
                imagedata.data[pixelindex+1] = green; // Green
                imagedata.data[pixelindex+2] = blue;  // Blue
                imagedata.data[pixelindex+3] = 255;   // Alpha
            }
        }
    }

        createImage(0,0,stevilka,3,0); 
        context.putImageData(imagedata, 0, 0);  

        canvas.onmousemove = function(e){
            console.log(e.clientX);
            for(key in pozicije){
                if(e.clientX >= parseInt(pozicije[key].split("-")[0]) && e.clientX <= parseInt(pozicije[key].split("-")[1])){
                    createImage(parseInt(pozicije[key].split("-")[0]),parseInt(pozicije[key].split("-")[1]),stevilka,prvaStevilka,key); 
                    context.putImageData(imagedata, 0, 0);  

                }
            }

        }
};





function createBar(){
    var koda = document.getElementById('codeArea').value;
    var reg = new RegExp('^[0-9]+$');
    if(reg.exec(koda)!=null){
        document.getElementById('codeArea').style.color="white";

        var x = document.getElementById("viewport");
        if(x!=null){
            x.parentNode.removeChild(x);
        }
        var canvas = document.createElement("canvas");
        canvas.setAttribute("id","viewport");
        canvas.setAttribute("width","380");
        canvas.setAttribute("height","290");
        document.getElementById("barDiv").appendChild(canvas);

        //resetiranje barve na structereTab
        var td = document.getElementById("structureTab").getElementsByTagName("td");
        for(i=0;i<30;i++){
            td[i].style.backgroundColor = "#fff";
        }
        calculateCheck(koda);

        var text = document.getElementById("text");
        text.innerHTML = "Prva številka "+ koda.charAt(0)+", kar pomeni, da pri izdelavi bar kode sledimo zaporedju "+structure[Number(koda.charAt(0))]+" za prvih 6 številk in zaprejedju RRRRRR za drugih 6 številk.\n";

        narisi(koda+calculateCheck(koda),Number(koda.charAt(0)));
    }
    else{
        document.getElementById('codeArea').style.color="red";
    }
    
}


function calculateCheck(stevilka){
    var skupaj = 0;
    var text = document.getElementById("text");
    text.innerHTML = text.value+"Zadnja številka je chech številka in jo izračunamo po formuli: ";
    for(i=0;i<stevilka.length;i++){
        if(i%2==0){ 
            text.innerHTML = text.value+stevilka.charAt(i)+" + ";
            skupaj+=Number(stevilka.charAt(i));
        }
        else{
                text.innerHTML = text.value+stevilka.charAt(i)+"*3 + ";
            skupaj+=Number(stevilka.charAt(i))*3;
        }
    }
    text.innerHTML = text.value+"x = "+skupaj+"\n";
    var x=0;
    while(Math.floor(skupaj+x)%10!=0){
        x++;
    }
    text.innerHTML = text.value+"Iz tega sledi da je x = "+x;
    return x;
}
