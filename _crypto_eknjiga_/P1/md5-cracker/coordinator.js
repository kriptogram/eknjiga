/* var numWorkers = 4 */ // NOTE: can't set this to be more than 8 without fixing the way numbers are carried
var workers = []
var startTime = undefined

var tries = 0;


function nastaviDelavce(numWorkers){
  var tab = document.querySelectorAll(".col-sm-11 .status");
  for (var i = 0; i < tab.length; i++) {
    var element = document.querySelector(".col-sm-11 .status");
    element.parentNode.removeChild(element);
    var el = document.querySelector(".col-sm-11 .log");
    el.parentNode.removeChild(el);
  }
  for (var i = 0; i < numWorkers; i++) {
    // Create worker
      /* Naredi novega delavca */
  var nDivDelavec = document.createElement("DIV");
  var nLogDelavec = document.createElement("DIV");
  nDivDelavec.setAttribute("id","worker"+i);
  nDivDelavec.setAttribute("class","status");
  document.getElementById("delavci").appendChild(nDivDelavec);

  nLogDelavec.setAttribute("id","worker"+i+"log");
  nLogDelavec.setAttribute("class","log");
  document.getElementById("delavci").appendChild(nLogDelavec);
  }

  var tab1 = document.querySelectorAll(".col-sm-11 div");
}


function vnosButtonFunkcija(){
  var doneWorkers = 0;  
  var numWorkers = undefined;
  var vnosText = undefined;
  var vnosHash = undefined;
  var vnosTimeout = undefined;
  var maxPwLen = undefined;

  var hashType = document.getElementById("hash-type").value;

  var hash_fun;
  var hashName = undefined;

    switch (hashType) {
        case "md5":
            hash_fun = enc_md5;
            hashName = "MD5";
            break;
        case "sha1":
            hash_fun = enc_sha1;
            hashName = "SHA-1";
            break;
        
        case "sha2":
            hash_fun = enc_sha2;
            hashName = "SHA-256";
            break;
    }

  tries = 0;
  
  startTime = +new Date
  numWorkers = parseInt(document.getElementById("vnosStDelavcev").innerHTML);
  nastaviDelavce(numWorkers); 
  
  if(document.getElementById("vnosString").value != "" && document.getElementById("hashVnosGesla").checked == false){
    vnosText = document.getElementById("vnosString").value;
    vnosHash = hash_fun(vnosText);
  }
  else if(document.getElementById("vnosString").value != "" && document.getElementById("hashVnosGesla").checked == true){
    vnosHash = dopolniHash(document.getElementById("vnosString").value);
    vnosText = '';
    if(parseInt(vnosHash.to, 16).toString(16) !== vnosHash.toLowerCase()){
      document.getElementById("mainlog").innerHTML = "<b>Hash ni v šestnajstiškem zapisu!</b>";
      return;
    }
  }
  else{
    document.getElementById("mainlog").innerHTML = "<b>Vnesi geslo ali hash!</b>";
    return;
  }

  vnosTimeout = parseInt(document.getElementById("vnosTimeout").value);
  maxPwLen = parseInt(document.getElementById("pwLength").value);

  if (!Number.isInteger(vnosTimeout)) {
    vnosTimeout = 1000;
  }

  if (!Number.isInteger(maxPwLen)) {
    maxPwLen = 5;
  }
  
  document.getElementById("sliderStDelavcev").disabled = true;
  
  document.getElementById("izpisHexHash").innerHTML = 'Vaše geslo: '+vnosText.bold()+'</br>'+ hashName +' hash zapis: '+vnosHash.bold();
  
  document.getElementById("vnosString").innerHTML = "Vstavljeno geslo: ";
  
  var includeNums = false;
  var includeLower = false;
  var includeUpper = false;
  var useLang = undefined;


  if(document.querySelector(".use_lttrs_lower").checked) {
    includeLower = true;
  }

  if(document.querySelector(".use_lttrs_upper").checked) {
    includeUpper = true;
  }

  if(document.querySelector(".use_nums").checked) {
    includeNums = true;
  }

  useLang = document.getElementById("lang").value;

    
    for (var i = 0; i < numWorkers; i++) {
    var worker = new Worker('worker.js')
      workers.push(worker)
      
      // Message handler
      worker.addEventListener('message', function (e) {
        switch (e.data.cmd) {
          case "tried":
            tries = e.data.data * numWorkers;
            break;

          case "status":
            status(e.data.data, e.data.id)
            break
      
          case "log":
            log(e.data.data, e.data.id)
            break

          case "checkedAll":
            doneWorkers++;
            if (doneWorkers >= numWorkers) {
              log("Geslo je daljše od nastavljene dolžine.");
              document.getElementById("sliderStDelavcev").disabled = false;
            }
            break
          
          case "setRate":
          status(addCommasToInteger(e.data.data) + " gesel na sekundo", e.data.id)
            break
      
          case "foundPassword":
            log("NAŠEL GESLO: " + e.data.data.pw)

            tries = e.data.data.tries * numWorkers;
      
            var totalTime = (+new Date - startTime) / 1000
            log("Skupen čas: " + totalTime + " sekund")
            log("Število poskusov: " + tries);
      
            workers.forEach(function(worker) {
              worker.terminate()
            })
            // log("Ustavil vse delavce.")
            document.getElementById("sliderStDelavcev").disabled = false;
            break

          case "timedOut":
            tries = e.data.data * numWorkers;
            
            log("Predpisani čas je potekel.");
            log("Število poskusov: " + tries);
            workers.forEach(function(worker) {
              worker.terminate()
            })
            // log("Ustavil vse delavce.")
            document.getElementById("sliderStDelavcev").disabled = false;
            break
      
          default:
            log("Glavna stran ne razume navodil " + e.data.cmd)
            break
        }
      })
      
      // Error handler
      worker.addEventListener('error', function(e) {
        log(['NAPAKA: Linija ', e.lineno, ' v ', e.filename, ': ', e.message].join(''))
        document.getElementById("sliderStDelavcev").disabled = false;
      })
      
      
      // Set worker settings
      worker.postMessage({ cmd: "setWorkerId", data: i })
      worker.postMessage({ cmd: "setMaxPassLength", data: maxPwLen })
      worker.postMessage({ cmd: "setPassToCrack", data: vnosHash })
      worker.postMessage({ cmd: "setTimeout", data: vnosTimeout })
      worker.postMessage({ cmd: "setHashFunction", data: hashType })
      worker.postMessage({ cmd: "setLangs", data: {lang: useLang, upper: includeUpper, nums: includeNums, lower: includeLower} })

      // Start worker
      worker.postMessage({ cmd: "performCrack", data: {start: i, hop: numWorkers} })
    }
  


  /* status("Iščem geslo") */
  // document.getElementById("mainlog").innerHTML = "Testiram male, velike tiskane črke in številke.";
  document.getElementById("mainlog").innerHTML = "";
}



/* var vnosText = document.getElementById("vnosString"); */
var vnosButton = document.getElementById("vnosButton");

  if(vnosButton != null){
    /* Izbrisal sem dodatne argumente, ker berem samo iz enega vnosnega polja */
    vnosButton.setAttribute("onClick", 'vnosButtonFunkcija()');
  }

var ponastaviButton = document.getElementById("ponastaviButton");

if(ponastaviButton != null) {

  ponastaviButton.setAttribute("onClick", 'ponastaviButtonFunkcija()');
}

function ponastaviButtonFunkcija() {

  location.reload();

}






// Helper functions

function addCommasToInteger(x) {
x = parseInt(x) + ''
var rgx = /(\d+)(\d{3})/
while (rgx.test(x)) {
  x = x.replace(rgx, '$1' + ',' + '$2')
}
return x
}

function status(msg, workerId) {
var prefix = workerId != null
  ? "Delavec " + (workerId+1) + ": "
  : " "

var selector = workerId != null
  ? "#worker" + workerId
  : "#main"

document.querySelector(selector).textContent = prefix + msg
}

function log(msg, workerId) {
var prefix = workerId != null
  ? "Delavec " + workerId + " pravi: "
  : " "

var fragment = document.createDocumentFragment();
fragment.appendChild(document.createTextNode(prefix + msg));


var selector = workerId != null
  ? "#worker" + workerId + "log"
  : "#mainlog"

document.querySelector(selector).appendChild(fragment)
}

function dopolniHash(hash){
  var nicle = '';
  for(var i=0; i<32 - hash.length; i++){
    nicle = nicle + '0';
  }
  return hash + nicle;
}


