function enc_sha1(string) {
    return(CryptoJS.SHA1(string).toString());
}

function enc_sha2(string) {
    return(CryptoJS.SHA256(string).toString());
}

function enc_md5(string) {
    return(CryptoJS.MD5(string).toString());
}