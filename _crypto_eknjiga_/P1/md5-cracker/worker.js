importScripts('new_crypto.js', "Crypto/rollups/sha1.js", "Crypto/rollups/sha256.js", "Crypto/rollups/md5.js")

var letters = [];
var nums = "0123456789".split("");
var lttrs_base_lower = "abcdefghijklmnoprstuvz".split("");
var lttrs_base_upper = "abcdefghijklmnoprstuvz".toUpperCase().split("");
var lttrs_eng_lower = "qwxy".split("");
var lttrs_eng_upper = "qwxy".toUpperCase().split("");
var lttrs_slo_lower = "čšž".split("");
var lttrs_slo_upper = "čšž".toUpperCase().split("");

var lang_eng = undefined;
var lang_slo = undefined;
var numsFlag = undefined;
var upperFlag = undefined;
var lowerFlag = undefined;

var hash_fun = undefined;

var d = new Date;
var time = 0;
var timeout = undefined;

var totalTries = undefined;

// Cracking settings
var workerId
	, maxPassLength = undefined
	, passToCrack = undefined

// Timer variables
var interval = 100000
	, count = 0
	, startTime = +new Date

function status(msg) {
	this.postMessage({ cmd: "status", data: msg, id: workerId })
}
function log(msg) {
	this.postMessage({ cmd: "log", data: msg, id: workerId })
}

function crack(options) {
	status("Začel z iskanjem gesla")

	if (letters.length == 0) {
		status("Prazen nabor znakov");
		return;
	}

	var hop = options.hop
		, length = 1
		, bufView = new Uint16Array(maxPassLength)
		, view = bufView.subarray(maxPassLength - length)
		, pw

	var lttr_index = new Array(maxPassLength).fill(0);

	lttr_index[maxPassLength - 1] = workerId;

	bufView[maxPassLength - 1] = letters[workerId].charCodeAt(0);

	while (true) {
		if(new Date >= time + timeout) {
			console.log(workerId, pw);
			this.postMessage({ cmd: "timedOut", data: totalTries})
			return
		}
		totalTries++;

		pw = String.fromCharCode.apply(null, view)

		if (hash_fun(pw) == passToCrack) {
			this.postMessage({ cmd: "foundPassword", data: {pw: pw, tries: totalTries}, id: workerId })
			return
		}

		// Check if we need to carry any numbers
		// Check from right to left

		lttr_index[maxPassLength - 1] += hop;

		//i pregleduje tabelo view, j pregleduje lttr_index
		for (var i = length - 1, j = maxPassLength - 1; i >= 0; --i, j--) {

			if (lttr_index[j] >= letters.length) {
				// need to carry					
				if (i == 0) {
					// need to add a new "place" to the left

					length += 1
					view[i] = letters[0].charCodeAt(0);
					view = bufView.subarray(maxPassLength - length)
					view[0] = letters[0].charCodeAt(0);
					lttr_index[j - 1] = 0;


					lttr_index[maxPassLength - 1] = workerId;
					view[length - 1] = letters[workerId].charCodeAt(0);
					
					if (length > maxPassLength) {
						this.postMessage({ cmd: "checkedAll" });
						return;
					}

				} 
				else {
					view[i] = letters[0].charCodeAt(0);
					lttr_index[j - 1]++;
					if(lttr_index[j - 1] < letters.length) {
						view[i - 1] = letters[lttr_index[j - 1]].charCodeAt(0);
					}
					else {
						view[i - 1] = letters[0].charCodeAt(0);
					}
				}
				
				lttr_index[j] = workerId;
			}

			else {
				
				//inkrementira zadnjo črko
				view[length - 1] = letters[lttr_index[maxPassLength - 1]].charCodeAt(0);
				
			}
		}

		

		// Timer stuff
		count += 1
		if (count % interval == 0) {
			this.postMessage({ cmd: "setRate", data: interval / ((new Date - startTime) / 1000), id: workerId })
			count = 0
			startTime = +new Date
		}
	}

}

this.addEventListener('message', function (e) {

	switch (e.data.cmd) {
		case "setWorkerId":
			workerId = e.data.data
			break

		case "setMaxPassLength":
			maxPassLength = e.data.data
			break

		case "setPassToCrack":
			passToCrack = e.data.data
			break

		case "setTimeout":
			timeout = e.data.data;
			break;

		case "setLangs":
			letters = [];

			if(e.data.data.nums) {
				letters = letters.concat(nums);
			}

			if (e.data.data.lower) {
				letters = letters.concat(lttrs_base_lower);
				switch (e.data.data.lang) {
					case "eng":
						letters = letters.concat(lttrs_eng_lower);
						break;
					case "slo":
						letters = letters.concat(lttrs_slo_lower);
						break;
					case "both":
						letters = letters.concat(lttrs_eng_lower);
						letters = letters.concat(lttrs_slo_lower);
						break;
				}
				
			}

			if (e.data.data.upper) {
				letters = letters.concat(lttrs_base_upper);
				switch (e.data.data.lang) {
					case "eng":
						letters = letters.concat(lttrs_eng_upper);
						break;
					case "slo":
						letters = letters.concat(lttrs_slo_upper);
						break;
					case "both":
						letters = letters.concat(lttrs_eng_upper);
						letters = letters.concat(lttrs_slo_upper);
						break;
				}
			}
			break;
			
		case "setHashFunction":
			switch (e.data.data) {
				case "md5":
					hash_fun = enc_md5;
					break;
				case "sha1":
					hash_fun = enc_sha1;
					break;
				
				case "sha2":
					hash_fun = enc_sha2;
					break;
			}
			break;

		case "performCrack":
			totalTries = 0;
			
			time = d.getTime();
			crack(e.data.data)
			break

		default:
			this.postMessage({ cmd: "log", data: "Delavec ne razume navodila " + e.data.cmd })
			break

	}

})

