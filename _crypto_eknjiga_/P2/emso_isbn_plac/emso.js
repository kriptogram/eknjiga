window.addEventListener('load', function(){

	document.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("izracunajBtn").click();
		  }
	});

	document.getElementById("reset").addEventListener("submit", function(e){
		e.preventDefault();
		var datum=document.getElementById("datum").value;
		if(datum.length==0){
			document.getElementById("rez").innerHTML="Nepravilen vhod - vnesi veljaven datum.";
			return;
		}
		var emso=datum.substring(8);
		emso=emso+datum.substring(5, 7);
		emso=emso+datum.substring(1, 4);
		var e=document.getElementById("drzava");
		var drzava=e.options[e.selectedIndex].value;
		var code = "";
		switch(drzava) {
			case("Ostalo"): code = "00"; break;
			case("Bosna in Hercegovina"): code = "10"; break;
			case("Črna gora"): code = "20"; break;
			case("Hrvaška"): code = "30"; break;
			case("Makedonija"): code = "40"; break;
			case("Slovenija"): code = "50"; break;
			case("Začasno prebivališče"): code = "60"; break;
			case("Srbija"): code = "70"; break;
			case("Pokrajina Vojvodina"): code = "80"; break;
			case("Pokrajina Kosovo"): code = "90"; break;
		}
		emso += code;
		var zapo=document.getElementById("zapo").value;
		if(zapo.length==3 && isNaN(zapo)==false){
			emso+=zapo;
		}else{
			document.getElementById("rez").innerHTML="Nepravilen vhod - zaporedna številka rojstva ima vedno 3 cifre.";
			return;
		}

		var spol;
		var sp=document.getElementsByName("spol");
		if(sp[0].checked==true)spol="M";
		else spol="Z";
		var temp=parseInt(zapo, 10);
		if(temp>500 && spol=="M"){
			document.getElementById("rez").innerHTML="Nepravilen vhod. Zaporedna številka rojstva</br>se ne sklada¸\
														z vnesenim spolom.";
			return;
		}
		if(temp<500 && spol=="Z"){
			document.getElementById("rez").innerHTML="Nepravilen vhod. Zaporedna številka rojstva</br>se ne sklada¸\
														z vnesenim spolom.";
			return;
		}
		var utezi=[7, 6, 5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
		var sum=0;
		for(var i=0; i<utezi.length; i++){
			var cifra=emso.charAt(i);
			cifra=parseInt(cifra, 10); 
			sum+=cifra*utezi[i];
		}
		sum=sum%11;
		var kontrol;
		var trig = null;
		if(sum==0){
			kontrol="0";
			emso+=kontrol;
		}else if(sum==1){
			zapo=parseInt(zapo, 10);
			zapo++;
			emso=emso.substring(0, 9);
			emso=emso+zapo.toString(10);
			var sum=0;
			for(var i=0; i<utezi.length; i++){
				var cifra=emso.charAt(i);
				cifra=parseInt(cifra, 10); 
				sum+=cifra*utezi[i];
			}
			sum=sum%11;
			if(sum==0)kontrol="0";
			else kontrol=(11-sum).toString(10);
			emso=emso+kontrol;
			trig = true;
		}else{
			kontrol=(11-sum).toString(10);
			emso+=kontrol;
			trig = false;
		}
		var rez=document.getElementById("rez");
		var temp = "";
		rez.classList.add("card");
		rez.classList.add("card-body");
		rez.innerHTML = "<b>Postopek izračuna:</b><br>";
		rez.innerHTML += "Prvih 7 števk EMŠO predstavlja vaš rojstni datum:<br>";
		rez.innerHTML += "<div class='card border-secondary card-body rez-card-row'><span style='color:red;'>" + datum.substring(8) + "</span>/"
			+ "<span style='color:green;'>" + datum.substring(5, 7) + "</span>/"
			+ datum.charAt(0) + "<span style='color:blue;'>" + datum.substring(1, 4) + "</span>"
			+ "&nbsp; &rarr; &nbsp; <span style='color:red;'>" + datum.substring(8) + "</span><span style='color:green;'>" + datum.substring(5, 7) + "</span><span style='color:blue;'>" + datum.substring(1, 4) + "</span>"
			+ "XXXXXX</div><br>";

		temp += datum.substring(8) + datum.substring(5, 7) + datum.substring(1, 4);

		rez.innerHTML += "8. in 9. števka predstavljata številko registra, ki označuje kraj rojstva:<br>";
		rez.innerHTML += "<div class='card border-secondary card-body rez-card-row'>" + drzava + "&nbsp; &rarr; &nbsp;<span style='color: #e36e00'>" + code + "</span>&nbsp; &rarr; &nbsp;" + temp + "<span style='color: #e36e00'>" + code + "</span>" + "XXXX</div><br>";

		temp += code;

		rez.innerHTML += "Nazadnje se vključi še zaporedna številka rojstva:<div class='card border-secondary card-body rez-card-row'><span style='color: #730080'>" + zapo + "</span> &nbsp; &rarr; &nbsp;" + temp + "<span style='color: #730080'>"+ zapo + "</span>X</div><br>";
		temp += zapo;

		if(trig == true) {
			rez.innerHTML += "<div class='card border-danger card-body rez-card-row'>POZOR! Ker je bil v naslednjem delu ostanek pri deljenju z zaporedno številko " + (zapo-1) + " enak 1, se zaporedna številka poveča za ena in postopek ponovi.</div><br>";
		}

		var sumHere = 0;
		var zapStr = zapo.toString(10);
		rez.innerHTML+="Zadnjo števko se določi s pomočjo kontrolne vsote:<div class='card border-secondary card-body rez-card-col'>Prvo števko se pomnoži s 7: " + temp.charAt(0) + " &times; 7 = " + Number(temp.charAt(0)) * 7 + "<br>"
		+"Drugo števko se pomnoži s 6: " + temp.charAt(1) + " &times; 6 = " + Number(temp.charAt(1)) * 6 + "<br>"
		+"Tretjo števko se pomnoži s 5: " + temp.charAt(2) + " &times; 5 = " + Number(temp.charAt(2)) * 5 + "<br>"
		+"Četrto števko se pomnoži s 4: " + temp.charAt(3) + " &times; 4 = " + Number(temp.charAt(3)) * 4 + "<br>"
		+"Peto števko se pomnoži s 3: " + temp.charAt(4) + " &times; 3 = " + Number(temp.charAt(4)) * 3 + "<br>"
		+"Šesto števko se pomnoži z 2: " + temp.charAt(5) + " &times; 2 = " + Number(temp.charAt(5)) * 2 + "<br>"
		+"Sedmo števko se pomnoži s 7: " + temp.charAt(6) + " &times; 7 = " + Number(temp.charAt(6)) * 7 + "<br>"
		+"Osmo števko se pomnoži s 6: " + temp.charAt(7) + " &times; 6 = " + Number(temp.charAt(7)) * 6 + "<br>"
		+"Deveto števko se pomnoži s 5: " + temp.charAt(8) + " &times; 5 = " + Number(temp.charAt(8)) * 5 + "<br>"
		+"Deseto števko se pomnoži s 4: " + zapStr.charAt(0) + " &times; 4 = " + Number(zapStr.charAt(0)) * 4 + "<br>"
		+"Enajsto števko se pomnoži s 3: " + zapStr.charAt(1) + " &times; 3 = " + Number(zapStr.charAt(1)) * 3 + "<br>"
		+"Dvanajsto števko se pomnoži z 2: " + zapStr.charAt(2) + " &times; 2 = " + Number(zapStr.charAt(2)) * 2 + "</div><br>";
		
		sumHere += Number(temp.charAt(0)) * 7;
		sumHere += Number(temp.charAt(1)) * 6;
		sumHere += Number(temp.charAt(2)) * 5;
		sumHere += Number(temp.charAt(3)) * 4;
		sumHere += Number(temp.charAt(4)) * 3;
		sumHere += Number(temp.charAt(5)) * 2;
		sumHere += Number(temp.charAt(6)) * 7;
		sumHere += Number(temp.charAt(7)) * 6;
		sumHere += Number(temp.charAt(8)) * 5;
		sumHere += Number(temp.charAt(9)) * 4;
		sumHere += Number(temp.charAt(10)) * 3;
		sumHere += Number(temp.charAt(11)) * 2;

		rez.innerHTML += "Zmnožke seštejemo in delimo z 11, da dobimo ostanek:<div class='card border-secondary card-body rez-card-row'>" + sumHere + " / 11 = " + Math.floor(sumHere / 11) + " ostanek " + sumHere % 11 + "</div><br>";
		if(trig == null) {
			rez.innerHTML += "<div class='card border-info card-body rez-card-row'>Ker je ostanek pri deljenju enak 0, je kontrolna vsota enaka&nbsp;<span style='color: #949600'>0</span>.<br></div>";
		} else if(trig == true) {
			rez.innerHTML+="Kontrolna vsota je:<div class='card border-info card-body rez-card-row'>11 - 3 =&nbsp;<span style='color: #949600'>" +kontrol+ "</span></br>";
		} else {
			rez.innerHTML += "Kontrolna vsota je:<div class='card border-info card-body rez-card-row'>11 - " + (sumHere % 11) + " =&nbsp;<span style='color: #949600'>" + (11 - sumHere%11) + "</span></div><br>";
		}

		rez.innerHTML += "Torej je celotna EMŠO številka:<div class='card border-secondary card-body rez-card-row'>" + temp + "<span style='color: #949600'>"+ kontrol + "</span></div>";
	});
});