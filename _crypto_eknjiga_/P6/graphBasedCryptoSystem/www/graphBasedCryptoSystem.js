// constants describing type of animation
let GENERATE = 0, ENCRYPT = 1, DECRYPT = 2;

// constants for drawing
let NODE_R = 10;
let LINE_WIDTH = 2;
let WIDTH = 500;
let HEIGHT = 500;
let MIN_DIST = 30;
let MAX_DIST = 200;
let GREEN = "#1F7857";
let BLACK = "#000000";

let alphabet = "abcdefghijklmnopqrstuvwxyzčšž";
let max_message_length = 5; // It can go up to 10 (calculated from Number.MAX_SAFE_INTEGER).


class Node {
  constructor(ida, is_priv, x_coord, y_coord) {
    this.id = ida;
    this.is_private = is_priv;
    this.x = x_coord;
    this.y = y_coord;
    this.neighbours = [];
    this.labels = ["", ""]; // first text is shown on upper right side of node and second on bottom right side
  }
  set_label(idx, text) {
    this.labels[idx] = text;
  }
  draw_edge(ctx, node, priv=false) {
    if(priv && this.is_private || !priv && this.id > node.id) {
      ctx.beginPath();
      ctx.moveTo(this.x, this.y);
      ctx.lineTo(node.x, node.y);
      ctx.stroke();
    }
  }
  draw_edges(ctx, priv=false) {
    set_color(ctx, BLACK);
    for(var i=0; i<this.neighbours.length; i++) {
      this.draw_edge(ctx, this.neighbours[i], priv);
    }
  }
  draw(ctx, draw_private) {
    ctx.beginPath();
    ctx.arc(this.x, this.y, NODE_R, 0, 2 * Math.PI);
    if(draw_private && this.is_private) set_color(ctx, GREEN);
    else set_color(ctx, BLACK);
    ctx.fill();
  }
  draw_label(ctx, idx) {
    ctx.fillStyle = GREEN;
    ctx.strokeStyle = "#FFFFFF";
    if(idx == 0) {
      ctx.strokeText(this.labels[0], this.x + NODE_R, this.y - NODE_R);
      ctx.fillText(this.labels[0], this.x + NODE_R, this.y - NODE_R);
    }
    else {
      var height = ctx.measureText(this.labels[1]).actualBoundingBoxAscent;
      ctx.strokeText(this.labels[1], this.x + NODE_R, this.y + NODE_R + height)
      ctx.fillText(this.labels[1], this.x + NODE_R, this.y + NODE_R + height)
    }
  }
  distance(x, y) {
    return Math.sqrt(Math.pow(this.x-x, 2) + Math.pow(this.y-y, 2));
  }
  connect_to(node) {
    this.neighbours.push(node);
    node.neighbours.push(this);
  }
  toString() {
    return this.is_private + ' ' + this.x + ' ' + this.y;
  }
  sum_neighbours() {
    var sum = this.labels[0];
    for(var el of this.neighbours) {
      sum += el.labels[0];
    }
    this.labels[1] = sum;
  }
}

class Graph {
  constructor(n_priv, n_add, ct) {
    this.n_private = n_priv;
    this.n_added = n_add;
    this.nodes = [];
    this.ctx = ct;
    this.construct_graph();
    this.animation = 0;
    this.set_animation_type(GENERATE);
    this.description = document.getElementById("animation_description");
    this.description.innerHTML = "Klikni naprej za začetek animacije.";
    document.getElementById("sporocilo").disabled = true;
  }
  construct_graph() {
    // construct an example graph
    this.nodes.push(new Node(1, false, 200, 100));
    this.nodes.push(new Node(2, false, 300, 100));
    this.nodes.push(new Node(3, false, 100, 200));
    this.nodes.push(new Node(4, true, 200, 200));
    this.nodes.push(new Node(5, true, 300, 200));
    this.nodes.push(new Node(6, true, 100, 300));
    this.nodes.push(new Node(7, false, 200, 300));
    this.nodes.push(new Node(8, false, 300, 300));
    this.nodes.push(new Node(9, false, 400, 300));
    this.nodes.push(new Node(10, false, 200, 400));
    this.nodes[0].connect_to(this.nodes[2]);
    this.nodes[0].connect_to(this.nodes[3]);
    this.nodes[1].connect_to(this.nodes[4]);
    this.nodes[2].connect_to(this.nodes[3]);
    this.nodes[3].connect_to(this.nodes[6]);
    this.nodes[4].connect_to(this.nodes[7]);
    this.nodes[4].connect_to(this.nodes[8]);
    this.nodes[5].connect_to(this.nodes[9]);
    this.nodes[6].connect_to(this.nodes[7]);
    this.nodes[6].connect_to(this.nodes[9]);
    this.nodes[7].connect_to(this.nodes[9]);
  }
  draw(draw_private=true) {
    for(var el of this.nodes) {
      el.draw_edges(this.ctx);
    }
    for(var el of this.nodes) {
      el.draw(this.ctx, draw_private);
    }
  }
  generation_animation() {
    /**
    * this.animation tells us in which state of animation we are
    *   0 means to show just private key
    *   1 means to show all nodes and each node is connected to one of the nodes from private key
    *   2 means to show whole graph
    *   3 means to show whole graph without private key
    */
    clear_screen(this.ctx);
    if(this.animation == 0) {
      this.description.innerHTML = "Ustvarimo vozlišča, ki bodo sestavljala exact dominating set (zasebni ključ).";
      for(var el of this.nodes) {
        if(el.is_private) {
          el.draw(this.ctx, true);
        }
      }
    }
    else if(this.animation == 1) {
      this.description.innerHTML = "Dodamo še ostala vozlišča. Vsakega takoj povežemo s poljubnim vozliščem iz exact dominating seta.";
      for(var el of this.nodes) {
        el.draw_edges(this.ctx, true);
      }
      for(var el of this.nodes) {
        if(el.is_private) {
          el.draw(this.ctx, true);
        }
        else {
          el.draw(this.ctx, true);
        }
      }
    }
    else if(this.animation == 2) {
      this.description.innerHTML = "Dodamo še povezave med ostalimi vozlišči.";
      this.draw();
    }
    else {
      this.description.innerHTML = "Dobili smo javni ključ (celoten graf).";
      this.draw(false);
    }
    this.animation++;
  }
  reset_generation_animation() {
    if(this.description != null) this.description.innerHTML = "Klikni naprej za začetek animacije.";
    this.animation=0;
    clear_screen(this.ctx);
  }
  set_animation_type(type) {
    this.animation_type = type;
    this.animation = 0;
    document.getElementById('encryption_output').innerHTML = "";
    document.getElementById('sporocilo').value = "";
    document.getElementById("sporocilo").disabled = true;
    for(var id of ['generiranje', 'sifriranje', 'odsifriranje']) {
        document.getElementById(id).className = "tab";
    }
    switch(type) {
      case GENERATE:
        document.getElementById('generiranje').className = "tab-active";
        this.reset_generation_animation();
        break;
      case ENCRYPT:
        document.getElementById('sifriranje').className = "tab-active";
        this.encryption_animation();
        break;
      case DECRYPT:
        document.getElementById('odsifriranje').className = "tab-active";
        this.decryption_animation();
        break;
    }
  }
  start_encryption() {
    this.description.innerHTML = "";
    this.animation = 1;
    this.encryption_animation();
  }
  encryption_animation() {
    /**
    * this.animation tells us in which state of animation we are
    *   0: show full graph (without private key) and wait for message to be written
    *   1: transform message to number
    *   2: write number as sum
    *   3: write numbers on nodes
    *   4: sum the neighbours
    *   5: delete original numbers on the nodes
    */
    switch(this.animation) {
      case 0:
        //this.description.innerHTML = "Vpiši sporočilo, ki ga želiš zašifrirati.";
        this.description.innerHTML = "";
        var sporocilo_input = document.getElementById("sporocilo");
        sporocilo_input.disabled = false;
        sporocilo_input.value = "Sem vpiši sporočilo";
        sporocilo_input.select();
        clear_screen(this.ctx);
        this.draw(false);
        break;
      case 1:
        clear_screen(this.ctx);
        this.draw(false);
        var sporocilo = document.getElementById('sporocilo').value;
        var num = text_to_int(sporocilo);
        if(num === -1) {
          document.getElementById('encryption_output').innerHTML = "";
          this.animation = 0;
          this.encryption_animation();
          return;
        }
        else {
          document.getElementById('encryption_output').innerHTML = num;
        }
        document.getElementById("sporocilo").disabled = true;
        this.description.innerHTML = "Sporočilo pretvorimo v število (da ga lahko zapišemo kot vsoto).";
        break;
      case 2:
        this.description.innerHTML = "Število zapišemo kot vsoto (toliko členov kot je vseh vozlišč).";
        var num = parseInt(document.getElementById('encryption_output').innerHTML);
        var node_numbers = write_as_sum(num, this.nodes.length);
        var output = num + " = " + node_numbers[0];
        for(var i=1; i<node_numbers.length; i++) {
          output += " + " + node_numbers[i];
        }
        document.getElementById('encryption_output').innerHTML = output;
        // write node_numbers on node
        for(var i=0; i<node_numbers.length; i++) {
          this.nodes[i].set_label(0, node_numbers[i]);
        }
        break;
      case 3:
        this.description.innerHTML = "Na vsako vozlišče zapišemo en člen vsote.";
        for(var node of this.nodes) {
          node.draw_label(this.ctx, 0);
        }
        break;
      case 4:
        this.description.innerHTML = "Vsakemu vozlišču prištejemo vse sosede.";
        for(var node of this.nodes) {
          node.sum_neighbours();
          node.draw_label(this.ctx, 1);
        }
        break;
      case 5:
        this.description.innerHTML = "Zbrišemo originalne člene vsote (ker niso del kriptograma).";
        clear_screen(this.ctx);
        this.draw(false);
        for(var node of this.nodes) {
          node.draw_label(this.ctx, 1);
        }
        document.getElementById('encryption_output').innerHTML = "";
        break;
    }
    this.animation++;
  }
  next_step() {
    switch(this.animation_type) {
      case GENERATE:
        this.generation_animation();
        break;
      case ENCRYPT:
        this.encryption_animation();
        break;
      case DECRYPT:
        this.decryption_animation();
        break;
    }
  }
  reset() {
    document.getElementById('encryption_output').innerHTML = "";
    document.getElementById('sporocilo').value = "";
    switch(this.animation_type) {
      case GENERATE:
        this.reset_generation_animation();
        break;
      case ENCRYPT:
        this.animation = 0;
        this.encryption_animation();
        break;
      case DECRYPT:
        this.animation = 0;
        this.decryption_animation();
        break;
    }
  }
  decryption_animation() {
    /**
    * this.animation tells us in which state of animation we are
    *   0: show full graph (without private key) with encrypted message
    *   1: show private key
    *   2: calculate sum
    *   3: transform sum to message
    */
    switch(this.animation) {
      case 0:
        this.description.innerHTML = "Začnemo s kriptogramom.";
        // if no message is encrypted
        if(this.nodes[0].labels[1] === "") {
          var reserve = [274, 194, 274, 291, 202, 17, 30, 26, 6, 38];
          for(var i=0; i<this.nodes.length; i++) {
            this.nodes[i].set_label(1, reserve[i]);
          }
        }
        document.getElementById('encryption_output').innerHTML = "";
        clear_screen(this.ctx);
        this.draw(false);
        for(var node of this.nodes) {
          node.draw_label(this.ctx, 1);
        }
        break;
      case 1:
        this.description.innerHTML = "Uporabimo zasebni ključ.";
        clear_screen(this.ctx);
        this.draw(true);
        for(var node of this.nodes) {
          node.draw_label(this.ctx, 1);
        }
        break;
      case 2:
        this.description.innerHTML = "Seštejemo vrednosti na vozliščih, ki so del zasebnega ključa.";
        var vsota = 0;
        for(var node of this.nodes) {
          if(node.is_private) {
            vsota += node.labels[1];
          }
        }
        document.getElementById('encryption_output').innerHTML = vsota;
        break;
      case 3:
        this.description.innerHTML = "Vsoto pretvorimo v sporočilo.";
        var sum = parseInt(document.getElementById('encryption_output').innerHTML);
        document.getElementById('encryption_output').innerHTML = int_to_text(sum);
        break;
    }
    this.animation++;
  }
}

// returns array of length n, where sum of all elements is equal to sum and all elements are bigger than 1
function write_as_sum(sum, n) {
  var result = [];
  for(var i=0; i<n-1; i++) {
    var a = Math.floor(Math.random() * (sum - n + i + 1)) + 1;
    result.push(a);
    sum -= a;
  }
  result.push(sum);
  return result;
}

function int_to_text(num) {
  num -= 50;
  var result = "";
  var n = alphabet.length + 1;
  while(num > 0) {
    result = alphabet.charAt(num % n - 1) + result;
    num = Math.floor(num / n);
  }
  return result;
}

// returns -1 if text is not ok
function text_to_int(text) {
  var result = 50; // to achieve that every message is >= 50
  var n = text.length;
  if(n === 0) {
    window.alert("Sporočilo je prazno.");
    return -1;
  }
  if(n > max_message_length) {
    window.alert("Sporočilo mora biti dolgo največ " + max_message_length + " znakov.");
    return -1;
  }
  var a = 1;
  for(var i=n-1; i>=0; i--) {
    if(alphabet.indexOf(text.charAt(i)) > -1) {
      result += a * (alphabet.indexOf(text.charAt(i)) + 1);
      a = a * (alphabet.length + 1);
    }
    else {
      window.alert("Znak '" + text.charAt(i) + "' ni v abecedi.");
      return -1;
    }
  }
  return result;
}

function clear_screen(ctx) {
  ctx.clearRect(0, 0, WIDTH, HEIGHT);
}

function set_color(ctx, color) {
  ctx.strokeStyle = color;
  ctx.fillStyle = color;
}

window.addEventListener("load", function(event) {
  var wrapper = document.getElementById('cchiperWrapper');

  // create canvas and graph
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  ctx.font = "20px Helvetica Neue Helvetica Arial sans-serif";
  ctx.lineWidth = LINE_WIDTH;
  var graph = new Graph(3, 7, ctx);

  // add onclick functions to buttons for switching between graph generation, encryption and decryption
  document.getElementById('generiranje').onclick = function() {graph.set_animation_type(GENERATE);};
  document.getElementById('sifriranje').onclick = function() {graph.set_animation_type(ENCRYPT);};
  document.getElementById('odsifriranje').onclick = function() {graph.set_animation_type(DECRYPT);};

  document.getElementById('naprej').onclick = function() {graph.next_step();};
  document.getElementById('reset').onclick = function() {graph.reset();};
});
