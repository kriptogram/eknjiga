// Configure mathjax to recognize $...$ as in-line math delimiter.
window.MathJax = {
  tex: {
    inlineMath: [['$', '$'], ['\\(', '\\)']]
  }
};

// Array of ...
var questions = [
    {
        correct: String.raw`$(\alpha^a)^{\gamma} (\alpha^k)^{\delta}$`,
        incorrect: [String.raw`$(\alpha^k)^{\gamma} (\alpha^a)^{\delta}$`,
                    String.raw`$(\alpha^x)^{\gamma} (\alpha^k)^{\delta}$`],
        hint: String.raw`Vstavimo formuli za $\beta$ in $\gamma$.`
    },
    {
        correct: String.raw`$\alpha^{a \gamma + k \delta}$`,
        incorrect: [String.raw`$\alpha^{a k \gamma \delta}$`,
                    String.raw`$\alpha^{a^{\gamma} + k^{\delta}}$`],
        hint: String.raw`Uporabimo pravila za računanje s potencami.`
    },
    {
        correct: String.raw`$\alpha^{a \gamma + k (x - a \gamma) k^{-1}}$`,
        incorrect: [String.raw`$\alpha^{a \gamma + k \alpha^k}$`,
                    String.raw`$\alpha^{a \gamma + k (x - a \gamma) k}$`],
        hint: String.raw`Vstavimo formulo za $\delta$.`
    },
    {
        correct: String.raw`$\alpha^{a \gamma + x - a \gamma}$`,
        incorrect: [String.raw`$\alpha^{a \gamma - a \gamma}$`,
                    String.raw`$\alpha^{a \gamma + k (x - a \gamma)}$`],
        hint: String.raw`Poenostavimo izraz.`
        /*(Dodatna razlaga: Velja $k k^{-1} \equiv 1 \pmod{(p-1)}$. V eksponentu računamo po modulu $p-1$, ker za vsak $x \in \mathbb{Z}_p^{*}$ velja $x^{p-1} \equiv 1 \pmod{p}$).*/
    },
    {
        correct: String.raw`$\alpha^x$`,
        incorrect: [String.raw`$\alpha^1$`,
                    String.raw`$\alpha^{2a \gamma + x}$`],
        hint: String.raw`Poenostavimo izraz.`
    }
];

var table, current_column = 0, correct_answer = -1;

function create_div(parent, id, class_name, row_num=null, col_num=null) {
    var div = document.createElement("div");
    div.id = id;
    div.className = class_name;
    if(row_num !== null) div.setAttribute("data-row-num", row_num);
    if(col_num !== null) div.setAttribute("data-col-num", col_num);
    parent.appendChild(div);
    return div;
}

function create_table(parent) {
    table = [];
    for(var i=0; i<3; i++) {
        row = create_div(parent, "", "row")
        table.push([]);
        for(var j=0; j<6; j++) {
            el = create_div(row, i+"-"+j, "col-lg rounded-lg clickable", i, j);
            table[i].push(el);
        }
    }
    table[1][0].innerHTML = String.raw`$\beta^{\gamma} \gamma^{\delta} \equiv$`;
    MathJax.typeset();
    current_column = 0;
}

function next_question() {
    current_column++;
    if(current_column < 6) {
        var question_number = current_column - 1;
        correct_answer = Math.floor(Math.random() * 3);
        table[correct_answer][current_column].innerHTML = questions[question_number].correct;
        var next = Math.floor(Math.random() * 2);
        for(var i=0; i<3; i++) {
            if(i != correct_answer) {
                table[i][current_column].innerHTML = questions[question_number].incorrect[next];
                next = (next + 1) % 2;
            }
        }
    }
    MathJax.typeset();
}

function hide_wrong_answers() {
    table[0][current_column].innerHTML = "";
    if(current_column == 5) {
        table[1][current_column].innerHTML = questions[current_column-1].correct.slice(0, -1) + "\\pmod{p}$"
    }
    else {
        table[1][current_column].innerHTML = questions[current_column-1].correct.slice(0, -1) + "\\equiv$";
    }
    table[2][current_column].innerHTML = "";
}

function set_hint(hint) {
    document.getElementById("hint-text").innerHTML = hint;
    if(hint !== "") MathJax.typeset();
}

window.onload = function() {
    // generate table
    create_table(document.getElementById("clickable-table"));

    next_question();

    // when element of table is clicked
    document.getElementById("clickable-table").onclick = function(e) {
        var clicked = null;
        for(var el of e.path) {
            if(el.classList != undefined && el.classList.contains("clickable")) {
                clicked = el;
            }
        }

        if(parseInt(clicked.getAttribute("data-col-num")) === current_column) {
            if(parseInt(clicked.getAttribute("data-row-num")) === correct_answer) {
                clicked.classList.add("bg-success");
                set_hint("");
                setTimeout(function() {
                        clicked.classList.remove("bg-success");
                        hide_wrong_answers();
                        next_question();
                    }, 200);
            }
            else {
                clicked.classList.add("bg-danger");
                set_hint(questions[current_column-1].hint);
                setTimeout(function() {clicked.classList.remove("bg-danger");}, 200);
            } 
        }
    };

    document.getElementById("hint-button").onclick = function() {
        if(current_column < 6) {
            set_hint(questions[current_column-1].hint);
        }
    };
};

function check_task2(praslike, praslike2, trki) {
    correct = {"odpornost_praslik_div": true, "odpornost_2praslik_div": true, "odpornost_trki_div": false};
    answers = {"odpornost_praslik_div": praslike, "odpornost_2praslik_div": praslike2, "odpornost_trki_div": trki};
    for(var name of ["odpornost_praslik_div", "odpornost_2praslik_div", "odpornost_trki_div"]) {
        var div = document.getElementById(name);
        if(answers[name] === correct[name]) {
            div.classList.remove("bg-danger");
            div.classList.add("bg-success");
        }
        else {
            div.classList.remove("bg-success");
            div.classList.add("bg-danger");
        }
    }
    document.getElementById("odpornost_praslik_razlaga").hidden = false;
    document.getElementById("odpornost_2praslik_razlaga").hidden = false;
    document.getElementById("odpornost_trki_razlaga").hidden = false;
}
