import Vue from 'vue'
import App from './App.vue'
import BV from 'bootstrap-vue'
Vue.config.productionTip = false
Vue.use(BV)
new Vue({
  render: h => h(App),
}).$mount('#app')
