var alphabet = ['a', 'b', 'c', 'č','d', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l','m', 'n','o', 'p', 'r', 's', 'š', 't', 'u', 'v', 'z', 'ž']
var alphabetUpper = ['A', 'B', 'C', 'Č','D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L','M', 'N','O', 'P', 'R', 'S', 'Š', 'T', 'U', 'V', 'Z', 'Ž']
var noLetters = alphabet.length;
//console.log(noLetters);
// This recieves messages of type "testmessage" from the server.
// See http://shiny.rstudio.com/gallery/server-to-client-custom-messages.html
// for details
var caesarShift = function(str, amount) {

	// Wrap the amount
	if (amount < 0)
		return caesarShift(str, amount + noLetters);

	// Make an output variable
	var output = '';

	// Go through each character
	for (var i = 0; i < str.length; i ++) {

		// Get the character we'll be appending
		var c = str[i];

		// If it's a letter...
		if (isNaN(c * 1)) {
			// Get its code
			var shift = amount;
			// Lowercase letters
			if (alphabet.includes(c)) {
				shift += alphabet.indexOf(c);
				c = alphabet[shift % alphabet.length];
				
			}
			// Uppercase letters
			if (alphabetUpper.includes(c)){
				shift += alphabetUpper.indexOf(c);
				c = alphabetUpper[shift % alphabetUpper.length];
				
			}
		}

		// Append
		output += c;

	}

	// All done!
	return output;
};

function createButton(context, value, func) {
    var button = document.createElement("input");
    button.type = "button";
    button.value = value;
    button.onclick = func;
    context.appendChild(button);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function shiftAllLetters(event){
  if(isBlank(event.target.value)) return;
  var shift = event.target.value.charCodeAt(0)  - textDec[event.target.id].charCodeAt(0) ;
  //alert(shift);
  var div = document.getElementById('chiperButtons');
  var i;
  for (i = 0; i < div.childNodes.length; i++) { 
    var node = div.childNodes[i];
    node.value = caesarShift(textDec[i],shift);
  }
}

function createTextInput(context, value, id ,func) {
    var elem = document.createElement("input");
    elem.type = "text";
    elem.value = value;
    elem.addEventListener('input', func);
    elem.id = id;
    context.appendChild(elem);
}