#
# Loading libs
#
r = getOption("repos")
r["CRAN"] = "http://cran.us.r-project.org"
options(repos = r)

libs = c("knitr")
for (lib in libs) {
  if (!is.element(lib, .packages(all.available = TRUE)) ) { install.packages(lib) }
  suppressPackageStartupMessages(suppressWarnings({ library(lib, character.only = TRUE) }))
}
rm(list = ls())


Sys.setlocale("LC_ALL", "sl_SI.UTF-8")


#
# Default settings for knitr content generation
#
opts_chunk$set(echo = FALSE)
opts_chunk$set(fig.align = "center")

addResourcePath('www', directoryPath = 'ceaserChiper/www-shared')
addResourcePath('js', directoryPath = 'ceaserChiper/www-shared')
addResourcePath('protokolGameHtml', directoryPath = 'ceaserChiper/ceaserAttack')