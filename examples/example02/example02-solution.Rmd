---
title: "example 02"
author: "Mia"
date: "25/06/2019"
output: 
  html_document
---



## Interactions (in Cryptography)

If you want to place a piscture, you can do it with the following:
```
<img src="img/AliceBobpng.png">
```

Now, we add new interactive snippet. It is non-shiny and is done ONLY using `html` and `javascript`.

Let us have following `html` file.

```
<!DOCTYPE html>
<html>
<body>

<h2>JavaScript Alert</h2>
<button id='alertButton'>Try it</button>
</body>
</html>
```

It represents the backbone of our interaction. The real interaction happens using `javascript` (i.e. `interaction.js`, in the `js` folder).

For example we can have the following:
```{.js}
function myFunction() {
  alert("I am an alert box!");
}

window.addEventListener("load", function(event) {
  var btn = document.getElementById('alertButton');
  btn.addEventListener('click', myFunction);
});
```

TBH, when adding the interaction to a `.RmD` file, you don't really need to create a valid html document.
In meaning, you don't need 

```
<!DOCTYPE html>
<html>
<body>
```

at the begining of the document, and

```
</body>
</html>
```

at the end of it. You can just use/add/have the html tags that are relevant for the interaction.

## Solution

<iframe src="alertButton.html" id = 'alert-button-iframe'></iframe>